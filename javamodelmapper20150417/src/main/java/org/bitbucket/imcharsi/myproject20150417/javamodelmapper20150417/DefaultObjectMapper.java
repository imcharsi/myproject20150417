/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417;

import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.*;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.InvitationModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.BanType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.EnterType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.PermissionType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.RoomTimelineModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.current.UserModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.history.UserNickHistoryModel;
import org.codehaus.jackson.*;
import org.codehaus.jackson.map.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.module.SimpleModule;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Kang Woo, Lee on 5/5/15.
 */
public class DefaultObjectMapper {
    static public final ObjectMapper objectMapper;

    private static class JsonSerializerUtil<T extends IEnum<T>> extends JsonSerializer<T> {
        @Override
        public void serialize(T value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
            jgen.writeNumber(value.getId());
        }
    }

    private static class JsonDeserializerUtil<T extends IEnum<T>> extends JsonDeserializer<T> {
        private IEnumConvert<T> convert;

        public JsonDeserializerUtil(IEnumConvert<T> convert) {
            this.convert = convert;
        }

        @Override
        public T deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return convert.valueOf(jp.getIntValue());
        }
    }

    static {
        objectMapper = new ObjectMapper();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        // jackson 과는 다르게 jackson asl 은 ObjectMapper 에서 time zone 을 정할 수 없다.
        // http://stackoverflow.com/questions/7556851/set-jackson-timezone-for-date-deserialization
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        objectMapper.setDateFormat(dateFormat);
        objectMapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        SimpleModule module = new SimpleModule("mapper", new Version(1, 0, 0, ""));
        module.addSerializer(EnterType.class, new JsonSerializerUtil<EnterType>());
        module.addDeserializer(EnterType.class, new JsonDeserializerUtil<EnterType>(Constants.enterTypeConvert));
        module.addSerializer(BanType.class, new JsonSerializerUtil<BanType>());
        module.addDeserializer(BanType.class, new JsonDeserializerUtil<BanType>(Constants.banTypeConvert));
        module.addSerializer(PermissionType.class, new JsonSerializerUtil<PermissionType>());
        module.addDeserializer(PermissionType.class, new JsonDeserializerUtil<PermissionType>(Constants.permissionTypeConvert));
        module.addSerializer(EnvelopeDataType.class, new JsonSerializerUtil<EnvelopeDataType>());
        module.addDeserializer(EnvelopeDataType.class, new JsonDeserializerUtil<EnvelopeDataType>(Constants.envelopeDataTypeConvert));
        objectMapper.registerModule(module);
    }

    public static List<EnvelopeModel<?>> deserializeEnvelopeList(String json) throws IOException {
        final Iterator<JsonNode> jsonNodeIterator = objectMapper.readTree(json).iterator();
        final List<EnvelopeModel<?>> envelopeModelList = new LinkedList<EnvelopeModel<?>>();
        while (jsonNodeIterator.hasNext()) {
            final JsonNode jsonNode = jsonNodeIterator.next();
            final Iterator<String> stringIterator = jsonNode.getFieldNames();
            final Iterator<JsonNode> jsonNodeIterator1 = jsonNode.iterator();
            final Map<String, JsonNode> map = new HashMap<String, JsonNode>();
            while (stringIterator.hasNext() && jsonNodeIterator1.hasNext()) {
                map.put(stringIterator.next(), jsonNodeIterator1.next());
            }
            envelopeModelList.add(innerConvert(map.get("dataType").asInt(), map.get("data"), map.get("id").asInt()));
        }
        return envelopeModelList;
    }

    private static EnvelopeModel<?> innerConvert(Integer dataType, JsonNode jsonNode, Integer id) throws IOException {
        if (dataType == Constants.userModelType) {
            return new EnvelopeModel(EnvelopeDataType.UserModelType, objectMapper.treeToValue(jsonNode, UserModel.class), id);
        } else if (dataType == Constants.invitationModelType) {
            return new EnvelopeModel(EnvelopeDataType.InvitationModelType, objectMapper.treeToValue(jsonNode, InvitationModel.class), id);
        } else if (dataType == Constants.roomTimelineModelType) {
            return new EnvelopeModel(EnvelopeDataType.RoomTimelineModelType, objectMapper.treeToValue(jsonNode, RoomTimelineModel.class), id);
        } else if (dataType == Constants.sayHiModelType) {
            return new EnvelopeModel(EnvelopeDataType.SayHiModelType, objectMapper.treeToValue(jsonNode, SayHiModel.class), id);
        } else if (dataType == Constants.logoutModelType) {
            return new EnvelopeModel(EnvelopeDataType.LogoutModelType, objectMapper.treeToValue(jsonNode, LogoutModel.class), id);
        } else if (dataType == Constants.userNickHistoryModelType) {
            return new EnvelopeModel(EnvelopeDataType.UserNickHistoryModelType, objectMapper.treeToValue(jsonNode, UserNickHistoryModel.class), id);
        } else {
            throw new RuntimeException();
        }
    }
}
