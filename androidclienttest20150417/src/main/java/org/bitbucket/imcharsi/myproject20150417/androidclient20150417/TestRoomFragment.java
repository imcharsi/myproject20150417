/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import com.ning.http.client.AsyncHttpClient;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomAttenderModel;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Kang Woo, Lee on 5/28/15.
 */
public class TestRoomFragment extends RoomFragment {
    public final BlockingQueue<Integer> talkBlockingQueue = new LinkedBlockingQueue<Integer>();
    public final BlockingQueue<Integer> leaveBlockingQueue = new LinkedBlockingQueue<Integer>();
    public final BlockingQueue<Integer> changeRoomNameBlockingQueue = new LinkedBlockingQueue<Integer>();
    public final BlockingQueue<Integer> webServiceBlockingQueue = new LinkedBlockingQueue<Integer>(10);
    public final BlockingQueue<Integer> roomAttenderListBlockingQueue = new LinkedBlockingQueue<Integer>();
    public final BlockingQueue<Integer> changeCurrentNickBlockingQueue = new LinkedBlockingQueue<Integer>();
    public Menu testContextMenu;
    public AlertDialog changeRoomNameDialog;
    public AlertDialog changeCurrentNickDialog;

    @Override
    protected AlertDialog createChangeRoomNameDialog(AlertDialog.Builder builder) {
        final AlertDialog.Builder newBuilder = builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                changeRoomNameDialog = null;
            }
        });
        changeRoomNameDialog = super.createChangeRoomNameDialog(newBuilder);
        return changeRoomNameDialog;
    }

    @Override
    protected AlertDialog createChangeCurrentNickDialog(AlertDialog.Builder builder) {
        final AlertDialog.Builder newBuilder = builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                changeCurrentNickDialog = null;
            }
        });
        changeCurrentNickDialog = super.createChangeRoomNameDialog(newBuilder);
        return changeCurrentNickDialog;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        testContextMenu = menu;
    }

    public static class TestLeaveRoomAsyncTask extends LeaveRoomAsyncTask {
        public TestLeaveRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ((TestRoomFragment) getRoomFragment()).leaveBlockingQueue.add(1);
        }
    }

    public static class TestTalkAsyncTask extends TalkAsyncTask {
        public TestTalkAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ((TestRoomFragment) getRoomFragment()).talkBlockingQueue.add(1);
        }
    }

    public static class TestWebSocketServiceReceiver extends WebSocketServiceReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final int oldCount = getRoomFragment().getEnvelopeModelAdapter().getCount();
            super.onReceive(context, intent);
            final int newCount = getRoomFragment().getEnvelopeModelAdapter().getCount();
            if (newCount != 0 && newCount != oldCount) {
                ((TestRoomFragment) getRoomFragment()).webServiceBlockingQueue.add(1);
            }
        }
    }

    public static class TestChangeRoomNameAsyncTask extends ChangeRoomNameAsyncTask {
        public TestChangeRoomNameAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            ((TestRoomFragment) getRoomFragment()).changeRoomNameBlockingQueue.add(1);
        }
    }

    public static class TestRoomAttenderListAsyncTask extends RoomAttenderListAsyncTask {
        public TestRoomAttenderListAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(List<RoomAttenderModel> models) {
            super.onPostExecute(models);
            ((TestRoomFragment) getRoomFragment()).roomAttenderListBlockingQueue.add(1);
        }
    }

    public static class TestChangeCurrentNickAsyncTask extends ChangeCurrentNickAsyncTask {
        public TestChangeCurrentNickAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            ((TestRoomFragment) getRoomFragment()).changeCurrentNickBlockingQueue.add(1);
        }
    }
}
