/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import com.ning.http.client.AsyncHttpClient;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.user.UserListFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.current.UserModel;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Kang Woo, Lee on 5/11/15.
 */
public class TestUserListFragment extends UserListFragment {
    final CountDownLatch createdUserListFragmentLatch = new CountDownLatch(1);
    final CountDownLatch showListLatch = new CountDownLatch(1);

    @Override
    public void onStart() {
        super.onStart();
        createdUserListFragmentLatch.countDown();
    }

    public static class TestUserListFragmentAsyncTask extends UserListFragmentAsyncTask {
        @Inject
        public TestUserListFragmentAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(List<UserModel> userModels) {
            super.onPostExecute(userModels);
            ((TestUserListFragment) getUserListFragment()).showListLatch.countDown();
        }
    }
}
