/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import com.ning.http.client.AsyncHttpClient;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MainFragment;

import javax.inject.Inject;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Kang Woo, Lee on 5/10/15.
 */
public class TestMainFragment extends MainFragment {
    public final BlockingQueue<Integer> logoutBlockingQueue = new LinkedBlockingQueue<Integer>();
    public final BlockingQueue<Integer> loginBlockingQueue = new LinkedBlockingQueue<Integer>();
    public final BlockingQueue<Integer> registerUserBlockingQueue = new LinkedBlockingQueue<Integer>();

    public static class TestLogoutAsyncTask extends LogoutAsyncTask {
        @Inject
        public TestLogoutAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            ((TestMainFragment) getMainFragment()).logoutBlockingQueue.add(1);
        }
    }

    static public class TestLoginAsyncTask extends LoginAsyncTask {
        @Inject
        public TestLoginAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ((TestMainFragment) getMainFragment()).loginBlockingQueue.add(1);
        }
    }

    static public class TestRegisterUserAsyncTask extends RegisterUserAsyncTask {
        @Inject
        public TestRegisterUserAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            ((TestMainFragment) getMainFragment()).registerUserBlockingQueue.add(1);
        }
    }
}
