/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import com.ning.http.client.AsyncHttpClient;
import dagger.Module;
import dagger.Provides;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MainFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MasterDaggerModule;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomListFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.user.UserListFragment;

/**
 * Created by Kang Woo, Lee on 5/10/15.
 */
@Module(includes = {MasterDaggerModule.class,},
//        injects = {MainFragment.LoginAsyncTask.class, MainFragment.LogoutAsyncTask.class, MainFragment.RegisterUserAsyncTask.class,
//                RoomListFragment.class,
//        },
        overrides = true, library = true)
public class TestMainDaggerModule {
    @Provides
    @SuppressWarnings("unused")
    public MainFragment provideTestMainFragment() {
        return new TestMainFragment();
    }

    @Provides
    @SuppressWarnings("unused")
    public MainFragment.LoginAsyncTask provideMainFragmentLoginAsyncTask(AsyncHttpClient asyncHttpClient) {
        return new TestMainFragment.TestLoginAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public MainFragment.LogoutAsyncTask provideMainFragmentLogoutAsyncTask(AsyncHttpClient asyncHttpClient) {
        return new TestMainFragment.TestLogoutAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public MainFragment.RegisterUserAsyncTask provideMainFragmentRegisterUserAsyncTask(AsyncHttpClient asyncHttpClient) {
        return new TestMainFragment.TestRegisterUserAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public UserListFragment provideTestUserListFragment() {
        return new TestUserListFragment();
    }

//    @Provides
//    @SuppressWarnings("unused")
//    public MainFragment.LoginAsyncTask provideTestLoginAsyncTask() {
//        return new TestMainFragment.TestLoginAsyncTask();
//    }

//    @Provides
//    @SuppressWarnings("unused")
//    public MainFragment.LogoutAsyncTask provideTestLogoutAsyncTask() {
//        return new TestMainFragment.TestLogoutAsyncTask();
//    }

    @Provides
    @SuppressWarnings("unused")
    public RoomListFragment provideTestRoomListFragment() {
        return new TestRoomListFragment();
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomFragment.LeaveRoomAsyncTask provideRoomFragmentLeaveRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
        return new TestRoomFragment.TestLeaveRoomAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomFragment provideRoomFragment() {
        return new TestRoomFragment();
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomFragment.TalkAsyncTask provideRoomFragmentTalkAsyncTask(AsyncHttpClient asyncHttpClient) {
        return new TestRoomFragment.TestTalkAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomFragment.WebSocketServiceReceiver provideRoomFragmentWSSR() {
        return new TestRoomFragment.TestWebSocketServiceReceiver();
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomFragment.ChangeRoomNameAsyncTask provideRoomFragmentCRNAT(AsyncHttpClient asyncHttpClient) {
        return new TestRoomFragment.TestChangeRoomNameAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomFragment.RoomAttenderListAsyncTask provideRoomFragmentRALAT(AsyncHttpClient asyncHttpClient) {
        return new TestRoomFragment.TestRoomAttenderListAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomFragment.ChangeCurrentNickAsyncTask provideRoomFragmentCCNAT(AsyncHttpClient asyncHttpClient) {
        return new TestRoomFragment.TestChangeCurrentNickAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomListFragment.EnterRoomAsyncTask provideRoomListFragmentERAT(AsyncHttpClient asyncHttpClient) {
        return new TestRoomListFragment.TestEnterRoomAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomListFragment.RoomListAsyncTask provideRoomListFragmentRoomListAsyncTask(AsyncHttpClient asyncHttpClient) {
        return new TestRoomListFragment.TestRoomListAsyncTask(asyncHttpClient);
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomListFragment.CreateRoomAsyncTask provideRoomListFragmentCreateRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
        return new TestRoomListFragment.TestCreateRoomAsyncTask(asyncHttpClient);
    }
}
