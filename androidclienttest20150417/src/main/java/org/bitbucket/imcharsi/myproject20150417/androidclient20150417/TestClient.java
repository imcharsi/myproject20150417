/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import com.ning.http.client.websocket.DefaultWebSocketListener;
import com.ning.http.client.websocket.WebSocket;
import com.ning.http.client.websocket.WebSocketUpgradeHandler;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnterRoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.RoomHistoryModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.talk.history.TalkHistoryModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.CertificationModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.current.UserModel;
import org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417.DefaultObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by Kang Woo, Lee on 5/28/15.
 */
@Getter
@Setter
public class TestClient {
    private MainApplication mainApplication;
    static public final String headerAccept = "application/json";
    final private AsyncHttpClient asyncHttpClient;
    final String serverUrl;

    public TestClient(MainApplication mainApplication) {
        setMainApplication(mainApplication);
        asyncHttpClient = mainApplication.getObjectGraph().get(AsyncHttpClient.class);
        serverUrl = mainApplication.getServerUrl();
    }

    public int registerUser(String userName, String userPassword) throws IOException, ExecutionException, InterruptedException {
        final UserModel userModel = new UserModel();
        userModel.setName(userName);
        userModel.setPassword(userPassword);
        final Response response = asyncHttpClient.preparePost(serverUrl + "/user").
                setHeader("Content-Type", MainApplication.ContentType).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(userModel)).execute().get();
        return response.getStatusCode();
    }

    public String login(String userName, String userPassword, Boolean logoutForce) throws IOException, ExecutionException, InterruptedException {
        final CertificationModel certificationModel = new CertificationModel(userName, userPassword, logoutForce);
        final Response response = asyncHttpClient.preparePost(serverUrl + "/login").
                setHeader("Content-Type", MainApplication.ContentType).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(certificationModel)).execute().get();
        return retrieveCookie(response.getHeaders("Set-Cookie"));
    }

    public int logout(String cookie) throws IOException, ExecutionException, InterruptedException {
        final Response response = asyncHttpClient.prepareDelete(serverUrl + "/login").
                setHeader("Cookie", cookie).execute().get();
        return response.getStatusCode();
    }

    public int grantOperator(Integer roomId, Integer userId, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(serverUrl).append("/room/").append(roomId).append("/operator");
        final Response response = asyncHttpClient.preparePost(stringBuilder.toString()).
                setHeader("Accept", headerAccept).
                setHeader("Cookie", cookie).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(userId)).execute().get();
        return response.getStatusCode();
    }

    public RoomModel getRoom(Integer roomId, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(serverUrl).append("/room/").append(roomId);
        final Response response = asyncHttpClient.prepareGet(stringBuilder.toString()).
                setHeader("Accept", headerAccept).
                setHeader("Cookie", cookie).execute().get();
        return DefaultObjectMapper.objectMapper.readValue(response.getResponseBody(), RoomModel.class);
    }

    public List<RoomModel> getAllCreatedRooms(String cookie) throws IOException, ExecutionException, InterruptedException {
        final Response response = asyncHttpClient.prepareGet(serverUrl + "/room/allCreatedRooms").
                setHeader("Accept", headerAccept).
                setHeader("Cookie", cookie).execute().get();
        return DefaultObjectMapper.objectMapper.readValue(response.getResponseBody(),
                DefaultObjectMapper.objectMapper.getTypeFactory().constructCollectionType(List.class, RoomModel.class));
    }

    public int enterRoom(Integer roomId, EnterRoomModel enterRoomModel, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(serverUrl).append("/room/").append(roomId).append("/roomAttenders");
        final Response response = asyncHttpClient.preparePost(stringBuilder.toString()).
                setHeader("Content-Type", MainApplication.ContentType).
                setHeader("Cookie", cookie).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(enterRoomModel)).execute().get();
        return response.getStatusCode();
    }

    public int createRoom(final String roomName, final String roomPassword, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        final RoomModel roomModel = new RoomModel();
        roomModel.setCurrentDetail(new RoomHistoryModel());
        roomModel.getCurrentDetail().setName(roomName);
        roomModel.getCurrentDetail().setPassword(roomPassword);
        stringBuilder.append(serverUrl).append("/room");
        final Response response = asyncHttpClient.preparePost(stringBuilder.toString()).
                setHeader("Content-Type", MainApplication.ContentType).
                setHeader("Cookie", cookie).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(roomModel)).execute().get();
        return response.getStatusCode();
    }

    public int leaveRoom(Integer roomId, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(serverUrl).append("/room/").append(roomId).append("/roomAttenders/self");
        final Response response = asyncHttpClient.prepareDelete(stringBuilder.toString()).
                setHeader("Cookie", cookie).execute().get();
        return response.getStatusCode();
    }

    public int talk(Integer roomId, String talk, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        final TalkHistoryModel talkHistoryModel = new TalkHistoryModel();
        talkHistoryModel.setTalk(talk);
        stringBuilder.append(serverUrl).append("/room/").append(roomId).append("/talk");
        final Response response = asyncHttpClient.preparePost(stringBuilder.toString()).
                setHeader("Content-Type", MainApplication.ContentType).
                setHeader("Cookie", cookie).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(talkHistoryModel)).execute().get();
        return response.getStatusCode();
    }

    private String retrieveCookie(final List<String> stringList) {
        final Set<String> set = new HashSet<String>();
        for (String header : stringList) {
            for (String string : header.split(";")) {
                set.add(string);
            }
        }
        final StringBuffer stringBuffer = new StringBuffer();
        for (String string : set) {
            stringBuffer.append(string).append(";");
        }
        return stringBuffer.toString();
    }

    public void connectWebSocket(WebSocketListener webSocketListener, String cookie) throws IOException, ExecutionException, InterruptedException {
        final AsyncHttpClient asyncHttpClient = mainApplication.getObjectGraph().get(AsyncHttpClient.class);
        webSocketListener.setClient(asyncHttpClient);
        final WebSocket webSocket = asyncHttpClient.prepareGet(serverUrl.replace("http://", "ws://") + "/user/self/ws").
                addHeader("Cookie", cookie).
                execute(new WebSocketUpgradeHandler.Builder().
                        addWebSocketListener(webSocketListener).build()).get();
        webSocket.sendTextMessage("hi");
    }

    public int requestComplete(List<Integer> idList, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(serverUrl).append("/user/self/messageQueue/complete");
        final Response response = asyncHttpClient.preparePut(stringBuilder.toString()).
                setHeader("Content-Type", MainApplication.ContentType).
                setHeader("Cookie", cookie).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(idList)).execute().get();
        return response.getStatusCode();
    }

    public int changeCurrentNick(String nick, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(serverUrl).append("/user/self/nick");
        final Response response = asyncHttpClient.preparePut(stringBuilder.toString()).
                setHeader("Content-Type", MainApplication.ContentType).
                setHeader("Cookie", cookie).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(nick)).execute().get();
        return response.getStatusCode();
    }

    public int logoutForceAll() throws IOException, ExecutionException, InterruptedException {
        final Response response = asyncHttpClient.prepareDelete(serverUrl + "/admin/logoutForce").execute().get();
        return response.getStatusCode();
    }

    public int setRoomName(Integer roomId, String roomName, String cookie) throws IOException, ExecutionException, InterruptedException {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(serverUrl).append("/room/").append(roomId).append("/name");
        final Response response = asyncHttpClient.preparePut(stringBuilder.toString()).
                setHeader("Content-Type", MainApplication.ContentType).
                setHeader("Cookie", cookie).
                setBody(DefaultObjectMapper.objectMapper.writeValueAsString(roomName)).execute().get();
        return response.getStatusCode();
    }

    public WebSocketListener allocWebSocketListener() {
        return new WebSocketListener();
    }

    @Setter
    @Getter
    public class WebSocketListener extends DefaultWebSocketListener {
        private AsyncHttpClient client;
        final private List<EnvelopeModel<?>> envelopeModelList;
        final public BlockingQueue<Integer> blockingQueue = new LinkedBlockingDeque<Integer>();
        private String cookie;

        public WebSocketListener() {
            envelopeModelList = new LinkedList<EnvelopeModel<?>>();
        }

        @Override
        public void onMessage(String message) {
            super.onMessage(message);
            try {
                final List<EnvelopeModel<?>> envelopeModelList = DefaultObjectMapper.deserializeEnvelopeList(message);
                final List<Integer> idList = new LinkedList<Integer>();
                for (EnvelopeModel envelopeModel : envelopeModelList) {
                    idList.add(envelopeModel.getId());
                }
                // message 를 받는 즉시 응답을 해준다.
                requestComplete(idList, cookie);
                this.envelopeModelList.addAll(envelopeModelList);
                blockingQueue.add(1);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClose(WebSocket websocket) {
            client.close();
        }
    }

}
