/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.app.Dialog;
import android.content.DialogInterface;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import dagger.ObjectGraph;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MainActivity;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomListFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.settings.MainPreferenceFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnterRoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.EnterType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.RoomInOutHistoryModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.RoomTimelineModel;

import java.util.List;
import java.util.Queue;
import java.util.Random;

/**
 * Created by Kang Woo, Lee on 5/29/15.
 */
public class TestSingle extends ActivityInstrumentationTestCase2<MainActivity> {
    private MainActivity mainActivity;
    private final Random random = new Random();
    final String userName = Integer.toHexString(random.nextInt());
    final String userPassword = Integer.toHexString(random.nextInt());
    final String userName2 = Integer.toHexString(random.nextInt());
    final String userPassword2 = Integer.toHexString(random.nextInt());
    final String roomName = Integer.toHexString(random.nextInt());
    private TestClient testClient;

    public TestSingle() {
        super(MainActivity.class);
    }

    // 여기서 할 것은, 앞으로 있을 test 의 반복부분을 따로 검사하여, 그 반복 부분에 관한 검사를 반복하지 않도록 하는 것이다.
    // 이 test 의 본문에서 수행되는 내용들이, 다른 test 의 setUp 단계에 들어가게 된다.
    @Override
    protected void setUp() throws Exception {
        // 뭔가 잘못 알고 있는것인가. 기다리지 않으면 가끔씩 실행 앞부분에서 breakpoint 가 걸리지 않을 때가 있다.
        Thread.sleep(1000);
        super.setUp();
        final MainApplication mainApplication = (MainApplication) getInstrumentation().getTargetContext().getApplicationContext();
        mainApplication.setObjectGraph(ObjectGraph.create(new TestMainDaggerModule()));
        mainActivity = getActivity();
        testClient = new TestClient(((MainApplication) getActivity().getApplication()));
        testClient.logoutForceAll();
    }

    @Override
    protected void tearDown() throws Exception {
        mainActivity.finish();
        super.tearDown();
    }

    public static void assertEmptyQueue(Queue<?> queue) {
        assertTrue(queue.isEmpty());
    }

    public static void setPreference(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity, final String userName, final String userPassword) throws Throwable {
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainActivity.getMainFragment().getButtonPreferences().performClick();
            }
        });
        Thread.sleep(100);

        // 앞서 만들었던 사용자 계정으로 login 사용자설정을 바꾼다.
        final MainPreferenceFragment mainPreferenceFragment = (MainPreferenceFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(MainPreferenceFragment.TAG);
        final EditTextPreference preferenceAccountName = (EditTextPreference) mainPreferenceFragment.
                getPreferenceScreen().findPreference(mainActivity.getResources().getString(R.string.keyAccountName));
        final EditTextPreference preferenceAccountPassword = (EditTextPreference) mainPreferenceFragment.
                getPreferenceScreen().findPreference(mainActivity.getResources().getString(R.string.keyAccountPassword));
        final CheckBoxPreference preferenceDebugMode = (CheckBoxPreference) mainPreferenceFragment.
                getPreferenceScreen().findPreference(mainActivity.getResources().getString(R.string.keyDebugMessage));

        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                preferenceAccountName.setText(userName);
                preferenceAccountPassword.setText(userPassword);
                preferenceDebugMode.setChecked(false);
            }
        });
        // 일관성을 위해서 fragment 를 벗어나는 것은 명시적으로 하기로 하자.
//        a.sendKeys(KeyEvent.KEYCODE_BACK);
//        Thread.sleep(100);
    }

    public static void logout(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity) throws Throwable {
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainActivity.getMainFragment().getButtonLogout().performClick();
            }
        });
        ((TestMainFragment) mainActivity.getMainFragment()).logoutBlockingQueue.take();
        assertEmptyQueue(((TestMainFragment) mainActivity.getMainFragment()).logoutBlockingQueue);
        Thread.sleep(100);
        assertNull(mainActivity.getMainFragment().getCookie());
    }

    public static void login(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity) throws Throwable {
        // login 을 시도한다.
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainActivity.getMainFragment().getButtonLogin().performClick();
            }
        });
        ((TestMainFragment) mainActivity.getMainFragment()).loginBlockingQueue.take();
        assertEmptyQueue(((TestMainFragment) mainActivity.getMainFragment()).loginBlockingQueue);
        Thread.sleep(100);
        // login 이 성공했으면 cookie 가 설정되어 있어야 한다.
        assertNotNull(mainActivity.getMainFragment().getCookie());
    }

    public static void registerUser(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity, final String userName, final String userPassword) throws Throwable {
        // 사용자 등록을 시도한다.
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainActivity.getMainFragment().getButtonRegisterUser().performClick();
            }
        });
        // 항상, 화면전환을 위한 여유 시간을 준다.
        Thread.sleep(100);
        // 사용자 등록을 위한 대화상자가 만들어졌는지 확인한다.
        assertNotNull(mainActivity.getMainFragment().getRegisterUserDialog());
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                final EditText editTextUserName = (EditText) mainActivity.getMainFragment().getRegisterUserDialog().findViewById(R.id.editTextUserName);
                final EditText editTextUserPassword = (EditText) mainActivity.getMainFragment().getRegisterUserDialog().findViewById(R.id.editTextUserPassword);
                editTextUserName.setText(userName);
                editTextUserPassword.setText(userPassword);
                mainActivity.getMainFragment().getRegisterUserDialog().getButton(Dialog.BUTTON_POSITIVE).performClick();
            }
        });

        ((TestMainFragment) mainActivity.getMainFragment()).registerUserBlockingQueue.take();
        assertEmptyQueue(((TestMainFragment) mainActivity.getMainFragment()).registerUserBlockingQueue);
        // 화면변화가 모두 끝나길 기다린다.
        Thread.sleep(100);
        // 대화상자를 가리키는 참조변수가 초기화되어야 한다.
        assertNull(mainActivity.getMainFragment().getRegisterUserDialog());
    }

    public static void enterAllCreatedRoomsFragment(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity) throws Throwable {
        // 개설된 대화방 목록 열람을 시도한다.
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainActivity.getMainFragment().getButtonAllCreatedRooms().performClick();
            }
        });
        // fragment 가 만들어지길 기다린다.
        Thread.sleep(100);
        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        roomListFragment.roomListBlockQueue.take();
        assertEmptyQueue(roomListFragment.roomListBlockQueue);
        // 개설된 대화방의 개수가 몇개인지 검증하지는 않는다. 이 method 가 사용되는 상황에 따라 보이는 대화방 개수가 다를 수 있기 때문이다.
    }

    public static void enterRoom(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity, final Integer position, final String roomPassword) throws Throwable {
        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                roomListFragment.getListView().performItemClick(roomListFragment.getListView(), position, roomListFragment.getListAdapter().getItemId(position));
            }
        });
        Thread.sleep(100);
        if (roomPassword != null) {
            assertNotNull(roomListFragment.enterRoomDialog);
            a.runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final EditText editTextRoomPassword = (EditText) roomListFragment.enterRoomDialog.findViewById(R.id.editTextRoomPassword);
                    editTextRoomPassword.setText(roomPassword);
                    roomListFragment.enterRoomDialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                }
            });
        }
        roomListFragment.enterRoomBlockQueue.take();
        assertEmptyQueue(roomListFragment.enterRoomBlockQueue);
        Thread.sleep(100);
        assertNull(roomListFragment.enterRoomDialog);

        final TestRoomFragment roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        Thread.sleep(100);
        // 대화방에 입장할 때 받는 web socket message 는 한개이다.
        assertEquals(roomFragment.getEnvelopeModelAdapter().getCount(), 1);
    }

    public static void createRoom(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity, final String roomName, final String roomPassword) throws Throwable {
        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        // 대화방을 개설한다.
        a.getInstrumentation().invokeMenuActionSync(mainActivity, R.id.menuItemCreateRoom, 0);
        // 화면변화가 완료되길 기다린다.
        Thread.sleep(100);
        assertNotNull(roomListFragment.createRoomDialog);
        // 만들 대화방의 제목만 쓴다. 암호는 쓰지 않는다.
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                final EditText editTextRoomName = (EditText) roomListFragment.createRoomDialog.findViewById(R.id.editTextRoomName);
                final EditText editTextRoomPassword = (EditText) roomListFragment.createRoomDialog.findViewById(R.id.editTextRoomPassword);
                editTextRoomName.setText(roomName);
                editTextRoomPassword.setText(roomPassword);
                roomListFragment.createRoomDialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
            }
        });
        // 대화방 개설 후 자동으로 대화방 화면으로 이동한다. 그래서, 대화방 목록 갱신은 이루어지지 않는다.
        roomListFragment.createRoomBlockQueue.take();
        assertEmptyQueue(roomListFragment.createRoomBlockQueue);
        Thread.sleep(100);
        // 대화방 개설 후 대화방 목록 갱신이 수행되지 않았다는 것을 확인한다.
        assertEmptyQueue(roomListFragment.roomListBlockQueue);
        assertNull(roomListFragment.createRoomDialog);

        final TestRoomFragment roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);
        // 입장하면 받는 message 들이 도착하는지 확인한다.
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        // 기본으로 전달받아야 하는 message 수를 확인한다.
        Thread.sleep(100);
        assertEquals(roomFragment.getEnvelopeModelAdapter().getCount(), 3);
    }

    public static void leaveRoom(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity) throws InterruptedException {
        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        final TestRoomFragment roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);

        a.getInstrumentation().invokeMenuActionSync(mainActivity, R.id.menuItemOut, 0);
        roomFragment.leaveBlockingQueue.take();
        assertEmptyQueue(roomFragment.leaveBlockingQueue);
        Thread.sleep(100);

        // 대화방을 퇴장하면, 대화방 목록 화면으로 돌아간다.
        roomListFragment.roomListBlockQueue.take();
        assertEmptyQueue(roomListFragment.roomListBlockQueue);
        // 대화방 퇴장의 경우 room fragment 를 벗어나게 된다. 이와 같이 동작 자체에 fragment 벗어나기가 이어지는 경우가 있다.
    }

    public static void changeRoomName(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity, final String roomName) throws Throwable {
        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        final TestRoomFragment roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);

        a.getInstrumentation().invokeMenuActionSync(mainActivity, R.id.menuItemChangeRoomName, 0);
        Thread.sleep(100);
        assertNotNull(roomFragment.changeRoomNameDialog);

        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                final EditText editTextRoomName = (EditText) roomFragment.changeRoomNameDialog.findViewById(R.id.editTextRoomName);
                editTextRoomName.setText(roomName);
                roomFragment.changeRoomNameDialog.getButton(Dialog.BUTTON_POSITIVE).performClick();
            }
        });

        roomFragment.changeRoomNameBlockingQueue.take();
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(roomFragment.changeRoomNameBlockingQueue);
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        Thread.sleep(100);
        assertNull(roomFragment.changeRoomNameDialog);
    }

    public static void changeCurrentNick(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity, final String nick) throws Throwable {
        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        final TestRoomFragment roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);

        a.getInstrumentation().invokeMenuActionSync(mainActivity, R.id.menuItemChangeCurrentNick, 0);
        Thread.sleep(100);
        assertNotNull(roomFragment.changeCurrentNickDialog);

        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                final EditText editTextNick = (EditText) roomFragment.changeCurrentNickDialog.findViewById(R.id.editTextNick);
                editTextNick.setText(nick);
                roomFragment.changeCurrentNickDialog.getButton(Dialog.BUTTON_POSITIVE).performClick();
            }
        });

        roomFragment.changeCurrentNickBlockingQueue.take();
        // 자신의 대화명이 바뀌는 경우, room timeline 에 섞여서 한번, 직접 user nick history 가 한번 이렇게 두번 따로 온다.
        // 그런데, 여기서는 한번의 기다림만 있다. 왜 그런가.
        // test room fragment 에서 써 놓은 test web socket service receiver 의 구현 내용을 살펴보면,
        // 현재 대화방에 관련된 message 를 담는 adapter 의 길이에 변화가 생긴 경우에만 blocking queue 를 조작하도록 되어 있다.
        // 그래서, 실제로는 web socket 으로 두개의 message 가 도착했지만, 한번의 blocking queue 조작만 있게 된다.
        // 한편, 다른 사람의 대화명이 바뀌는 경우, room timeline 만 한번 온다.
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(roomFragment.changeCurrentNickBlockingQueue);
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        Thread.sleep(100);
        assertNull(roomFragment.changeCurrentNickDialog);
    }

    public static void talk(final ActivityInstrumentationTestCase2<MainActivity> a, final MainActivity mainActivity, final String talk) throws Throwable {
        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        final TestRoomFragment roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);

        final EditText editTextTalk = (EditText) roomFragment.getView().findViewById(R.id.editTextTalk);
        final Button buttonTalk = (Button) roomFragment.getView().findViewById(R.id.buttonTalk);
        a.runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                // 대화를 쓰고.
                editTextTalk.setText(talk);
                // 대화를 전송한다.
                buttonTalk.performClick();
            }
        });
        // 이 기다림은, 명령을 완료했다는 것을 기다리는 것이고,
        roomFragment.talkBlockingQueue.take();
        // 이 기다림은, message 를 전달받았다는 것을 기다리는 것이다.
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(roomFragment.talkBlockingQueue);
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        // 화면변화에 걸리는 시간을 기다린다.
        Thread.sleep(100);
        // 대화 전송에 따른 web socket message 가 도착했다는 사실만 확인한다. 도착한 message 의 내용은 밖에서 명시적으로 확인한다.
    }

    public void testSetUpTemplate() throws Throwable {
        // 사용자를 등록한다.
        registerUser(this, mainActivity, userName, userPassword);
        // 접속 정보를 바꾼다.
        setPreference(this, mainActivity, userName, userPassword);
        // 첫화면으로 돌아간다.
        sendKeys(KeyEvent.KEYCODE_BACK);
        Thread.sleep(100);
        // 접속한다.
        login(this, mainActivity);
        // 대화방 목록 화면으로 간다.
        enterAllCreatedRoomsFragment(this, mainActivity);
        // 대화방을 개설한다.
        createRoom(this, mainActivity, roomName, "");

        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        final TestRoomFragment roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);

        // 임시 client 로 사용자 등록을 한다.
        assertEquals(testClient.registerUser(userName2, userPassword2), 200);
        // 임시 client 로 login 한다.
        final String cookie = testClient.login(userName2, userPassword2, true);
        final TestClient.WebSocketListener webSocketListener = testClient.allocWebSocketListener();
        webSocketListener.setCookie(cookie);
        // 임시 client 로 web socket 접속을 한다.
        testClient.connectWebSocket(webSocketListener, cookie);
        // 접속할 때 보내는 say hi 의 응답을 기다린다.
        webSocketListener.blockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        // 임시 client 의 관점에서 개설된 대화방 목록을 구한다.
        final List<RoomModel> roomModels = testClient.getAllCreatedRooms(cookie);
        // android client 가 만든 대화방이 목록에서 보여야 한다.
        assertEquals(roomModels.size(), 1);
        final RoomModel roomModel = roomModels.get(0);
        // 대화방 입장을 시도한다.
        assertEquals(testClient.enterRoom(roomModel.getId(), new EnterRoomModel(), cookie), 200);
        // 임시 client 로 대화방 입장을 알리는 web socket message 가 도착해야 한다.
        webSocketListener.blockingQueue.take();
        // android client 로 대화방 입장을 알리는 web socket message 가 도착해야 한다.
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);

        // 예문을 만든다.
        final String talk = Integer.toHexString(random.nextInt());

        assertEquals(testClient.talk(roomModel.getId(), talk, cookie), 200);
        // 임시 client 가 대화를 하는 경우, 밖에서 직접 message 수신을 대기한다.
        webSocketListener.blockingQueue.take();
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        Thread.sleep(100);
        // 임시 client 가 보낸 대화를 임시 client 가 제대로 전달받았는지 확인한다.
        assertEquals(
                ((EnvelopeModel<RoomTimelineModel>) webSocketListener.getEnvelopeModelList().
                        get(webSocketListener.getEnvelopeModelList().size() - 1)).
                        getData().getTalkHistory().getTalk(), talk);
        // 임시 client 가 보낸 대화를 android client 가 제대로 전달받았는지 확인한다.
        assertEquals(
                ((EnvelopeModel<RoomTimelineModel>) roomFragment.
                        getEnvelopeModelAdapter().
                        getItem(roomFragment.getEnvelopeModelAdapter().getCount() - 1)).
                        getData().getTalkHistory().getTalk(), talk);


        final String talk2 = Integer.toHexString(random.nextInt());
        // 대화를 쓴다. 이 경우, talk method 안에서 message 수신을 대기했으므로, android client 에 관해서는 대기할 필요 없다.
        talk(this, mainActivity, talk2);
        webSocketListener.blockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        // web socket 으로 대화를 전달받았는지 확인한다.
        // android client 가 보낸 대화를 임시 client 가 제대로 전달받았는지 확인한다.
        assertEquals(
                ((EnvelopeModel<RoomTimelineModel>) webSocketListener.getEnvelopeModelList().
                        get(webSocketListener.getEnvelopeModelList().size() - 1)).
                        getData().getTalkHistory().getTalk(), talk2);
        // android client 가 보낸 대화를 android client 가 제대로 전달받았는지 확인한다.
        assertEquals(
                ((EnvelopeModel<RoomTimelineModel>) roomFragment.
                        getEnvelopeModelAdapter().
                        getItem(roomFragment.getEnvelopeModelAdapter().getCount() - 1)).
                        getData().getTalkHistory().getTalk(), talk2);

        // 임시 client 가 대화방을 퇴장한다.
        assertEquals(testClient.leaveRoom(roomModel.getId(), cookie), 200);
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        webSocketListener.blockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        // 임시 client 가 logout 한다.
        assertEquals(testClient.logout(cookie), 200);

        // 퇴장한다.
        leaveRoom(this, mainActivity);
        // 이 단계에서는, 대화방 목록 fragment 에 있게 된다.
        // 다시한번 back button 을 눌려 첫 화면으로 되돌아간다.
        sendKeys(KeyEvent.KEYCODE_BACK);
        Thread.sleep(100);
        // 접속종료한다.
        logout(this, mainActivity);
    }

    public void testEnterPrivateRoom() throws Throwable {
        // 사용자를 등록한다.
        registerUser(this, mainActivity, userName, userPassword);
        // 접속 정보를 바꾼다.
        setPreference(this, mainActivity, userName, userPassword);
        // 첫화면으로 돌아간다.
        sendKeys(KeyEvent.KEYCODE_BACK);
        Thread.sleep(100);
        // 접속한다.
        login(this, mainActivity);

        // 임시 client 로 사용자 등록을 한다.
        assertEquals(testClient.registerUser(userName2, userPassword2), 200);
        // 임시 client 로 login 한다.
        final String cookie = testClient.login(userName2, userPassword2, true);
        final TestClient.WebSocketListener webSocketListener = testClient.allocWebSocketListener();
        webSocketListener.setCookie(cookie);
        // 임시 client 로 web socket 접속을 한다.
        testClient.connectWebSocket(webSocketListener, cookie);
        // 접속할 때 보내는 say hi 의 응답을 기다린다.
        webSocketListener.blockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        // 임시 client 가 암호있는 대화방을 개설한다.
        final String roomName = Integer.toHexString(random.nextInt());
        final String roomPassword = Integer.toHexString(random.nextInt());
        testClient.createRoom(roomName, roomPassword, cookie);
        webSocketListener.blockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);

        // 대화방 목록 화면으로 간다.
        enterAllCreatedRoomsFragment(this, mainActivity);
        Thread.sleep(100);
        final TestRoomListFragment roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        // 비공개 대화방에 입장한다.
        enterRoom(this, mainActivity, 0, roomPassword);
        // android client 가 대화방 입장을 했다는 web socket message 를 임시 client 가 받았는지 확인한다.
        webSocketListener.blockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        // 입장했다는 message 를 받아야 한다.
        final RoomInOutHistoryModel roomInOutHistory = ((EnvelopeModel<RoomTimelineModel>) webSocketListener.getEnvelopeModelList().get(webSocketListener.getEnvelopeModelList().size() - 1)).getData().getRoomInOutHistory();
        assertEquals(roomInOutHistory.getEnterType(), EnterType.Enter);
        assertEquals(roomInOutHistory.getUser().getName(), userName);

        final TestRoomFragment roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);

        // 임시 client 의 관점에서 개설된 대화방 목록을 구한다.
        final List<RoomModel> roomModels = testClient.getAllCreatedRooms(cookie);
        // android client 가 만든 대화방이 목록에서 보여야 한다.
        assertEquals(roomModels.size(), 1);
        final RoomModel roomModel = roomModels.get(0);

        // 예문을 만든다.
        final String talk = Integer.toHexString(random.nextInt());

        assertEquals(testClient.talk(roomModel.getId(), talk, cookie), 200);
        // 임시 client 가 대화를 하는 경우, 밖에서 직접 message 수신을 대기한다.
        webSocketListener.blockingQueue.take();
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        Thread.sleep(100);
        // 임시 client 가 보낸 대화를 임시 client 가 제대로 전달받았는지 확인한다.
        assertEquals(
                ((EnvelopeModel<RoomTimelineModel>) webSocketListener.getEnvelopeModelList().
                        get(webSocketListener.getEnvelopeModelList().size() - 1)).
                        getData().getTalkHistory().getTalk(), talk);
        // 임시 client 가 보낸 대화를 android client 가 제대로 전달받았는지 확인한다.
        assertEquals(
                ((EnvelopeModel<RoomTimelineModel>) roomFragment.
                        getEnvelopeModelAdapter().
                        getItem(roomFragment.getEnvelopeModelAdapter().getCount() - 1)).
                        getData().getTalkHistory().getTalk(), talk);


        final String talk2 = Integer.toHexString(random.nextInt());
        // 대화를 쓴다. 이 경우, talk method 안에서 message 수신을 대기했으므로, android client 에 관해서는 대기할 필요 없다.
        talk(this, mainActivity, talk2);
        webSocketListener.blockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        // web socket 으로 대화를 전달받았는지 확인한다.
        // android client 가 보낸 대화를 임시 client 가 제대로 전달받았는지 확인한다.
        assertEquals(
                ((EnvelopeModel<RoomTimelineModel>) webSocketListener.getEnvelopeModelList().
                        get(webSocketListener.getEnvelopeModelList().size() - 1)).
                        getData().getTalkHistory().getTalk(), talk2);
        // android client 가 보낸 대화를 android client 가 제대로 전달받았는지 확인한다.
        assertEquals(
                ((EnvelopeModel<RoomTimelineModel>) roomFragment.
                        getEnvelopeModelAdapter().
                        getItem(roomFragment.getEnvelopeModelAdapter().getCount() - 1)).
                        getData().getTalkHistory().getTalk(), talk2);

        // 임시 client 가 대화방을 퇴장한다.
        assertEquals(testClient.leaveRoom(roomModel.getId(), cookie), 200);
        roomFragment.webServiceBlockingQueue.take();
        assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        webSocketListener.blockingQueue.take();
        assertEmptyQueue(webSocketListener.blockingQueue);
        // 임시 client 가 logout 한다.
        assertEquals(testClient.logout(cookie), 200);

        // 퇴장한다.
        leaveRoom(this, mainActivity);
        // 이 단계에서는, 대화방 목록 fragment 에 있게 된다.
        // 다시한번 back button 을 눌려 첫 화면으로 되돌아간다.
        sendKeys(KeyEvent.KEYCODE_BACK);
        Thread.sleep(100);
        // 접속종료한다.
        logout(this, mainActivity);
    }
}
