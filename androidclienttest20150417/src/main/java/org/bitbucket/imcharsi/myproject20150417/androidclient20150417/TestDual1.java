/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;
import dagger.ObjectGraph;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MainActivity;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomListFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnterRoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeDataType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.RoomTimelineModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.history.UserNickHistoryModel;

import java.util.List;
import java.util.Random;

/**
 * Created by Kang Woo, Lee on 5/7/15.
 */
public class TestDual1 extends ActivityInstrumentationTestCase2<MainActivity> {
    private MainActivity mainActivity;
    private final Random random = new Random();
    final String userName = Integer.toHexString(random.nextInt());
    final String userPassword = Integer.toHexString(random.nextInt());
    final String userName2 = Integer.toHexString(random.nextInt());
    final String userPassword2 = Integer.toHexString(random.nextInt());
    final String roomName = Integer.toHexString(random.nextInt());
    private TestClient testClient;
    private String testClientCookie;
    private TestClient.WebSocketListener testClientWebSocketListener;
    private TestRoomListFragment roomListFragment;
    private TestRoomFragment roomFragment;
    private RoomModel roomModel;

    public TestDual1() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        // 뭔가 잘못 알고 있는것인가. 기다리지 않으면 가끔씩 실행 앞부분에서 breakpoint 가 걸리지 않을 때가 있다.
        Thread.sleep(1000);
        super.setUp();
        final MainApplication mainApplication = (MainApplication) getInstrumentation().getTargetContext().getApplicationContext();
        mainApplication.setObjectGraph(ObjectGraph.create(new TestMainDaggerModule()));
        mainActivity = getActivity();

        testClient = new TestClient(((MainApplication) getActivity().getApplication()));
        testClient.logoutForceAll();

        try {
            // 사용자를 등록한다.
            TestSingle.registerUser(this, mainActivity, userName, userPassword);
            // 접속 정보를 바꾼다.
            TestSingle.setPreference(this, mainActivity, userName, userPassword);
            // 첫화면으로 돌아간다.
            sendKeys(KeyEvent.KEYCODE_BACK);
            Thread.sleep(100);
            // 접속한다.
            TestSingle.login(this, mainActivity);
            // 대화방 목록 화면으로 간다.
            TestSingle.enterAllCreatedRoomsFragment(this, mainActivity);
            // 대화방을 개설한다.
            TestSingle.createRoom(this, mainActivity, roomName, "");
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }

        // 임시 client 로 사용자 등록을 한다.
        assertEquals(testClient.registerUser(userName2, userPassword2), 200);
        // 임시 client 로 login 한다.
        testClientCookie = testClient.login(userName2, userPassword2, true);
        testClientWebSocketListener = testClient.allocWebSocketListener();
        testClientWebSocketListener.setCookie(testClientCookie);
        // 임시 client 로 web socket 접속을 한다.
        testClient.connectWebSocket(testClientWebSocketListener, testClientCookie);
        // 접속할 때 보내는 say hi 의 응답을 기다린다.
        testClientWebSocketListener.blockingQueue.take();
        TestSingle.assertEmptyQueue(testClientWebSocketListener.blockingQueue);
        final List<RoomModel> roomModels = testClient.getAllCreatedRooms(testClientCookie);
        // android client 가 만든 대화방이 목록에서 보여야 한다.
        assertEquals(roomModels.size(), 1);

        roomListFragment = (TestRoomListFragment) mainActivity.getMainFragment().getFragmentManager().findFragmentByTag(RoomListFragment.TAG);
        roomFragment = (TestRoomFragment) roomListFragment.getFragmentManager().findFragmentByTag(RoomFragment.TAG);

        roomModel = roomModels.get(0);
        // 대화방 입장을 시도한다.
        assertEquals(testClient.enterRoom(roomModel.getId(), new EnterRoomModel(), testClientCookie), 200);
        // 임시 client 로 대화방 입장을 알리는 web socket message 가 도착해야 한다.
        testClientWebSocketListener.blockingQueue.take();
        // android client 로 대화방 입장을 알리는 web socket message 가 도착해야 한다.
        roomFragment.webServiceBlockingQueue.take();
        TestSingle.assertEmptyQueue(testClientWebSocketListener.blockingQueue);
        TestSingle.assertEmptyQueue(roomFragment.webServiceBlockingQueue);
    }

    @Override
    protected void tearDown() throws Exception {
        // 시작에 관한 중복만 정리하고, 종료에 관해서는 각 검사에서 직접 한다.
        mainActivity.finish();
        super.tearDown();
    }

    // 운영자에 의한 대화명 바꾸기는 검사할수 없었다. 우선 운영자 권한 수여를 해야 하는데, list view 의 item 에 대한 long click 을 하는 방법을 모르겠다.
    public void test001_ChangeRoomNameByOwner() throws Throwable {
        // 대화방 제목을 바꾼다.
        final String roomName = Integer.toHexString(random.nextInt());
        TestSingle.changeRoomName(this, mainActivity, roomName);
        // 같은 대화방 안에 있으므로, 방장의 대화방 제목 변경에 관한 web socket message 를 받는다.
        testClientWebSocketListener.blockingQueue.take();
        TestSingle.assertEmptyQueue(testClientWebSocketListener.blockingQueue);
        // web socket 으로 전달받은 room history 변경 message 를 확인한다.
        final EnvelopeModel<RoomTimelineModel> envelopeModel = (EnvelopeModel<RoomTimelineModel>) testClientWebSocketListener.getEnvelopeModelList().get(testClientWebSocketListener.getEnvelopeModelList().size() - 1);
        final EnvelopeModel<RoomTimelineModel> envelopeModel2 = (EnvelopeModel<RoomTimelineModel>) roomFragment.getEnvelopeModelAdapter().getItem(roomFragment.getEnvelopeModelAdapter().getCount() - 1);
        assertEquals(envelopeModel.getData().getRoomHistory().getName(), roomName);
        assertEquals(envelopeModel2.getData().getRoomHistory().getName(), roomName);
        // 대화방 제목이 바뀌었는지 확인한다.
        final RoomModel roomModel2 = testClient.getRoom(roomModel.getId(), testClientCookie);
        assertEquals(roomModel2.getCurrentDetail().getName(), roomName);

        // 임시 client 가 대화방을 퇴장한다.
        assertEquals(testClient.leaveRoom(roomModel.getId(), testClientCookie), 200);
        roomFragment.webServiceBlockingQueue.take();
        TestSingle.assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        testClientWebSocketListener.blockingQueue.take();
        TestSingle.assertEmptyQueue(testClientWebSocketListener.blockingQueue);
        // 임시 client 가 logout 한다.
        assertEquals(testClient.logout(testClientCookie), 200);

        // 퇴장한다.
        TestSingle.leaveRoom(this, mainActivity);
        // 이 단계에서는, 대화방 목록 fragment 에 있게 된다.
        // 다시한번 back button 을 눌려 첫 화면으로 되돌아간다.
        sendKeys(KeyEvent.KEYCODE_BACK);
        Thread.sleep(100);
        // 접속종료한다.
        TestSingle.logout(this, mainActivity);
    }

    public void test002_ChangeCurrentNick() throws Throwable {
        // 대화명을 바꾼다.
        final String nick = Integer.toHexString(random.nextInt());
        TestSingle.changeCurrentNick(this, mainActivity, nick);
        // 같은 대화방 안에 있으므로, 방장의 대화방 제목 변경에 관한 web socket message 를 받는다.
        testClientWebSocketListener.blockingQueue.take();
        TestSingle.assertEmptyQueue(testClientWebSocketListener.blockingQueue);
        // web socket 으로 전달받은 room history 변경 message 를 확인한다.
        final EnvelopeModel<RoomTimelineModel> envelopeModel = (EnvelopeModel<RoomTimelineModel>) testClientWebSocketListener.getEnvelopeModelList().get(testClientWebSocketListener.getEnvelopeModelList().size() - 1);
        final EnvelopeModel<RoomTimelineModel> envelopeModel2 = (EnvelopeModel<RoomTimelineModel>) roomFragment.getEnvelopeModelAdapter().getItem(roomFragment.getEnvelopeModelAdapter().getCount() - 1);
        assertEquals(envelopeModel.getData().getUserNickHistory().getNick(), nick);
        assertEquals(envelopeModel2.getData().getUserNickHistory().getNick(), nick);
        // 기타 message 로 분류되는 envelope model<user nick history model> 에 관한 toast 가 나타났다는 것은, 일단 확인하지 말자.

        // 임시 client 도 대화명 바꾸기를 시도한다.
        final String nick2 = Integer.toHexString(random.nextInt());
        assertEquals(testClient.changeCurrentNick(nick2, testClientCookie), 200);
        roomFragment.webServiceBlockingQueue.take();
        TestSingle.assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        // 임시 client 의 web socket listener 구현은 기타 message 와 대화방 message 를 구분하지 않는다. 따라서 두번 기다린다.
        testClientWebSocketListener.blockingQueue.take();
        testClientWebSocketListener.blockingQueue.take();
        TestSingle.assertEmptyQueue(testClientWebSocketListener.blockingQueue);
        final EnvelopeModel<?> envelopeModel3 = testClientWebSocketListener.getEnvelopeModelList().get(testClientWebSocketListener.getEnvelopeModelList().size() - 2);
        final EnvelopeModel<?> envelopeModel4 = testClientWebSocketListener.getEnvelopeModelList().get(testClientWebSocketListener.getEnvelopeModelList().size() - 1);
        final EnvelopeModel<RoomTimelineModel> envelopeModel5 = (EnvelopeModel<RoomTimelineModel>) roomFragment.getEnvelopeModelAdapter().getItem(roomFragment.getEnvelopeModelAdapter().getCount() - 1);
        // web service 가 보내는 응답의 순서를 정확히 알 수 없다. 둘중 하나는 이것이고 나머지 하나는 저것이므로 각각의 경우에 따라 검사한다. 번거롭다.
        if (envelopeModel3.getDataType() == EnvelopeDataType.RoomTimelineModelType) {
            assertEquals(((RoomTimelineModel) envelopeModel3.getData()).getUserNickHistory().getNick(), nick2);
        }
        if (envelopeModel3.getDataType() == EnvelopeDataType.UserNickHistoryModelType) {
            assertEquals(((UserNickHistoryModel) envelopeModel3.getData()).getNick(), nick2);
        }
        if (envelopeModel4.getDataType() == EnvelopeDataType.RoomTimelineModelType) {
            assertEquals(((RoomTimelineModel) envelopeModel4.getData()).getUserNickHistory().getNick(), nick2);
        }
        if (envelopeModel4.getDataType() == EnvelopeDataType.UserNickHistoryModelType) {
            assertEquals(((UserNickHistoryModel) envelopeModel4.getData()).getNick(), nick2);
        }
        assertEquals(envelopeModel5.getData().getUserNickHistory().getNick(), nick2);

        // 임시 client 가 대화방을 퇴장한다.
        assertEquals(testClient.leaveRoom(roomModel.getId(), testClientCookie), 200);
        roomFragment.webServiceBlockingQueue.take();
        TestSingle.assertEmptyQueue(roomFragment.webServiceBlockingQueue);
        testClientWebSocketListener.blockingQueue.take();
        TestSingle.assertEmptyQueue(testClientWebSocketListener.blockingQueue);
        // 임시 client 가 logout 한다.
        assertEquals(testClient.logout(testClientCookie), 200);

        // 퇴장한다.
        TestSingle.leaveRoom(this, mainActivity);
        // 이 단계에서는, 대화방 목록 fragment 에 있게 된다.
        // 다시한번 back button 을 눌려 첫 화면으로 되돌아간다.
        sendKeys(KeyEvent.KEYCODE_BACK);
        Thread.sleep(100);
        // 접속종료한다.
        TestSingle.logout(this, mainActivity);
    }
}
