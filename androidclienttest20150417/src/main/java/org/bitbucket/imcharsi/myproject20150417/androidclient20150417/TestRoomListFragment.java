/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.ning.http.client.AsyncHttpClient;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomListFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomModel;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by Kang Woo, Lee on 5/11/15.
 */
public class TestRoomListFragment extends RoomListFragment {
    public BlockingQueue<Integer> createRoomBlockQueue = new LinkedBlockingDeque<Integer>();
    public BlockingQueue<Integer> enterRoomBlockQueue = new LinkedBlockingDeque<Integer>();
    public BlockingQueue<Integer> roomListBlockQueue = new LinkedBlockingDeque<Integer>();
    public AlertDialog createRoomDialog;
    public AlertDialog enterRoomDialog;

    @Override
    protected AlertDialog createCreateRoomDialog(AlertDialog.Builder builder) {
        createRoomDialog = super.createCreateRoomDialog(builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                createRoomDialog = null;
            }
        }));
        return createRoomDialog;
    }


    @Override
    protected AlertDialog createEnterRoomDialog(AlertDialog.Builder builder) {
        enterRoomDialog = super.createEnterRoomDialog(builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                enterRoomDialog = null;
            }
        }));
        return enterRoomDialog;
    }

    public static class TestRoomListAsyncTask extends RoomListAsyncTask {
        @Inject
        public TestRoomListAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(List<RoomModel> roomModels) {
            super.onPostExecute(roomModels);
            ((TestRoomListFragment) getRoomListFragment()).roomListBlockQueue.add(1);
        }
    }

    public static class TestCreateRoomAsyncTask extends CreateRoomAsyncTask {
        @Inject
        public TestCreateRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            ((TestRoomListFragment) getRoomListFragment()).createRoomBlockQueue.add(1);
        }
    }

    public static class TestEnterRoomAsyncTask extends EnterRoomAsyncTask {
        @Inject
        public TestEnterRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        @Override
        protected void onPostExecute(Integer roomId) {
            super.onPostExecute(roomId);
            ((TestRoomListFragment) getRoomListFragment()).enterRoomBlockQueue.add(1);
        }
    }
}
