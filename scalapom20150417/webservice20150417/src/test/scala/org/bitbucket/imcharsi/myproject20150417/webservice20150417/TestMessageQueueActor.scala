/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import akka.actor.{ Actor, ActorRef, Props }
import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnvelopeModel, EnvelopeModelEx }
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor._
import org.scalatest.FunSuite

import scala.concurrent.duration.{ Duration, _ }
import scala.concurrent.{ Await, Promise }
import scala.util.Try

/**
 * Created by Kang Woo, Lee on 5/1/15.
 */
class TestMessageQueueActor extends FunSuite {
  val promise: Stream[Promise[Int]] = Promise[Int]() #:: Promise[Int]() #:: Promise[Int]() #:: Promise[Int]() #:: Promise[Int]() #:: Promise[Int]() #:: Stream.empty

  def number(i: Int): Stream[Int] = i #:: number(i + 1)

  implicit val timeout = Timeout(1.seconds)

  // 우선 요청을 넣고, 가져가라는 요청을 받은 후, 가져오기만 하고 확인은 해주지 않는다.
  // message 를 또 넣은 후, 다시 가져가라는 요청이 올 때, 가져왔던 것 전부 확인해준다.
  // 다시 message 를 넣은 후, 가져가라는 요청을 받고 확인해준 개수가 1개이면 검사가 되는 것이다.
  // 한편, user actor 는 임의로 만들어야겠다. requestWithdraw 에 대한 응답에 맞춰서 요청 가져오기를 시도해야 한다.
  test("message queue actor") {
    val iteratorForActor = promise.iterator
    // promise 의 매 success 를 구분하지 않으면, 이 성공이 어느 시점의 성공인지 구분할 수 없게 된다. 따라서, 실패한 검사인데도 성공한 검사로 판단할 수 있게 된다.
    val mockUserActor = Option(MasterActorSystem.actorSystem.actorOf(Props(new MockUserActor(iteratorForActor, number(0).iterator))))
    val messageQueueActor = MasterActorSystem.actorSystem.actorOf(Props(new SampleMessageQueueActor(mockUserActor)))
    val iterator = promise.iterator

    // 첫번째 message 를 보낸다.
    messageQueueActor.!(PushMessageCommand(List(EnvelopeModelEx(Some(UserModelEx(None, None, None, None, Some("hi"), Some("bye")))))))
    // user actor 가 request withdraw 요청을 받을 때까지 기다린다.
    assert(Await.result(iterator.next().future, Duration.Inf) == 0)
    // message 를 구하기만 한다.
    val firstMessageQueue = Await.result(messageQueueActor.?(GetMessageQueueCommand()), Duration.Inf).asInstanceOf[List[EnvelopeModel[_]]]
    assert(firstMessageQueue.length == 1)

    // 두번째 message 를 구한다.
    messageQueueActor.!(PushMessageCommand(List(EnvelopeModelEx(Some(RoomModelEx(None, None, None, None, None, None))))))
    // 첫번째 message 쌓기에 대한 가져가기 요청이 있은후 완료응답을 하기전에 message 가 또 쌓이면 가져가기 요청을 하지 않는다.
    // 0.5 초이면, message 쌓기에 따른 알림이 일어나기엔 너무 긴 시간이고, 첫번째 message 쌓기 이후 알림재개가 일어나기에는 너무 짧은 시간이다.
    val tmpPromise1 = iterator.next()
    assert(Try(Await.result(tmpPromise1.future, (0.5).seconds)).isFailure)
    // 이번에는 알림재개를 기다린다.
    assert(Await.result(tmpPromise1.future, Duration.Inf) == 1)
    // message 를 구하기만 한다.
    val secondMessageQueue = Await.result(messageQueueActor.?(GetMessageQueueCommand()), Duration.Inf).asInstanceOf[List[EnvelopeModel[_]]]
    assert(secondMessageQueue.length == 2)

    // 방금 받았던 message 중 한개에 대해서만 완료 응답을 한다.
    messageQueueActor.!(CompleteMessageCommand(secondMessageQueue.take(1).flatMap(_.id).toSet))
    // 완료응답을 했는데도 message queue 에 message 가 남았으므로, 바로 알림을 보낸다.
    // 알림재개는 1초 시간간격이므로 이보다 짧은 시간안에 알림이 왔으면 알림재개로 인한 알림이 아니다.
    assert(Await.result(iterator.next().future, (0.5).seconds) == 2)

    // 세번째 message 를 쌓는다.
    messageQueueActor.!(PushMessageCommand(List(EnvelopeModelEx(Some(RoomInOutHistoryModelEx(None, None, None, None, None, None, None, None))))))
    // 세번째 message 쌓기에 앞서 앞의 완료응답이 모든 message 에 대해 완료응답을 하지 않았으므로 message queue 에는 message 가 남아있게 된다.
    // 따라서 세번째 message 쌓기는 알림을 일으키지 않고, 완료응답 단계에서 설정된 알림재개로 인한 알림을 받게 된다.
    // 완료응답 단계에서 설정된 알림재개가 제대로 기능하는지 확인한다.
    val tmpPromise2 = iterator.next()
    // 이번 기다림은 실패해야 한다. 완료응답 단계에서 설정된 알림재개로 인한 기다림이다.
    assert(Try(Await.result(tmpPromise2.future, (0.5).seconds)).isFailure)
    assert(Await.result(tmpPromise2.future, Duration.Inf) == 3)
    // message 를 구하기만 한다.
    val thirdMessageQueue = Await.result(messageQueueActor.?(GetMessageQueueCommand()), Duration.Inf).asInstanceOf[List[EnvelopeModel[_]]]
    assert(thirdMessageQueue.length == 2)
    // 알림재개에 관한 검사는 앞에서 했다. 바로 완료응답을 해준다.
    // 완료응답을 하지 않았기 때문인지 모르겠는데, 알림재개 때문에 iterator.next() 가 계속 호출된다.
    messageQueueActor.!(CompleteMessageCommand(thirdMessageQueue.flatMap(_.id).toSet))

    // message 가 지연되도록 쌓는다.
    messageQueueActor.!(PushMessageCommand(List(EnvelopeModelEx(Some(RoomInOutHistoryModelEx(None, None, None, None, None, None, None, None)))), true))
    val tmpPromise3 = iterator.next()
    // 아무 message 가 없는 상태에서 쌓았으면, 지연기능을 쓰지 않았다면 알림이 바로 도착했어야 한다.
    assert(Try(Await.result(tmpPromise3.future, (0.5).seconds)).isFailure)
    assert(Try(Await.result(tmpPromise3.future, (1).seconds)).isSuccess)
    // 완료응답을 하지 않는다.
    // buffering 기능을 끈다.
    messageQueueActor.!(ChangeBufferingModeCommand(false))
    messageQueueActor.!(PushMessageCommand(List(EnvelopeModelEx(Some(RoomInOutHistoryModelEx(None, None, None, None, None, None, None, None)))), true))
    // 앞에서 완료응답을 하지 않았으므로, 그리고 buffering 기능을 껐으므로, message 를 쌓을때마다 알림이 도착한다.
    assert(Try(Await.result(iterator.next().future, (0.5).seconds)).isSuccess)
    // 완료응답을 한다.
    val fourthMessageQueue = Await.result(messageQueueActor.?(GetMessageQueueCommand()), Duration.Inf).asInstanceOf[List[EnvelopeModel[_]]]
    assert(fourthMessageQueue.length == 2)
    messageQueueActor.!(CompleteMessageCommand(fourthMessageQueue.flatMap(_.id).toSet))
  }

  class SampleMessageQueueActor(mockUserActor: Option[ActorRef]) extends MessageQueueActor(None, 1) {
    override protected[this] def requestWithdraw(): Unit = {
      mockUserActor.foreach(_.!(None))
    }
  }

  class MockUserActor(promise: Iterator[Promise[Int]], number: Iterator[Int]) extends Actor {
    override def receive: Receive = {
      case None ⇒ promise.next().success(number.next())
    }
  }

}
