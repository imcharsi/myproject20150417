/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import io.backchat.hookup._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModel.EnvelopeDataType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.CertificationModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnvelopeModel, LogoutModel }
import org.json4s._
import org.scalatest.BeforeAndAfterAll
import org.scalatra.test.scalatest.ScalatraFunSuite

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Try

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
class TestUserServlet extends ScalatraFunSuite with EmbeddedJettyTrait with BeforeAndAfterAll with EtcUtilTrait {
  implicit val formats = DefaultFormats.lossless

  protected override def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createSchemeUtil()
    Thread.sleep(3500)
  }

  protected override def afterAll(): Unit = {
    DriverSettings.dropSchemeUtil()
    super.afterAll()
  }

  test("register user") {
    val name: Option[String] = EtcUtilTrait.generateUserName()
    // cookie 없이 사용자 등록을 한다.
    val userId = registerUser(name, Some("bye"))
    assert(UserDao.getOne(userId).flatMap(_.name) == name)
    // 중복이면 실패해야 한다.
    registerUser(name, Some("bye"), 500)
    // 등록한 사용자 계정을 사용해서 접속을 시도한다.
    val cookie = login(CertificationModel(name, Some("bye")))
    logout(cookie)
  }

  test("login/logout") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    // 잘 된다. 한번 cookie 가 설정되고 난 이후에는, 또 다시 바뀌지 않는 이상 응답에서 cookie header 가 되돌아오지 않는다. 원래 이렇다.
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    echo(cookie)
    assert(getSelfInfo(cookie).isDefined)
    logout(cookie)
    logout(cookie, 401)
    echo(cookie, 401)
  }

  test("logoutForce") {
    // 그냥 login 해서 실패를 예상.
    // 강제접속종료를 켜고 login 해서 성공을 예상.
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    login(CertificationModel(userModel.name, userModel.password), 500)
    echo(cookie)
    val cookie2 = login(CertificationModel(userModel.name, userModel.password, Some(true)))
    echo(cookie, 401)
    echo(cookie2)
    logout(cookie, 401)
    logout(cookie2, 200)
  }

  test("logout and web socket disconnect") {
    // logout 에 따라, server 에서 자동으로 web socket 도 끊어주는지 확인한다.
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val promiseList1 = createPromiseList(5)
    val promiseIterator11 = promiseList1.iterator
    val promiseIterator12 = promiseList1.iterator
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    Try(Await.result(client.connect(), 10.seconds)) match {
      case scala.util.Success(Success) ⇒
      case _ ⇒ assert(false)
    }
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client) // ping 비슷하게 뭔가 하나 보내줘야 한다. 이유는 모르겠다.
    Await.result(promiseIterator11.next().future, 3.seconds)
    logout(cookie)
    // 자발적인 접속종료의 경우에는 web socket 으로부터 접속종료를 알리는 message 가 오지 않는다.
    // 아래의 promise 는 web socket 연결 종료에 의해 성공으로 바뀌게 된다.
    Await.result(promiseIterator11.next().future, Duration.Inf)
    assert(client.isConnected == false)
  }

  test("logoutForce and web socket disconnect") {
    // 강제접속종료에 따라, server 에서, 이전 login 과 관련된 web socket 을 자동으로 끊어주는지 확인한다.
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val promiseList1 = createPromiseList(5)
    val promiseIterator11 = promiseList1.iterator
    val promiseIterator12 = promiseList1.iterator
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    Try(Await.result(client.connect(), 3.seconds)) match {
      case scala.util.Success(Success) ⇒
      case _ ⇒ assert(false)
    }
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val cookie2 = login(CertificationModel(userModel.name, userModel.password, Some(true)))
    // 아래와 같이 traffic 을 만들어주는 것을 반드시 기억해야 한다. 강제접속종료가 성공했다고 바로 session 이 무효로 바뀌는것이 아니다.
    // 구현에 따라, 강제접속종료 이후 한번의 traffic 이 더 가야 session 무효화 단계에 들어서게 된다.
    // 강제 접속 종료가 성공하면 이전의 user actor 에게 actor 자체를 종료시킬 것을 요청하게 되고 이어서 web socket 을 닫게 된다.
    // 따라서 아래와 같은 확인용 traffic 은 할 필요가 없다.
    // echo(cookie, 401)

    // 강제접속종료에 의한 logout 을 알리는 message 가 web socket 으로 전달되었는지 확인한다.
    val resultList: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 5.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    assert(resultList.filter(t ⇒ t.dataType == Some(EnvelopeDataType.LogoutModelType)).
      flatMap(_.data).
      map(_.asInstanceOf[LogoutModel]).
      flatMap(_.userId) == List(userModel.id).flatten)
    // web socket 연결 종료를 기다린다.
    Await.result(promiseIterator11.next().future, Duration.Inf)
    assert(client.isConnected == false)
    logout(cookie2)
  }

  // web socket 중복접속이 안되도록 해 놓은 것이 제대로 기능하는지 검사하려고 했는데, scalatra 때문인지 atmosphere 때문인지 이유는 알 수 없고
  // 중복접속이 안된다. tcp/ip 연결만 되었고, 그 이후 단계로 넘어가지 않는다. 따라서, client 는 connect 결과를 기다리고 있게 된다.
  //  test("duplicated web socket connection") {
  //  }

  // dao 에서는 검사를 편하게 하기 위해 정지점을 정할 수 있도록 했지만, web service 접속을 검사할 때는 정지점을 정할수가 없다.
  // 따라서, 모든 시점 중첩 검사는 web service 가 아닌 dao 단계에서 해야 한다.

  test("web socket and message queue") {
    val promiseList1 = createPromiseList(10)
    val promiseIterator11 = promiseList1.iterator
    val promiseIterator12 = promiseList1.iterator
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))

    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    Try(Await.result(client.connect(), 3.seconds)) match {
      case scala.util.Success(Success) ⇒
      case _ ⇒ assert(false)
    }
    // 접속이 성공해야 다음 할 것을 한다.
    Await.result(promiseIterator11.next().future, 3.seconds)
    // 한번 뭔가를 보내줘야 다음부터 잘 된다. 이상하다.
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    pushSampleMessage(cookie)
    // 첫번째 message 쌓기에 따른 알림을 기다린다.
    val result1 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    assert(result1.length == 1)
    pushSampleMessage(cookie)
    // 완료응답없이 두번째 message 쌓기를 했을 때는, 쌓기를 했다는 이유 때문에 알림이 오지는 않는다. 이 때는 알림을 한 이후 설정한 알림재개를 통해서 알림이 이루어진다.
    val tmpPromise = promiseIterator11.next()
    assert(Try(Await.result(tmpPromise.future, 3.seconds)).isFailure)
    // 알림재개 시간간격을 10초로 해놨다. 따라서 첫번째 알림이 있은 뒤 10초 뒤에 다시 알림이 온다. 실패를 예상하고 3초를 기다렸으므로 최소 7초를 더 기다려야 하는데 8초를 기다려본다.
    // 또한, iterator.next() 의 순서가 달라지게 되므로, 앞서 실패했던 promise 를 다시 쓴다. 앞의 실패는 시간초과로 인한 실패지 promise 자체가 실패로 결론 난것이 아니므로, 또 써도 된다.
    val result2 = Await.result(tmpPromise.future, 8.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    assert(result2.length == 2)
    // 자료 한개에 대해서만 완료응답을 해준다.
    requestComplete(result2.take(1), cookie)
    // 즉시 알림이 와야 한다.
    val result3 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    assert(result3.length == 1)

    pushSampleMessage(cookie)
    // 알림재개에 의한 알림이 와야 한다. 이 검사는 최소 20초를 기다려야 하는 검사다. 다른 방법 없나.
    val tmpPromise2 = promiseIterator11.next()
    // 세번째 message 쌓기는 비어있는 message queue 에 대한 쌓기가 아니므로 알림이 바로 오지 않고,
    assert(Try(Await.result(tmpPromise2.future, 3.seconds)).isFailure)
    // 알림재개로 인한 알림이 온다.
    val result4 = Await.result(tmpPromise2.future, 8.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    assert(result4.length == 2)
    requestComplete(result4, cookie)
    // 알림재개에 관한 검사는 위에서 이미 했다. 3번째 message 쌓기에 대한 알림재개 검사를 또 할 필요 없다.
    // 3번째 message 쌓기를 하는 이유는, 완료응답을 했을 때, 쌓여있던 message 들이 전부 지워지는지 확인하기 위해서이다.
    // 3번째 message 를 보내서 돌아온 응답이 1개의 message 가 아니면 잘못된 것이다.
    logout(cookie)
    Await.result(promiseIterator11.next().future, Duration.Inf)
  }

  test("message queue and re-connect") {
    val promiseList1 = createPromiseList(10)
    val promiseIterator11 = promiseList1.iterator
    val promiseIterator12 = promiseList1.iterator

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))

    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    Try(Await.result(client.connect(), 3.seconds)) match {
      case scala.util.Success(Success) ⇒
      case _ ⇒ assert(false)
    }
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    pushSampleMessage(cookie)
    val result1 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    assert(result1.length == 1)
    // 일부러 web socket 만 끊는다.
    client.disconnect()
    Await.result(promiseIterator11.next().future, 3.seconds)
    // 다시 web socket 만 잇는다.

    val client2 = new SampleWebSocketClient(promiseIterator12, cookie)
    Try(Await.result(client2.connect(), 3.seconds)) match {
      case scala.util.Success(Success) ⇒
      case _ ⇒ assert(false)
    }
    Await.result(promiseIterator11.next().future, 3.seconds)
    // 알림재개로 인한 알림을 기다린다.
    val tmpPromise1 = promiseIterator11.next()
    assert(Try(Await.result(tmpPromise1.future, 7.seconds)).isFailure)
    val result2 = Await.result(tmpPromise1.future, 4.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    assert(result1.length == 1)
    // message 를 보내줄 것을 요청한다.
    requestResend(cookie)
    // 명시적으로 요청했으므로, 바로 답이 와야 한다.
    val result3 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(result3, cookie)
    logout(cookie)
    Await.result(promiseIterator11.next().future, Duration.Inf)
  }

  test("getAllLoginUsers") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    // cookie 가 있어야 목록을 구할 수 있다. 그래서 명령을 성공적으로 수행했다면 반드시 1개 이상의 결과를 구하게 된다.
    assert(getAllLoginUsers(cookie).length == 1)
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    assert(getAllLoginUsers(cookie).length == 2)
    logout(cookie2)
    assert(getAllLoginUsers(cookie).length == 1)
    logout(cookie)
  }

  test("no response for long time(5sec) when try login.") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password), 200, Some(5000))
    logout(cookie)
    login(CertificationModel(userModel.name, userModel.password), 500, Some(11000))
    // 위의 login 만 하고 끝내면 안된다. 위에서는 일부러 시간을 끌었는데, 이와 유사한 상황은 실제로도 어떤 이유로든지 일어날 수 있다.

    // 문제는, 이 검사의 경우 WorkerLoginActor 에서 실제 작업을 하고 있는 중에, 이에 대한 요청을 가장 처음으로 했던 LoginAuthStrategy 는
    // 시간초과를 이유로 응답을 기다리다가 실패한다. client 는 실패했다는 응답을 받게 되는데, server 는 성공하게 되는 것이다.
    // client 를 무한정 기다리게 할 수 없기 때문에, 10초로 정했는데, server 단계에서 10초 이상 소요되는 경우가 생긴다 하더라도
    // 이것이 server 단계에서 반드시 실패로 결론이 나는 것은 아니다.
    // 중간에 자료처리를 실패하게 만드는 것은 지금 현재로서는 가능하지 않다. 성공한 접속을 성공을 확인한 시점에 다시 끊어주는 방법밖에 없다.
    // 이것은 server 에서 자동으로 하지 말고, 사용자가 수동으로 하도록 하자.
    //  fixme 그래서 사용자는, 10초 시간초과로 접속이 실패하는 경우, server 에서는 접속이 성공했는지 다시 확인해야 하는 이상한 상황이 나타나게 된다.
    // 대책으로는 session timeout 등을 활용할 수 있다.

    // 한편, 이 검사에서는 server 에서는 성공한 접속을 끊기 위해 다시 한번 강제접속종료를 시도한다.

    // 이와 같이 정리를 해주지 않으면, package/project 단위 test 실행의 경우 모든 test 가 같은 jvm 에서 실행되는 특성으로 인해서,
    // 정리되지 않은 자원이 계속남아 있게 되고 이후에 충돌을 하게 되는 문제가 생긴다.
    // 구체적으로는 actor 가 문제된다. 매 test 마다 actor system 을 초기화하는 방법이 있었으면 좋겠는데,
    // 지금 현재 구성으로는 actor system 이 scala object 에서 한번만 만들어지도록 되어 있다.
    // 이와 같이 http server/client 검사를 하는 상황에서는 의존성 주입을 사용하기도 쉽지 않다.
    // client 인 이 test 가 매번 초기화할 actor system 을 server 가 어떻게 참조하도록 할 것인가.
    // client 는 실패한 것으로 응답을 받으면서 server 는 성공하게 되는 이와 같은 결론에 이르게 되는 구상에 문제가 있다.

    // 이 검사의 경우, 11초를 기다리도록 했으므로 10초 시간초과로 인해 이 test 가 실행을 재개하는 시점에서는 아직 WorkerLoginActor 가 작업을 수행중이다.
    // 따라서 여유있게 2초를 더 기다렸다가 강제접속종료를 시도한 뒤, 다시 접속종료를 해주는 식으로 자원정리를 해준다.
    Thread.sleep(2000)
    val cookie2 = login(CertificationModel(userModel.name, userModel.password, Some(true)))
    logout(cookie2)
  }
}
