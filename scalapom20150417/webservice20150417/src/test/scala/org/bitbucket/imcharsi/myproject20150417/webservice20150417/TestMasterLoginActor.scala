/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.CertificationModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModelEx
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor.{ LoginRequestCommand, MasterActorSystem, MasterLoginActor }
import org.scalatest.{ BeforeAndAfterAll, FunSuite }

import scala.concurrent.duration._
import scala.concurrent.{ Await, Promise }
import scala.util.Try

/**
 * Created by Kang Woo, Lee on 4/30/15.
 */
class TestMasterLoginActor extends FunSuite with BeforeAndAfterAll {
  implicit val timeout = Timeout(1.day)
  var userName1: Option[String] = None
  var userName2: Option[String] = None

  protected override def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createSchemeUtil()
    userName1 = EtcUtilTrait.generateUserName()
    userName2 = EtcUtilTrait.generateUserName()
    UserDao.postOne(UserModelEx(None, None, None, None, userName1, Some("bye")))
    UserDao.postOne(UserModelEx(None, None, None, None, userName2, Some("bye")))
  }

  protected override def afterAll(): Unit = {
    DriverSettings.dropSchemeUtil()
    super.afterAll()
  }

  test("try duplicated login") {
    // 아래와 같이, method 를 message 로서 전달하는 이런 용법으로 actor 를 써서는 안된다. 이는 test 를 목적으로 했다.
    // 이 test 의 요지는 다음과 같다.
    // 동일사용자 인증의 경우, 동일 actor 로 요청을 줄 세우는데, 이때 정말로 줄 세우기를 하는지 확인하는 것이 이 test 의 목적이다.
    //    val promiseResult1: Promise[UserModel] = Promise()
    //    val promiseResult2: Promise[UserModel] = Promise()
    //    val promiseResult3: Promise[UserModel] = Promise()
    val promise1: Promise[Unit] = Promise()
    val signal1: () ⇒ Unit = () ⇒ {
      // 첫번째 인증요청이 대기하도록 한다.
      Await.ready(promise1.future, Duration.Inf)
    }
    var testResult: Boolean = false
    val shouldBeRun: () ⇒ Unit = () ⇒ {
      // 첫번째 인증요청이 대기하는 중에 도착한 인증요청은 동일 actor 로 분배되어야 한다.
      testResult = true
    }
    val actorResponse1 = MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterLoginActor.actorName) ?
      LoginRequestCommand(CertificationModel(userName1, Some("bye")), None, None, Map((1, signal1)))
    val actorResponse2 = MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterLoginActor.actorName) ?
      LoginRequestCommand(CertificationModel(userName1, Some("bye")), None, None, Map((2, shouldBeRun)))
    val actorResponse3 = MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterLoginActor.actorName) ?
      LoginRequestCommand(CertificationModel(userName2, Some("bye")), None, None, Map.empty)
    val result3 = Try(Await.result(actorResponse3, Duration.Inf))
    // 첫번째 인증요청이 진행하도록 한다.
    promise1.success(())
    val result1 = Try(Await.result(actorResponse1, Duration.Inf))
    val result2 = Try(Await.result(actorResponse2, Duration.Inf))
    assert(result3.isSuccess)
    assert(result1.isSuccess)
    assert(result2.isFailure)
    assert(testResult)
  }
}
