/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import java.net.URI

import com.fasterxml.jackson.databind.DeserializationFeature
import io.backchat.hookup.HookupClient.Receive
import io.backchat.hookup._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ BanModel, InvitationModel, RoomAttenderModel, RoomModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.CertificationModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnterRoomModel, EnvelopeModel }
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.authentication.{ AutheticationSupport, LoginServlet }
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.room.current.RoomServletTrait
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.user.current.UserServletTrait
import org.json4s.jackson.JsonMethods._
import org.scalatra.test.scalatest.ScalatraFunSuite

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, Promise }

/**
 * Created by Kang Woo, Lee on 5/3/15.
 */
trait EtcUtilTrait extends ScalatraFunSuite {
  val headerAccept = Map(("Accept" -> "application/json"))
  val headerContentType = Map(("Content-Type" -> "application/json"))

  def requestComplete(envelopeModelIdList: List[EnvelopeModel[_]], cookie: Map[String, String]): Unit = {
    put(f"/${UserServletTrait.user}/${UserServletTrait.self}/${UserServletTrait.messageQueue}/${UserServletTrait.complete}",
      DefaultObjectMapper.writeValueAsString(envelopeModelIdList.flatMap(_.id)),
      cookie ++ headerContentType) {
        status.should(equal(200))
      }
  }

  def login(certificationModel: CertificationModel, expectedResult: Int = 200, waitTimeForTest: Option[Int] = None): Map[String, String] = {
    val waitTimeForTestHeader = waitTimeForTest.map(x ⇒ (AutheticationSupport.waitTimeForTest, x.toString)).toMap
    post(f"/${LoginServlet.login}", DefaultObjectMapper.writeValueAsString(certificationModel), headerContentType ++ waitTimeForTestHeader) {
      status.should(equal(expectedResult))
      Map(("Cookie" -> response.headers.apply("Set-Cookie").mkString(";")))
    }
  }

  def logout(cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    delete(f"/${LoginServlet.login}", Nil, cookie) {
      status.should(equal(expectedResult))
    }
  }

  def getSelfInfo(cookie: Map[String, String], expectedResult: Int = 200): Option[UserModel] = {
    get(f"/${UserServletTrait.user}/${UserServletTrait.self}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
      DefaultObjectMapper.readValue[Option[UserModel]](body)
    }
  }

  def getAllUsers(expectedResult: Int = 200): List[UserModel] = {
    get(f"/${UserServletTrait.user}", Nil, headerAccept) {
      status.should(equal(expectedResult))
      DefaultObjectMapper.readValue[List[UserModel]](body)
    }
  }

  def createRoom(roomModel: RoomModel, cookie: Map[String, String]): Option[Int] = {
    post(f"/${RoomServletTrait.room}", DefaultObjectMapper.writeValueAsString(roomModel), cookie ++ headerContentType) {
      status.should(equal(200))
      DefaultObjectMapper.readValue[Option[Int]](body)
    }
  }

  def enterRoom(roomId: Option[Int], cookie: Map[String, String], enterRoomModel: EnterRoomModel, expectedResult: Int = 200): Unit = {
    post(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.roomAttenders}", DefaultObjectMapper.writeValueAsString(enterRoomModel), cookie) {
      status.should(equal(expectedResult))
    }
  }

  def invite(roomId: Option[Int], invitationModel: InvitationModel, cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    post(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.invitation}", DefaultObjectMapper.writeValueAsString(invitationModel), headerContentType ++ cookie) {
      status.should(equal(expectedResult))
    }
  }

  def leaveRoom(roomId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    delete(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.roomAttenders}/${UserServletTrait.self}", Nil, cookie) {
      status.should(equal(expectedResult))
    }
  }

  def clearUserBan(roomId: Option[Int], userId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    delete(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.ban}/${userId.get}", Nil, cookie) {
      status.should(equal(expectedResult))
    }
  }

  def setRoomPassword(roomId: Option[Int], password: Option[String], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    put(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.password}", DefaultObjectMapper.writeValueAsString(password), cookie ++ headerContentType) {
      status.should(equal(expectedResult))
    }
  }

  def getAllInvitation(cookie: Map[String, String], expectedResult: Int = 200): List[InvitationModel] = {
    get(f"/${UserServletTrait.user}/${UserServletTrait.self}/${UserServletTrait.invitaion}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
      DefaultObjectMapper.readValue[List[InvitationModel]](body)
    }
  }

  def setCurrentNick(nick: Option[String], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    put(f"/${UserServletTrait.user}/${UserServletTrait.self}/${UserServletTrait.nick}", DefaultObjectMapper.writeValueAsString(nick), cookie ++ headerContentType) {
      status.should(equal(expectedResult))
    }
  }

  def setRoomName(roomId: Option[Int], name: Option[String], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    put(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.name}", DefaultObjectMapper.writeValueAsString(name), cookie ++ headerContentType) {
      status.should(equal(expectedResult))
    }
  }

  def getBanList(roomId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): List[BanModel] = {
    get(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.ban}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
      DefaultObjectMapper.readValue[List[BanModel]](body)
    }
  }

  def leaveRoomByWho(roomId: Option[Int], byWhoId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    delete(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.roomAttenders}/${byWhoId.get}", Nil, cookie) {
      status.should(equal(expectedResult))
    }
  }

  def talk(roomId: Option[Int], talk: Option[String], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    post(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.talk}", DefaultObjectMapper.writeValueAsString((new TalkHistoryModel).copy(talk = talk)), headerContentType ++ cookie) {
      status.should(equal(expectedResult))
    }
  }

  def revokeOwnerPermission(roomId: Option[Int], candidateUserId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    put(f"/${RoomServletTrait.room}/${roomId.get}", DefaultObjectMapper.writeValueAsString((new RoomModel()).copy(currentDetail = Some(new RoomHistoryModel().copy(userId = candidateUserId)))), headerContentType ++ cookie) {
      status.should(equal(expectedResult))
    }
  }

  def grantOperatorPermission(roomId: Option[Int], userId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    post(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.operator}", DefaultObjectMapper.writeValueAsString(userId), headerContentType ++ cookie) {
      status.should(equal(expectedResult))
    }
  }

  def revokeOperatorPermission(roomId: Option[Int], operatorUserId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    delete(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.operator}/${operatorUserId.get}", Nil, cookie) {
      status.should(equal(expectedResult))
    }
  }

  def revokeOperatorPermissionSelf(roomId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    delete(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.operator}/${UserServletTrait.self}", Nil, cookie) {
      status.should(equal(expectedResult))
    }
  }

  def verifyWebSocketClientConnection(client: HookupClient): Unit = {
    Await.result(client.connect(), Duration.Inf) match {
      case Success ⇒
      //      case Some(scala.util.Success(Success)) ⇒
      case _ ⇒ assert(false)
    }
  }

  def verityWebSocketAlive(client: HookupClient, expected: Boolean = true): Unit = {
    assert(client.isConnected == expected)
  }

  def sayHi(client: HookupClient, expected: Boolean = true): Unit = {
    client.send("hi")
  }

  def createPromiseList(count: Int): List[Promise[Any]] = {
    def createPromise(): Promise[Any] = Promise[Any]()
    def promiseStream(): Stream[Promise[Any]] = createPromise() #:: promiseStream()
    promiseStream().take(count).toList
  }

  def echo(cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    get(f"/${UserServletTrait.user}/${UserServletTrait.echo}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
    }
  }

  def shutdown(expectedResult: Int = 200): Unit = {
    delete(f"/${AdminServlet.admin}/${AdminServlet.logoutForce}") {
      status.should(equal(expectedResult))
    }
  }

  def pushSampleMessage(cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    post(f"/${UserServletTrait.user}/${UserServletTrait.self}/${UserServletTrait.messageQueue}/${UserServletTrait.pushSampleMessage}", Nil, cookie) {
      status.should(equal(expectedResult))
    }
  }

  def requestResend(cookie: Map[String, String], expectedResult: Int = 200): Unit = {
    put(f"/${UserServletTrait.user}/${UserServletTrait.self}/${UserServletTrait.messageQueue}/${UserServletTrait.request}", Nil, cookie) {
      status.should(equal(expectedResult))
    }
  }

  def getAllLoginUsers(cookie: Map[String, String], expectedResult: Int = 200): List[UserModel] = {
    get(f"/${UserServletTrait.user}/${UserServletTrait.allLoginUsers}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
      DefaultObjectMapper.readValue[List[UserModel]](body)
    }
  }

  def getAllCreatedRooms(cookie: Map[String, String], expectedResult: Int = 200): List[RoomModel] = {
    get(f"/${RoomServletTrait.room}/${RoomServletTrait.allCreatedRooms}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
      DefaultObjectMapper.readValue[List[RoomModel]](body)
    }
  }

  def getRoom(roomId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Option[RoomModel] = {
    get(f"/${RoomServletTrait.room}/${roomId.get}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
      DefaultObjectMapper.readValue[Option[RoomModel]](body)
    }
  }

  def getSelfRoomAttenderModel(roomId: Option[Int], cookie: Map[String, String], expectedResult: Int = 200): Option[RoomAttenderModel] = {
    get(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.roomAttenders}/${UserServletTrait.self}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
      if (status == 200) {
        DefaultObjectMapper.readValue[Option[RoomAttenderModel]](body)
      } else {
        None
      }
    }
  }

  def getAllRoomAttenders(cookie: Map[String, String], expectedResult: Int = 200): List[RoomAttenderModel] = {
    get(f"/${UserServletTrait.user}/${UserServletTrait.self}/${UserServletTrait.allRoomAttenders}", Nil, cookie ++ headerAccept) {
      status.should(equal(expectedResult))
      DefaultObjectMapper.readValue[List[RoomAttenderModel]](body)
    }
  }

  def registerUser(name: Option[String], password: Option[String], expectedResult: Int = 200): Option[Int] = {
    post(f"/${UserServletTrait.user}", DefaultObjectMapper.writeValueAsString(new UserModel().copy(name = name, password = password)), headerContentType) {
      status.should(equal(expectedResult))
      if (expectedResult == 200) {
        DefaultObjectMapper.readValue[Option[Int]](body)
      } else {
        None
      }
    }
  }

  class SampleWebSocketClient(promiseIterator: Iterator[Promise[Any]], cookie: Map[String, String]) extends DefaultHookupClient(HookupClientConfig(new URI(f"ws://localhost:8080/${UserServletTrait.user}/${UserServletTrait.self}/${UserServletTrait.ws}"), initialHeaders = cookie)) {
    mapper.configure(DeserializationFeature.USE_BIG_INTEGER_FOR_INTS, false)

    override def receive: Receive = {
      case Connected ⇒
        // client 에서는 connected 에 이어 바로 send 가 된다. 몰랐던 사실인데, connected 이후에 뭐라도 오고가야 나머지가 되는듯 하다.
        promiseIterator.next().success(())
      case Disconnected(x) ⇒
        promiseIterator.next().success(())
      case JsonMessage(x) ⇒
        // client 에서도 이와 같은 방식으로 자료를 받아야 한다.

        // fixme 한가지 문제가 있다. 이것은 여기서는 해결할 수 없고, json4s 단계에서 해결해야 한다. 정수를 big integer 로 해석한다.
        // 관련 항목은, org.json4s.jackson.JsonMethods.mapper 의 초기화 단계 중, org.json4s.jackson.Json4sScalaModule 의 org.json4s.jackson.JValueDeserializer 를 등록하는 곳이다.
        // 여기서, json4s 용 node 인 JInt 를 만들면서, big integer 로 해석하고 있다. jackson 설정 중, DeserializationFeature.USE_BIG_INTEGER_FOR_INTS 를 인식하도록 해야 하는 것이다.
        // 실제로, 부동소수에 관해서는 DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS 를 인식하고 있다.
        // 그보다는, json4s 의 기능 중 하나인 JInt 가 big integer 를 인자로 받도록 쓰여져 있다. JBigInteger 와 JInt 라고 나누어져 있지 않다.
        // 그래서 해결방법은, deserialize 한 것을 다시 DefaultObjectMapper 로 serialize 했다가 또 다시 DefaultObjectMapper 로 deserialize 하는 것이다.
        // 어차피 test 이기 때문에 문제될 것 없고, android 에서는 json4s 를 쓰지 않을 것이기 때문에 역시 문제될 것 없다.
        // 아무래도, json4s 는 deserialize 단계에서 scala reflection 을 사용해서 BigInt 와 Int 를 구분해서 다루려고 했기 때문에
        // 따로 integer 와 big integer 를 위한 JValue 를 구분해서 만들지 않았던것 같다.

        // json4s 가 만든 JValue tree 를 jackson 의 JsonNode 로 바로 바꾸면 JInt 가 BigIntegerNode 로 바뀌는 문제를 풀기 위해서 낭비를 했다.
        // 4. jackson 이 읽은 json 을 변환한다.
        promiseIterator.next().success(DefaultObjectMapper.deserializeEnvelopeList(
          // 3. 이번에는 json4s 가 아니라 jackson 이 직접 json 을 읽는다.
          DefaultObjectMapper.readTree(
            // 2. 다시 json 으로 바꾼다.
            DefaultObjectMapper.writeValueAsString(
              // 1. big integer JValue JInt 를 BigIntegerNode 로 바꿔서 변환을 시도한다.
              DefaultObjectMapper.deserializeEnvelopeList(asJsonNode(x))))))
      case TextMessage(x) ⇒
        promiseIterator.next().success(x)
    }
  }

}

object EtcUtilTrait {
  private var counter = 1

  def generateUserName(): Option[String] = {
    val result = Some(counter.toString)
    counter += 1
    result
  }
}
