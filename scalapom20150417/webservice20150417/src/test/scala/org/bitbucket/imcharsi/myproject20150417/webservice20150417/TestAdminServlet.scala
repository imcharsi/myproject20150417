/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.CertificationModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModelEx
import org.scalatest.BeforeAndAfterAll
import org.scalatra.test.scalatest.ScalatraFunSuite

/**
 * Created by Kang Woo, Lee on 5/27/15.
 */
class TestAdminServlet extends ScalatraFunSuite with EmbeddedJettyTrait with BeforeAndAfterAll with EtcUtilTrait with DateMapperTrait {
  protected override def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createSchemeUtil()
    Thread.sleep(3500)
  }

  protected override def afterAll(): Unit = {
    DriverSettings.dropSchemeUtil()
    super.afterAll()
  }

  test("login/logout") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    echo(cookie)
    echo(cookie2)
    assert(getSelfInfo(cookie).isDefined)
    assert(getSelfInfo(cookie2).isDefined)
    shutdown()
    logout(cookie, 401)
    logout(cookie2, 401)
  }
}
