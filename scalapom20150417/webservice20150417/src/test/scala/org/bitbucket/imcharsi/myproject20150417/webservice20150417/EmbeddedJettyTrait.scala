/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.webapp.WebAppContext
import org.scalatra.test.scalatest.ScalatraFunSuite

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
trait EmbeddedJettyTrait extends ScalatraFunSuite {
  private var _server: Server = null
  // 아래와 같이 해야, scalatra test 기본 기능인 get/post/put/delete 도 같이 쓸 수 있다.
  override lazy val server = _server

  override def start(): Unit = {
    //    super.start()
    // scalatra 기본 server 는 사용하지 않는다.
    // 또한 이와 같이 하면, test 에서 개별적으로 servlet 을 설정할수 있었던 기능을 쓸 수 없게 된다.
    // 결론은, web.xml 에 설정한 그대로 실행된다.
    // 그냥, 각각의 test 가 모든 servlet 을 전부 읽도록 하자.
    _server = new Server(8080)
    val context = new WebAppContext()
    context.setContextPath("/")
    //    http://stackoverflow.com/questions/9997292/how-to-read-environment-variables-in-scala
    // intellij test runner 로 실행할 때와 maven test runner 로 실행할 때 work directory 가 달라진다. 따라서 아래와 같이 조정해야 한다.
    // 환경변수가 없는 경우가 maven test runner 로 실행할 때의 경우이다.
    scala.util.Properties.envOrNone("resourceBasePrefix").orElse(Some("")).map(_ + "src/main/webapp").foreach(context.setResourceBase(_))
    _server.setHandler(context)
    _server.start()
  }

  override def stop(): Unit = {
    _server.stop()
    _server.join()
    // scalatra 기본 server 는 사용하지 않는다.
    //    super.stop()
  }
}
