/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import java.util.Calendar

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current.{ InvitationDao, RoomDao }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history.RoomInOutHistoryDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.RoomAttenderTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModel.EnvelopeDataType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.{ RoomHistoryModelEx, RoomTimelineModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.CertificationModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.UserNickHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnterRoomModel, EnvelopeModel, LogoutModel }
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.room.current.RoomServletTrait
import org.scalatest.BeforeAndAfterAll
import org.scalatra.test.scalatest.ScalatraFunSuite

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.util.Try

/**
 * Created by Kang Woo, Lee on 4/30/15.
 */
class TestRoomServlet extends ScalatraFunSuite with EmbeddedJettyTrait with BeforeAndAfterAll with EtcUtilTrait with DateMapperTrait {
  protected override def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createSchemeUtil()
    // 왜 이와 같이 기다려줘야 하는가. 바뀐 logoutForce 절차에 따라, 종료를 시도했어도 그 처리가 즉시 나타나지 않는다.
    // package/project 단위 test 를 수행하면 동일 jvm 에 모든 test 가 수행되므로,
    // 앞에서 미처 완료되지 않은 logout 작업이 있다면 뒤의 test 에 영향을 주게 된다. 완료하는 데 필요한 충분한 시간을 기다렸다가 test 를 수행한다.
    // 자발적인 logout 은 session 을 무효화하는 방식으로 한다. session 이 무효화되는 것을 servlet 단계에서는 기다리지 않으므로,
    // 자발적인 logout 의 경우에는 logoutForce 의 완료를 기다릴 수 없다.
    Thread.sleep(3500)
  }

  protected override def afterAll(): Unit = {
    DriverSettings.dropSchemeUtil()
    super.afterAll()
  }

  // room 은 http method 의미를 약간 다르게 쓴다. 기본 crud 검사는 하지 않는다.

  test("create room through http post") {
    val promiseList1 = createPromiseList(10)
    val promiseList2 = createPromiseList(10)
    val promiseIterator11 = promiseList1.iterator
    val promiseIterator12 = promiseList1.iterator
    val promiseIterator21 = promiseList2.iterator
    val promiseIterator22 = promiseList2.iterator
    // 편의로, 직접 만든다.
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye2")))).get
    val emptyEnterRoomModel = EnterRoomModel(None, None)
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val roomId = createRoom(roomModel, cookie)
    // 대화방 개설에 따른 message 응답이 와야 한다.
    val result11 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    // 응답을 해야 한다. 아니면 다음 message 가 쌓여도 알림이 바로 오지 않는다.
    requestComplete(result11, cookie)

    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    // 여기서 clearPrivacyData 의 기능여부를 단순하게 검사한다. clearPrivacyData 가 전체적으로 동작한다는 것은 model 단계에서 검사했으므로,
    // 여기서는 room actor 가 clearPrivacyData 를 사용했는지 여부만 검사한다. 결론은, 반환자료 중 user model 의 암호가 비었는지 살펴보는 것이다.
    val envelopeModelList = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    assert(envelopeModelList.headOption.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).flatMap(_.data).flatMap(_.roomInOutHistory).flatMap(_.user).flatMap(_.password).isEmpty)
    assert(envelopeModelList.headOption.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).flatMap(_.data).flatMap(_.roomInOutHistory).flatMap(_.user).flatMap(_.id) == userModel2.id)
    requestComplete(envelopeModelList, cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // room attender model 의 방장여부, 운영자여부가 잘 계산되는지 확인한다.
    assert(getSelfRoomAttenderModel(roomId, cookie).flatMap(_.currentOwnerFlag) == Some(true))
    assert(getSelfRoomAttenderModel(roomId, cookie2).flatMap(_.currentOwnerFlag) == Some(false))

    get(f"/${RoomServletTrait.room}/${roomId.get}/${RoomServletTrait.roomAttenders}", Nil, cookie2 ++ headerAccept) {
      DefaultObjectMapper.readValue[List[RoomAttenderModel]](body).length.should(equal(2))
      status.should(equal(200))
    }
    // 중복 입장.
    enterRoom(roomId, cookie2, emptyEnterRoomModel, 500)
    // 퇴장.
    leaveRoom(roomId, cookie)
    // 앞의 퇴장은 성공적인 퇴장으로 예상되므로, 알림을 기다린다.
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    get(f"/${RoomServletTrait.room}/${roomId.get}", Nil, cookie2 ++ headerAccept) {
      val x = DefaultObjectMapper.readValue[Option[RoomModel]](body)
      assert(x.flatMap(_.currentDetail).flatMap(_.userId) == userModel2.id)
      status.should(equal(200))
    }
    // 중복 퇴장.
    leaveRoom(roomId, cookie, 500)
    // 성공적인 퇴장.
    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    // 아래는 시점이 약간 복잡하다. 설명을 자세히 읽어야 한다.
    // 퇴장에 관한 http 요청의 결과가 오는 시점은
    // 우선, client 가 보낸 http 요청을 room servlet 이 받고 다시 room servlet 이 room actor 에게 퇴장 요청 message 를 보내면
    // room actor 가 이 요청을 처리한 후 응답을 다시 되돌려보내고 그 응답을 servlet 이 받아서 다시 client 에게 http 응답을 한것을 client 가 받은 시점이다.
    // 한편, 위에서 room actor 가 응답 message 를 보낸 시점부터 시작해서 방폐쇄가 완료되기 전까지의 사이 시간에 아래의 자료구하기 요청이 수행될 수 있는 것이다.
    // 응답 message 를 받아서 http client 가 그 응답을 받게 되기까지 걸리는 시간이 방폐쇄를 완료하는 데 걸리는 시간보다 짧을수도 있는 것이다.
    // 요약하면, client 가 방정보 구하기를 수행하는 시점이 방폐쇄가 완료되는 시점보다 빠를 수 있는 것이다.
    // 그래서 client 가 아래의 자료구하기를 수행할때 방 안에 아무도 없는데 아직 방은 폐쇄되지 않은 상태를 보게 될 수 있는 것이다.
    // 이 검사에서는 1초의 시간이면 room actor 가 방폐쇄까지 전부 완료하기에 충분하다고 본다.
    Thread.sleep(1000)
    get(f"/${RoomServletTrait.room}/${roomId.get}", Nil, cookie2 ++ headerAccept) {
      val x = DefaultObjectMapper.readValue[Option[RoomModel]](body)
      assert(x.flatMap(_.currentDetail).flatMap(_.userId) == None)
      assert(x.flatMap(_.closedDateTime).isDefined)
      status.should(equal(200))
    }
    // 폐쇄된 방에 입장을 시도한다. 실패해야 한다.
    enterRoom(roomId, cookie, emptyEnterRoomModel, 500)
    logout(cookie)
    logout(cookie2)
    // 바뀐 구현에서는 logout 에 대한 응답은 바로 올 수 있어도, web socket 연결 종료는 바로 하지 않는다.
    // 따라서, web socket 연결이 종료되었는지 여부를 확인하기 위해서는 약간의 시간을 기다려줘야 한다.
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
  }

  test("invitation") {
    val promiseList1 = createPromiseList(20)
    val promiseList2 = createPromiseList(20)
    val promiseList3 = createPromiseList(20)
    val promiseIterator11 = promiseList1.iterator
    val promiseIterator12 = promiseList1.iterator
    val promiseIterator21 = promiseList2.iterator
    val promiseIterator22 = promiseList2.iterator
    val promiseIterator31 = promiseList3.iterator
    val promiseIterator32 = promiseList3.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)
    // 방장이 될 사용자 하나와, 운영자가 아니면서 초대를 수행할 사용자 하나, 초대를 받을 사용자 하나를 만든다.
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye2")))).get
    val userModel3 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye2")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val cookie3 = login(CertificationModel(userModel3.name, userModel3.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    val client3 = new SampleWebSocketClient(promiseIterator32, cookie3)
    verifyWebSocketClientConnection(client3)
    Await.result(promiseIterator31.next().future, 3.seconds)
    sayHi(client3)
    Await.result(promiseIterator31.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), Some("password"), None)))
    val roomId = createRoom(roomModel, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    // 암호 있는 방에 암호 없이 입장을 시도한다.
    enterRoom(roomId, cookie2, emptyEnterRoomModel, 500)

    // 암호를 사용하여 입장을 시도한다.
    enterRoom(roomId, cookie2, EnterRoomModel(Some("password"), None))
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 방장도 아니고 운영자도 아니면서 초대를 시도한다. 실패해야 한다.
    invite(roomId, (new InvitationModel).copy(userId = userModel3.id), cookie2, 500)

    invite(roomId, (new InvitationModel).copy(userId = userModel3.id), cookie)
    val invitation = Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    // 초대를 받는 사람만 web socket 으로 알림을 받는다.
    requestComplete(invitation, cookie3)
    val invitationId = invitation.head.data.map(_.asInstanceOf[InvitationModel]).flatMap(_.id)

    // 이 사용자의 초대장 목록에 초대장이 있어야 한다.
    assert(getAllInvitation(cookie3).map(_.id) == List(invitationId))

    // 틀린 암호와 유효한 초대장을 사용하여 입장을 시도한다.
    enterRoom(roomId, cookie3, EnterRoomModel(Some("incorrect"), invitationId))
    // 초대장을 사용했을 경우, 초대장을 사용했다는 표시가 남아야 한다. 이는 RoomInOutHistoryModel 의 invidationId 로 확인한다.
    // 모든 사용자가 동일한 message 를 받으므로 하나만 확인하면 된다.
    val result1 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    result1.headOption.flatMap(_.data).
      map(_.asInstanceOf[RoomTimelineModel]).
      map(_.roomInOutHistoryId).
      flatMap(RoomInOutHistoryDao.getOne(_)).
      foreach(x ⇒ x.invitationId == invitationId)
    requestComplete(result1, cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    // 이 사용자의 초대장 목록에 초대장이 없어야 한다.
    assert(getAllInvitation(cookie3).map(_.id).isEmpty)

    // 시간이 만료된 초대장으로 입장을 시도하기에 앞서 퇴장한다.
    leaveRoom(roomId, cookie3)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    // 다시 초대한다.
    invite(roomId, (new InvitationModel).copy(userId = userModel3.id), cookie)
    val invitation2 = Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(invitation2, cookie3)
    val invitationId2 = invitation2.head.data.map(_.asInstanceOf[InvitationModel]).flatMap(_.id)

    // 시간만료상황을 만들기 위해 자료를 조작한다.
    val now = DriverSettings.now()
    val calendar = Calendar.getInstance()
    calendar.setTime(now.get)
    calendar.add(Calendar.SECOND, -1)
    val newNow = calendar.getTime
    InvitationDao.getOne(invitationId2).map(_.copy(expirationDateTime = Some(newNow), invitationDateTime = Some(newNow))).map(InvitationDao.putOne(_))

    // 시간이 만료된 초대장을 사용하여 입장을 시도한다.
    enterRoom(roomId, cookie3, EnterRoomModel(Some("incorrect"), invitationId2), 500)

    // 운영자는 초대할 수 있음을 확인한다.
    // 먼저 운영자권한을 수여한다.
    grantOperatorPermission(roomId, userModel2.id, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    // 운영자가 초대한다.
    invite(roomId, (new InvitationModel).copy(userId = userModel3.id), cookie2)
    val invitation3 = Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(invitation3, cookie3)
    val invitationId3 = invitation3.head.data.map(_.asInstanceOf[InvitationModel]).flatMap(_.id)
    // 운영자가 보낸 초대장을 사용하여 입장을 시도한다.
    enterRoom(roomId, cookie3, EnterRoomModel(Some("incorrect"), invitationId3))
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    // 퇴장한다.
    leaveRoom(roomId, cookie2)
    val envelopeModelList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    val envelopeModelList2: List[EnvelopeModel[_]] = Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    val envelopeModelList3: List[EnvelopeModel[_]] = Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    // 퇴장에 관한 room in/out history 가 한번씩만 전달되는지 확인한다.
    // 퇴장자 자신을 제외한 나머지 참가자에 대해서는 퇴장에 관한 room in/out history 가 두번 전달되는 문제가 있었다.
    // 문제는, 퇴장자 자신은 퇴장에 관한 room in/out history 만 받도록 하고, 나머지 참가자는 모든 message 를 받도록 하려는 의도에 있었다.
    // 그냥, 퇴장자를 포함한 모두가 모든 message 를 똑같이 받도록 했다. 아래는, 어쨌든 중복이 없음을 확인하는 검사다.
    assert(envelopeModelList1.flatMap(_.data).map(_.asInstanceOf[RoomTimelineModel]).filter(_.roomInOutHistoryId.isDefined).length == 1)
    assert(envelopeModelList2.flatMap(_.data).map(_.asInstanceOf[RoomTimelineModel]).filter(_.roomInOutHistoryId.isDefined).length == 1)
    assert(envelopeModelList3.flatMap(_.data).map(_.asInstanceOf[RoomTimelineModel]).filter(_.roomInOutHistoryId.isDefined).length == 1)
    requestComplete(envelopeModelList1, cookie)
    requestComplete(envelopeModelList2, cookie2)
    requestComplete(envelopeModelList3, cookie3)

    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    leaveRoom(roomId, cookie3)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    logout(cookie)
    logout(cookie2)
    logout(cookie3)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
    Await.result(promiseIterator31.next().future, Duration.Inf)
  }

  test("talk/change nick") {
    val x1 = createPromiseList(15)
    val x2 = createPromiseList(15)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator
    val promiseIterator21 = x2.iterator
    val promiseIterator22 = x2.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye2")))).get

    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val roomId = createRoom(roomModel, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    talk(roomId, Some("talk 1"), cookie)
    val envelopeModelIdList = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelIdList, cookie)
    assert {
      envelopeModelIdList.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).
        flatMap(_.data).
        filter(_.talkHistory.isDefined).
        flatMap(_.talkHistory).flatMap(_.nick).flatMap(_.nick).isEmpty
    }

    setCurrentNick(Some("nick#1"), cookie)
    // user nick history 와 room timeline 이 따로 온다. 각각 확인해줘야 한다.
    val envelopeModelIdList2 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelIdList2, cookie)
    assert {
      envelopeModelIdList2.filter(_.dataType == Some(EnvelopeDataType.UserNickHistoryModelType)).
        map(_.asInstanceOf[EnvelopeModel[UserNickHistoryModel]]).
        flatMap(_.data).map(_.nick) == List(Some("nick#1"))
    }
    val envelopeModelIdList22 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelIdList22, cookie)
    assert {
      envelopeModelIdList22.filter(_.dataType == Some(EnvelopeDataType.RoomTimelineModelType)).
        map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).
        flatMap(_.data).flatMap(_.userNickHistory).map(_.nick) == List(Some("nick#1"))
    }

    talk(roomId, Some("talk 1"), cookie)
    val envelopeModelIdList3 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelIdList3, cookie)
    assert {
      envelopeModelIdList3.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).
        flatMap(_.data).
        filter(_.talkHistory.isDefined).
        flatMap(_.talkHistory).flatMap(_.nick).map(_.nick) == List(Some("nick#1"))
    }

    talk(roomId, Some("talk 2"), cookie2, 500)

    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    talk(roomId, Some("talk 3"), cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    setCurrentNick(Some("nick#22"), cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    val envelopeModelIdList4 = Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelIdList4, cookie2)

    // userModel 사용자가 대화명을 바꿨고, 같은 대화방에 있던 userModel2 사용자는 userModel 사용자의 대화명이 바뀌었다는 message 를 받았다.
    assert {
      envelopeModelIdList4.filter(_.dataType == Some(EnvelopeDataType.RoomTimelineModelType)).
        map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).
        flatMap(_.data).flatMap(_.userNickHistory).map(_.nick) == List(Some("nick#22"))
    }

    talk(roomId, Some("talk 4"), cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    logout(cookie2)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
  }

  test("getAllCreatedRooms") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    assert(getAllCreatedRooms(cookie).isEmpty)
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #2"), None, None)))
    val roomId = createRoom(roomModel, cookie)
    assert(getAllCreatedRooms(cookie).length == 1)
    assert(getSelfRoomAttenderModel(roomId, cookie).isDefined)
    val roomModel2 = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #3"), None, None)))
    val roomId2 = createRoom(roomModel2, cookie)
    assert(getAllCreatedRooms(cookie).length == 2)
    leaveRoom(roomId2, cookie)
    // 첫번째 test 인 create room ... test 와 동일하다. 약간 기다려줘야 한다.
    Thread.sleep(500)
    assert(getAllCreatedRooms(cookie).length == 1)
    leaveRoom(roomId, cookie)
    Thread.sleep(500)
    assert(getAllCreatedRooms(cookie).isEmpty)
    assert(getSelfRoomAttenderModel(roomId, cookie, 404).isEmpty)
    logout(cookie)
  }

  test("getAllCreatedRooms with userId") {
    val x1 = createPromiseList(15)
    val x2 = createPromiseList(15)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator
    val promiseIterator21 = x2.iterator
    val promiseIterator22 = x2.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #5"), None, None)))
    val roomId = createRoom(roomModel, cookie)
    val resultList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList1, cookie)

    assert(getAllCreatedRooms(cookie).flatMap(_.currentAttendingRoom).filter(_ == true).length == 1)
    assert(getAllCreatedRooms(cookie2).flatMap(_.currentAttendingRoom).filter(_ == true).isEmpty)

    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    assert(getAllCreatedRooms(cookie2).flatMap(_.currentAttendingRoom).filter(_ == true).length == 1)

    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    logout(cookie2)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
    /*
//        val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
//        val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
//        val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
//        val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
//          case Some((a, _)) ⇒ Some(a)
//        }
//        assert(RoomDao.getAllCreatedRooms(userModel.id).flatMap(_.currentAttendingRoom).filter(_ == true).length == 1)
//        assert(RoomDao.getAllCreatedRooms(userModel2.id).flatMap(_.currentAttendingRoom).filter(_ == true).isEmpty)

//        RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
//        assert(RoomDao.getAllCreatedRooms(userModel2.id).flatMap(_.currentAttendingRoom).filter(_ == true).length == 1)

        RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
        RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
        RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
        */
  }

  test("logout with leaving every room/retrieveRoomsThatUserAttended") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    assert(getAllRoomAttenders(cookie).isEmpty)
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #3"), None, None)))
    val roomId = createRoom(roomModel, cookie)
    val roomModel2 = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #4"), None, None)))
    val roomId2 = createRoom(roomModel, cookie)
    assert(getAllRoomAttenders(cookie).length == 2)
    logout(cookie)
    Thread.sleep(1000)
    assert(RoomDao.getOne(roomId).flatMap(_.closedDateTime).isDefined)
    assert(RoomDao.getOne(roomId2).flatMap(_.closedDateTime).isDefined)
  }

  test("logoutForce without leave room and verify no remaining room.") {
    val x1 = createPromiseList(10)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    // login
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    // web socket 연결
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #4"), None, None)))
    // 대화방 개설
    val roomId = createRoom(roomModel, cookie)
    val resultList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList1, cookie)
    talk(roomId, Some("talk 1"), cookie)
    val resultList2: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList2, cookie)
    // 강제접속종료로 재접속
    val cookie2Future: Future[Map[String, String]] = Future(login(CertificationModel(userModel.name, userModel.password, Some(true))))
    // web socket 연결 종료 대기. 강제접속종료로 인해 대화방에서 퇴장되었다는 것을 message 를 통해 확인해야 한다.
    val webSocketFuture: Future[Try[Unit]] = Future {
      // auth map 을 최대한 늦게 지우기로 했다. auth map 이 지워지지 않고 있으면 인증이 성공하므로 따라서 인증을 바탕으로 하는 완료응답은 제한 시간이 지나기 전에는 성공해야 한다.
      // 위의 강제접속종료 시도는 3초의 시간지연이 있다. 강제접속종료를 시작하고나서 3초가 지난뒤에 web socket 의 응답을 확인하기에는
      // 이 검사부분의 의도와 맞지 않다. 그래서 강제접속종료 시도를 시작하고나서 바로 web socket 의 응답을 기다린다.
      // 사실상 두개의 작업이 동시에 수행된다.
      // 관련된 내용과 그에 대한 설명은 user actor 에 있다.
      Try {
        // logout message 는 빨리 온다.
        val resultList3: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
        requestComplete(resultList3, cookie)
        assert(resultList3.flatMap(_.data).
          filter(_.isInstanceOf[LogoutModel]).
          map(_.asInstanceOf[LogoutModel]).map(_.userId) == List(userModel.id))
        // 퇴장에 관한 message 는 늦게 온다.
        val resultList4: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
        requestComplete(resultList4, cookie)
        assert(resultList4.flatMap(_.data).
          filter(_.isInstanceOf[RoomTimelineModel]).
          map(_.asInstanceOf[RoomTimelineModel]).
          map(_.roomInOutHistory).
          exists(x ⇒ x.flatMap(_.userId) == userModel.id && x.flatMap(_.enterType) == Some(EnterType.Leave)))
      }
    }
    val cookie2 = Await.result(cookie2Future, 4.seconds)
    assert(Await.result(webSocketFuture, 4.seconds).isSuccess)
    // 이전의 cookie 는 무효가 됐음을 확인.
    logout(cookie, 401)
    // 대화방이 폐쇄되었음을 확인하기 위해서는 약간의 시간을 기다려줘야 한다. 위에 있는 create room through http post 에 설명을 썼다.
    Thread.sleep(500)
    val resultRoomModel: Option[RoomModel] = RoomDao.getOne(roomId)
    // 대화방이 폐쇄되었음을 확인.
    assert(resultRoomModel.flatMap(_.closedDateTime).isDefined)
    val action: DBIOAction[Seq[RoomAttenderModel], NoStream, Effect.Read] = RoomAttenderTable.filter(t ⇒ t.roomId === resultRoomModel.flatMap(_.id)).result
    val future = DriverSettings.database.run(action)
    val result: Seq[RoomAttenderModel] = Await.result(future, Duration.Inf)
    // 대화방에 참석중인 사람이 없음을 확인.
    assert(result.isEmpty)
    logout(cookie2)
  }

  test("leaveRoom and byWhoId by Owner") {
    val x1 = createPromiseList(15)
    val x2 = createPromiseList(15)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator
    val promiseIterator21 = x2.iterator
    val promiseIterator22 = x2.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #5"), None, None)))
    // 대화방 개설
    val roomId = createRoom(roomModel, cookie)
    val resultList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList1, cookie)

    // 대화방 목록을 조회할 때, 입장금지된 대화방이 아닌 것으로 나타나야 한다.
    assert(getAllCreatedRooms(cookie2).filter(_.id === roomId).map(_.currentBannedRoom) == List(Some(false)))

    // 추가로 입장한다.
    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 자발적인 퇴장이다.
    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 입장금지자 목록이 비어있어야 한다.
    assert(getBanList(roomId, cookie).isEmpty)

    // 이번에는 강제퇴장을 시험하기 위해 다시 입장한다.
    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 자기자신에 대한 강제퇴장은 할 수 없다.
    leaveRoomByWho(roomId, userModel2.id, cookie2, 500)
    // 방장권한이 있어도 자기자신은 강제퇴장시킬 수 없다.
    leaveRoomByWho(roomId, userModel.id, cookie, 500)
    // 권한이 없는 사람은 다른사람을 강제퇴장시킬 수 없다.
    leaveRoomByWho(roomId, userModel.id, cookie2, 500)
    // 방장에 의해 강제퇴장된다.
    leaveRoomByWho(roomId, userModel2.id, cookie)
    val envelopeModelList = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    val envelopeModelList2 = Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelList, cookie)
    requestComplete(envelopeModelList2, cookie2)
    // 강제퇴장인지 확인한다.
    assert(envelopeModelList.flatMap(_.data).map(_.asInstanceOf[RoomTimelineModel]).filter(_.banHistoryId.isDefined).isEmpty == false)
    assert(envelopeModelList2.flatMap(_.data).map(_.asInstanceOf[RoomTimelineModel]).filter(_.banHistoryId.isDefined).isEmpty == false)

    // 대화방 목록을 조회할 때, 입장금지된 대화방이 아닌 것으로 나타나야 한다.
    assert(getAllCreatedRooms(cookie2).filter(_.id === roomId).map(_.currentBannedRoom) == List(Some(true)))

    // 입장금지자 목록에 입장금지 설정 대상자가 있어야 한다.
    val banList1 = getBanList(roomId, cookie)
    assert(banList1.headOption.flatMap(_.user).flatMap(_.id) == userModel2.id)

    // 강제퇴장 이후 입장 시도는 실패해야 한다.
    enterRoom(roomId, cookie2, emptyEnterRoomModel, 500)

    // 입장금지를 해제한다.
    clearUserBan(roomId, userModel2.id, cookie)
    // 입장금지 해제후 입장한다.
    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    assert(getBanList(roomId, cookie).isEmpty)
    // 중복된 입장금지 해제는 실패해야 한다.
    clearUserBan(roomId, userModel2.id, cookie, 500)

    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    logout(cookie2)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
  }

  test("revokeOwnerPermission") {
    val x1 = createPromiseList(15)
    val x2 = createPromiseList(15)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator
    val promiseIterator21 = x2.iterator
    val promiseIterator22 = x2.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #5"), None, None)))
    // 대화방 개설
    val roomId = createRoom(roomModel, cookie)
    val resultList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList1, cookie)

    // 방장후보가 없으면 방장권한 위임을 할 수 없다.
    revokeOwnerPermission(roomId, None, cookie, 500)
    // 방장은 자신을 방장후보로 방장권한 위임을 수행할 수 없다.
    revokeOwnerPermission(roomId, userModel.id, cookie, 500)
    // 대화방에 있지 않은 사람에게 방장권한을 위임할 수 없다.
    revokeOwnerPermission(roomId, userModel2.id, cookie, 500)

    // 추가로 입장한다.
    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    // 방장이 아닌 사람은 방장권한 위임을 할 수 없다.
    revokeOwnerPermission(roomId, None, cookie2, 500)
    revokeOwnerPermission(roomId, userModel.id, cookie2, 500)

    // 방장후보를 정하지 않아도 방장권한이 위임되어야 한다.
    revokeOwnerPermission(roomId, None, cookie)
    val envelopeModelList = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelList, cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    val resultRoomTimelineList = envelopeModelList.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).flatMap(_.data)

    assert(resultRoomTimelineList.flatMap(_.permissionHistory).filter(_.user.flatMap(_.id) == userModel.id).flatMap(_.permissionType).filter(_ == PermissionType.RevokeOwner) == List(PermissionType.RevokeOwner))
    assert(resultRoomTimelineList.flatMap(_.permissionHistory).filter(_.user.flatMap(_.id) == userModel2.id).flatMap(_.permissionType).filter(_ == PermissionType.GrantOwner) == List(PermissionType.GrantOwner))
    assert(resultRoomTimelineList.flatMap(_.roomHistory).flatMap(_.user).map(_.id) == List(userModel2.id))

    // 더 이상 방장이 아니므로 방장권한을 위임할 수 없다.
    revokeOwnerPermission(roomId, None, cookie, 500)
    // 방장후보를 정하고 방장권한을 위임한다.
    revokeOwnerPermission(roomId, userModel.id, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    // 자신을 방장후보로 지정한다면, 대화방에 방장을 제외한 다른 사람이 있더라도 방장권한 위임은 실패해야 한다.
    revokeOwnerPermission(roomId, userModel.id, cookie, 500)

    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    logout(cookie2)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
  }

  test("grantOperatorPermission/revokeOperatorPermission") {
    val x1 = createPromiseList(15)
    val x2 = createPromiseList(15)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator
    val promiseIterator21 = x2.iterator
    val promiseIterator22 = x2.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #5"), None, None)))
    // 대화방 개설
    val roomId = createRoom(roomModel, cookie)
    val resultList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList1, cookie)

    // 방장은 자신에게 운영자권한을 수여할 수 있다.
    grantOperatorPermission(roomId, userModel.id, cookie)
    val envelopeModelList: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelList, cookie)
    val resultRoomTimelineList = envelopeModelList.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).flatMap(_.data)
    assert(resultRoomTimelineList.flatMap(_.permissionHistory).filter(_.user.flatMap(_.id) == userModel.id).flatMap(_.permissionType).filter(_ == PermissionType.GrantOperator) == List(PermissionType.GrantOperator))
    // 중복해서 운영자권한을 수여할 수 없다.
    grantOperatorPermission(roomId, userModel.id, cookie, 500)
    // 운영자는 자신의 운영자권한을 반납할 수 있다.
    revokeOperatorPermissionSelf(roomId, cookie)
    val envelopeModelList2: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelList2, cookie)
    val resultRoomTimelineList2 = envelopeModelList2.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).flatMap(_.data)
    assert(resultRoomTimelineList2.flatMap(_.permissionHistory).filter(_.user.flatMap(_.id) == userModel.id).flatMap(_.permissionType).filter(_ == PermissionType.RevokeOperator) == List(PermissionType.RevokeOperator))

    // 중복해서 운영자권한을 회수할 수 없다.
    revokeOperatorPermission(roomId, userModel.id, cookie, 500)

    // 다른 사용자 입장
    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    // 방장만 운영자권한을 수여할 수 있다. 운영자가 아닌 사람의 운영자권한 수여시도.
    revokeOperatorPermission(roomId, userModel2.id, cookie2, 500)
    // 방장이 운영자권한수여
    grantOperatorPermission(roomId, userModel2.id, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    // 방장만 운영자권한을 수여할 수 있다. 방장이 아닌 운영자가 다른사람에게 운영자권한 수여시도.
    grantOperatorPermission(roomId, userModel.id, cookie2, 500)
    // 방장이 다시 자신에게 운영자권한 수여.
    grantOperatorPermission(roomId, userModel.id, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    // 방장만 운영자권한을 회수할 수 있다. 방장이 아닌 운영자가 방장의 운영자권한 회수시도.
    revokeOperatorPermission(roomId, userModel.id, cookie2, 500)
    // 방장이 아닌 운영자가 자신의 운영자권한을 반납.
    revokeOperatorPermission(roomId, userModel2.id, cookie2)
    // 운영자가 아닌 사람이 다른 사람의 운영자권한 회수 시도.
    revokeOperatorPermission(roomId, userModel.id, cookie2, 500)
    // 운영자가 아닌 사람이 자신의 운영자권한 반납시도.
    revokeOperatorPermission(roomId, userModel2.id, cookie2, 500)

    // 대화방에서 퇴장할 때, 운영자권한도 자동으로 없어지는지 확인하기 위해서 다시 운영자권한 수여.
    grantOperatorPermission(roomId, userModel2.id, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 퇴장. web socket 으로 받은 message 에 운영자권한 반납에 관한 내용이 있는지 확인.
    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    val envelopeModelList3: List[EnvelopeModel[_]] = Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelList3, cookie)
    val resultRoomTimelineList3 = envelopeModelList3.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).flatMap(_.data)
    assert(resultRoomTimelineList3.flatMap(_.permissionHistory).filter(_.user.flatMap(_.id) == userModel2.id).flatMap(_.permissionType).filter(_ == PermissionType.RevokeOperator) == List(PermissionType.RevokeOperator))

    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    logout(cookie2)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
  }

  test("setRoomPassword") {
    val promiseList1 = createPromiseList(20)
    val promiseList2 = createPromiseList(20)
    val promiseList3 = createPromiseList(20)
    val promiseIterator11 = promiseList1.iterator
    val promiseIterator12 = promiseList1.iterator
    val promiseIterator21 = promiseList2.iterator
    val promiseIterator22 = promiseList2.iterator
    val promiseIterator31 = promiseList3.iterator
    val promiseIterator32 = promiseList3.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)
    // 방장이 될 사용자 하나와, 운영자가 아니면서 초대를 수행할 사용자 하나, 초대를 받을 사용자 하나를 만든다.
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye2")))).get
    val userModel3 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye2")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val cookie3 = login(CertificationModel(userModel3.name, userModel3.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    val client3 = new SampleWebSocketClient(promiseIterator32, cookie3)
    verifyWebSocketClientConnection(client3)
    Await.result(promiseIterator31.next().future, 3.seconds)
    sayHi(client3)
    Await.result(promiseIterator31.next().future, 3.seconds)

    // 암호없이 대화방을 만들었다.
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val roomId = createRoom(roomModel, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    // 암호없는 대화방에 입장한다.
    enterRoom(roomId, cookie2, EnterRoomModel(None, None))
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 운영자나 방장만 암호를 정할 수 있다.
    setRoomPassword(roomId, Some("password"), cookie2, 500)
    grantOperatorPermission(roomId, userModel2.id, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    // 암호를 정한다.
    setRoomPassword(roomId, Some("password"), cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 암호있는 대화방에 암호없이 입장시도한다.
    enterRoom(roomId, cookie3, emptyEnterRoomModel, 500)
    // 암호있는 대화방에 암호를 사용하여 입장시도한다.
    enterRoom(roomId, cookie3, emptyEnterRoomModel.copy(password = Some("password")))
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)
    // 운영자도 암호를 바꿀 수 있는지 확인하기 위해 일단 퇴장한다.
    leaveRoom(roomId, cookie3)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    // 운영자가 암호를 바꾼다.
    setRoomPassword(roomId, Some("password2"), cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    // 틀린 암호를 사용하여 입장 시도한다.
    enterRoom(roomId, cookie3, emptyEnterRoomModel.copy(password = Some("password")), 500)

    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    logout(cookie2)
    logout(cookie3)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
    Await.result(promiseIterator31.next().future, Duration.Inf)
  }

  test("leaveRoom and byWhoId by Operator") {
    val promiseList1 = createPromiseList(20)
    val promiseList2 = createPromiseList(20)
    val promiseList3 = createPromiseList(20)
    val promiseIterator11 = promiseList1.iterator
    val promiseIterator12 = promiseList1.iterator
    val promiseIterator21 = promiseList2.iterator
    val promiseIterator22 = promiseList2.iterator
    val promiseIterator31 = promiseList3.iterator
    val promiseIterator32 = promiseList3.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)
    // 방장이 될 사용자 하나와, 운영자가 아니면서 초대를 수행할 사용자 하나, 초대를 받을 사용자 하나를 만든다.
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye2")))).get
    val userModel3 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye2")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val cookie3 = login(CertificationModel(userModel3.name, userModel3.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    val client3 = new SampleWebSocketClient(promiseIterator32, cookie3)
    verifyWebSocketClientConnection(client3)
    Await.result(promiseIterator31.next().future, 3.seconds)
    sayHi(client3)
    Await.result(promiseIterator31.next().future, 3.seconds)

    // 암호없이 대화방을 만들었다.
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val roomId = createRoom(roomModel, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    enterRoom(roomId, cookie2, EnterRoomModel(None, None))
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    enterRoom(roomId, cookie3, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    // 운영자 권한없이 강제퇴장을 시도한다.
    leaveRoomByWho(roomId, userModel3.id, cookie2, 500)

    // 운영자권한을 수여한다.
    grantOperatorPermission(roomId, userModel2.id, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    leaveRoomByWho(roomId, userModel3.id, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    // 입장금지 된 방에 입장을 시도한다.
    enterRoom(roomId, cookie3, emptyEnterRoomModel, 500)

    // 입장금지 설정을 운영자가 지운다.
    clearUserBan(roomId, userModel3.id, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 다시 입장한다.
    enterRoom(roomId, cookie3, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)

    leaveRoom(roomId, cookie3)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    requestComplete(Await.result(promiseIterator31.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie3)
    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    logout(cookie2)
    logout(cookie3)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
    Await.result(promiseIterator31.next().future, Duration.Inf)
  }

  test("change room name") {
    val x1 = createPromiseList(15)
    val x2 = createPromiseList(15)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator
    val promiseIterator21 = x2.iterator
    val promiseIterator22 = x2.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #5"), None, None)))
    // 대화방 개설
    val roomId = createRoom(roomModel, cookie)
    val resultList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList1, cookie)

    enterRoom(roomId, cookie2, emptyEnterRoomModel)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 방장이나 운영자만 대화방 제목을 바꿀 수 있어야 한다.
    setRoomName(roomId, Some("room ##5"), cookie2, 500)

    // 방장은 대화방 제목을 바꿀 수 있다.
    setRoomName(roomId, Some("room ##5"), cookie)
    val envelopeModelIdList = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelIdList, cookie)
    assert {
      envelopeModelIdList.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).
        flatMap(_.data).
        filter(_.roomHistory.isDefined).
        flatMap(_.roomHistory).map(_.name) == List(Some("room ##5"))
    }
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    assert(getRoom(roomId, cookie).flatMap(_.currentDetail).flatMap(_.name) == Some("room ##5"))

    grantOperatorPermission(roomId, userModel2.id, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    // 운영자는 대화방 제목을 바꿀 수 있다.
    setRoomName(roomId, Some("room ###5"), cookie2)
    val envelopeModelIdList2 = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(envelopeModelIdList2, cookie)
    assert {
      envelopeModelIdList2.map(_.asInstanceOf[EnvelopeModel[RoomTimelineModel]]).
        flatMap(_.data).
        filter(_.roomHistory.isDefined).
        flatMap(_.roomHistory).map(_.name) == List(Some("room ###5"))
    }
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)
    assert(getRoom(roomId, cookie).flatMap(_.currentDetail).flatMap(_.name) == Some("room ###5"))

    leaveRoom(roomId, cookie2)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)
    requestComplete(Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie2)

    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    logout(cookie2)
    Await.result(promiseIterator11.next().future, Duration.Inf)
    Await.result(promiseIterator21.next().future, Duration.Inf)
  }

  test("invalidAllInvitationWithUser") {
    val x1 = createPromiseList(15)
    val x2 = createPromiseList(15)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator
    val promiseIterator21 = x2.iterator
    val promiseIterator22 = x2.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #5"), None, None)))
    // 대화방 개설
    val roomId = createRoom(roomModel, cookie)
    val resultList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList1, cookie)

    invite(roomId, (new InvitationModel).copy(userId = userModel2.id), cookie)
    val invitation = Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(invitation, cookie2)
    val invitationId = invitation.head.data.map(_.asInstanceOf[InvitationModel]).flatMap(_.id)

    invite(roomId, (new InvitationModel).copy(userId = userModel2.id), cookie)
    val invitation2 = Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(invitation2, cookie2)
    val invitationId2 = invitation2.head.data.map(_.asInstanceOf[InvitationModel]).flatMap(_.id)

    logout(cookie2)
    Await.result(promiseIterator21.next().future, Duration.Inf)

    Thread.sleep(500)

    assert(InvitationDao.getOne(invitationId).flatMap(_.validity) == Some(false))
    assert(InvitationDao.getOne(invitationId2).flatMap(_.validity) == Some(false))

    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    logout(cookie)
    Await.result(promiseIterator11.next().future, Duration.Inf)
  }

  test("invalidAllInvitationWithRoom") {
    val x1 = createPromiseList(15)
    val x2 = createPromiseList(15)
    val promiseIterator11 = x1.iterator
    val promiseIterator12 = x1.iterator
    val promiseIterator21 = x2.iterator
    val promiseIterator22 = x2.iterator
    val emptyEnterRoomModel = EnterRoomModel(None, None)

    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, EtcUtilTrait.generateUserName(), Some("bye")))).get
    val cookie = login(CertificationModel(userModel.name, userModel.password))
    val cookie2 = login(CertificationModel(userModel2.name, userModel2.password))
    val client = new SampleWebSocketClient(promiseIterator12, cookie)
    verifyWebSocketClientConnection(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    sayHi(client)
    Await.result(promiseIterator11.next().future, 3.seconds)
    val client2 = new SampleWebSocketClient(promiseIterator22, cookie2)
    verifyWebSocketClientConnection(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)
    sayHi(client2)
    Await.result(promiseIterator21.next().future, 3.seconds)

    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #5"), None, None)))
    // 대화방 개설
    val roomId = createRoom(roomModel, cookie)
    val resultList1: List[EnvelopeModel[_]] = Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(resultList1, cookie)

    invite(roomId, (new InvitationModel).copy(userId = userModel2.id), cookie)
    val invitation = Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(invitation, cookie2)
    val invitationId = invitation.head.data.map(_.asInstanceOf[InvitationModel]).flatMap(_.id)

    invite(roomId, (new InvitationModel).copy(userId = userModel2.id), cookie)
    val invitation2 = Await.result(promiseIterator21.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]]
    requestComplete(invitation2, cookie2)
    val invitationId2 = invitation2.head.data.map(_.asInstanceOf[InvitationModel]).flatMap(_.id)

    leaveRoom(roomId, cookie)
    requestComplete(Await.result(promiseIterator11.next().future, 3.seconds).asInstanceOf[List[EnvelopeModel[_]]], cookie)

    Thread.sleep(500)
    assert(InvitationDao.getOne(invitationId).flatMap(_.validity) == Some(false))
    assert(InvitationDao.getOne(invitationId2).flatMap(_.validity) == Some(false))

    logout(cookie2)
    Await.result(promiseIterator21.next().future, Duration.Inf)
    logout(cookie)
    Await.result(promiseIterator11.next().future, Duration.Inf)
  }
}
