/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import com.fasterxml.jackson.databind.JsonNode
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ RoomAttenderModel, RoomAttenderModelEx }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.{ UserModel, UserModelEx }
import org.scalatest.FunSuite

import scala.collection.JavaConversions

/**
 * Created by Kang Woo, Lee on 5/1/15.
 */
class TestJsonUtil extends FunSuite {
  test("hi") {
    val model = RoomAttenderModelEx(None, None, None, None, None).copy(relatedRoomInOutHistory = Some(RoomInOutHistoryModelEx(None, None, None, None, None, None, None, Some(EnterType.Enter))))
    val model2 = UserModelEx(None, None, None, None, Some("hi"), None)
    val envelope = Envelope(Some(1), Some(model))
    val envelope2 = Envelope(Some(2), Some(model2))
    val list = List(envelope, envelope2)
    val json = DefaultObjectMapper.writeValueAsString(list)
    val tree = DefaultObjectMapper.readTree(json)

    // http://stackoverflow.com/questions/19711695/jackson-convert-jsonnode-into-pojo
    // 여러개의 서로 다른 자료를 모아서 보낸 것을 받고 싶을 때. server 에서는 이런 code 를 쓸 일이 없다. client 에서 써야 한다.
    // android 에서는 json4s 가 안되기 때문에, 이와 같이 할수밖에 없다.
    val map = Map[Int, JsonNode ⇒ Option[_]](
      (1, x ⇒ Option(DefaultObjectMapper.treeToValue[RoomAttenderModel](x))),
      (2, x ⇒ Option(DefaultObjectMapper.treeToValue[UserModel](x))))
    val x = JavaConversions.asScalaIterator(tree.elements()).
      map(x ⇒ map(x.findValue("id").asInt()).apply(x.findValue("data"))).
      toList

    x.foreach {
      case Some(x: RoomAttenderModel) ⇒ println(x)
      case Some(x: UserModel) ⇒ println(x)
    }
  }
}

case class Envelope[T](id: Option[Int], data: Option[T])
