/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.{ BanModel ⇒ JBanModel, OperatorModel ⇒ JOperatorModel, RoomAttenderModel ⇒ JRoomAttenderModel, RoomModel ⇒ JRoomModel }
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.{ PermissionHistoryModel ⇒ JPermissionHistoryModel, PermissionType ⇒ JPermissionType, RoomHistoryModel ⇒ JRoomHistoryModel, RoomInOutHistoryModel ⇒ JRoomInOutHistoryModel, RoomTimelineModel ⇒ JRoomTimelineModel }
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.current.{ UserModel ⇒ JUserModel }
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.{ LogoutModel ⇒ JLogoutModel, SayHiModel ⇒ JSayHiModel }
import org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417.{ DefaultObjectMapper ⇒ JDefaultObjectMapper }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.BanHistoryModel.BanType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnvelopeModel, EnvelopeModelEx, LogoutModel, SayHiModel }
import org.scalatest.FunSuite

/**
 * Created by Kang Woo, Lee on 5/5/15.
 */
class TestCompatible extends FunSuite {
  // type parameter 의 class instance 를 구하기.
  // http://stackoverflow.com/questions/1305563/how-to-instantiate-an-instance-of-type-represented-by-type-parameter-in-scala
  def template[ScalaType, JavaType](t: ScalaType)(implicit x: Manifest[JavaType], xx: Manifest[ScalaType]): Unit = {
    val json1 = DefaultObjectMapper.writeValueAsString(t)
    val deserialize1 = JDefaultObjectMapper.objectMapper.readValue(json1, x.runtimeClass)
    val json2 = JDefaultObjectMapper.objectMapper.writeValueAsString(deserialize1)
    val deserialize2 = DefaultObjectMapper.readValue[Option[ScalaType]](json2)
    val json3 = DefaultObjectMapper.writeValueAsString(deserialize2)
    val deserialize3 = JDefaultObjectMapper.objectMapper.readValue(json3, x.runtimeClass)
    deserialize2.foreach(x ⇒ assert(t == x))
    assert(deserialize1 == deserialize3)
  }

  // UserModel ==
  // UserLoginHistoryModel ==
  // UserNickHistoryModel ==

  // RoomModel ==
  // InvitationModel ==
  // OperatorModel ==
  // BanModel
  // RoomAttenderModel ==

  // RoomHistoryModel ==
  // PermissionHistoryModel ==
  // RoomInOutHistoryModel ==
  // BanHistoryModel ==
  // RoomTimelineModel ==

  // TalkHistoryModel ==

  // EnvelopeModel ==

  test("UserModel/UserLoginHistoryModel/UserNickHistoryModel") {
    val userModel = UserModel(Some(1), Some(2), Some(3), Some(4), Some("5"), Some("6"),
      Some(UserLoginHistoryModel(Some(7), Some(8), Some(9), DriverSettings.now(), DriverSettings.now(),
        Some((new UserModel).copy(name = Some("10"))))),
      Some(UserNickHistoryModel(Some(11), Some(12), Some(13), DriverSettings.now(), Some("14"),
        Some((new UserModel()).copy(name = Some("15"))))))
    template[UserModel, JUserModel](userModel)
  }

  test("RoomModel/RoomHistoryModel/UserModel/PermissionHistoryModel") {
    val roomModel = RoomModel(Some(1), Some(2), DriverSettings.now(), DriverSettings.now(), Some(3), Some(4),
      Some(RoomHistoryModel(Some(5), Some(6), Some(7), Some(8), Some(9), DriverSettings.now(), Some("10"), Some("11"), Some(12),
        Some((new RoomModel()).copy(id = Some(14))),
        Some((new UserModel()).copy(name = Some("15"))),
        Some((new UserModel()).copy(name = Some("16"))),
        Some(PermissionHistoryModel(Some(19), Some(20), Some(21), Some(22), Some(23), Some(PermissionType.RevokeOwner), DriverSettings.now(),
          Some((new UserModel()).copy(name = Some("24"))),
          Some((new RoomModel()).copy(id = Some(25))),
          Some((new UserModel()).copy(name = Some("26"))))), Some(true))),
      Some((new UserModel()).copy(name = Some("17"))), Some(false), Some(true))
    template[RoomModel, JRoomModel](roomModel)
  }

  test("RoomAttenderModel/RoomInOutHistoryModel/InvitationModel") {
    val roomAttenderModel = RoomAttenderModel(Some(1), Some(2), Some(3), Some(4), Some(5),
      Some((new UserModel()).copy(name = Some("6"))),
      Some((new RoomModel()).copy(id = Some(7))),
      Some(RoomInOutHistoryModel(Some(8), Some(9), Some(10), Some(11), Some(12), Some(13), DriverSettings.now(), Some(EnterType.Leave),
        Some((new UserModel()).copy(name = Some("14"))),
        Some((new RoomModel()).copy(id = Some(15))),
        Some(InvitationModel(Some(16), Some(17), Some(18), Some(19), Some(20), DriverSettings.now(), DriverSettings.now(), Some(true),
          Some((new UserModel()).copy(name = Some("21"))),
          Some((new RoomModel()).copy(id = Some(22))),
          Some((new UserModel()).copy(name = Some("23"))))),
        Some((new UserModel()).copy(name = Some("24"))))), Some(true), Some(false))
    template[RoomAttenderModel, JRoomAttenderModel](roomAttenderModel)
  }

  test("OperatorModel") {
    val operatorModel = OperatorModel(Some(1), Some(2), Some(3), Some(4), Some(5),
      Some((new UserModel()).copy(name = Some("6"))),
      Some((new RoomModel()).copy(id = Some(7))),
      Some((new PermissionHistoryModel()).copy(id = Some(8))))
    template[OperatorModel, JOperatorModel](operatorModel)
  }

  test("BanModel/BanHistoryModel") {
    val banModel = BanModel(Some(1), Some(2), Some(3), Some(4), Some(4),
      Some((new UserModel()).copy(id = Some(5))),
      Some((new RoomModel()).copy(id = Some(6))),
      Some(BanHistoryModel(Some(7), Some(8), Some(9), Some(10), Some(11), Some(BanType.Disallow), DriverSettings.now(),
        Some((new UserModel()).copy(id = Some(12))),
        Some((new RoomModel()).copy(id = Some(13))),
        Some((new UserModel()).copy(id = Some(14))))))
    template[BanModel, JBanModel](banModel)
  }

  test("RoomTimelineModel/TalkHistoryModel") {
    val roomTimelineModel = RoomTimelineModel(Some(1), Some(2), Some(3), Some(4), Some(5), Some(6), Some(7), Some(8), Some(81),
      Some((new RoomModel()).copy(id = Some(9))),
      Some((new BanHistoryModel()).copy(id = Some(10))),
      Some((new PermissionHistoryModel()).copy(id = Some(11))),
      Some((new RoomHistoryModel()).copy(id = Some(12))),
      Some((new RoomInOutHistoryModel()).copy(id = Some(13))),
      Some(TalkHistoryModel(Some(14), Some(15), Some(16), Some(17), Some(18), DriverSettings.now(), Some("19"),
        Some((new UserModel()).copy(id = Some(20))),
        Some((new RoomModel()).copy(id = Some(21))),
        Some((new UserNickHistoryModel()).copy(id = Some(22))))),
      Some((new UserNickHistoryModel()).copy(id = Some(23))))
    template[RoomTimelineModel, JRoomTimelineModel](roomTimelineModel)
  }

  test("EnvelopeModel") {
    val userModel = EnvelopeModelEx(Some(UserModel(Some(1), Some(2), Some(3), Some(4), Some("5"), Some("6"),
      Some(UserLoginHistoryModel(Some(7), Some(8), Some(9), DriverSettings.now(), DriverSettings.now(),
        Some((new UserModel).copy(name = Some("10"))))),
      Some(UserNickHistoryModel(Some(11), Some(12), Some(13), DriverSettings.now(), Some("14"),
        Some((new UserModel()).copy(name = Some("15")))))))).copy(id = Some(1))
    val roomTimelineModel = EnvelopeModelEx(Some(RoomTimelineModel(Some(1), Some(2), Some(3), Some(4), Some(5), Some(6), Some(7), Some(8), Some(81),
      Some((new RoomModel()).copy(id = Some(9))),
      Some((new BanHistoryModel()).copy(id = Some(10))),
      Some((new PermissionHistoryModel()).copy(id = Some(11))),
      Some((new RoomHistoryModel()).copy(id = Some(12))),
      Some((new RoomInOutHistoryModel()).copy(id = Some(13))),
      Some(TalkHistoryModel(Some(14), Some(15), Some(16), Some(17), Some(18), DriverSettings.now(), Some("19"),
        Some((new UserModel()).copy(id = Some(20))),
        Some((new RoomModel()).copy(id = Some(21))),
        Some((new UserNickHistoryModel()).copy(id = Some(22))))),
      Some((new UserNickHistoryModel()).copy(id = Some(23)))))).copy(id = Some(2))
    val list = List[EnvelopeModel[_]](userModel, roomTimelineModel)
    // 4. java 에서 쓴 json 을 scala 에서 읽고
    val result = DefaultObjectMapper.deserializeEnvelopeList(
      DefaultObjectMapper.readTree(
        // 3. java 에서 읽은 것을 java 에서 json 으로 쓰고
        JDefaultObjectMapper.objectMapper.writeValueAsString(
          // 2. json 을 java 에서 읽고
          JDefaultObjectMapper.deserializeEnvelopeList(
            // 1. 원본을 scala 에서 json 으로 쓰고.
            DefaultObjectMapper.writeValueAsString(list)))))
    assert(list == result)
  }

  test("SayHiModel/LogoutModel") {
    val sayHiModel = SayHiModel(Some("hi"))
    template[SayHiModel, JSayHiModel](sayHiModel)
    val logoutModel = LogoutModel(Some(1))
    template[LogoutModel, JLogoutModel](logoutModel)
  }
}
