/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import akka.actor.Status.Failure
import akka.actor.{ Actor, Props }
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor.MasterActorSystem

/**
 * Created by Kang Woo, Lee on 5/2/15.
 */
class SampleActor extends Actor {
  // scalatra akka 연습용으로 썼다. 참고하자.

  override def receive: Receive = {
    case TestRequest(id) ⇒
      // sender() ! id.map(_ + 1)
      // http://grokbase.com/t/gg/akka-user/149495nw45/scala-ask-pattern-how-to-return-an-exception
      sender() ! Failure(new RuntimeException)
  }
}

object SampleActor {
  val sampleActor = "sampleActor"

  MasterActorSystem.actorSystem.actorOf(Props(new SampleActor), sampleActor)
}

case class TestRequest(id: Option[Int])
