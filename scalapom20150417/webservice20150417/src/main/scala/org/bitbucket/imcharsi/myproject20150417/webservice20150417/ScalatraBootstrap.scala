/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import javax.servlet.ServletContext

import org.bitbucket.imcharsi.myproject20150417.webservice20150417.authentication.LoginServlet
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.room.current.{ RoomServlet, RoomServletTrait }
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.user.current.{ UserServlet, UserServletTrait }
import org.scalatra.LifeCycle

/**
 * Created by i on 4/17/15.
 */
class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext): Unit = {
    super.init(context)
    context.mount(classOf[LoginServlet], "/login/*")
    context.mount(classOf[UserServlet], f"/${UserServletTrait.user}/*")
    context.mount(classOf[RoomServlet], f"/${RoomServletTrait.room}/*")
    context.mount(classOf[SampleServlet], f"/${SampleServlet.sampleServlet}/*")
    context.mount(classOf[AdminServlet], s"/${AdminServlet.admin}/*")
  }
}
