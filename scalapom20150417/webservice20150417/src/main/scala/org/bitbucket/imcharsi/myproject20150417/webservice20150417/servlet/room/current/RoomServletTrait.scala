/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.room.current

import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current.{ BanDao, RoomAttenderDao, RoomDao ⇒ Dao }
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.current.RoomDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnterRoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ BanModel, InvitationModel, RoomAttenderModel, RoomModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor._
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.DaoServletTrait
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.user.current.UserServletTrait
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.{ DefaultObjectMapper, DurationConstant }
import org.scalatra.Params

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, Future }

/**
 * Created by Kang Woo, Lee on 4/30/15.
 */
trait RoomServletTrait extends DaoServletTrait[RoomModel, RoomDao] {

  implicit private def validator1(x: RoomModel) = true

  implicit private def validator2(a: RoomModel, b: RoomModel) = a.id == b.id

  implicit private val dao = Dao

  implicit private def idPicker() = RoomServletTrait.paramRoomId(params)

  implicit val timeout = Timeout(DurationConstant.threeSeconds)

  override protected def postEach()(implicit manifest: Manifest[RoomModel], dao: RoomDao, validator: Validator1): Option[Int] = {
    val userModel = authenticate()
    // 방 개설과 관련된 자료 중, 방 제목과 암호를 설정하는 방법이 필요한데, roomModel.currentDetail 에 값을 써넣기로 하자.
    val roomModel = DefaultObjectMapper.readValue[Option[RoomModel]](request.body)
    val future = MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName) ? CreateRoomCommand(userModel.get, roomModel.get)
    val result = Await.result(future, Duration.Inf).asInstanceOf[RoomModel]
    result.id
  }

  post(f"/", testContentTypeHeader)(optionResultWrapper(postEach))
  // get route 가 없으면, atmosphere 구현 때문에 동작하지 않는다. 이유는 모른다. 일단, 임시로 써둔다.
  get(f"/:${RoomServletTrait.roomId}", testAcceptHeader)(optionResultWrapper(getEachRoom))
  put(f"/:${RoomServletTrait.roomId}", testContentTypeHeader)(optionResultWrapper(putRevokeOwner))
  put(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.password}", testContentTypeHeader)(optionResultWrapper(putSetRoomPassword))
  put(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.name}", testContentTypeHeader)(optionResultWrapper(putSetRoomName))

  get(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.roomAttenders}", testAcceptHeader)(listResultWrapper(getRoomAttenderList))
  post(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.roomAttenders}")(optionResultWrapper(postEnterRoom))

  // 경로 정의 순서를 아래와 같이 해야 한다. 반대로 하면 잘 안된다.
  delete(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.roomAttenders}/:${UserServletTrait.userId}")(deleteLeaveRoom)
  delete(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.roomAttenders}/${UserServletTrait.self}")(deleteLeaveRoom)
  get(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.roomAttenders}/${UserServletTrait.self}", testAcceptHeader)(optionResultWrapper(getSelfRoomAttenderModel))

  // 방폐쇄에 관한 url 은 있으면 안된다. 방폐쇄는 방에 아무도 없게 됐을 때 자동으로 수행되어야 한다.
  post(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.invitation}", testContentTypeHeader)(optionResultWrapper(postInvite))
  post(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.talk}", testContentTypeHeader)(optionResultWrapper(postTalk))
  get(f"/${RoomServletTrait.allCreatedRooms}", testAcceptHeader)(listResultWrapper(getAllCreatedRooms))

  // 경로를, /room/:roomId/ban/:userId 에 대해 post 요청을 하는 식으로 할 수도 있다. 일단, 그냥 놔두기.
  delete(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.ban}/:${UserServletTrait.userId}")(optionResultWrapper(deleteClearUserBan))
  get(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.ban}", testAcceptHeader)(listResultWrapper(getBanList))

  post(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.operator}", testContentTypeHeader)(optionResultWrapper(postGrantOperator))
  delete(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.operator}/:${UserServletTrait.userId}")(optionResultWrapper(deleteRevokeOperator))
  delete(f"/:${RoomServletTrait.roomId}/${RoomServletTrait.operator}/${UserServletTrait.self}")(optionResultWrapper(deleteRevokeOperatorSelf))

  private def getEachRoom(): Option[RoomModel] = {
    authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    Dao.getOne(roomId).map(_.clearPrivacyData())
  }

  private def putSetRoomName(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val name = DefaultObjectMapper.readValue[Option[String]](request.body)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        SetRoomNameCommand(roomId, name, userModel.flatMap(_.id))
    }.flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
      Some(1)
    }
  }

  private def putSetRoomPassword(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val password = DefaultObjectMapper.readValue[Option[String]](request.body)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        SetRoomPasswordCommand(userModel.flatMap(_.id), roomId, password)
    }.flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
      Some(1)
    }
  }

  private def deleteRevokeOperatorSelf(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val userId = userModel.flatMap(_.id)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        RevokeOperatorCommand(userModel.flatMap(_.id), roomId, userId)
    }.flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
      Some(1)
    }
  }

  private def deleteRevokeOperator(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val userId = UserServletTrait.paramUserId(params)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        RevokeOperatorCommand(userModel.flatMap(_.id), roomId, userId)
    }.flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
      Some(1)
    }
  }

  private def postGrantOperator(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val userId = DefaultObjectMapper.readValue[Option[Int]](request.body)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        GrantOperatorCommand(userModel.flatMap(_.id), roomId, userId)
    }.flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
      Some(1)
    }
  }

  private def putRevokeOwner(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val roomModel = DefaultObjectMapper.readValue[Option[RoomModel]](request.body)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        RevokeOwnerCommand(userModel.flatMap(_.id), roomId, roomModel.flatMap(_.currentDetail).flatMap(_.userId))
    }.flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
      Some(1)
    }
  }

  private def getBanList(): List[BanModel] = {
    authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    BanDao.getAllByRoom(roomId).toList.map(_.clearPrivacyData())
  }

  private def deleteClearUserBan(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val userId = UserServletTrait.paramUserId(params)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        ClearUserBanCommand(userId, roomId, userModel.flatMap(_.id))
    }.map(_.asInstanceOf[Future[Option[Int]]]).flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
    }
  }

  private def getSelfRoomAttenderModel(): Option[RoomAttenderModel] = {
    val userModel = authenticate()
    RoomAttenderDao.retrieveRoomsThatUserAttended(userModel.flatMap(_.id)).
      filter(_.roomId == RoomServletTrait.paramRoomId(params)).
      headOption.map(_.clearPrivacyData())
  }

  private def getAllCreatedRooms(): List[RoomModel] = {
    val userModel = authenticate()
    Dao.getAllCreatedRooms(userModel.flatMap(_.id)).toList.map(_.clearPrivacyData())
  }

  private def postTalk(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val talkHistoryModel = DefaultObjectMapper.readValue[Option[TalkHistoryModel]](request.body)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        TalkCommand(userModel.flatMap(_.id), roomId, talkHistoryModel.flatMap(_.talk))
    }.map(_.asInstanceOf[Future[Option[Int]]]).flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
    }
  }

  private def postInvite(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val invitationModel = DefaultObjectMapper.readValue[Option[InvitationModel]](request.body)

    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
        InviteCommand(invitationModel.flatMap(_.userId), userModel.flatMap(_.id))
    }.map(_.asInstanceOf[Future[Option[Int]]]).flatMap { x ⇒
      Await.result(x, DurationConstant.threeSeconds)
    }
  }

  private def getRoomAttenderList(): List[RoomAttenderModel] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ? RetrieveRoomAttenderSetCommand(roomId)
    }.map(_.asInstanceOf[Future[Set[RoomAttenderModel]]]).map { x ⇒
      Await.result(x, DurationConstant.threeSeconds).toList.map(_.clearPrivacyData())
    }.get
  }

  private def postEnterRoom(): Option[Int] = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val enterRoomModel = DefaultObjectMapper.readValue[Option[EnterRoomModel]](request.body)
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ? EnterRoomCommand(userModel.flatMap(_.id), roomId, enterRoomModel)
    }.map(_.asInstanceOf[Future[Option[Int]]]).flatMap { x ⇒
      // actor 에 대해 응답을 기다리는 요청을 보내면 기다린다. 없는 actor 에 대해 요청을 보내면 없다고 하지 않고, 기다린다. 그래서 시간제한이 필요하다.
      Await.result(x, DurationConstant.threeSeconds)
    }
  }

  private def deleteLeaveRoom(): Unit = {
    val userModel = authenticate()
    val roomId = RoomServletTrait.paramRoomId(params)
    val userId = UserServletTrait.paramUserId(params)
    val leaveRoomCommand = userId match {
      case Some(_) ⇒ LeaveRoomCommand(userId, roomId, userModel.flatMap(_.id))
      // :userId 경로가 없으면 self 이다.
      case None ⇒ LeaveRoomCommand(userModel.flatMap(_.id), roomId, None)
    }
    roomId.map(_.toString).map { x ⇒
      MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ? leaveRoomCommand
    }.map(_.asInstanceOf[Future[List[Option[Int]]]]).map { x ⇒
      // 방 퇴장 요청의 수행 결과는 무시된다. 성공여부만 중요하다.
      Await.result(x, DurationConstant.threeSeconds)
    }.get
  }
}

object RoomServletTrait {
  val room = "room"
  val roomId = "roomId"
  val roomAttenders = "roomAttenders"
  val invitation = "invitation"
  val talk = "talk"
  val allCreatedRooms = "allCreatedRooms"
  val ban = "ban"
  val operator = "operator"
  val password = "password"
  val name = "name"

  def paramRoomId(p: Params): Option[Int] = p.get(roomId).filterNot(_.isEmpty).map(_.toInt)
}
