/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.user.current

import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current.{ InvitationDao, RoomAttenderDao }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.{ UserDao ⇒ Dao }
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ InvitationModel, RoomAttenderModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnvelopeModelEx, SayHiModel }
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor._
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.DaoServletTrait
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.{ AtmosphereClientResourceTrait, DefaultObjectMapper, DurationConstant }
import org.scalatra.atmosphere._
import org.scalatra.{ ActionResult, Ok, Params }

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{ Failure, Success, Try }

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
trait UserServletTrait extends DaoServletTrait[UserModel, UserDao] {

  implicit private def validator1(x: UserModel) = true

  implicit private def validator2(a: UserModel, b: UserModel) = a.id == b.id

  implicit private val dao = Dao

  implicit private def idPicker() = UserServletTrait.paramUserId(params)

  implicit val timeout = Timeout(DurationConstant.threeSeconds)

  post("/", testContentTypeHeader)(optionResultWrapper(postEachOne))
  atmosphere(f"/${UserServletTrait.self}/${UserServletTrait.ws}")(userAtmosphereClient)
  put(f"/${UserServletTrait.self}/${UserServletTrait.messageQueue}/${UserServletTrait.request}")(putRequest)
  put(f"/${UserServletTrait.self}/${UserServletTrait.messageQueue}/${UserServletTrait.complete}", testContentTypeHeader)(putComplete)
  get(f"/${UserServletTrait.echo}")(getEcho)
  post(f"/${UserServletTrait.self}/${UserServletTrait.messageQueue}/${UserServletTrait.pushSampleMessage}")(postPushSampleMessage)
  get(f"/${UserServletTrait.allLoginUsers}", testAcceptHeader)(listResultWrapper(getAllLoginUsers))
  get(f"/${UserServletTrait.self}", testAcceptHeader)(optionResultWrapper(getSelf))
  get(f"/${UserServletTrait.self}/${UserServletTrait.allRoomAttenders}", testAcceptHeader)(listResultWrapper(getAllRoomAttenders))
  // fixme 시험용으로만 써야 한다.
  get(f"/", testAcceptHeader)(listResultWrapper(getAllUsers))
  get(f"/${UserServletTrait.self}/${UserServletTrait.invitaion}", testAcceptHeader)(listResultWrapper(getAllInvitation))
  put(f"/${UserServletTrait.self}/${UserServletTrait.nick}", testContentTypeHeader)(optionResultWrapper(putSetCurrentNick))

  private def postEachOne(): Option[Int] = {
    val userModel = DefaultObjectMapper.readValue[Option[UserModel]](request.body)
    Try {
      userModel.map(x ⇒ new UserModel().copy(name = x.name, password = x.password)).
        flatMap(x ⇒ Dao.postOne(x))
    } match {
      case Success(x) ⇒ x
      case Failure(x) ⇒ throw x
    }
  }

  private def putSetCurrentNick(): Option[Int] = {
    val userModel = authenticate()
    val nick = DefaultObjectMapper.readValue[Option[String]](request.body)
    userModel.flatMap(_.currentLoginHistoryId).map(_.toString).
      map(x ⇒ MasterActorSystem.actorSystem / MasterUserActor.actorName / x).
      map(x ⇒ MasterActorSystem.actorSystem.actorSelection(x)).
      map(_ ? SetCurrentNickCommand(userModel.flatMap(_.id), nick)).
      map(x ⇒ Try(Await.result(x, DurationConstant.threeSeconds))).
      flatMap {
        case Success(_) ⇒ Some(1)
        case Failure(x) ⇒ throw x
      }
  }

  private def getAllInvitation(): List[InvitationModel] = {
    val userModel = authenticate()
    InvitationDao.getAllAboutUser(userModel.flatMap(_.id)).toList.map(_.clearPrivacyData())
  }

  private def getAllUsers(): List[UserModel] = {
    // 관리용으로 쓴다면 관리자여부를 확인하는 인증을 여기서 한다.
    Dao.getAllUsers().toList.map(_.clearPrivacyData())
  }

  private def getAllRoomAttenders(): List[RoomAttenderModel] = {
    val userModel = authenticate()
    RoomAttenderDao.retrieveRoomsThatUserAttended(userModel.flatMap(_.id)).toList.map(_.clearPrivacyData())
  }

  private def getSelf(): Option[UserModel] = {
    authenticate().flatMap(x ⇒ Dao.getOne(x.id))
  }

  private def getAllLoginUsers(): List[UserModel] = {
    authenticate()
    Dao.getAllLoginUsers().toList.map(_.clearPrivacyData())
  }

  private def getEcho(): ActionResult = {
    // 아래와 같이 쓰면 된다. 자동으로 jsessionid cookie 가 생긴다. 편리하다.
    authenticate()
    session.put("1", "2")
    Ok()
  }

  private def postPushSampleMessage(): ActionResult = {
    val userModel = authenticate()
    // message queue 에 message 를 넣는다.
    userModel.flatMap(_.currentLoginHistoryId).map(_.toString).
      map(x ⇒ MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x)).
      map(_.!(PushMessageCommand(List(EnvelopeModelEx(userModel)))))
    Ok()
  }

  private def putRequest(): ActionResult = {
    val userModel = authenticate()
    userModel.flatMap(_.currentLoginHistoryId).map(_.toString).
      map(x ⇒ MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x)).
      map(_.!(SelfAwakeningCommand()))
    Ok()
  }

  private def putComplete(): ActionResult = {
    val userModel = authenticate()
    val completeSet = DefaultObjectMapper.readValue[List[Int]](request.body).toSet
    userModel.flatMap(_.currentLoginHistoryId).map(_.toString).
      map(x ⇒ MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x)).
      map(_.!(CompleteMessageCommand(completeSet)))
    Ok()
  }

  private def userAtmosphereClient(): AtmosphereClient = {
    new UserAtmosphereClient
  }

  class UserAtmosphereClient extends AtmosphereClient with AtmosphereClientResourceTrait {
    override def receive: AtmoReceive = {
      case Connected ⇒
        implicit val resource = Some(atmosphereResource())
        Try(authenticate2()) match {
          case Success(Some(userModel)) ⇒
            // atmosphere route 설정 단계에서, 보통의 params 기능을 사용할수 없다. 왜냐하면, implicit HttpServletRequest 객체가 없기 때문이다.
            // 그래서 직접 정규표현식을 사용하여 url 을 검사한다.
            //    http://stackoverflow.com/questions/4636610/regular-expression-and-pattern-matching-in-scala
            // 경로 검사는 안해도 된다. 경로를 고정시켰다. 위의 정규표현식 문서는 참고하자.
            // todo atmosphere client 의 연결 종료의 이유를 정할수 있나.
            // web socket 자체가 중복되는 경우에 대해서는 금지할 방법이 없다. 각 사용자 접속마다 고유한 actor 의 존재 여부를 써서 web socket 의 허용 여부를 정하자.
            // 그런데, atmosphere 인지 scalatra 인지 어느 단계인지는 모르겠는데, 중복 접속이 안된다. tcp/ip 접속만 되고, 그 이후 단계로 넘어가지 않는다.
            // 여기서 user actor 에게 web socket 참조를 전달해야 하고, 전달이 성공했는지 확인하도록 해야 한다. 전달이 실패하면 web socket 을 끊어야 한다.
            // 왜 이와 같은 일이 생길 수 있는가. web socket 연결 성공 이후 user actor 에게 요청을 보냈는데,
            // 그 앞에 쌓였던 message 중에 logout force command 가 있어서 web socket 설정 변경 요청을 처리하기 전에 actor 가 정지할 수 있다.
            // 그래서 이 단계에서는 반드시 성공여부를 확인해야 한다.
            userModel.currentLoginHistoryId.map(_.toString).
              map(x ⇒ MasterActorSystem.actorSystem / MasterUserActor.actorName / x).
              map(x ⇒ MasterActorSystem.actorSystem.actorSelection(x)).
              map(_ ? ChangeAtmosphereClientCommand(Some(this))).
              map(x ⇒ Try(Await.result(x, DurationConstant.threeSeconds))).
              foreach {
                case Success(None) ⇒ None
                case x ⇒ atmosphereResource().close()
              }
          case x ⇒
            atmosphereResource().close()
        }
      // scalatra web socket server 에서는 connected 에 이어 바로 send 가 안되는듯 하다.
      case Disconnected(_, _) ⇒
        Try(authenticate2()) match {
          case Success(Some(userModel)) ⇒
            userModel.currentLoginHistoryId.map(_.toString).
              map(x ⇒ MasterActorSystem.actorSystem / MasterUserActor.actorName / x).
              map(x ⇒ MasterActorSystem.actorSystem.actorSelection(x)).
              map(_ ? ChangeAtmosphereClientCommand(None)).
              map(x ⇒ Try(Await.result(x, DurationConstant.threeSeconds))).
              foreach {
                case Success(None) ⇒ None
                case x ⇒ atmosphereResource().close()
              }
          case _ ⇒
        }
      case TextMessage(x) ⇒
        send(TextMessage(DefaultObjectMapper.writeValueAsString(
          List(EnvelopeModelEx(Some(SayHiModel(Some("hi")))).copy(id = Some(-1))))))
    }
  }
}

object UserServletTrait {
  val self = "self"
  val userId = "userId"
  val user = "user"
  val echo = "echo"
  val pushSampleMessage = "pushSampleMessage"
  val complete = "complete"
  val request = "request"
  val ws = "ws"
  val messageQueue = "messageQueue"
  val allLoginUsers = "allLoginUsers"
  val allRoomAttenders = "allRoomAttenders"
  val invitaion = "invitation"
  val nick = "nick"

  def paramUserId(p: Params): Option[Int] = p.get(userId).filterNot(_.isEmpty).map(_.toInt)
}
