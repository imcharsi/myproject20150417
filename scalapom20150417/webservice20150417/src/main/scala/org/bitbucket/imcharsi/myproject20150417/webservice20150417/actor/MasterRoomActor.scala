/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor

import akka.actor.{ Actor, Props, Status }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current.RoomDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history.RoomTimelineDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait.NotFound
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

import scala.util.{ Failure, Success, Try }

/**
 * Created by Kang Woo, Lee on 4/24/15.
 */
class MasterRoomActor extends Actor {
  override def receive: Receive = {
    case CreateRoomCommand(userModel, roomModel) ⇒
      Try {
        RoomDao.createRoom(userModel, roomModel)
      } match {
        case Success(Some((roomModel, roomTimelineIds))) ⇒
          // 개설자가 입장했다는 것을 room actor 는 어떻게 인식할 것인가. room actor 시작시점에 입장자 목록을 구한다.
          roomModel.id.map(_.toString).foreach(context.actorOf(Props(new RoomActor(roomModel.id)), _))
          sender() ! roomModel
          val roomTimelineModelList = roomTimelineIds.filter(_.isDefined).
            map(x ⇒ RoomTimelineDao.getOne(x)).
            map(x ⇒ x.map(_.clearPrivacyData())).
            map(x ⇒ EnvelopeModelEx(x))
          // 이 message 를 받을 대상은 개설자 하나 뿐이다.
          userModel.currentLoginHistoryId.map(_.toString).
            map(x ⇒ MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x)).
            foreach { x ⇒ x.!(PushMessageCommand(roomTimelineModelList))
            }
        case Success(None) ⇒
          sender() ! Status.Failure(new NotFound)
        case Failure(x) ⇒
          sender() ! Status.Failure(x)
      }
  }
}

sealed trait MasterRoomActorCommand

case class CreateRoomCommand(userModel: UserModel, roomModel: RoomModel) extends MasterRoomActorCommand

object MasterRoomActor {
  val actorName = "room"
  MasterActorSystem.actorSystem.actorOf(Props(new MasterRoomActor), actorName)
}
