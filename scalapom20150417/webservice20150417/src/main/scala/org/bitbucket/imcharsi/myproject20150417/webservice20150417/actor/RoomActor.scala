/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor

import java.util.logging.Logger

import akka.actor.{ Actor, PoisonPill }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history.RoomTimelineDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomAttenderModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnterRoomModel, EnvelopeModelEx }

import scala.util.Try

/**
 * Created by Kang Woo, Lee on 4/24/15.
 */
class RoomActor(roomId: Option[Int]) extends Actor {
  // 입장/퇴장이 있을 때마다, 현재 입장중인 사용자 목록을 새로 구하기로 하자.
  // 매번 새로운 목록을 구할 것이므로, mutable 로 할 필요 없다.
  private[this] var roomAttenderSet: Set[RoomAttenderModel] = Set.empty

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()
    // 외부에서 database 를 직접 조작하지 않는다면, 이는 무조건 성공하게 되어 있다. 어떠한 요청보다도 우선하여 이 명령이 실행되기 때문이다.
    // 왜 여기서 이 명령을 수행하는가. 개설자는 자동으로 입장하기 때문이다.
    // 개설자에게 최초로 message 를 보내는 것은 여기서 하지 않고, master room actor 에서 room actor 를 만들고 난 후 바로 하기로 하자.
    refreshRoomAttenderSet()
  }

  override def receive: Receive = {
    case EnterRoomCommand(userId, roomId, enterRoomModel) ⇒
      val roomTimelineId = Try(RoomDao.enterRoom(userId, roomId, enterRoomModel)) match {
        case scala.util.Success(x) ⇒
          sender() ! x // 이는 http 요청에 대한 응답이다.
          x
        case scala.util.Failure(x) ⇒
          sender() ! akka.actor.Status.Failure(x)
          None
      }
      refreshRoomAttenderSet()
      // 아래는 web socket 으로 broadcast 를 한다.
      // 요청이 실패했다면, 주어질 인자는 none 이 되고, broadcast 를 하지 않게 된다. 따라서, 실패할 것으로 예상되는 동작을 검사하는 경우 응답을 기다려서는 안된다.
      val x = roomAttenderSet
      broadcastRoomTimelineModel(List(roomTimelineId).filter(_.isDefined), x)
    case LeaveRoomCommand(userId, roomId, byWhoId) ⇒
      val roomTimelineId = Try(RoomDao.leaveRoom(userId, roomId, byWhoId)) match {
        case scala.util.Success(x) ⇒
          sender() ! x
          x
        case scala.util.Failure(x) ⇒
          sender() ! akka.actor.Status.Failure(x)
          // 실패했으면, room timeline id 목록은 nil 이 되고, 결국 아무런 message 를 쌓지 않게 된다.
          Nil
      }
      // 퇴장의 경우, 입장자 목록을 갱신하기 전에 broadcast 를 한다. 퇴장자 자신도 퇴장에 관한 알림을 받아야 하기 때문이다.
      // 이와 같이 var 을 val 로서 다루지 않으면 제대로 동작하지 않는다.
      // 퇴장자 자신을 포함한 모두가 퇴장자의 퇴장에 관련된 message 를 전달받도록 하자.
      // 원래 의도는 퇴장자 자신은 퇴장에 관한 room in/out history 만 보고 나머지 참가자가 모든 message 를 보도록 하는 것이었는데,
      // leaveRoom 이 반환하는 room timeline 의 순서를 기대해서는 안된다.
      val x1 = roomAttenderSet
      broadcastRoomTimelineModel(roomTimelineId, x1)
      refreshRoomAttenderSet()

      if (roomAttenderSet.isEmpty) {
        // 방 폐쇄에 관한 응답은 아무도 받지 않는다. 사용자는 자신이 방을 나갔다는 것만 확인할 수 있다.
        // 그런데, 실패한 경우는 있어서는 안된다. 방에 아무도 없는데 방폐쇄가 실패했다면 문제 있는 것이다. 있어서는 안되는 예외이다.
        Try(RoomDao.closeRoom(roomId)) match {
          case scala.util.Failure(x) ⇒
            Logger.getGlobal.info(x.toString)
          // 방폐쇄에 실패한 actor 는 남겨둬야 하나. 일단 남겨두자.
          case scala.util.Success(_) ⇒
            // 방폐쇄 자료처리를 성공했으면, 이 대화방에 관한 actor 를 지운다.
            // PoisonPill 보다 먼저 쌓인 message 는 계속 actor 가 처리하게 되는데, 이 동작들은 database 자료처리 단계에서 전부 실패할 것이지만,
            // actor 자체에 상태를 남겨두고 database 자료처리조차도 수행하지 않도록 하면 어떨까.
            // 일단, 자료처리가 제대로 되고 있다는 것을 확인하기 위해, 실패여부를 database 자료처리에 의하도록 하자.

            // 초대장을 무효로 하는 동작이 실패하더라도 신경쓰지 않는다.
            // 이 단계까지 오는 동안 거쳤던 작업들을 되돌릴 수 없기때문이다.
            Try(InvitationDao.invalidAllInvitationWithRoom(roomId))

            self ! PoisonPill
        }
      }
    case RetrieveRoomAttenderSetCommand(roomId) ⇒
      if (roomId != this.roomId) {
        // 무슨 오류인지 명확히 하지 않았다.
        sender() ! akka.actor.Status.Failure(new RuntimeException)
      }
      // 보낼 자료는 읽기 전용이므로, 문제 될 것 없다.
      sender() ! roomAttenderSet
    case InviteCommand(userId, byWhoId) ⇒
      Try(RoomDao.invite(roomId, userId, byWhoId)) match {
        case scala.util.Success(x) ⇒
          // 아래의 응답은 http 요청에 대한 응답이고, 초대를 하는 사람에게 초대가 성공했음을 알린다.
          sender() ! x
          // 아래에서는, message queue 로 전달한다. 이는 다시 web socket 으로 전달되고, 초대대상자에게 초대가 왔음을 알리게 된다.
          val invitation = EnvelopeModelEx(InvitationDao.getOne(x).map(_.clearPrivacyData()))
          UserDao.getOne(userId).
            flatMap(_.currentLoginHistoryId).
            map(_.toString).map(x ⇒
              MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x)).
            foreach(x ⇒ x ! PushMessageCommand(List(invitation)))
        case scala.util.Failure(x) ⇒ sender() ! akka.actor.Status.Failure(x)
      }

    case TalkCommand(userId, roomId, talk) ⇒
      Try(RoomDao.talk(roomId, userId, talk)) match {
        // 대화보내기의 자료처리에 성공했으면
        case scala.util.Success(x) ⇒
          // 대화를 보낸사람에게 자료처리에 성공했음을 알리고.
          sender() ! x
          // 대화방에 입장해 있는 모든 사람들에게 대화를 전한다.
          val roomAttenderSetX = roomAttenderSet
          broadcastRoomTimelineModel(List(x), roomAttenderSetX)
        case scala.util.Failure(x) ⇒ sender() ! akka.actor.Status.Failure(x)
      }
    case ClearUserBanCommand(userId, roomId, byWhoId) ⇒
      Try(BanDao.clearUserBan(userId, roomId, byWhoId)) match {
        case scala.util.Success(x) ⇒
          sender() ! x
          val roomAttenderSetX = roomAttenderSet
          broadcastRoomTimelineModel(List(x), roomAttenderSetX)
        case scala.util.Failure(x) ⇒
          sender() ! akka.actor.Status.Failure(x)
      }
    case RevokeOwnerCommand(userId, roomId, candidateUserId) ⇒
      Try(RoomDao.revokeOwnerPermission(userId, roomId, candidateUserId)) match {
        case scala.util.Success(x) ⇒
          sender() ! None
          refreshRoomAttenderSet()
          val roomAttenderSetX = roomAttenderSet
          broadcastRoomTimelineModel(x, roomAttenderSetX)
        case scala.util.Failure(x) ⇒
          sender() ! akka.actor.Status.Failure(x)
      }
    case GrantOperatorCommand(ownerId, roomId, userId) ⇒
      Try(OperatorDao.grantOperatorPermission(ownerId, roomId, userId)) match {
        case scala.util.Success(x) ⇒
          sender() ! None
          refreshRoomAttenderSet()
          val roomAttenderSetX = roomAttenderSet
          broadcastRoomTimelineModel(List(x), roomAttenderSetX)
        case scala.util.Failure(x) ⇒
          x.printStackTrace()
          sender() ! akka.actor.Status.Failure(x)
      }
    case RevokeOperatorCommand(userId, roomId, operatorId) ⇒
      Try(OperatorDao.revokeOperatorPermission(userId, roomId, operatorId)) match {
        case scala.util.Success(x) ⇒
          sender() ! None
          refreshRoomAttenderSet()
          val roomAttenderSetX = roomAttenderSet
          broadcastRoomTimelineModel(List(x), roomAttenderSetX)
        case scala.util.Failure(x) ⇒
          sender() ! akka.actor.Status.Failure(x)
      }
    case SetRoomPasswordCommand(byWhoId, roomId, password) ⇒
      Try(RoomDao.setRoomPassword(byWhoId, roomId, password)) match {
        case scala.util.Success(x) ⇒
          sender() ! None
          val roomAttenderSetX = roomAttenderSet
          broadcastRoomTimelineModel(List(x), roomAttenderSetX)
        case scala.util.Failure(x) ⇒
          sender() ! akka.actor.Status.Failure(x)
      }
    case SetRoomNameCommand(roomId, name, byWhoId) ⇒
      Try(RoomDao.setRoomName(roomId, name, byWhoId)) match {
        case scala.util.Success(x) ⇒
          sender() ! None
          val roomAttenderSetX = roomAttenderSet
          broadcastRoomTimelineModel(List(x), roomAttenderSetX)
        case scala.util.Failure(x) ⇒
          sender() ! akka.actor.Status.Failure(x)
      }
    case JustRelayRoomTimelineCommand(roomTimelineIds) ⇒
      val roomAttenderSetX = roomAttenderSet
      broadcastRoomTimelineModel(roomTimelineIds, roomAttenderSetX)
  }

  private[this] def refreshRoomAttenderSet(): Unit = {
    // 여기서 일어나는 예외는, 예상되는 예외가 아니라, 일어나서는 안되는 예외이다. 예를 들면, actor 를 거치지 않고 직접 database 에 접근을 했다던가 하는 식이다.
    // todo 이와 같은 예외의 경우에는, 방을 강제로 폐쇄시키는 방법을 찾아보자.
    Try {
      roomAttenderSet = RoomAttenderDao.retrieveUsersInRoom(roomId).toSet
    }
  }

  private[this] def broadcastRoomTimelineModel(roomTimelineId: List[Option[Int]], roomAttenderSet: Set[RoomAttenderModel]): Unit = {
    if (!roomTimelineId.isEmpty) {
      val roomTimelineModelList = roomTimelineId.filter(_.isDefined).
        // slick 에는 .inSet 이라는 기능이 있다. 왜 쓰지 않는가. room timeline 의 상세를 한번에 구하기 위해서 사용할 sql 이 길어져서
        // 오히려 낭비라 판단하고, room timeline 의 내용을 보고 구해야 할 내용만을 구하는 sql 을 한번 더 실행하는 방식으로 했다.
        // 한번의 sql 로 전부 읽어낼 수 있으면 .inSet 을 쓰는게 좋다.
        map(x ⇒ RoomTimelineDao.getOne(x)).
        // 이와 같이, 비용이 최소가 될수 있도록, message 를 broadcast 하기 직전의 단계에서 clearPrivacyData 를 해줘야 한다.
        // message queue actor 단계에서 하게 되면 broadcast 대상수만큼 반복하게 되어 낭비가 심하다. 누락이 없도록 주의해야 한다.
        map(x ⇒ x.map(_.clearPrivacyData())).
        map(x ⇒ EnvelopeModelEx(x))
      roomAttenderSet.map(_.user.flatMap(_.currentLoginHistoryId)).flatten.map(_.toString).
        map(x ⇒ MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x)).
        foreach(x ⇒ x.!(PushMessageCommand(roomTimelineModelList)))
    }
  }
}

sealed trait RoomActorCommand

case class EnterRoomCommand(userId: Option[Int], roomId: Option[Int], enterRoomModel: Option[EnterRoomModel]) extends RoomActorCommand

// 아래의 command 로 자발적인 퇴장. 강제적인 퇴장을 다룬다. 구상에 따라, 지금 현재로서는 system 에 의한 퇴장을 구분할 수 없다. 구분하지 않기로 했다.
case class LeaveRoomCommand(userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int]) extends RoomActorCommand

case class RetrieveRoomAttenderSetCommand(roomId: Option[Int]) extends RoomActorCommand

case class InviteCommand(userId: Option[Int], byWhoId: Option[Int]) extends RoomActorCommand

case class TalkCommand(userId: Option[Int], roomId: Option[Int], talk: Option[String]) extends RoomActorCommand

case class ClearUserBanCommand(userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int]) extends RoomActorCommand

case class RevokeOwnerCommand(userId: Option[Int], roomId: Option[Int], candidateUserId: Option[Int]) extends RoomActorCommand

case class GrantOperatorCommand(ownerId: Option[Int], roomId: Option[Int], userId: Option[Int]) extends RoomActorCommand

case class RevokeOperatorCommand(ownerId: Option[Int], roomId: Option[Int], userId: Option[Int]) extends RoomActorCommand

case class SetRoomPasswordCommand(byWhoId: Option[Int], roomId: Option[Int], password: Option[String]) extends RoomActorCommand

case class SetRoomNameCommand(roomId: Option[Int], name: Option[String], byWhoId: Option[Int]) extends RoomActorCommand

case class JustRelayRoomTimelineCommand(roomTimelineIds: List[Option[Int]]) extends RoomActorCommand
