/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import org.atmosphere.cpr.AtmosphereResource
import org.scalatra.atmosphere._

import scala.reflect.runtime.{ universe ⇒ ru }

/**
 * Created by i on 4/17/15.
 */
trait AtmosphereClientResourceTrait {
  // AtmosphereClient 에 아래와 같은 줄이 있는데, 이걸 밖에서 볼 방법이 없다. 따라서 임시방편으로 reflection 을 썼다.
  //    @volatile private[atmosphere] var resource: AtmosphereResource = _
  // 아래 기능이 있어야 server 에서 연결을 끊을 수 있다.
  //    http://docs.scala-lang.org/overviews/reflection/environment-universes-mirrors.html
  private val mirror = ru.runtimeMirror(getClass.getClassLoader)
  private val thisMirror = mirror.reflect(this)
  private val x = ru.typeOf[AtmosphereClient].decl(ru.TermName("resource")).asMethod
  private val atmosphereResourceMethod = thisMirror.reflectMethod(x)

  var userLoginHistoryId: Option[Int] = None

  def atmosphereResource(): AtmosphereResource = atmosphereResourceMethod.apply().asInstanceOf[AtmosphereResource]
}

