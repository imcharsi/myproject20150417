/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor

import akka.actor.{ Actor, Props }

import scala.util.{ Failure, Success, Try }

/**
 * Created by Kang Woo, Lee on 4/24/15.
 */
class MasterUserActor extends Actor {
  override def receive: Receive = {
    case CreateUserActorCommand(userLoginHistoryId) ⇒
      Try {
        userLoginHistoryId.map(_.toString).
          map(context.actorOf(Props(new UserActor(userLoginHistoryId, sender())), _))
      } match {
        case Success(x) ⇒ sender() ! None
        case Failure(x) ⇒ sender() ! akka.actor.Status.Failure(x)
      }
  }
}

sealed trait MasterUserActorCommand

case class CreateUserActorCommand(userLoginHistoryId: Option[Int]) extends MasterUserActorCommand

object MasterUserActor {
  val actorName = "user"
  MasterActorSystem.actorSystem.actorOf(Props(new MasterUserActor), actorName)
}
