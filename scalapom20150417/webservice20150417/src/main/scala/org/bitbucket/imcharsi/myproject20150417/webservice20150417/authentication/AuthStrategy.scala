/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.authentication

import javax.servlet.http.{ HttpServletRequest, HttpServletResponse, HttpSession }

import _root_.akka.util.Timeout
import akka.pattern._
import org.atmosphere.cpr.AtmosphereResource
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.CertificationModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.DefaultObjectMapper
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor.{ LoginRequestCommand, MasterActorSystem, MasterLoginActor }
import org.scalatra._
import org.scalatra.auth.{ ScentryConfig, ScentryStrategy, ScentrySupport }
import org.scalatra.servlet.ServletApiImplicits

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{ Failure, Success, Try }

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
class SessionAuthStrategy(override protected val app: ScalatraBase) extends ScentryStrategy[UserModel] with ServletApiImplicits {
  // ServletApiImplicits 가 있어야 app.session.get(...) 와 같은 편리한 문법을 쓸 수 있다.

  //  https://gist.github.com/casualjim/4400115

  override def unauthenticated()(implicit request: HttpServletRequest, response: HttpServletResponse): Unit = {
    super.unauthenticated()
    // 여기서는, session 을 무효로 하기만 하고, session listener 에서 실제 필요한 작업을 하기로 하자.
    app.session.invalidate()
  }

  override def authenticate()(implicit request: HttpServletRequest, response: HttpServletResponse): Option[UserModel] = {
    //    app.session.setMaxInactiveInterval(5) // 이 문장은 잘 된다. 그런데, 만료됐을 때 뭔가를 하고 싶으면 어떻게 하는가.
    // session 만료와는 다르게 session 생성 시점에서는 아무것도 할 것이 없다. 명시적으로 인증절차를 거쳐야 접속 이력을 남길 수 있다.
    // 실제 인증은 여기서 이루어지고, 아래의 toSession 은 다른 용도인 듯 하다.
    // Scentry.scentryAuthKey 가 있는지 확인할 것이 아니라, 앞의 인증단계에서 인증을 똑바로 했다면 남겨놨을 뭔가를 여기서 찾아보아야 한다.
    authenticateWithSession(app.session)
  }

  def authenticateWithSession(session: HttpSession): Option[UserModel] = {
    session.get(AutheticationSupport.userLoginHistoryId).map(_.asInstanceOf[Option[Int]]) match {
      // 인증은 있었는데,
      case x @ Some(_) ⇒ x.flatMap(AutheticationSupport.authMap.get(_)) match {
        // 아무도 강제접속종료를 하지 않았다.
        case x @ Some(_) ⇒ x
        // 누군가가 강제접속종료를 했다.
        case None ⇒
          // 다른 사용자에 의해 강제접속종료됐을 수도 있다. 이번에 강제접속종료된 것을 확인했으니 session 을 무효로 한다.
          // 이 단계에 이르기전까지는 강제접속종료됐어도 session 은 여전히 남아 있는 것이다.
          // 이 단계는, 강제접속종료된 이후 관련 session 을 사용한 또 다른 traffic 에 대한 인증을 시도할 때 이르게 된다.
          // web socket 끊기는 web socket 을 끊을 수 있는 actor 에게 끊을 것을 지시하는 방법으로 해야 한다.
          // 이 모든 절차를 session listener 에서 한다.
          session.invalidate()
          None // 아무것도 없으면 인증을 하지 않았거나, 강제접속종료된 것.
      }
      // 애초에 인증이 없었다.
      case None ⇒ None
    }
  }
}

class LoginAuthStrategy(override protected val app: ScalatraBase) extends ScentryStrategy[UserModel] with ServletApiImplicits with Control {
  // login 을 시도하면서 기다릴 수 있는 시간을 10초로 하자. jvm 과 database 가 예열이 되어 있지 않은 상태라도 10초면 충분할 것이라 본다.
  implicit val timeout = Timeout(10.seconds)

  override def isValid(implicit request: HttpServletRequest): Boolean = {
    // 사용자 계정과 암호를 확인하는 strategy 와 session 만 확인하는 strategy 를 같이 등록해놓고, 여기서 경로를 확인하여, 특정경로일 때만, 특정 strategy 가 동작하도록 하면 되겠다.
    request.requestMethod match {
      case Post ⇒ request.pathInfo == "" && request.getServletPath == f"/${LoginServlet.login}"
      case _ ⇒ false
    }
  }

  override def authenticate()(implicit request: HttpServletRequest, response: HttpServletResponse): Option[UserModel] = {
    val certification = DefaultObjectMapper.readValue[CertificationModel](request.body)
    // client 에서 기능 시험을 하면서 jvm/database 예열 때문에 login 시간이 오래 걸릴 때 인증은 성공했는데 결과는 실패하는 경우가 있다.
    // actor timeout 설정때문인데, 10 초로 정했고 이정도면 충분하다고 본다. 한편, 여기서의 header 는 의도적으로 약간의 시간지연을 걸어서 검사를 하기 위해 쓴다.
    val waitTimeForTest = Try(request.header(AutheticationSupport.waitTimeForTest).filterNot(_.trim().isEmpty).map(_.toInt)).getOrElse(None)
    // 이와 같이 하면, 이전에 비해 뭐가 좋은가.
    // 이전의 방식으로 하면, 동일 사용자 인증의 경우 transaction 과 version 을 사용하여 동시성 제어를 해야 한다.
    // actor 를 사용하면, 동일 사용자 인증은 요청을 줄세운 다음 하나의 actor 에 몰고, 서로 다른 사용자 인증은 각각의 actor 에 분배할 수 있다.
    // 서로 다른 사용자 인증의 경우 각각의 actor 는, 다루려고 하는 자료에 관해 경쟁을 하지 않는다.
    // 한편, 동일 사용자 인증의 경우, 역시 경쟁은 있을 수 없는데, 요청이 순서대로 쌓이기 때문이다. 앞의 요청이 처리되기 전까지는 뒤의 요청은 처리되지 않는다.
    // 따라서, 구현이 좀 더 단순해진다.
    //    MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterLoginActor.actorName) ! LoginRequestCommand(certification, None)
    val future = MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterLoginActor.actorName) ? LoginRequestCommand(certification, None, waitTimeForTest)
    Try(Await.result(future, Duration.Inf)) match {
      case Success(Some(x: UserModel)) ⇒
        app.session.put(AutheticationSupport.userLoginHistoryId, x.currentLoginHistoryId)
        Some(x)
      case Success(None) ⇒ halt(NotFound())
      case Success(_) ⇒ halt(InternalServerError())
      case Failure(x) ⇒ halt(InternalServerError(x))
    }
  }
}

object AutheticationSupport {

  import scala.reflect.runtime.{ universe ⇒ ru }

  val loginKey = ru.typeOf[AutheticationSupport.type].decl(ru.TermName("loginKey")).fullName
  val userLoginHistoryId = ru.typeOf[AutheticationSupport.type].decl(ru.TermName("userLoginHistoryId")).fullName
  val waitTimeForTest = "waitTimeForTest"

  case class LoginKeyType(userLoginHistoryId: Option[Int], userModel: UserModel)

  // 이 concurrent map 은 강제접속종료 문제를 해결하기 위해 쓴다.
  // a session 이 어떻게 b session 을 직접 무효화시킬 수 있는가. java servlet api 수준에서 가능한 방법이 있지 않다면,
  // 이 project 에서 사용하는 우회적인 방법을 쓰는게 낫다고 생각한다.
  val authMap: scala.collection.concurrent.Map[Option[Int], UserModel] = scala.collection.concurrent.TrieMap()
}

trait AutheticationSupport extends ScentrySupport[UserModel] {
  self: ScalatraBase ⇒
  // 왜 이와 같이 하는가. scalatra atmosphere 접속에 대한 인증을 할 방법이 간단하지 않다.
  // atmosphere route 설정 단계에서는 문법적으로는 session 을 구할 방법이 없다. 따라서, web socket 연결시점부터 시작해서 임의 시점에 인증을 시도해야 한다.
  // atmosphere 연결 성공시점에서는, atmosphere resource 를 통해 session 을 구할 수 있지만, scentry 문법을 통해 strategy 를 구할 수가 없다.
  // 따라서, 내부적으로 scentry 를 사용하는 authenticate() 는 동작하지 않는 것이다.
  // 그래서 별개의 인증 method 가 필요한데, 인증 method 를 따로 써도 atmosphere client 안에서는 scentry 를 구할수 없으므로,
  // authenticate() 가 하는 방식으로 인증을 시도할 수는 없는 것이다. 그래서 참조를 바깥에 써두는 것이다.
  val sessionAuthStrategy = new SessionAuthStrategy(self)

  override protected def configureScentry(): Unit = {
    super.configureScentry()
    scentry.unauthenticated {
      scentry.strategies("session").unauthenticated()
    }
  }

  override protected def registerAuthStrategies(): Unit = {
    super.registerAuthStrategies()
    scentry.register("login", _ ⇒ new LoginAuthStrategy(self))
    scentry.register("session", _ ⇒ sessionAuthStrategy)
  }

  override protected def fromSession: PartialFunction[String, UserModel] = {
    // 여기까지 왔다는 말은, 앞서 인증을 거쳤다는 뜻이다.
    case x ⇒
      session.get(AutheticationSupport.userLoginHistoryId).map(_.asInstanceOf[Option[Int]]).flatMap(AutheticationSupport.authMap.get(_)) match {
        case Some(x) ⇒ x
        case _ ⇒ halt(InternalServerError()) // 그런데 아무것도 없으면 예외. 그런데 여기서 그냥 예외만 써도 되나. session 을 무효로 할 필요는 없나.
      }
  }

  override protected def toSession: PartialFunction[UserModel, String] = {
    case x ⇒ x.name.get
  }

  override protected def authenticate()(implicit request: HttpServletRequest, response: HttpServletResponse): Option[UserModel] = {
    super.authenticate() match {
      case None ⇒ halt(Unauthorized())
      case x ⇒ x
    }
  }

  protected def authenticate2()(implicit resource: Option[AtmosphereResource] = None, request: HttpServletRequest, response: HttpServletResponse): Option[UserModel] = {
    // atmosphere resource 가 있으면, web socket 연결에 대한 인증이라고 가정한다.
    // 이 method 를 사용하는 것은, scalatra 가 정한 일반적인 문법에 맞지 않다. 주의해서 써야 한다.
    resource.map(_.session()).
      map(sessionAuthStrategy.authenticateWithSession(_)).
      orElse(Some(authenticate())).flatten
  }

  override protected def scentryConfig: ScentryConfiguration = (new ScentryConfig {}).asInstanceOf[ScentryConfiguration]
}