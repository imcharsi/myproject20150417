/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet

import org.bitbucket.imcharsi.myproject20150417.webservice20150417.authentication.AutheticationSupport
import org.json4s.{ DefaultFormats, Formats, JsonAST, jackson }
import org.scalatra.atmosphere.{ AtmosphereSupport, SimpleJsonWireFormat, WireFormat }
import org.scalatra.json.JacksonJsonSupport
import org.scalatra.{ ScalatraServlet, SessionSupport }

/**
 * Created by i on 4/17/15.
 */
class DefaultScalatraServlet extends ScalatraServlet with JacksonJsonSupport with AtmosphereSupport with SessionSupport with AutheticationSupport {
  override protected implicit def jsonFormats: Formats = DefaultFormats.lossless

  before() {
    contentType = formats("json")
    // 아래와 같이 그냥 쓰면 된다. 편리하다.
    cookies.+=("1", "1")
  }

  val _wireFormat = new JacksonSimpleWireFormat

  override protected implicit def wireFormat: WireFormat = _wireFormat

  // 중요! 내장되어 있는 기능을, AtmosphereClient 안에서 그대로 사용하면 제대로 동작하지 않는다.
  // java.lang.NoSuchMethodError: org.json4s.jackson.JsonMethods$class.render(Lorg/json4s/jackson/JsonMethods;Lorg/json4s/JsonAST$JValue;)Lorg/json4s/JsonAST$JValue;
  // at org.scalatra.atmosphere.JacksonSimpleWireformat.render(JacksonSimpleWireformat.scala:5)
  // 한편, get/put/delete/post 에서는 아무 문제없이 있는 그대로 써도 잘 동작한다.
  class JacksonSimpleWireFormat(implicit val format: Formats) extends SimpleJsonWireFormat with jackson.JsonMethods {
    protected def renderJson(json: JsonAST.JValue): String = compact(render(json)(format))
  }

}
