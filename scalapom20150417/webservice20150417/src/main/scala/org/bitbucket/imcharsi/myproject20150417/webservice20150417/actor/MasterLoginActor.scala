/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor

import akka.actor._
import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.UserLoginHistoryTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.CertificationModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.DurationConstant
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.authentication.AutheticationSupport

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{ Success, Try }

/**
 * Created by Kang Woo, Lee on 4/29/15.
 */
class MasterLoginActor extends Actor {
  // 특정 사용자의 인증을 처리하는 worker actor 가 이미 있는지 여부를 확인한다.
  private val incompleteWorkerMap: scala.collection.mutable.Map[Option[String], ActorRef] = scala.collection.mutable.HashMap()
  // 아래는, 다음과 같은 용도로 쓴다.
  // 먼저, 첫번째 요청을 worker actor 로 보냈고, 응답이 아직 오지 않았다. 다시 동일한 사용자에 관한 요청이 왔고 동일한 worker actor 로 요청을 보냈다.
  // 첫번째 요청에 대한 응답이 왔는데, 이 시점에서 앞서 만들었던 worker actor 를 지워야 할지 말아야 할지 어떻게 판단할 것인가.
  // 응답이 오지 않은 요청이 있다면, 아직 요청을 배정받은 actor 를 지워서는 안된다.
  private val incompleteWorkerQueue: scala.collection.mutable.Map[ActorRef, scala.collection.mutable.Set[Option[String]]] = scala.collection.mutable.HashMap()
  // actorIdGenerator 는 지웠다. 없어도 된다. actor 에게 message 를 보내고 응답이 오길 기다리는 방법이 있는 것을 몰랐다.

  override def receive: Receive = {
    // signal 인자는 test 목적으로만 써야 한다. 일정시점에서 반드시 기다리게 해야 하는데, 그냥은 할 방법이 없다.
    case command @ LoginRequestCommand(x, _, waitTimeForTest, signal) ⇒
      incompleteWorkerMap.get(x.name).map { actorRef ⇒
        signal.get(2).foreach(_.apply())
        // 특정 사용자에 관한 인증을 수행하는 worker actor 가 이미 있으면,
        incompleteWorkerQueue(actorRef).add(x.name)
        // 동일한 자원에 대한 접근의 동시성 제어를 하지 않고, 인증요청을 줄 세운다.
        actorRef ! command.copy(originalSender = Option(sender())) // worker actor 에서 원래 요청에 대한 응답을 직접 보낸다. 따라서 ActorRef 가 필요하다.
      }.orElse {
        // 동일한 자원에 대한 경쟁이 없으므로, 새로운 worker actor 를 만들어서 작업을 분배한다.
        Some {
          // 아래의 줄들에서, var actorIdGenerator ... 를 직접 사용하니, 제대로 동작하지 않는다. 0 으로 전달했는데,
          // 아래의 Complete ... 에서 돌아오는 값은 0 이 아닌 경우가 생긴다.
          // 그래서 val 로 전달하니 잘 된다. 알아두자.
          val actorRef: ActorRef = context.actorOf(Props(new WorkerLoginActor))
          incompleteWorkerMap.put(x.name, actorRef)
          incompleteWorkerQueue.put(actorRef, scala.collection.mutable.Set(x.name))
          actorRef ! command.copy(originalSender = Option(sender()))
        }
      }
    case CompleteResponseCommand(name, signal) ⇒
      // 응답을 기다리고 있는 요청을 지운다. 그런데, 아래의 조건문이 없는 줄들에서는 반드시 성공해야 하는데, 실패할 수 있는가.
      incompleteWorkerQueue.get(sender()).foreach(_.remove(name))
      // 해당 worker actor 에서 처리중이거나 대기중인 요청이 하나도 없으면,
      if (incompleteWorkerQueue.get(sender()).exists(_.isEmpty)) {
        // map 을 정리하고,
        incompleteWorkerQueue.remove(sender())
        incompleteWorkerMap.remove(name)
        // worker actor 도 지운다.
        sender() ! PoisonPill
      }
  }
}

class WorkerLoginActor extends Actor {
  implicit val timeout = Timeout(DurationConstant.tenSeconds)

  override def receive: Actor.Receive = {
    case LoginRequestCommand(certification, originalSender, waitTimeForTest, signal) ⇒
      // 강제접속종료를 원한다면, 시도한다.
      certification.logoutForce.filter(_ == true).foreach { _ ⇒
        // 강제 접속 종료를 할 것을 user actor 에게 요청한다.
        UserDao.getUserByNamePassword(certification.name.get, certification.password.get).
          flatMap(_.currentLoginHistoryId).map(_.toString).
          map(MasterActorSystem.actorSystem / MasterUserActor.actorName / _).
          map(MasterActorSystem.actorSystem.actorSelection(_) ? LogoutForceCommand(true)).
          // 결과를 기다려야 한다. 기다리지 않고 넘어가면 접속종료 자료처리 중에 또 다른 접속을 시도하게 되어,
          // 이미 접속중인 계정에 접속을 시도하는 예외가 생기게 된다.
          foreach(x ⇒ Try(Await.result(x, DurationConstant.tenSeconds)))
        // 성공했든 실패했든 다음의 인증시도로 넘어간다.
      }
      // 인증을 시도해본다.
      // 아래의 auth map 은 어디서나 바꿀 수 있도록 되어 있었다. 결론은, 조심해서 써야 한다.
      login(certification, signal) match {
        case Success(x @ Some(xx)) ⇒
          AutheticationSupport.authMap.put(xx.currentLoginHistoryId, xx)
          // login 단계에서 user actor 를 만든다. actor 를 만드는 단계에서는 web socket 설정을 하지 않는다. fixme 실패하는 경우에 대한 대책이 없다.
          MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterUserActor.actorName) ! CreateUserActorCommand(xx.currentLoginHistoryId)
          // login 단계에서 message queue actor 를 만든다. fixme 실패하는 경우에 대한 대첵이 없다.
          MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName) ! CreateMessageQueueActorCommand(xx.currentLoginHistoryId)
          // 시간 지연이 걸리는 상황을 연출하기 위해 쓴다. 기능적으로는 아무 의미 없다.
          waitTimeForTest.foreach(Thread.sleep(_))
          originalSender.foreach(_ ! x)
        case scala.util.Failure(x) ⇒
          originalSender.foreach(_ ! akka.actor.Status.Failure(x))
      }
      // master actor 입장에서는 성패여부는 중요하지 않다. 인증결과는, 최초로 요청을 보낸 쪽으로 알렸기 때문이다.
      sender() ! CompleteResponseCommand(certification.name, signal)
  }

  private def login(certification: CertificationModel, signal: Map[Any, () ⇒ Unit]): Try[Option[UserModel]] = {
    val action = UserDao.loginAction(certification.name.get, certification.password.get).
      flatMap { userLoginHistoryId ⇒
        val step1: DBIOAction[Option[Option[UserModel]], NoStream, Effect.Read] =
          UserLoginHistoryTable.filter(t ⇒ t.id === userLoginHistoryId).
            joinLeft(UserTable).on(_.userId === _.id).
            map { case (_, a) ⇒ a }.result.headOption
        step1.map { x ⇒
          signal.get(1).foreach(_.apply())
          x.flatten
        }
      }
    val future = DriverSettings.database.run(action.transactionally)
    Try(Await.result(future, Duration.Inf))
  }
}

object MasterLoginActor {
  val actorName = "masterLoginActor"
  MasterActorSystem.actorSystem.actorOf(Props(new MasterLoginActor), actorName)
}

/*object WorkerLoginActor {
  // 아래 동시성 map 을 언제 어디서나 다룰 수 있게 하면, 실수로라도 값을 바꿀 가능성이 있다.
  // session listener 에서 auth map 을 다뤄야 한다. 그냥 어디서나 map 을 다룰 수 있게 하는 것이 편하겠다.
  // 그래서, 앞서 써뒀던 map 을 그냥 쓴다.
  //  private val authMap: scala.collection.concurrent.Map[Option[Int], UserModel] = scala.collection.concurrent.TrieMap()
}*/

sealed trait LoginCommand

case class LoginRequestCommand(certification: CertificationModel, originalSender: Option[ActorRef], waitTimeForTest: Option[Int] = None, signal: Map[Any, () ⇒ Unit] = Map.empty) extends LoginCommand

case class CompleteResponseCommand(name: Option[String], signal: Map[Any, () ⇒ Unit] = Map.empty) extends LoginCommand
