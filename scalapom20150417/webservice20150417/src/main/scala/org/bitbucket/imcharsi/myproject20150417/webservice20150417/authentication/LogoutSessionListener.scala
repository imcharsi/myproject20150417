/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.authentication

import javax.servlet.http.{ HttpSessionEvent, HttpSessionListener }

import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.DurationConstant
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor.{ LogoutForceCommand, MasterActorSystem, MasterUserActor }
import org.scalatra.Control
import org.scalatra.servlet.ServletApiImplicits

import scala.concurrent.Await
import scala.util.Try

/**
 * Created by Kang Woo, Lee on 4/22/15.
 */
class LogoutSessionListener extends HttpSessionListener with ServletApiImplicits with Control {
  implicit val timeout = Timeout(DurationConstant.tenSeconds)
  //  http://www.mkyong.com/servlet/a-simple-httpsessionlistener-example-active-sessions-counter/
  override def sessionDestroyed(se: HttpSessionEvent): Unit = {
    // 모든 필요한 처리를 하도록 user actor 에게 알린다.
    // 인증여부는 auth map 으로 확인한다.
    se.getSession.get(AutheticationSupport.userLoginHistoryId).
      map(_.asInstanceOf[Option[Int]]).
      // 이미 접속 종료처리가 되었다면 auth map 에 아무것도 없게 된다. 이 상태는 servlet session 만 남아있을 뿐 아무것도 남아있지 않은 상태이다.
      // 그래서 auth map 에 아무것도 없으면 아무것도 하지 않는다. 이미 앞에서 auth map 을 지우면서 다 정리했기 때문이다.
      filter(x ⇒ AutheticationSupport.authMap.contains(x)).
      flatMap(_.map(_.toString)).
      map(MasterActorSystem.actorSystem / MasterUserActor.actorName / _).
      map(MasterActorSystem.actorSystem.actorSelection(_) ? LogoutForceCommand(false)).
      foreach(x ⇒ Try(Await.result(x, DurationConstant.tenSeconds)))
  }

  override def sessionCreated(se: HttpSessionEvent): Unit = {
  }
}
