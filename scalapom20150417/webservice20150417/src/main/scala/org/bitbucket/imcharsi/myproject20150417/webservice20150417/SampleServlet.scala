/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor.MasterActorSystem
import org.scalatra.{ AsyncResult, FutureSupport, ScalatraServlet }

import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }

/**
 * Created by Kang Woo, Lee on 5/2/15.
 */
class SampleServlet extends ScalatraServlet with FutureSupport {
  // scalatra akka 연습용으로 썼다. 참고하자.

  override protected implicit def executor: ExecutionContext = MasterActorSystem.actorSystem.dispatcher

  implicit val timeout = Timeout(1.day)

  before() {
    contentType = "application/json"
  }

  get("/async")(async)

  private def async(): AsyncResult = {
    val future = MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / SampleActor.sampleActor) ? TestRequest(Some(1))
    SampleAsyncResult(future)
  }

  private case class SampleAsyncResult(val future: Future[_]) extends AsyncResult {
    override val is: Future[_] = {
      //      Promise().failure(new RuntimeException).future
      future
    }
  }

}

object SampleServlet {
  val sampleServlet = "sampleServlet"
}
