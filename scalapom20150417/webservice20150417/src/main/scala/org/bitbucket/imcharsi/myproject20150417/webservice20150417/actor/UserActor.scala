/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor

import akka.actor.{ Actor, ActorRef, PoisonPill }
import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current.{ InvitationDao, RoomAttenderDao }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.history.{ UserLoginHistoryDao, UserNickHistoryDao }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnvelopeModel, EnvelopeModelEx, LogoutModel }
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.authentication.AutheticationSupport
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.{ AtmosphereClientResourceTrait, DefaultObjectMapper, DurationConstant, ModelOne }
import org.json4s._
import org.scalatra.atmosphere.{ AtmosphereClient, JsonMessage, TextMessage }

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.{ Failure, Success, Try }

/**
 * Created by Kang Woo, Lee on 4/24/15.
 */
class UserActor(userLoginHistoryId: Option[Int], originalSender: ActorRef, signal: Map[Int, () ⇒ Unit] = Map.empty) extends Actor {
  implicit val formats = DefaultFormats.lossless
  implicit val timeout = Timeout(DurationConstant.tenSeconds)
  private[this] var atmosphereClient: Option[AtmosphereClient with AtmosphereClientResourceTrait] = None

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    super.postStop()
    //    println(getClass.getName + ":postStop")
    // user actor 를 종료할 때 auth map 을 바꾼다. logout 자료처리를 끝내자마자 auth map 을 바꾸니 다음과 같은 것이 또 문제된다.
    // 현재 android client 는 web socket 을 통해 envelope model 을 받는 즉시 응답을 하도록 되어 있다.
    // 그런데, auth map 을 빨리 지우는 방법으로 하면, http method 를 사용하여 완료응답을 하는 시점에서 인증을 실패하는 동시에,
    // session 무효화 단계를 거치게 된다. 관련된 내용은 SessionAuthStrategy 이다.
    // 그래서 web socket 의 동작에 영향을 주는 session 이 무효화되면서 web socket 도 더이상 동작을 하지 않게 된다.
    // 결론은, auth map 을 최대한 늦게 지운다.
    AutheticationSupport.authMap.remove(userLoginHistoryId)
    Try(atmosphereClient.foreach(_.atmosphereResource().close()))
  }

  override def receive: Receive = {
    //    http://stackoverflow.com/questions/23348480/json4s-convert-type-to-jvalue
    case x @ ModelOne(_, _) ⇒ atmosphereClient.foreach(_.send(JsonMessage(Extraction.decompose(x))))
    case LogoutForceCommand(force) ⇒ {
      val userId = UserLoginHistoryDao.getOne(userLoginHistoryId).flatMap(_.userId)
      val userModel = UserDao.getOne(userId)

      Try(UserDao.logoutForce(userModel.flatMap(_.name).get, userModel.flatMap(_.password).get)) match {
        case Success(x) ⇒
          // logout 단계에서 web socket 으로 message 보낼 때는, message queue buffering 기능을 끈다.
          // message 가 쌓일 때마다 web socket 전송을 시작한다. 이는 logout 단계에서 message 를 즉시 보내기 위한 편의기능이다.
          // 이 기능이 없으면, message 가 전부 쌓일거라 예상되는 시각에 맞춰서 self awakening 을 보내줘야 하는 문제가 생긴다. 예상을 하게 된다.
          // 지금 상황에서는 완료응답이 오지 않으므로 buffering 기능을 끄면, message 가 쌓일 때마다, 앞에서 보냈지만 완료응답을 받지 못한 message 들이 중복해서 전송된다.
          // 이는 약간의 낭비인데, 지금까지의 구상을 놓고 보면 다른 대안이 없다. logout 처리 단계의 시간제한인 3초동안만 이와 같이 동작하므로 큰 부담은 없을 것이다.
          userLoginHistoryId.filter(_ ⇒ force).
            map(_.toString).
            map(x ⇒ MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x).
            map(x ⇒ MasterActorSystem.actorSystem.actorSelection(x)).
            foreach(x ⇒ x ! ChangeBufferingModeCommand(false))
          // 현재 참가중인 모든 대화방에서 퇴장한다. logout 단계에서 퇴장하는 외에 강제접속종료 단계에서도 똑같이 퇴장을 하도록 해야 한다.
          // 두 장소에서 user actor 에게 logout force command 요청을 보내면 된다.
          RoomAttenderDao.retrieveRoomsThatUserAttended(userId).
            flatMap { roomAttenderModel ⇒
              roomAttenderModel.roomId.map(_.toString).map { x ⇒
                // system 에 의한 퇴장은 일단 구분하지 말자.
                MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ? LeaveRoomCommand(userId, roomAttenderModel.roomId, None)
              }
            }.map { x ⇒
              Await.result(x, Duration.Inf)
            }
          // 접속종료를 알린다.
          // 문제는, session 이 무효가 되면, web socket 에 의한 자료 전송도 가능하지 않다는 점이다.
          // 이와 같이 접속종료를 알리는 이유는, 접속종료가 되었다는 것을 사용자가 모를 수 있기 때문이다.
          // 자발적으로 접속종료를 하는 사용자에게 접속종료를 알려야 할 필요는 없다.
          // 자발적인 접속종료를 할때는 session 을 무효화하는 데 이어 session listener 가 logout 요청을 user actor 에게 보내게 되는데,
          // user actor 가 요청을 받은 시점에서는 이미 session 이 무효로 되었기 때문에, web socket 을 이용한 작업이 가능하지 않다.
          // 결론은, 강제접속종료인 경우에만 접속종료를 알리도록 한다.
          userLoginHistoryId.filter(_ ⇒ force).
            map(_.toString).
            map(x ⇒ MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x).
            map(x ⇒ MasterActorSystem.actorSystem.actorSelection(x)).
            // 지워야 할 개인정보가 포함되지 않는다.
            foreach(x ⇒ x ! PushMessageCommand(List(EnvelopeModelEx(Some(LogoutModel(userId))))))

          // 아주 복잡한 상황이다. 설명이 필요하다.
          // 강제접속종료를 client 에게 알려야 한다. 그런데, 이전의 방법대로 하면, message queue 와 web socket 이 너무 빨리 닫히기 때문에 알릴수가 없다.
          // 우선, web socket 자료전송을 하기 위해 거치는 단계는 다음과 같음을 이해해야 한다.
          // 필요한 message 를 message queue actor 에 쌓은 후, message queue actor 에게 web socket 자료 전송을 해 줄것을 명시적으로 요청한다.
          // message queue actor 는 다시 user actor 에게 request withdraw 요청을 보내게 되고, user actor 는 이어서 message queue actor 에게 get message 요청을 보내게 된다.
          // 이어서 오는 응답을 사용하여 web socket 으로 자료전송을 시작하게 된다.
          // 여기서, 너무 빨리 message queue actor 와 web socket 을 닫으면 안되므로 약간의 시간지연이 필요한데, 이를 위해 thread.sleep 를 사용할 수는 없다.
          // 왜냐하면, thread.sleep 때문에, message queue actor 가 보내는 request withdraw 요청에 응답을 빨리 할 수 없기 때문이다.
          // thread.sleep 로 인해서, actor 는 sleep 를 하고 있는 thread 에 의해 block 되었다. 그래서 요청에 응답을 할 수 없는 것이다.
          // 또한, message queue actor 에게 self awakening 을 보낸 후, 이어서 바로 quit message queue 요청을 보내어서도 안되는데,
          // message queue 는 request withdraw 요청을 user actor 에게 보낸 후, user actor 로부터 get message 요청을 받아야 하기때문이다.
          // 최소한 get message 요청을 받기 전까지는 quit message queue 요청이 쌓여서는 안되는 것이다.
          // 또한, 실질적인 종료처리가 완료되기전까지는 완료했음을 요청자에게 알려서도 안된다.
          // 역시, 스스로를 바로 종료시켜서도 안되는데, user actor 는 message queue actor 로부터 request withdraw 요청을 받아야 하기 때문이다.
          // 그래서 대안은 다음과 같다.
          // 우선, 필요한 message 를 쌓는다. message queue actor 에게 self awakening 요청을 보낸다.
          // 3초 뒤에 message queue actor 에게 quit message queue 요청이 전달되도록 일정을 정한다.
          // 3초 뒤에 요청자에게 응답을 보내도록 일정을 정한다. 3초 뒤에 스스로에게 정지 요청을 보내도록 일정을 정한다.
          // 이와 같이 하면, 3초가 지나기 전에, message queue actor 가 self awakening 에 대한 응답으로 user actor 에게 request withdraw 요청을 보내게 되고,
          // 다시 user actor 는 message queue actor 에게 get message 요청을 보내게 되며, 이에 대한 응답을 받아서 user actor 는 web socket 자료 전송을 시작하게 된다.
          // 3초가 지나기 전에 이 모든 과정이 끝날 것으로 기대한다.

          // message queue actor 도 남아있으면 안 된다. 그러나 바로 요청을 보내서는 안된다.
          userLoginHistoryId.map(_.toString).
            map(x ⇒ MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x).
            map(x ⇒ MasterActorSystem.actorSystem.actorSelection(x).resolveOne()).
            foreach(x ⇒ x.map(x ⇒ MasterActorSystem.actorSystem.scheduler.scheduleOnce(DurationConstant.threeSeconds, x, QuitMessageQueueActorCommand())))
          // 요청자에게 응답한다. 이 요청은 자발적 접속종료에 의해 session listener 에서 할수도 있고, 강제접속종료에 의해 worker login actor 에서 할 수도 있다.
          // 바로 요청을 보내서는 안된다.
          // 아래와 같이 val 로 하지 않고 그냥 sender() 라고 쓰니깐 잘 안된다.
          val senderVal = sender()
          // 강제접속종료의 경우, 이전 접속 사용자에게 web socket message 를 전달할 시간을 주기 위해 다음 사용자가 기다리도록 하는 것이다.
          // 따라서, 강제접속종료가 아니면 접속을 시도하는 사용자를 기다리게 할 필요가 없다.
          // 또한, 자발적인 접속종료를 시도하는 사용자를 기다리게 할 필요도 없다.
          // 결론은, 실행이 여기를 지나가는 경우는, 강제접속종료로 인해 이전 접속사용자의 접속을 끊어야 할 때와 자발적인 접속종료를 시도할 때 두가지이다.
          // 강제접속종료에 이은 접속을 시도한다 하더라도, 여기에 이르기 전 단계인 WorkerLoginActor 에서 이전 접속 사용자의 존재여부를 확인하여,
          // 이전 접속 사용자가 없으면 강제접속종료를 시도하지 않게 되고, 따라서 기다리지 않게 된다.
          if (force) {
            MasterActorSystem.actorSystem.scheduler.scheduleOnce(DurationConstant.threeSeconds, senderVal, None)
          } else {
            senderVal ! None
          }
          // actor 가 남아있으면 안된다. 몰랐는데, 여러개의 test 를 이어서 할때는 하나의 jvm 에서 실행되는 듯 하다.
          // 그래서 MasterActorSystem object 가 한번만 실행되는듯 하다.
          // test 단계마다 만들었던 user actor 가 매 test 이후 없어지지 않고 계속 남아있는 것이다.
          // web socket 은 actor 종료단계에서 끊어지도록 한다. 정지 요청 역시 바로 보내서는 안된다.
          MasterActorSystem.actorSystem.scheduler.scheduleOnce(DurationConstant.threeSeconds, self, PoisonPill)
          // 모든 초대장을 무효로 한다.
          // 실패하더라도 상관하지 않는다.
          Try(InvitationDao.invalidAllInvitationWithUser(userId))
        case Failure(x) ⇒
          // 강제 접속종료에 관한 자료처리에 실패했으면 아무것도 하지 않는다.
          sender() ! akka.actor.Status.Failure(x)
      }
    }
    case RequestWithdrawCommand() ⇒
      atmosphereClient.foreach { atmosphereClient ⇒
        userLoginHistoryId.map(_.toString).
          map(x ⇒ MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x) ? GetMessageQueueCommand()).
          map(Await.result(_, Duration.Inf)).
          map(_.asInstanceOf[List[EnvelopeModel[_]]]).
          map(DefaultObjectMapper.writeValueAsString(_)).
          //          map { x ⇒ println(getClass.getName + ":" + x); x }.
          // 실패할 수 있다. 실패해도 무시한다. 실패할 수 있는 경우로, 자발적인 접속종료에서 session listener 가 logout 요청을 user actor 에게 보내는 경우이다.
          // user actor 가 요청을 받아서 처리를 시작하는 시점에서는 session 은 이미 무효로 되었다. session 이 필요한 web socket 동작은 더이상 가능하지 않다.
          //          foreach(x ⇒ println(Try(atmosphereClient.send(TextMessage(x)))))
          foreach(x ⇒ Try(atmosphereClient.send(TextMessage(x))))
      }
    // todo 여기서 설정한 후 이후의 전송 과정에서 문제가 있으면 web socket 을 끊고, 참조를 초기화 할 것인가, 아니면 그냥 놔둘 것인가. 일단은, 그냥 놔두자.
    case x @ ChangeAtmosphereClientCommand(newAtmosphereClient) ⇒
      Try(atmosphereClient.foreach(_.atmosphereResource().close()))
      atmosphereClient = newAtmosphereClient
      // 요청자인 UserAtmosphereClient 는 반드시 응답을 받아야 한다. 응답을 받지 못하면 user actor 가 없다고 보고 web socket 을 끊는다.
      sender() ! None
    // web socket 이 바뀌거나 설정되더라도 그간 쌓여있는 message 를 자동으로 다시 보내주지는 않기로 하자.
    case SetCurrentNickCommand(userId, nick) ⇒
      Try(UserDao.setCurrentNick(userId, nick)) match {
        case Success((userNickHistoryId, ids)) ⇒
          sender() ! userNickHistoryId
          val list = UserNickHistoryDao.getOne(userNickHistoryId).map(_.clearPrivacyData()).map(x ⇒ EnvelopeModelEx(Some(x))).toList
          userLoginHistoryId.map(_.toString).
            map(x ⇒ MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterMessageQueueActor.actorName / x)).
            foreach(x ⇒ x.!(PushMessageCommand(list)))
          // 이 사용자가 참가중인 모든 대화방에, 대화명이 바뀌었음을 알리는 room timeline 을 알려준다.
          // 각 대화방별로 한개씩 room timeline 이 만들어졌으므로, 알릴때도 각 대화방에 관련된 room timeline 만 알려줘야 한다.
          ids.map {
            case ((roomTimelineId, roomId)) ⇒
              roomId.map(_.toString).map { x ⇒
                MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterRoomActor.actorName / x) ?
                  JustRelayRoomTimelineCommand(List(roomTimelineId))
              }
          }
        case Failure(x) ⇒ sender() ! akka.actor.Status.Failure(x)
      }
  }
}

sealed trait UserActorCommand

case class LogoutForceCommand(force: Boolean) extends UserActorCommand

case class RequestWithdrawCommand() extends UserActorCommand

case class ChangeAtmosphereClientCommand(atmosphereClient: Option[AtmosphereClient with AtmosphereClientResourceTrait]) extends UserActorCommand

case class SetCurrentNickCommand(userId: Option[Int], nick: Option[String]) extends UserActorCommand
