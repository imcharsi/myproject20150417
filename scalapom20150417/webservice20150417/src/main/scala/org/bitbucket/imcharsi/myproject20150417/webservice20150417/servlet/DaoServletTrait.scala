/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet

import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.mixin.Dao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.DefaultObjectMapper

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
trait DaoServletTrait[ModelType <: IdVersionModelTrait[ModelType], DaoType <: Dao[ModelType]] extends DaoServlet {

  type Validator1 = ModelType ⇒ Boolean
  type Validator2 = (ModelType, ModelType) ⇒ Boolean
  type IdPicker = () ⇒ Option[Int]

  //  http://stackoverflow.com/questions/25285865/scala-no-manifest-available-for-type-t
  protected def getEach()(implicit manifest: Manifest[ModelType], dao: DaoType, validator: Validator1, idPicker: IdPicker): Option[ModelType] = {
    dao.getOne(idPicker()).filter(validator(_))
  }

  protected def postEach()(implicit manifest: Manifest[ModelType], dao: DaoType, validator: Validator1): Option[Int] = {
    DefaultObjectMapper.readValue[Option[ModelType]](request.body).filter(_.id.isEmpty).
      map(_.initVersion()).filter(validator(_)).flatMap(dao.postOne(_))
  }

  protected def putEach()(implicit manifest: Manifest[ModelType], dao: DaoType, validator1: Validator1, validator2: Validator2, idPicker: IdPicker): Option[Int] = {
    DefaultObjectMapper.readValue[Option[ModelType]](request.body).filter(_.id.isDefined).filter(x ⇒ getEach.exists(validator2(x, _))).
      map(dao.putOne(_))
  }

  protected def deleteEach()(implicit manifest: Manifest[ModelType], dao: DaoType, validator: Validator1, idPicker: IdPicker): Option[Int] = {
    getEach.filter(validator(_)).map(x ⇒ dao.deleteOne(x.id, paramVersion))
  }

  protected def setupPath(prefix: String, idWord: String)(implicit manifest: Manifest[ModelType], dao: DaoType, validator1: Validator1, validator2: Validator2, idPicker: IdPicker): Unit = {
    get(f"/${prefix}:${idWord}", testAcceptHeader)(optionResultWrapper(getEach))
    post(f"/${prefix}", testContentTypeHeader)(optionResultWrapper(postEach))
    put(f"/${prefix}:${idWord}", testContentTypeHeader)(optionResultWrapper(putEach))
    delete(f"/${prefix}:${idWord}")(optionResultWrapper(deleteEach))
  }
}
