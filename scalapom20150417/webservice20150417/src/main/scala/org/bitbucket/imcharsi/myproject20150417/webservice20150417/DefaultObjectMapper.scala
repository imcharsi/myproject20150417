/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import java.util.TimeZone

import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.core.{ JsonGenerator, JsonParser }
import com.fasterxml.jackson.databind._
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.bitbucket.imcharsi.myproject20150417.model20150417.CustomEnum
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModel.EnvelopeDataType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModel.EnvelopeDataType._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.InvitationModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.BanHistoryModel.BanType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomTimelineModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.UserNickHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnvelopeModel, EnvelopeModelEx, LogoutModel, SayHiModel }

import scala.collection.JavaConversions

/**
 * Created by i on 4/17/15.
 */
object DefaultObjectMapper extends ObjectMapper with ScalaObjectMapper {

  // json4s 의 EnumSerializer 를 써봤는데, case class 의 constructor val 중,
  // Option[Enum] 의 경우, serialize 만 잘되고 deserialize 가 안된다. 그냥 Enum 은 잘 된다.
  // 그래서 jackson 으로 해봤다.
  //http://manuel.bernhardt.io/2011/11/01/writing-custom-deserializers-for-jerkson/
  //http://stackoverflow.com/questions/18561689/custom-json-serialization-of-structured-scala-case-classes
  //http://wiki.fasterxml.com/JacksonHowToCustomDeserializers
  //http://wiki.fasterxml.com/JacksonHowToCustomSerializers

  // 아래와 같이 하면 중복을 조금 줄일 수 있다.
  trait JsonSerializerUtil[T <: CustomEnum] extends JsonSerializer[T] {
    override def serialize(value: T, jgen: JsonGenerator, provider: SerializerProvider): Unit = {
      jgen.writeNumber(value.id)
    }
  }

  class JsonDeserializerUtil[T <: CustomEnum](enum: Int ⇒ T) extends JsonDeserializer[T] {
    override def deserialize(jp: JsonParser, ctxt: DeserializationContext): T = {
      enum.apply(jp.getIntValue)
    }
  }

  object EnterTypeSerializer extends JsonSerializerUtil[EnterType]

  object EnterTypeDeserializer extends JsonDeserializerUtil[EnterType](EnterType.apply)

  object BanTypeSerializer extends JsonSerializerUtil[BanType]

  object BanTypeDeserializer extends JsonDeserializerUtil(BanType.apply(_))

  object PermissionTypeSerializer extends JsonSerializerUtil[PermissionType]

  object PermissionTypeDeserializer extends JsonDeserializerUtil(PermissionType.apply(_))

  object EnvelopeDataTypeSerializer extends JsonSerializerUtil[EnvelopeDataType]

  object EnvelopeDataTypeDeserializer extends JsonDeserializerUtil[EnvelopeDataType](EnvelopeDataType.apply)

  registerModule(DefaultScalaModule)
  setDateFormat(new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))
  setTimeZone(TimeZone.getTimeZone("UTC"))
  val module = new SimpleModule("custom")
  module.addSerializer(classOf[EnterType], EnterTypeSerializer)
  module.addDeserializer(classOf[EnterType], EnterTypeDeserializer)
  module.addSerializer(classOf[BanType], BanTypeSerializer)
  module.addDeserializer(classOf[BanType], BanTypeDeserializer)
  module.addSerializer(classOf[PermissionType], PermissionTypeSerializer)
  module.addDeserializer(classOf[PermissionType], PermissionTypeDeserializer)
  module.addSerializer(classOf[EnvelopeDataType], EnvelopeDataTypeSerializer)
  module.addDeserializer(classOf[EnvelopeDataType], EnvelopeDataTypeDeserializer)

  registerModule(module)
  // http://stackoverflow.com/questions/11757487/how-to-tell-jackson-to-ignore-a-field-during-serialization-if-its-value-is-null
  setSerializationInclusion(Include.NON_NULL)

  def deserializeEnvelopeList(jsonNode: JsonNode): List[EnvelopeModel[_]] = {
    val map = Map[Int, (JsonNode, Int) ⇒ EnvelopeModel[_]](
      (UserModelType.id, (x, y) ⇒ EnvelopeModelEx(Option(DefaultObjectMapper.treeToValue[UserModel](x))).copy(id = Option(y))),
      (InvitationModelType.id, (x, y) ⇒ EnvelopeModelEx(Option(DefaultObjectMapper.treeToValue[InvitationModel](x))).copy(id = Option(y))),
      (SayHiModelType.id, (x, y) ⇒ EnvelopeModelEx(Option(DefaultObjectMapper.treeToValue[SayHiModel](x))).copy(id = Option(y))),
      (LogoutModelType.id, (x, y) ⇒ EnvelopeModelEx(Option(DefaultObjectMapper.treeToValue[LogoutModel](x))).copy(id = Option(y))),
      (UserNickHistoryModelType.id, (x, y) ⇒ EnvelopeModelEx(Option(DefaultObjectMapper.treeToValue[UserNickHistoryModel](x))).copy(id = Option(y))),
      (RoomTimelineModelType.id, (x, y) ⇒ EnvelopeModelEx(Option(DefaultObjectMapper.treeToValue[RoomTimelineModel](x))).copy(id = Option(y))))
    JavaConversions.asScalaIterator(jsonNode.iterator()).
      map { x ⇒
        // JsonNode.iterator() 는 값 node 를 구해주는 듯 하다?
        val iterator = JavaConversions.asScalaIterator(x.iterator())
        val iterator2 = JavaConversions.asScalaIterator(x.fieldNames())
        // json 으로 주어지는 각 값의 순서에 기대면 안된다.
        val jsonMap = iterator2.toList.zip(iterator.toList).toMap
        map(jsonMap("dataType").asInt()).apply(jsonMap("data"), jsonMap("id").asInt())
      }.toList
  }
}
