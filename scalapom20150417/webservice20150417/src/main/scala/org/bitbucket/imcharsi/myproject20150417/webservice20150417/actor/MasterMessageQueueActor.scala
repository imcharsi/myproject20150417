/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor

import akka.actor.{ Actor, Props }

/**
 * Created by Kang Woo, Lee on 5/1/15.
 */
class MasterMessageQueueActor extends Actor {
  override def receive: Receive = {
    case CreateMessageQueueActorCommand(userLoginHistoryId) ⇒
      userLoginHistoryId.map(_.toString).map { x ⇒
        context.actorOf(Props(new MessageQueueActor(userLoginHistoryId)), x)
      }
  }
}

sealed trait MasterMessageQueueActorCommand

case class CreateMessageQueueActorCommand(userLoginHistoryId: Option[Int]) extends MasterMessageQueueActorCommand

object MasterMessageQueueActor {
  val actorName = "masterMessageQueueActor"
  MasterActorSystem.actorSystem.actorOf(Props(new MasterMessageQueueActor), actorName)
}
