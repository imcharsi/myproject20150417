/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417

import akka.pattern._
import akka.util.Timeout
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor.{ LogoutForceCommand, MasterActorSystem, MasterUserActor }
import org.scalatra.{ ActionResult, InternalServerError, Ok, ScalatraServlet }

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Try

/**
 * Created by Kang Woo, Lee on 5/27/15.
 */
class AdminServlet extends ScalatraServlet {
  implicit val timeout = Timeout(10.seconds)
  delete(f"/${AdminServlet.logoutForce}")(deleteLogoutForceAllUsers)

  // android 검사를 편하게 하기 위해서 쓴다. server 의 사용자 접속을 전부 끊는다.
  private def deleteLogoutForceAllUsers(): ActionResult = {
    UserDao.getAllLoginUsers().flatMap(_.currentLoginHistoryId).map(_.toString).
      map(MasterActorSystem.actorSystem / MasterUserActor.actorName / _).
      map(MasterActorSystem.actorSystem.actorSelection(_) ? LogoutForceCommand(true)).
      map(x ⇒ Try(Await.result(x, DurationConstant.tenSeconds))).forall(_.isSuccess) match {
        case true ⇒ Ok()
        case false ⇒ InternalServerError()
      }
  }
}

object AdminServlet {
  val admin = "admin"
  val logoutForce = "logoutForce"
}
