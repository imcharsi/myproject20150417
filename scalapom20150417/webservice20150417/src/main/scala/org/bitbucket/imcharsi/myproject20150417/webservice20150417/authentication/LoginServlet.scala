/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.authentication

import org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet.DaoServlet
import org.scalatra.{ ActionResult, Ok }

/**
 * Created by Kang Woo, Lee on 4/22/15.
 */
class LoginServlet extends DaoServlet {
  get("/") {
    Ok()
  }
  post("/", testContentTypeHeader)(innerPost)
  delete("/")(innerDelete) // 일단은, login 을 성공한 이후에 동일한 session 에서 /login 에 대해 delete 를 수행하면 logout 을 하는 것으로 하자. 또 바꾸면 된다.

  private def innerPost(): ActionResult = {
    authenticate()
    Ok()
  }

  private def innerDelete(): ActionResult = {
    val userModel = authenticate()
    // 접속종료에서 필요한 모든 자료처리는 user actor 에서 한다. user actor 에게 요청을 보내는 것은 session listener 에서 한다.
    logOut()
    Ok()
  }

}

object LoginServlet {
  val login = "login"
}
