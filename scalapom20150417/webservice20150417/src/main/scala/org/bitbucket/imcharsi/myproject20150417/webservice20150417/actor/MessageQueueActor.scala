/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor

import akka.actor.{ Actor, Cancellable, PoisonPill }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModel
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.actor.MasterActorSystem.actorSystem.dispatcher

import scala.collection.immutable.Queue
import scala.concurrent.duration._

/**
 * Created by Kang Woo, Lee on 5/1/15.
 */
class MessageQueueActor(userLoginHistoryId: Option[Int], waitSeconds: Int = 10) extends Actor {
  // 동시성 제어는 할 필요가 없다고 본다. actor message 는 줄세우기가 자동으로 이루어진다고 알고 있다.
  private[this] var messageQueue: Queue[EnvelopeModel[_]] = Queue.empty
  private[this] var cancellable: Option[Cancellable] = None
  private[this] var idGenerator = 0 // fixme 범위를 크게 하는 것이 좋겠지만, 일단 int 형으로 두자.
  private[this] var useBuffering = true

  override def receive: Receive = {
    case PushMessageCommand(x, defer) ⇒
      // var 을 직접 쓰면, 이상하게 동작할때가 있다.
      x.foreach { x ⇒
        val id = idGenerator
        // 누락의 위험이 있더라도, 여기서 clearPrivacyData 를 해서는 안된다.
        // message 100 개가 broadcast 된다면 100번의 clearPrivacyData 호출이 있게 된다.
        // broadcast 에 앞서서 clearPrivacyData 를 한번만 해주면 되는 것이다. 비용이 더 크다.
        // 따라서 message 를 쌓아야 할 필요가 있을 때마다 주의하는 것이 좋겠다.
        messageQueue = messageQueue.enqueue(x.copy(id = Some(id)))
        idGenerator = idGenerator + 1
      }
      // 바로 보내지않고 모아서 보내기 위해 일부러 지연을 걸었다면,
      if (defer) {
        // 걸어놓은 self awakening 을 풀고
        cancellable.foreach(_.cancel())
        // 다시 self awakening 을 건다. 계속 message 를 지연전송방식으로 보내면 계속 self awakening 의 발동시각이 밀리게 된다.
        cancellable = Some(MasterActorSystem.actorSystem.scheduler.scheduleOnce(waitSeconds.seconds, self, SelfAwakeningCommand()))
      }
      // 앞서 알림시각을 정했다면, 그대로 쓴다. 알림시각이 정해진 이후부터 알림이 일어나기 전까지 계속해서 message 가 쌓이게 놔두고 매 쌓기마다 알리지는 않는다.
      // 아니면, buffering 기능을 끄면 message 를 받을 때마다 web socket 전송을 시작하게 된다. logout 단계에서 message 를 보낼 때 쓴다.
      if ((cancellable.isEmpty && defer == false) || useBuffering == false) {
        cancellable.foreach(_.cancel())
        cancellable = Some(MasterActorSystem.actorSystem.scheduler.scheduleOnce(waitSeconds.seconds, self, SelfAwakeningCommand()))
        // 앞서 알림시각을 정한 것이 없을 때, 알린다.
        requestWithdraw()
      }
    case GetMessageQueueCommand() ⇒
      // 먼저 받은 message 가 앞에 오도록 하기 위해 queue 를 썼다. 줄 때는 list 로 준다.
      val list = messageQueue.toList
      sender() ! list
    case CompleteMessageCommand(idList) ⇒
      // 완료처리한다.
      // messageQueue 에 없는 message id 가 완료응답으로 올 수 있다. 무시한다.
      messageQueue = messageQueue.filterNot(x ⇒ x.id.exists(idList.contains(_)))
      cancellable.foreach(_.cancel())
      cancellable = None
      // 전달받은 id 목록에 대해서 완료처리 했는데
      if (!messageQueue.isEmpty) {
        // 또 남아있다면. 이 경우는 client 가 완료응답을 보내는 사이 시간에 또 message 가 쌓인 경우에 해당된다.
        // 이 때는 바로 알린다.
        cancellable = Some(MasterActorSystem.actorSystem.scheduler.scheduleOnce(waitSeconds.seconds, self, SelfAwakeningCommand()))
        requestWithdraw()
      }
    case SelfAwakeningCommand() ⇒
      // 외부에서 직접 깨워줄수도 있다. 깨어나면 무조건 초기화한다.
      cancellable.foreach(_.cancel())
      // 스스로 다시 깨어났는데,
      if (!messageQueue.isEmpty) {
        cancellable = Some(MasterActorSystem.actorSystem.scheduler.scheduleOnce(waitSeconds.seconds, self, SelfAwakeningCommand()))
        requestWithdraw()
      } else {
        // message 가 남아있지 않으면, message 가 남아있다고 user actor 에게 알려줘서는 안되고, 또한 다시 깨어날 필요도 없다.
        cancellable = None
      }
    // PoisonPill 은 외부에서는 전달이 안되는듯 하다.
    case QuitMessageQueueActorCommand() ⇒
      cancellable.foreach(_.cancel())
      self ! PoisonPill
    case ChangeBufferingModeCommand(useBuffering) ⇒
      this.useBuffering = useBuffering
  }

  protected[this] def requestWithdraw(): Unit = {
    // 아예, 여기서 자료까지 같이 줄 수도 있다. 그런데, 그와 같이 하면, 자료 전달을 할때, web socket 이 아닌 get http method 를 사용하면 방법이 없게 된다. 일단, 놔두자.
    // 달라고 하지 않았는데도 자료를 전달하는 유형의 통신은 actor 양자간 통신에서만 가능하다. get http method 를 처리하는 쪽에서는 당연히 actor 가 없기 때문에 여기서 자료전달을 해서는 안된다.
    // get http method 를 사용한다면, 자료가 전달되는 과정은 다음과 같다.
    // 먼저 user actor 에게 알리고, user actor 는 atmosphere client 를 사용하여 web socket client 에게 알린다.
    // web socket client 는 알림을 받고, get http method 를 보내면, server get http method 처리 단계에서 message queue 로 응답을 기다리는 get message 요청을 보낸다.
    // actor 로부터 응답을 받으면 다시 http get 응답을 보내면 된다.

    // 처음에 주석을 쓸 당시의 의도와는 다르게, server 가 client 에게 자료를 전달해야 할 때, web socket 을 사용해서 직접 보내도록 했다.
    // 그러나, network 를 통해 오고가는 traffic 의 분실/누락 등으로 인하여 자료가 제대로 전달이 되지 않을 수 있으므로,
    // client 로부터 잘 전달받았다는 확인을 받기 전까지는 message 가 남아있어야 한다.
    // 한편, 앞의 주석에서 뜻한 바는, 아래의 actor message 전달과정에서 단순히 가져가라는 뜻의 actor message 만을 보내지 말고, 모든 message 를 전부 담아서 보낼수도 있다는 구상에 관한 것이다.
    // web socket 으로 message 를 전달하기로 한 이상 그와 같이 해도 된다. 일단, 놔두자.
    userLoginHistoryId.map(_.toString).
      foreach(x ⇒ MasterActorSystem.actorSystem.actorSelection(MasterActorSystem.actorSystem / MasterUserActor.actorName / x) ! RequestWithdrawCommand())
  }
}

sealed trait MessageQueueActorCommand

case class PushMessageCommand[T](envelope: List[EnvelopeModel[_]], defer: Boolean = false) extends MessageQueueActorCommand

case class GetMessageQueueCommand() extends MessageQueueActorCommand

case class CompleteMessageCommand(idList: Set[Int]) extends MessageQueueActorCommand

case class SelfAwakeningCommand() extends MessageQueueActorCommand

case class QuitMessageQueueActorCommand() extends MessageQueueActorCommand

case class ChangeBufferingModeCommand(useBuffering: Boolean = true) extends MessageQueueActorCommand
