/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.webservice20150417.servlet

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait
import org.bitbucket.imcharsi.myproject20150417.webservice20150417.DefaultObjectMapper
import org.scalatra.{ ActionResult, InternalServerError, NotFound, Ok }

import scala.util.{ Failure, Success, Try }

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
class DaoServlet extends DefaultScalatraServlet {
  protected def testAcceptHeader: Boolean = Option(request.getHeader("Accept")).exists(_.split(";").exists(_.contains("application/json")))

  protected def testContentTypeHeader: Boolean = Option(request.getHeader("Content-Type")).exists(_.split(";").exists(_.contains("application/json")))

  protected def paramVersion: Option[Int] = params.get(IdVersionModelTrait.version).filterNot(_.isEmpty).map(_.toInt)

  protected def postValidate(x: Option[Int]): Option[Int] = {
    x match {
      case None ⇒ throw new InternalError()
      case x @ Some(_) ⇒ x
    }
  }

  protected def listResultWrapper[T](f: () ⇒ List[T]): ActionResult = {
    Try {
      f()
    } match {
      case Success(x) ⇒ Ok(DefaultObjectMapper.writeValueAsString(x))
      case Failure(x) ⇒ InternalServerError(x)
    }
  }

  protected def optionResultWrapper[T](f: () ⇒ Option[T]): ActionResult = {
    Try {
      f()
    } match {
      case Success(Some(x)) ⇒ Ok(DefaultObjectMapper.writeValueAsString(x))
      case Success(None) ⇒ NotFound()
      case Failure(x) ⇒ InternalServerError(x)
    }
  }
}
