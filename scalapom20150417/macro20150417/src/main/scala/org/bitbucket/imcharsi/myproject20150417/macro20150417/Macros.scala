/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.macro20150417

import scala.language.experimental.macros

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
object Macros {
  // 각종 번거로운 작업을 macro 를 사용하여 자동으로 해보자.
  //  http://stackoverflow.com/questions/13446528/howto-model-named-parameters-in-method-invocations-with-scala-macros
  // fixme 잘 안된다. 무엇이 잘 안되는가. case class 의 copy method 의 존재를 case class 가 상속하는 trait 에서는 알 수 없다.

  import scala.reflect.macros.whitebox.Context

  def modifyVersion[TT <: { def copyVersion(version: Option[Int]): TT }](x: TT, version: Option[Int]): TT = macro modifyVersionImpl[TT]

  def modifyVersionImpl[T: c.WeakTypeTag](c: Context)(x: c.Expr[T], version: c.Expr[Option[Int]]): c.Expr[T] = {
    import c.universe._
    val tree = reify(x.splice).tree
    val copy = x.actualType.member(TermName("copyVersion"))
    copy match {
      case s: MethodSymbol if (s.paramLists.flatten.map(_.name).contains(TermName("version"))) ⇒
        c.Expr[T](Apply(Select(tree, copy), AssignOrNamedArg(Ident(TermName("version")), reify(version.splice).tree) :: Nil))
      case _ ⇒ c.abort(c.enclosingPosition, "no eligible copy method")
    }
  }
}

