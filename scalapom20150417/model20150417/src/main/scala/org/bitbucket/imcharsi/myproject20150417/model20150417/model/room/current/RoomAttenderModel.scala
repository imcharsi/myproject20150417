/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current

import org.bitbucket.imcharsi.myproject20150417.model20150417.NotPersisted
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ IdVersionModelTrait, UserRoomModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/30/15.
 */
case class RoomAttenderModel(override val id: Option[Int],
                             override val version: Option[Int],
                             override val userId: Option[Int],
                             override val roomId: Option[Int],
                             val relatedRoomInOutHistoryId: Option[Int],
                             override val user: Option[UserModel],
                             override val room: Option[RoomModel],
                             val relatedRoomInOutHistory: Option[RoomInOutHistoryModel],
                             // 계산열이다. 현재 방장인지, 현재 운영자인지 표시한다. database 에서 boolean 형으로 계산결과를 구하지 않고,
                             // isCurrentOwner 의 경우에는 sql 검색 결과를 구한 뒤, room.ownerId 와 roomAttender.userId 가 같은지 비교한 결과를 쓰고,
                             // isCurrentOperator 의 경우는 operator table 과의 join left 결과가 not null 인지 여부를 판단한다.
                             @NotPersisted val currentOwnerFlag: Option[Boolean],
                             @NotPersisted val currentOperatorFlag: Option[Boolean]) extends IdVersionModelTrait[RoomAttenderModel] with UserRoomModelTrait {
  def this() = this(None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): RoomAttenderModel = copy(version = increaseVersion())

  override def initVersion(): RoomAttenderModel = copy(version = Some(1))

  override def clearPrivacyData(): RoomAttenderModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      relatedRoomInOutHistory = relatedRoomInOutHistory.map(_.clearPrivacyData()))
  }
}

object RoomAttenderModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], roomId: Option[Int], relatedRoomInOutHistoryId: Option[Int]): RoomAttenderModel =
    RoomAttenderModel(id, version, userId, roomId, relatedRoomInOutHistoryId, None, None, None, None, None)

  def unapply(x: RoomAttenderModel) = Option((x.id, x.version, x.userId, x.roomId, x.relatedRoomInOutHistoryId))
}
