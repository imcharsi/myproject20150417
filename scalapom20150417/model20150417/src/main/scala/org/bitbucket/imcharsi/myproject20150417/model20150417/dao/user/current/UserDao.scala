/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.dao.user.current

import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.mixin.Dao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
trait UserDao extends Dao[UserModel] {
  def login(name: String, password: String): Option[Int]

  def logout(userLoginHistoryId: Option[Int]): Boolean

  // 외부에서 종료시킬 접속에 대해 구체적으로 알 필요가 없다.
  // 한편, 자료처리만 수행하고 actor 는 다루지 않는다. actor 는 web service 에서 다룬다.
  def logoutForce(name: String, password: String): Option[Int] // 실패하면, 예외가 생긴다. 따라서 반환결과도 필요없다.

  // 현재 접속중인 모든 사용자를 알려준다.
  def getAllLoginUsers(): Seq[UserModel]

  def getUserByNamePassword(name: String, password: String): Option[UserModel]

  def getAllUsers(): Seq[UserModel]

  def setCurrentNick(userId: Option[Int], nick: Option[String]): (Option[Int], Seq[(Option[Int], Option[Int])])
}

