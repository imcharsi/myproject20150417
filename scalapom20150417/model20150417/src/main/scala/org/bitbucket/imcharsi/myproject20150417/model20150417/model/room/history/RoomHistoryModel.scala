/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.NotPersisted
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/18/15.
 */
case class RoomHistoryModel(override val id: Option[Int],
                            override val version: Option[Int],
                            override val roomId: Option[Int],
                            override val userId: Option[Int],
                            // 여기서의 byWho 는 누구에 의한 변화인지를 뜻한다. 운영자도 방제목을 바꿀 수 있다.
                            override val byWhoId: Option[Int],
                            override val when: Option[Date],
                            name: Option[String],
                            password: Option[String],
                            relatedPermissionHistoryId: Option[Int],

                            override val room: Option[RoomModel],
                            // user 라고 썼지만, owner 로 다루자.
                            override val user: Option[UserModel],
                            override val byWho: Option[UserModel],
                            relatedPermissionHistory: Option[PermissionHistoryModel],
                            @NotPersisted val needPassword: Option[Boolean]) extends CommonRoomHistoryModelTrait[RoomHistoryModel] {
  def this() = this(None, None, None, None, None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): RoomHistoryModel = copy(version = increaseVersion())

  override def initVersion(): RoomHistoryModel = copy(version = Some(1))

  // 대화방의 권한자는 소유자를 추방할 수 없다.

  override def clearPrivacyData(): RoomHistoryModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      byWho = byWho.map(_.clearPrivacyData()),
      relatedPermissionHistory = relatedPermissionHistory.map(_.clearPrivacyData()),
      password = None)
  }
}

object RoomHistoryModelEx {
  def apply(id: Option[Int], version: Option[Int], roomId: Option[Int], userId: Option[Int], byWhoId: Option[Int], when: Option[Date], name: Option[String], password: Option[String], relatedPermissionHistoryId: Option[Int]) =
    RoomHistoryModel(id, version, roomId, userId, byWhoId, when, name, password, relatedPermissionHistoryId, None, None, None, None, None)

  def unapply(x: RoomHistoryModel) = Option((x.id, x.version, x.roomId, x.userId, x.byWhoId, x.when, x.name, x.password, x.relatedPermissionHistoryId))
}
