/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.CustomEnum
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ InvitationModel, RoomModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/18/15.
 */
case class RoomInOutHistoryModel(override val id: Option[Int],
                                 override val version: Option[Int],
                                 override val userId: Option[Int],
                                 override val roomId: Option[Int],
                                 invitationId: Option[Int],
                                 override val byWhoId: Option[Int],
                                 override val when: Option[Date],
                                 //  http://stackoverflow.com/questions/19273805/how-to-persist-enum-value-in-slick
                                 enterType: Option[EnterType],

                                 override val user: Option[UserModel],
                                 override val room: Option[RoomModel],
                                 // 초대를 받아서 입장을 했으면, 관련 초대를 표시한다.
                                 invitation: Option[InvitationModel],
                                 // 퇴장을 당했으면, 누가 퇴장을 시켰는지 표시한다. byWho 이지만 누가 퇴장시켰는지 가리키는 용도로 쓴다.
                                 override val byWho: Option[UserModel]) extends CommonRoomHistoryModelTrait[RoomInOutHistoryModel] {
  def this() = this(None, None, None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): RoomInOutHistoryModel = copy(version = increaseVersion())

  override def initVersion(): RoomInOutHistoryModel = copy(version = Some(1))

  // 이 model 은 이력이다. 따라서, 입장에 관한 이력이 한개, 퇴장에 관한 이력이 한개 남는다.

  override def clearPrivacyData(): RoomInOutHistoryModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      invitation = invitation.map(_.clearPrivacyData()),
      byWho = byWho.map(_.clearPrivacyData()))
  }
}

object RoomInOutHistoryModel {

  sealed trait EnterType extends CustomEnum

  object EnterType {
    def apply(id: Int): EnterType = {
      id match {
        case Enter.id ⇒ Enter
        case Leave.id ⇒ Leave
      }
    }

    case object Enter extends EnterType {
      override val id: Int = 0
    }

    case object Leave extends EnterType {
      override val id: Int = 1
    }
  }
}

object RoomInOutHistoryModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], roomId: Option[Int], invitationId: Option[Int], byWhoId: Option[Int], when: Option[Date], enterType: Option[EnterType]) =
    RoomInOutHistoryModel(id, version, userId, roomId, invitationId, byWhoId, when, enterType, None, None, None, None)

  def unapply(x: RoomInOutHistoryModel) = Option((x.id, x.version, x.userId, x.roomId, x.invitationId, x.byWhoId, x.when, x.enterType))
}