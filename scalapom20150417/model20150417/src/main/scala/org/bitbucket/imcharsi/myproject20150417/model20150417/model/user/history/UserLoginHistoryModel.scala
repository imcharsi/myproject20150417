/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ IdVersionModelTrait, UserModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by KangWoo, Lee on 4/18/15.
 */
case class UserLoginHistoryModel(override val id: Option[Int],
                                 override val version: Option[Int],
                                 override val userId: Option[Int],
                                 loginDateTime: Option[Date],
                                 logoutDateTime: Option[Date],

                                 override val user: Option[UserModel]) extends IdVersionModelTrait[UserLoginHistoryModel] with UserModelTrait {
  def this() = this(None, None, None, None, None, None)

  override def modifyVersion(): UserLoginHistoryModel = copy(version = increaseVersion())

  override def initVersion(): UserLoginHistoryModel = copy(version = Some(1))

  override def clearPrivacyData(): UserLoginHistoryModel = {
    val x = copy(user = user.map(_.clearPrivacyData()))
    x
  }
}

object UserLoginHistoryModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], loginDateTime: Option[Date], logoutDateTime: Option[Date]) =
    UserLoginHistoryModel(id, version, userId, loginDateTime, logoutDateTime, None)

  def unapply(x: UserLoginHistoryModel) = Option((x.id, x.version, x.userId, x.loginDateTime, x.logoutDateTime))
}
