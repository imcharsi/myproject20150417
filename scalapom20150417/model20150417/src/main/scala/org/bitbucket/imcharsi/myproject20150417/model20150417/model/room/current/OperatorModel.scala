/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ IdVersionModelTrait, UserRoomModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/19/15.
 */
case class OperatorModel(override val id: Option[Int],
                         override val version: Option[Int],
                         override val userId: Option[Int],
                         override val roomId: Option[Int],
                         relatedPermissionHistoryId: Option[Int], // todo room model 에도 똑같은 취지의 value field 가 있다. trait 로 묶을 것인가. 묶는다면 table 도 똑같이 묶어야 한다.

                         override val user: Option[UserModel],
                         override val room: Option[RoomModel],
                         // 사용자가 방운영권한을 수여받게 된 이력.
                         relatedPermissionHistory: Option[PermissionHistoryModel]) extends IdVersionModelTrait[OperatorModel] with UserRoomModelTrait {
  def this() = this(None, None, None, None, None, None, None, None)

  override def modifyVersion(): OperatorModel = copy(version = increaseVersion())

  override def initVersion(): OperatorModel = copy(version = Some(1))
  // 지금 현재 방운영권한이 있는 사용자만을 다룬다.

  override def clearPrivacyData(): OperatorModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      relatedPermissionHistory = relatedPermissionHistory.map(_.clearPrivacyData()))
  }
}

object OperatorModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], roomId: Option[Int], relatedPermissionHistoryId: Option[Int]) =
    OperatorModel(id, version, userId, roomId, relatedPermissionHistoryId, None, None, None)

  def unapply(x: OperatorModel) = Option((x.id, x.version, x.userId, x.roomId, x.relatedPermissionHistoryId))
}
