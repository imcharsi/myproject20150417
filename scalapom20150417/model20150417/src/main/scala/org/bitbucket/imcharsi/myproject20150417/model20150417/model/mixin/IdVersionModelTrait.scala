/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin

/**
 * Created by i on 4/18/15.
 */
trait IdVersionModelTrait[T] {
  val id: Option[Int]
  val version: Option[Int]

  def modifyVersion(): T

  def initVersion(): T

  def increaseVersion(): Option[Int] = version.map(_ + 1)

  // 공개되어서는 안 되는 정보가 밖으로 나가지 않도록 사전에 걸러내기 위해 쓴다.
  def clearPrivacyData(): T
}

object IdVersionModelTrait {
  val version = "version"

  class VersionException extends RuntimeException

  class NotFound extends RuntimeException

}

