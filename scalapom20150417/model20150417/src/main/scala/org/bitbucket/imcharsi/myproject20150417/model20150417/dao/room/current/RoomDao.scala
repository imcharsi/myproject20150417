/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.current

import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.mixin.Dao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnterRoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
trait RoomDao extends Dao[RoomModel] {
  def createRoom(userModel: UserModel, roomModel: RoomModel): Option[(RoomModel, List[Option[Int]])]

  def enterRoom(userId: Option[Int], roomId: Option[Int], enterRoomModel: Option[EnterRoomModel]): Option[Int]

  // 퇴장에 관한 자료 처리를 하면서 기록된 모든 RoomTimelineModel 의 id 를 알려준다.
  // byWhoId 가 지정되면 byWhoId 사용자에 의한 강제퇴장을 시도한다.
  // todo system 에 의한 퇴장을 구분할 수가 없는데, 일단 넘어가자.
  def leaveRoom(userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int]): List[Option[Int]]

  def closeRoom(roomId: Option[Int]): Unit

  // userId 는 초대받는 사람, byWhoId 는 초대하는 사람이다.
  def invite(roomId: Option[Int], userId: Option[Int], byWhoId: Option[Int]): Option[Int]

  // nick 은 user model 의 current nick history 에서 구한다.
  def talk(roomId: Option[Int], userId: Option[Int], talk: Option[String]): Option[Int]

  def getAllCreatedRooms(userId: Option[Int] = None): Seq[RoomModel]

  // 방장권한을 위임한다.
  def revokeOwnerPermission(userId: Option[Int], roomId: Option[Int], candidateUserId: Option[Int]): List[Option[Int]]

  def setRoomPassword(byWhoId: Option[Int], roomId: Option[Int], password: Option[String]): Option[Int]

  def setRoomName(roomId: Option[Int], name: Option[String], byWhoId: Option[Int]): Option[Int]
}
