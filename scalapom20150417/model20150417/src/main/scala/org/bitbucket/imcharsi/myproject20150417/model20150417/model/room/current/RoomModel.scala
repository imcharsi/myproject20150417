/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.NotPersisted
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ ByWhoModelTrait, IdVersionModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/18/15.
 */
case class RoomModel(override val id: Option[Int],
                     override val version: Option[Int],
                     // 개설시각. none 일 수 없다.
                     createdDateTime: Option[Date],
                     // 폐쇄시각. 폐쇄시각이 있으면 더 이상 입장할 수 없다. 아무도 없는 방이면 자동으로 폐쇄된다.
                     closedDateTime: Option[Date],
                     currentDetailId: Option[Int],
                     override val byWhoId: Option[Int], // byWhoId 이지만, 누가 만들었는지 가리키는 용도로 쓰자.
                     // 처음 구상은, room model 에 owner 를 넣는 것이었는데, 이것은 중복이다.
                     // room history 에도 소유자를 뜻하는 userId 가 있다. room model 은 항상 room history 를 가지므로,
                     // 결국 중복이다.

                     currentDetail: Option[RoomHistoryModel],
                     // 대화방 개설자.
                     override val byWho: Option[UserModel],
                     @NotPersisted currentAttendingRoom: Option[Boolean],
                     @NotPersisted currentBannedRoom: Option[Boolean]) extends IdVersionModelTrait[RoomModel] with ByWhoModelTrait {
  def this() = this(None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): RoomModel = copy(version = version.map(_ + 1))

  override def initVersion(): RoomModel = copy(version = Some(1))

  override def clearPrivacyData(): RoomModel = {
    copy(
      currentDetail = currentDetail.map(_.clearPrivacyData()),
      byWho = byWho.map(_.clearPrivacyData()))
  }
}

object RoomModelEx {
  def apply(id: Option[Int], version: Option[Int], createdDateTime: Option[Date], closedDateTime: Option[Date], currentDetailId: Option[Int], byWhoId: Option[Int]) =
    RoomModel(id, version, createdDateTime, closedDateTime, currentDetailId, byWhoId, None, None, None, None)

  def unapply(x: RoomModel) = Option((x.id, x.version, x.createdDateTime, x.closedDateTime, x.currentDetailId, x.byWhoId))
}

class NotEmptyRoomException extends RuntimeException

class ClosedRoomException extends RuntimeException

class NotOperatorException extends RuntimeException

class PrivateRoomException extends RuntimeException

class NotOwnerException extends RuntimeException
