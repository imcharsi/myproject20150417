/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.CustomEnum
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.BanHistoryModel.BanType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/18/15.
 */
case class BanHistoryModel(override val id: Option[Int],
                           override val version: Option[Int],
                           override val userId: Option[Int],
                           override val roomId: Option[Int],
                           override val byWhoId: Option[Int],
                           // 금지유형.
                           banType: Option[BanType],
                           override val when: Option[Date],

                           // 금지 대상자.
                           override val user: Option[UserModel],
                           override val room: Option[RoomModel],
                           // 금지한 사람.
                           override val byWho: Option[UserModel]) extends CommonRoomHistoryModelTrait[BanHistoryModel] {
  def this() = this(None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): BanHistoryModel = copy(version = increaseVersion())

  override def initVersion(): BanHistoryModel = copy(version = Some(1))

  // 본 model 은 이력이다. 따라서, 금지를 설정하는 이력도 있을 수 있고, 금지를 해제하는 이력도 있을 수 있다.
  // 따라서 추가전용이고, 갱신/삭제를 하지 않는다.

  override def clearPrivacyData(): BanHistoryModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      byWho = byWho.map(_.clearPrivacyData()))
  }
}

object BanHistoryModel {

  sealed trait BanType extends CustomEnum

  object BanType {
    def apply(id: Int): BanType = {
      id match {
        case Allow.id ⇒ Allow
        case Disallow.id ⇒ Disallow
      }
    }

    case object Allow extends BanType {
      override val id: Int = 0
    }

    case object Disallow extends BanType {
      override val id: Int = 1
    }

  }

}

object BanHistoryModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int], banType: Option[BanType], when: Option[Date]) =
    BanHistoryModel(id, version, userId, roomId, byWhoId, banType, when, None, None, None)

  def unapply(x: BanHistoryModel) = Option((x.id, x.version, x.userId, x.roomId, x.byWhoId, x.banType, x.when))
}
