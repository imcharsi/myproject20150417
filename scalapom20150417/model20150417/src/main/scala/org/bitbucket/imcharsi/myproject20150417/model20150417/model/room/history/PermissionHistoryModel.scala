/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.CustomEnum
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/18/15.
 */
case class PermissionHistoryModel(override val id: Option[Int],
                                  override val version: Option[Int],
                                  override val userId: Option[Int],
                                  override val roomId: Option[Int],
                                  override val byWhoId: Option[Int],
                                  permissionType: Option[PermissionType],
                                  // 권한 수여나 회수의 시각을 표시한다.
                                  override val when: Option[Date],

                                  // 권한 수여/회수 대상자.
                                  override val user: Option[UserModel],
                                  override val room: Option[RoomModel],
                                  // 권한을 수여하거나 회수한 사람이 누구인지 표시한다.
                                  // 방장이 방장권한을 위임하지 않고 방을 나가면 누가 방장이 될 것인가.
                                  // 그보다 방을 나간 방장이 다른 누군가에게 방장 권한을 위임한 것으로 기록할 것인가. system 이 지정하는 것으로 기록할 것인가.
                                  // none 이면 system 에 의한 변화로 보자.
                                  // 방장권한도 회수와 수여를 따로 기록하자. 한개의 기록이 회수와 수여를 동시에 의미하지 않고 회수는 회수대로, 수여는 수여대로 의미하게 하자.
                                  override val byWho: Option[UserModel]) extends CommonRoomHistoryModelTrait[PermissionHistoryModel] {
  def this() = this(None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): PermissionHistoryModel = copy(version = version.map(_ + 1))

  override def initVersion(): PermissionHistoryModel = copy(version = Some(1))

  // 본 model 은, 추가 전용이다. 갱신하지 않는다.

  override def clearPrivacyData(): PermissionHistoryModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      byWho = byWho.map(_.clearPrivacyData()))
  }
}

object PermissionHistoryModel {

  sealed trait PermissionType extends CustomEnum

  // 방장권한 위임 0, 방장권한 회수 1, 운영권한 위임 2, 운영권한 회수 3
  object PermissionType {
    def apply(id: Int): PermissionType = id match {
      case GrantOwner.id ⇒ GrantOwner
      case RevokeOwner.id ⇒ RevokeOwner
      case GrantOperator.id ⇒ GrantOperator
      case RevokeOperator.id ⇒ RevokeOperator
    }

    case object GrantOwner extends PermissionType {
      override val id: Int = 0
    }

    case object RevokeOwner extends PermissionType {
      override val id: Int = 1
    }

    case object GrantOperator extends PermissionType {
      override val id: Int = 2
    }

    case object RevokeOperator extends PermissionType {
      override val id: Int = 3
    }

  }

}

object PermissionHistoryModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int], permissionType: Option[PermissionType], when: Option[Date]) =
    PermissionHistoryModel(id, version, userId, roomId, byWhoId, permissionType, when, None, None, None)

  def unapply(x: PermissionHistoryModel) = Option((x.id, x.version, x.userId, x.roomId, x.byWhoId, x.permissionType, x.when))
}
