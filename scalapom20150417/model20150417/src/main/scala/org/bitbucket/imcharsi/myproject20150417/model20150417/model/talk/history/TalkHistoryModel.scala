/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ IdVersionModelTrait, UserRoomModelTrait, WhenModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.UserNickHistoryModel

/**
 * Created by Kang Woo, Lee on 4/18/15.
 */
case class TalkHistoryModel(override val id: Option[Int],
                            override val version: Option[Int],
                            override val userId: Option[Int],
                            override val roomId: Option[Int],
                            nickId: Option[Int],
                            override val when: Option[Date],
                            talk: Option[String],

                            override val user: Option[UserModel],
                            override val room: Option[RoomModel],
                            nick: Option[UserNickHistoryModel]) extends IdVersionModelTrait[TalkHistoryModel] with WhenModelTrait with UserRoomModelTrait {
  def this() = this(None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): TalkHistoryModel = copy(version = increaseVersion())

  override def initVersion(): TalkHistoryModel = copy(version = Some(1))

  // 비밀대화는 어떻게 할 것인가. 비밀대화 기능을 따로 두지 말고, 대화자와 상대방만 참여하는 방을 자동으로 만들어주도록 하자.
  // 이와 같이 해야, 나중에 대화에 참가할 사람을 늘리고 싶을 때 얼마든지 늘일수 있다.

  override def clearPrivacyData(): TalkHistoryModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      nick = nick.map(_.clearPrivacyData()))
  }
}

object TalkHistoryModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], roomId: Option[Int], nickId: Option[Int], when: Option[Date], talk: Option[String]) =
    TalkHistoryModel(id, version, userId, roomId, nickId, when, talk, None, None, None)

  def unapply(x: TalkHistoryModel) = Option((x.id, x.version, x.userId, x.roomId, x.nickId, x.when, x.talk))
}
