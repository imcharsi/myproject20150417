/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }

/**
 * Created by Kang Woo, Lee on 4/18/15.
 */
case class UserModel(override val id: Option[Int],
                     override val version: Option[Int],
                     currentLoginHistoryId: Option[Int],
                     currentNickHistoryId: Option[Int],
                     // 계정은 중복될 수 없다.
                     name: Option[String],
                     // 반드시 암호가 있어야 한다.
                     password: Option[String],

                     // currentLoginHistory.isDefined 이면 현재 사용중이다.
                     currentLoginHistory: Option[UserLoginHistoryModel],
                     // 반드시 대화명이 하나 지정되어 있어야 한다. 대화명은 중복될 수 있다.
                     currentNickHistory: Option[UserNickHistoryModel]) extends IdVersionModelTrait[UserModel] {
  def this() = this(None, None, None, None, None, None, None, None)

  override def modifyVersion(): UserModel = copy(version = increaseVersion())

  override def initVersion(): UserModel = copy(version = Some(1))

  override def clearPrivacyData(): UserModel = {
    val x = copy(
      password = None,
      currentNickHistory = currentNickHistory.map(_.clearPrivacyData()),
      currentLoginHistory = currentLoginHistory.map(_.clearPrivacyData()))
    x
  }
}

object UserModel {

  class AlreadyLoginException extends RuntimeException

  class AlreadyLogoutException extends RuntimeException

}

object UserModelEx {
  def apply(id: Option[Int], version: Option[Int], currentLoginHistoryId: Option[Int],
            currentNickHistoryId: Option[Int], name: Option[String], password: Option[String]) =
    UserModel(id, version, currentLoginHistoryId, currentNickHistoryId, name, password, None, None)

  def unapply(x: UserModel) = Option((x.id, x.version, x.currentLoginHistoryId, x.currentNickHistoryId, x.name, x.password))
}
