/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.current

import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.mixin.Dao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.OperatorModel

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
trait OperatorDao extends Dao[OperatorModel] {
  def getOneByUserRoom(userId: Option[Int], roomId: Option[Int]): Option[OperatorModel]

  // 운영자 권한을 수여한다.
  def grantOperatorPermission(ownerId: Option[Int], roomId: Option[Int], userId: Option[Int]): Option[Int]

  // 방장이 운영자의 운영자권한을 회수하거나 운영자가 자신의 운영자권한을 반납한다.
  def revokeOperatorPermission(userId: Option[Int], roomId: Option[Int], operatorUserId: Option[Int]): Option[Int]
}
