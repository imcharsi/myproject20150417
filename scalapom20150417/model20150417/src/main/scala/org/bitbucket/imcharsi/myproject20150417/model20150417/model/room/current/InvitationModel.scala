/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ ByWhoModelTrait, IdVersionModelTrait, UserRoomModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/18/15.
 */
case class InvitationModel(override val id: Option[Int],
                           override val version: Option[Int],
                           override val userId: Option[Int],
                           override val roomId: Option[Int],
                           override val byWhoId: Option[Int],
                           // 초대한 시각.
                           invitationDateTime: Option[Date],
                           // 초대가 만료되는 시각.
                           expirationDateTime: Option[Date],
                           // 본 초대가 유효한지 여부를 나타낸다. 초대는 일회용으로 하자.
                           // 따라서 한번 초대에 응했으면 응했던 초대를 사용하여 다시 들어갈 수 없도록 하자.
                           validity: Option[Boolean],

                           // 초대 대상자.
                           override val user: Option[UserModel],
                           override val room: Option[RoomModel],
                           // 초대한 사람.
                           override val byWho: Option[UserModel]) extends IdVersionModelTrait[InvitationModel] with UserRoomModelTrait with ByWhoModelTrait {
  def this() = this(None, None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): InvitationModel = copy(version = version.map(_ + 1))

  override def initVersion(): InvitationModel = copy(version = Some(1))

  override def clearPrivacyData(): InvitationModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      byWho = byWho.map(_.clearPrivacyData()))
  }
}

object InvitationModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int], invitationDateTime: Option[Date], expirationDateTime: Option[Date], validity: Option[Boolean]) =
    InvitationModel(id, version, userId, roomId, byWhoId, invitationDateTime, expirationDateTime, validity, None, None, None)

  def unapply(x: InvitationModel) = Option((x.id, x.version, x.userId, x.roomId, x.byWhoId, x.invitationDateTime, x.expirationDateTime, x.validity))
}
