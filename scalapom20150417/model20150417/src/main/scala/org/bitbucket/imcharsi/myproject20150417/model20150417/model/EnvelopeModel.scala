/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model

import org.bitbucket.imcharsi.myproject20150417.model20150417.CustomEnum
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModel.EnvelopeDataType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }

/**
 * Created by Kang Woo, Lee on 5/1/15.
 */
case class EnvelopeModel[T](dataType: Option[EnvelopeDataType], data: Option[T], id: Option[Int] = None)

object EnvelopeModel {

  sealed trait EnvelopeDataType extends CustomEnum

  object EnvelopeDataType {
    def apply(id: Int): EnvelopeDataType = id match {
      case UserModelType.id ⇒ UserModelType
      case UserLoginHistoryModelType.id ⇒ UserLoginHistoryModelType
      case UserNickHistoryModelType.id ⇒ UserNickHistoryModelType
      case RoomModelType.id ⇒ RoomModelType
      case InvitationModelType.id ⇒ InvitationModelType
      case OperatorModelType.id ⇒ OperatorModelType
      case RoomAttenderModelType.id ⇒ RoomAttenderModelType
      case BanModelType.id ⇒ BanModelType
      case RoomHistoryModelType.id ⇒ RoomHistoryModelType
      case BanHistoryModelType.id ⇒ BanHistoryModelType
      case PermissionHistoryModelType.id ⇒ PermissionHistoryModelType
      case RoomInOutHistoryModelType.id ⇒ RoomInOutHistoryModelType
      case RoomTimelineModelType.id ⇒ RoomTimelineModelType
      case TalkHistoryModelType.id ⇒ TalkHistoryModelType
      case SayHiModelType.id ⇒ SayHiModelType
      case LogoutModelType.id ⇒ LogoutModelType
    }

    case object UserModelType extends EnvelopeDataType {
      override val id: Int = 1
    }

    case object UserLoginHistoryModelType extends EnvelopeDataType {
      override val id: Int = 2
    }

    case object UserNickHistoryModelType extends EnvelopeDataType {
      override val id: Int = 3
    }

    case object RoomModelType extends EnvelopeDataType {
      override val id: Int = 4
    }

    case object InvitationModelType extends EnvelopeDataType {
      override val id: Int = 5
    }

    case object OperatorModelType extends EnvelopeDataType {
      override val id: Int = 6
    }

    case object RoomAttenderModelType extends EnvelopeDataType {
      override val id: Int = 7
    }

    case object BanModelType extends EnvelopeDataType {
      override val id: Int = 8
    }

    case object RoomHistoryModelType extends EnvelopeDataType {
      override val id: Int = 9
    }

    case object BanHistoryModelType extends EnvelopeDataType {
      override val id: Int = 10
    }

    case object PermissionHistoryModelType extends EnvelopeDataType {
      override val id: Int = 11
    }

    case object RoomInOutHistoryModelType extends EnvelopeDataType {
      override val id: Int = 12
    }

    case object RoomTimelineModelType extends EnvelopeDataType {
      override val id: Int = 13
    }

    case object TalkHistoryModelType extends EnvelopeDataType {
      override val id: Int = 14
    }

    case object SayHiModelType extends EnvelopeDataType {
      override val id: Int = 15
    }

    case object LogoutModelType extends EnvelopeDataType {
      override val id: Int = 16
    }

  }

}

object EnvelopeModelEx {

  def apply[T](data: Option[T]): EnvelopeModel[T] = {
    import EnvelopeDataType._
    val dataType = data match {
      case x @ Some(_: UserModel) ⇒ UserModelType
      case x @ Some(_: UserLoginHistoryModel) ⇒ UserLoginHistoryModelType
      case x @ Some(_: UserNickHistoryModel) ⇒ UserNickHistoryModelType
      case x @ Some(_: RoomModel) ⇒ RoomModelType
      case x @ Some(_: InvitationModel) ⇒ InvitationModelType
      case x @ Some(_: OperatorModel) ⇒ OperatorModelType
      case x @ Some(_: RoomAttenderModel) ⇒ RoomAttenderModelType
      case x @ Some(_: BanModel) ⇒ BanModelType
      case x @ Some(_: RoomHistoryModel) ⇒ RoomHistoryModelType
      case x @ Some(_: BanHistoryModel) ⇒ BanHistoryModelType
      case x @ Some(_: PermissionHistoryModel) ⇒ PermissionHistoryModelType
      case x @ Some(_: RoomInOutHistoryModel) ⇒ RoomInOutHistoryModelType
      case x @ Some(_: RoomTimelineModel) ⇒ RoomTimelineModelType
      case x @ Some(_: TalkHistoryModel) ⇒ TalkHistoryModelType
      case x @ Some(_: SayHiModel) ⇒ SayHiModelType
      case x @ Some(_: LogoutModel) ⇒ LogoutModelType
    }
    EnvelopeModel(Some(dataType), data)
  }
}
