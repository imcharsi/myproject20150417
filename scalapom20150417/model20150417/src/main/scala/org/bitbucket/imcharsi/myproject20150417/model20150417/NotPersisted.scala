/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417

/**
 * Created by Kang Woo, Lee on 4/19/15.
 */
class NotPersisted extends annotation.StaticAnnotation

// model 에, 각종 계산결과를 담아야 할 필요가 있을 수 있다. 이 value field 들은, database table column 이 되어서는 안된다.
// 필요한 value field 는 column 으로 선언되었고 그 외에는 선언되지 않았다는 것을 검증할 필요가 있다.
