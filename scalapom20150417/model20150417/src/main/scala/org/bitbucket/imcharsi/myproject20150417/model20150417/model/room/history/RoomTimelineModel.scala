/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ IdVersionModelTrait, RoomModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.UserNickHistoryModel

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
case class RoomTimelineModel(override val id: Option[Int],
                             override val version: Option[Int],
                             override val roomId: Option[Int],
                             // 대화방 안에서 일어나는 모든 일들을 각 table 을 돌아다니면서 시간순서로 정렬해서 보기는 번거롭다.
                             // database table 에서 제약조건을 걸어야 하는데, 아래의 5개의 항목 중 반드시 한개의 값만 null 이 아니어야 하고 나머지는 null 이어야 한다.
                             // 사용예는 다음과 같다.
                             // 대화방이 개설되면, roomHistoryId 가 설정된 timeline 이 하나 추가된다..
                             // 이어서 방장이 입장을 하게 되는데, roomInOutHistoryId 가 설정된 timeline 이 하나 추가된다.
                             // 구체적으로 시간을 적지 않는다. 대신, message queue 와 같은 것을 사용하여,
                             // queue 에서 message 를 꺼내어 database 에 저장하는 actor 와 비슷한 것이 시각을 정하도록 한다.
                             // 따라서, event 를 만드는 당사자는 event 의 발생시각을 정할 수 없다.
                             banHistoryId: Option[Int],
                             permissionHistoryId: Option[Int],
                             roomHistoryId: Option[Int],
                             roomInOutHistoryId: Option[Int],
                             talkHistoryId: Option[Int],
                             userNickHistoryId: Option[Int],

                             override val room: Option[RoomModel],
                             banHistory: Option[BanHistoryModel],
                             permissionHistory: Option[PermissionHistoryModel],
                             roomHistory: Option[RoomHistoryModel],
                             roomInOutHistory: Option[RoomInOutHistoryModel],
                             talkHistory: Option[TalkHistoryModel],
                             userNickHistory: Option[UserNickHistoryModel]) extends IdVersionModelTrait[RoomTimelineModel] with RoomModelTrait {
  def this() = this(None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): RoomTimelineModel = copy(version = increaseVersion())

  override def initVersion(): RoomTimelineModel = copy(version = Some(1))

  override def clearPrivacyData(): RoomTimelineModel = {
    copy(
      room = room.map(_.clearPrivacyData()),
      banHistory = banHistory.map(_.clearPrivacyData()),
      permissionHistory = permissionHistory.map(_.clearPrivacyData()),
      roomHistory = roomHistory.map(_.clearPrivacyData()),
      roomInOutHistory = roomInOutHistory.map(_.clearPrivacyData()),
      talkHistory = talkHistory.map(_.clearPrivacyData()),
      userNickHistory = userNickHistory.map(_.clearPrivacyData()))
  }
}

object RoomTimelineModelEx {
  def apply(id: Option[Int], version: Option[Int], roomId: Option[Int], banHistoryId: Option[Int], permissionHistoryId: Option[Int], roomHistoryId: Option[Int], roomInOutHistoryId: Option[Int], talkHistoryId: Option[Int], userNickHistoryId: Option[Int]) =
    RoomTimelineModel(id, version, roomId, banHistoryId, permissionHistoryId, roomHistoryId, roomInOutHistoryId, talkHistoryId, userNickHistoryId, None, None, None, None, None, None, None)

  def unapply(x: RoomTimelineModel) = Option((x.id, x.version, x.roomId, x.banHistoryId, x.permissionHistoryId, x.roomHistoryId, x.roomInOutHistoryId, x.talkHistoryId, x.userNickHistoryId))

}
