/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ IdVersionModelTrait, UserRoomModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.BanHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by Kang Woo, Lee on 4/19/15.
 */
case class BanModel(override val id: Option[Int],
                    override val version: Option[Int],
                    override val userId: Option[Int],
                    override val roomId: Option[Int],
                    relatedBanHistoryId: Option[Int],

                    // 금지 대상자.
                    override val user: Option[UserModel],
                    override val room: Option[RoomModel],
                    // none 이어서는 안된다. 금지대상자로 지정한다면, 반드시 관련된 금지설정 이력이 있어야 한다.
                    relatedBanHistory: Option[BanHistoryModel]) extends IdVersionModelTrait[BanModel] with UserRoomModelTrait {
  def this() = this(None, None, None, None, None, None, None, None)

  override def modifyVersion(): BanModel = copy(version = increaseVersion())

  override def initVersion(): BanModel = copy(version = Some(1))

  // 지금 현재 대화방의 입장금지 사용자 목록을 다룬다. 추가/삭제를 할 수 있다. 갱신은 의미가 없다.
  // 금지대상자가 되었다가 해제되었으면 그 사용자를 목록에 현재 금지대상자 목록에 남길 필요가 없다.

  override def clearPrivacyData(): BanModel = {
    copy(
      user = user.map(_.clearPrivacyData()),
      room = room.map(_.clearPrivacyData()),
      relatedBanHistory = relatedBanHistory.map(_.clearPrivacyData()))
  }
}

object BanModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], roomId: Option[Int], relatedBanHistoryId: Option[Int]) =
    BanModel(id, version, userId, roomId, relatedBanHistoryId, None, None, None)

  def unapply(x: BanModel) = Option((x.id, x.version, x.userId, x.roomId, x.relatedBanHistoryId))
}

class InvalidBanOperationException extends RuntimeException
class BannedException extends RuntimeException
