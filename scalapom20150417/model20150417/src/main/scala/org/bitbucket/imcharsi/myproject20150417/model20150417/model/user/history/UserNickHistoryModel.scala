/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.{ IdVersionModelTrait, UserModelTrait, WhenModelTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

/**
 * Created by KangWoo, Lee on 4/18/15.
 */
case class UserNickHistoryModel(override val id: Option[Int],
                                override val version: Option[Int],
                                override val userId: Option[Int],
                                override val when: Option[Date],
                                // 대화명은 각 방마다 다를 수 있도록 할 것인가, 전체 공통으로 할 것인가. 일단 공통으로.
                                nick: Option[String],

                                override val user: Option[UserModel]) extends IdVersionModelTrait[UserNickHistoryModel] with UserModelTrait with WhenModelTrait {
  def this() = this(None, None, None, None, None, None)

  override def modifyVersion(): UserNickHistoryModel = copy(version = increaseVersion())

  override def initVersion(): UserNickHistoryModel = copy(version = Some(1))

  override def clearPrivacyData(): UserNickHistoryModel = {
    val x = copy(user = user.map(_.clearPrivacyData()))
    x
  }
}

object UserNickHistoryModelEx {
  def apply(id: Option[Int], version: Option[Int], userId: Option[Int], when: Option[Date], nick: Option[String]) =
    UserNickHistoryModel(id, version, userId, when, nick, None)

  def unapply(x: UserNickHistoryModel) = Option((x.id, x.version, x.userId, x.when, x.nick))
}
