/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModel.EnvelopeDataType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.{ EnvelopeModelEx, LogoutModel, SayHiModel }
import org.scalatest.FunSuite

/**
 * Created by Kang Woo, Lee on 5/1/15.
 */
class TestEnvelopeModel extends FunSuite {
  test("envelope model apply") {
    import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnvelopeModel.EnvelopeDataType._
    Map[EnvelopeDataType, Any]((UserModelType, new UserModel()),
      (UserLoginHistoryModelType, new UserLoginHistoryModel()),
      (UserNickHistoryModelType, new UserNickHistoryModel()),
      (RoomModelType, new RoomModel()),
      (InvitationModelType, new InvitationModel()),
      (OperatorModelType, new OperatorModel()),
      (RoomAttenderModelType, new RoomAttenderModel()),
      (BanModelType, new BanModel()),
      (RoomHistoryModelType, new RoomHistoryModel()),
      (BanHistoryModelType, new BanHistoryModel()),
      (PermissionHistoryModelType, new PermissionHistoryModel()),
      (RoomInOutHistoryModelType, new RoomInOutHistoryModel()),
      (RoomTimelineModelType, new RoomTimelineModel()),
      (TalkHistoryModelType, new TalkHistoryModel()),
      (SayHiModelType, SayHiModel(None)),
      (LogoutModelType, LogoutModel(None))).foreach { case (a, b) ⇒ assert(EnvelopeModelEx(Some(b)).dataType == Some(a)) }
  }
}
