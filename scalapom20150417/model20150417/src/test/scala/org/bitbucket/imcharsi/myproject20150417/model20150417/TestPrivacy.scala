/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }
import org.scalamock.scalatest.MockFactory
import org.scalatest.FunSuite

/**
 * Created by Kang Woo, Lee on 5/18/15.
 */
class TestPrivacy extends FunSuite with MockFactory {
  test("UserModel") {
    // 중요한 것은, 특정단계가 실행되었다는 것을 확인하고, 특히 특정 class 에서는 특정 값도 지워졌다는 것을 확인해야 한다. 바로 아래 한단계 세부자료만 하면 된다.
    // 아래의 문서를 참고하였는데,
    // http://stackoverflow.com/questions/16558995/check-if-a-method-was-called-inside-another-method
    // 특정 method 가 실행되었다는 것을 확인하는 방법이 필요하다. 그래서 mock 을 써보았다. 잘 된다.
    // http://scalamock.org/
    // 이와 같은 검사가 타당한 것으로 인정될 수 있다고 생각하는 이유는, 모든 model 들이 각자의 세부자료 모두에 대해서 clearPrivacyData 를 부른다는 점과,
    // UserModel 과 RoomHistoryModel 의 경우 암호도 추가로 지워진다는 점 두 가지를 확인하기 때문이다.
    // 예를 들어서, InvitationModel 에 대한 clearPrivacyModel 이 내부의 자료인 user, byWho, room 에 대해 또다시 중첩하여 clearPrivacyData 를 부른다는 점과,
    // UserModel 의 clearPrivacyModel 이 currentUserNickHistory 와 currentLoginHistory 에 대해 clearPrivacyData 를 부른다는 점 등을
    // 각각 따로 확인하는 방법으로 검사를 해도 문제가 없다고 보는 것이다.
    // 각 model 의 모든 세부자료가 전부 검사되었다는 것을 자동으로 확인할 방법은 reflection 을 쓰면 가능할 듯 한데,
    // 일단 test code 에 확인하고자 하는 세부자료를 직접 나열한다.
    val userLoginHistory: UserLoginHistoryModel = mock[UserLoginHistoryModel]
    val userNickHistory: UserNickHistoryModel = mock[UserNickHistoryModel]
    (userLoginHistory.clearPrivacyData _).expects().returning(new UserLoginHistoryModel())
    (userNickHistory.clearPrivacyData _).expects().returning(new UserNickHistoryModel())
    val user: UserModel = new UserModel().copy(
      currentLoginHistory = Some(userLoginHistory),
      currentNickHistory = Some(userNickHistory),
      password = Some("password"))
    val clearedData = user.clearPrivacyData()
    // 암호가 사용된다.
    assert(clearedData.password.isEmpty)
  }

  test("UserLoginHistoryModel") {
    val user: UserModel = mock[UserModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    val userLoginHistory: UserLoginHistoryModel = new UserLoginHistoryModel().copy(user = Some(user))
    userLoginHistory.clearPrivacyData()
  }

  test("UserNickHistoryModel") {
    val user: UserModel = mock[UserModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    val userNickHistory: UserNickHistoryModel = new UserNickHistoryModel().copy(user = Some(user))
    userNickHistory.clearPrivacyData()
  }

  test("BanModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val banHistory = mock[BanHistoryModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (banHistory.clearPrivacyData _).expects().returning(new BanHistoryModel())
    val data = new BanModel().copy(user = Some(user), room = Some(room), relatedBanHistory = Some(banHistory))
    data.clearPrivacyData()
  }

  test("InvitationModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val byWho = mock[UserModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (byWho.clearPrivacyData _).expects().returning(new UserModel())
    val data = new InvitationModel().copy(user = Some(user), room = Some(room), byWho = Some(byWho))
    data.clearPrivacyData()
  }

  test("OperatorModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val permissionHistory = mock[PermissionHistoryModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (permissionHistory.clearPrivacyData _).expects().returning(new PermissionHistoryModel())
    val data = new OperatorModel().copy(user = Some(user), room = Some(room), relatedPermissionHistory = Some(permissionHistory))
    data.clearPrivacyData()
  }

  test("RoomAttenderModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val roomInOutHistory = mock[RoomInOutHistoryModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (roomInOutHistory.clearPrivacyData _).expects().returning(new RoomInOutHistoryModel())
    val data = new RoomAttenderModel().copy(user = Some(user), room = Some(room), relatedRoomInOutHistory = Some(roomInOutHistory))
    data.clearPrivacyData()
  }

  test("RoomModel") {
    val byWho = mock[UserModel]
    val owner = mock[UserModel]
    val roomHistory = mock[RoomHistoryModel]
    (byWho.clearPrivacyData _).expects().returning(new UserModel())
    (roomHistory.clearPrivacyData _).expects().returning(new RoomHistoryModel())
    val data = new RoomModel().copy(byWho = Some(byWho), currentDetail = Some(roomHistory))
    data.clearPrivacyData()
  }

  test("BanHistoryModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val byWho = mock[UserModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (byWho.clearPrivacyData _).expects().returning(new UserModel())
    val data = new BanHistoryModel().copy(user = Some(user), room = Some(room), byWho = Some(byWho))
    data.clearPrivacyData()
  }

  test("PermissionHistoryModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val byWho = mock[UserModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (byWho.clearPrivacyData _).expects().returning(new UserModel())
    val data = new PermissionHistoryModel().copy(user = Some(user), room = Some(room), byWho = Some(byWho))
    data.clearPrivacyData()
  }

  test("RoomHistoryModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val byWho = mock[UserModel]
    val relatedPermissionHistory = mock[PermissionHistoryModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (byWho.clearPrivacyData _).expects().returning(new UserModel())
    (relatedPermissionHistory.clearPrivacyData _).expects().returning(new PermissionHistoryModel())
    val data = new RoomHistoryModel().copy(user = Some(user), room = Some(room), byWho = Some(byWho), relatedPermissionHistory = Some(relatedPermissionHistory))
    // 암호가 사용된다.
    assert(data.clearPrivacyData().password.isEmpty)
  }

  test("RoomInOutHistoryModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val byWho = mock[UserModel]
    val invitation = mock[InvitationModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (byWho.clearPrivacyData _).expects().returning(new UserModel())
    (invitation.clearPrivacyData _).expects().returning(new InvitationModel())
    val data = new RoomInOutHistoryModel().copy(user = Some(user), room = Some(room), byWho = Some(byWho), invitation = Some(invitation))
    data.clearPrivacyData()
  }

  test("RoomTimelineModel") {
    val room = mock[RoomModel]
    val banHistory = mock[BanHistoryModel]
    val talkHistory = mock[TalkHistoryModel]
    val roomHistory = mock[RoomHistoryModel]
    val roomInOutHistory = mock[RoomInOutHistoryModel]
    val permissionHistory = mock[PermissionHistoryModel]
    val userNickHistory = mock[UserNickHistoryModel]
    (banHistory.clearPrivacyData _).expects().returning(new BanHistoryModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (talkHistory.clearPrivacyData _).expects().returning(new TalkHistoryModel())
    (permissionHistory.clearPrivacyData _).expects().returning(new PermissionHistoryModel())
    (roomHistory.clearPrivacyData _).expects().returning(new RoomHistoryModel())
    (roomInOutHistory.clearPrivacyData _).expects().returning(new RoomInOutHistoryModel())
    (userNickHistory.clearPrivacyData _).expects().returning(new UserNickHistoryModel())
    val data = new RoomTimelineModel().copy(room = Some(room), banHistory = Some(banHistory), talkHistory = Some(talkHistory), permissionHistory = Some(permissionHistory), roomHistory = Some(roomHistory), roomInOutHistory = Some(roomInOutHistory), userNickHistory = Some(userNickHistory))
    data.clearPrivacyData()
  }

  test("TalkHistoryModel") {
    val user = mock[UserModel]
    val room = mock[RoomModel]
    val userNickHistory: UserNickHistoryModel = mock[UserNickHistoryModel]
    (user.clearPrivacyData _).expects().returning(new UserModel())
    (room.clearPrivacyData _).expects().returning(new RoomModel())
    (userNickHistory.clearPrivacyData _).expects().returning(new UserNickHistoryModel())
    val data = new TalkHistoryModel().copy(user = Some(user), room = Some(room), nick = Some(userNickHistory))
    data.clearPrivacyData()
  }
}
