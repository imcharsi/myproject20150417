/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.model20150417

import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.{ TalkHistoryModel, TalkHistoryModelEx }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.{ UserModel, UserModelEx }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserLoginHistoryModelEx, UserNickHistoryModel, UserNickHistoryModelEx }
import org.bitbucket.imcharsi.myproject20150417.testutil20150417.TestUtilTrait
import org.scalatest.FunSuite

/**
 * Created by Kang Woo, Lee on 4/19/15.
 */
class TestModelClassValidity extends FunSuite with TestUtilTrait {
  test("model class validity 1") {
    assert(testModelClassValidity1[FailTestClass, IdVersionModelTrait[_]] == false)

    assert(testModelClassValidity1[BanModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[InvitationModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[OperatorModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[RoomModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[BanHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[PermissionHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[RoomHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[RoomInOutHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[TalkHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[UserModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[UserLoginHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[UserNickHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[RoomTimelineModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity1[RoomAttenderModel, IdVersionModelTrait[_]])
  }

  test("model class validity 2") {
    assert(testModelClassValidity2[FailTestClass, IdVersionModelTrait[_]] == false)

    assert(testModelClassValidity2[BanModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[InvitationModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[OperatorModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[RoomModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[BanHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[PermissionHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[RoomHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[RoomInOutHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[TalkHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[UserModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[UserLoginHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[UserNickHistoryModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[RoomTimelineModel, IdVersionModelTrait[_]])
    assert(testModelClassValidity2[RoomAttenderModel, IdVersionModelTrait[_]])
  }

  test("model class validity 3") {
    assert(testModelClassValidity3[FailTestClass, NotPersisted, IdVersionModelTrait[_]](FailTestClassEx) == false)

    assert(testModelClassValidity3[BanModel, NotPersisted, IdVersionModelTrait[_]](BanModelEx))
    assert(testModelClassValidity3[InvitationModel, NotPersisted, IdVersionModelTrait[_]](InvitationModelEx))
    assert(testModelClassValidity3[OperatorModel, NotPersisted, IdVersionModelTrait[_]](OperatorModelEx))
    assert(testModelClassValidity3[RoomModel, NotPersisted, IdVersionModelTrait[_]](RoomModelEx))
    assert(testModelClassValidity3[BanHistoryModel, NotPersisted, IdVersionModelTrait[_]](BanHistoryModelEx))
    assert(testModelClassValidity3[PermissionHistoryModel, NotPersisted, IdVersionModelTrait[_]](PermissionHistoryModelEx))
    assert(testModelClassValidity3[RoomHistoryModel, NotPersisted, IdVersionModelTrait[_]](RoomHistoryModelEx))
    assert(testModelClassValidity3[RoomInOutHistoryModel, NotPersisted, IdVersionModelTrait[_]](RoomInOutHistoryModelEx))
    assert(testModelClassValidity3[TalkHistoryModel, NotPersisted, IdVersionModelTrait[_]](TalkHistoryModelEx))
    assert(testModelClassValidity3[UserModel, NotPersisted, IdVersionModelTrait[_]](UserModelEx))
    assert(testModelClassValidity3[UserLoginHistoryModel, NotPersisted, IdVersionModelTrait[_]](UserLoginHistoryModelEx))
    assert(testModelClassValidity3[UserNickHistoryModel, NotPersisted, IdVersionModelTrait[_]](UserNickHistoryModelEx))
    assert(testModelClassValidity3[RoomTimelineModel, NotPersisted, IdVersionModelTrait[_]](RoomTimelineModelEx))
    assert(testModelClassValidity3[RoomAttenderModel, NotPersisted, IdVersionModelTrait[_]](RoomAttenderModelEx))
  }

  test("model class validity 4") {
    assert(testModelClassValidity4[FailTestClass] == false)

    assert(testModelClassValidity4[BanModel])
    assert(testModelClassValidity4[InvitationModel])
    assert(testModelClassValidity4[OperatorModel])
    assert(testModelClassValidity4[RoomModel])
    assert(testModelClassValidity4[BanHistoryModel])
    assert(testModelClassValidity4[PermissionHistoryModel])
    assert(testModelClassValidity4[RoomHistoryModel])
    assert(testModelClassValidity4[RoomInOutHistoryModel])
    assert(testModelClassValidity4[TalkHistoryModel])
    assert(testModelClassValidity4[UserModel])
    assert(testModelClassValidity4[UserLoginHistoryModel])
    assert(testModelClassValidity4[UserNickHistoryModel])
    assert(testModelClassValidity4[RoomTimelineModel])
    assert(testModelClassValidity4[RoomAttenderModel])
  }
}

case class FailTestClass(@NotPersisted userId: Option[Int], user: Option[UserModel], room: Option[RoomModel]) {
}

object FailTestClassEx {
  def apply(userId: Option[Int]) = FailTestClass(userId, None, None)

  def unapply(x: FailTestClass) = Option[Tuple1[Option[Int]]](Tuple1(x.userId))
}

