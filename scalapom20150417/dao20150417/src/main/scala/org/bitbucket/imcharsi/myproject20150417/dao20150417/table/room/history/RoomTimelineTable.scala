/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin.{ IdVersionTableTrait, RoomTableTrait }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.talk.history.TalkHistoryTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.UserNickHistoryTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.{ RoomTimelineModel, RoomTimelineModelEx }

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
class RoomTimelineTable(t: Tag) extends Table[RoomTimelineModel](t, "room_timeline_table") with IdVersionTableTrait[RoomTimelineModel] with RoomTableTrait[RoomTimelineModel] {
  def banHistoryId = column[Option[Int]]("ban_history_id")

  def permissionHistoryId = column[Option[Int]]("perm_hist_id")

  def roomHistoryId = column[Option[Int]]("room_hist_id")

  def roomInOutHistoryId = column[Option[Int]]("room_inout_hist_id")

  def talkHistoryId = column[Option[Int]]("talk_hist_id")

  def userNickHistoryId = column[Option[Int]]("user_nick_hist_id")

  def foreignKeyBanHistory = foreignKey(tableName + "_ban_history_id", banHistoryId, BanHistoryTable)(_.id)

  def foreignKeyPermissionHistory = foreignKey(tableName + "_perm_hist_id", permissionHistoryId, PermissionHistoryTable)(_.id)

  def foreignKeyRoomHistory = foreignKey(tableName + "_room_hist_id", roomHistoryId, RoomHistoryTable)(_.id)

  def foreignKeyRoomInOutHistory = foreignKey(tableName + "_room_inout_hist_id", roomInOutHistoryId, RoomInOutHistoryTable)(_.id)

  def foreignKeyTalkHistory = foreignKey(tableName + "_talk_hist_id", talkHistoryId, TalkHistoryTable)(_.id)

  def foreignKeyUserNickHistory = foreignKey(tableName + "_user_nick_hist_id", userNickHistoryId, UserNickHistoryTable)(_.id)

  override def * = (id, version, roomId, banHistoryId, permissionHistoryId, roomHistoryId, roomInOutHistoryId, talkHistoryId, userNickHistoryId) <> ((RoomTimelineModelEx.apply _).tupled, RoomTimelineModelEx.unapply)
}

object RoomTimelineTable extends TableQuery[RoomTimelineTable](x ⇒ new RoomTimelineTable(x))
