/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.WhenModelTrait

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
trait WhenTableTrait[T <: WhenModelTrait] extends Table[T] with DateMapperTrait {
  def when = column[Option[Date]]("when")
}
