/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin.IdVersionTableTrait
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.mixin.Dao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
trait SlickDao[ModelType <: IdVersionModelTrait[ModelType], TableType <: IdVersionTableTrait[ModelType]] extends Dao[ModelType] with DateMapperTrait {
  val tableQuery: TableQuery[TableType]

  override def getOne(id: Option[Int]): Option[ModelType] = {
    DriverSettings.run(getOneAction(id).transactionally)
  }

  override def postOne(data: ModelType): Option[Int] = {
    DriverSettings.run(postOneAction(data).transactionally)
  }

  override def putOne(data: ModelType): Int = {
    DriverSettings.run(putOneAction(data).transactionally)
  }

  override def deleteOne(id: Option[Int], version: Option[Int]): Int = {
    DriverSettings.run(deleteOneAction(id, version).transactionally)
  }

  // 아래와 같이 Action 을 반환하는 method 를 두지 않으면, 여러개의 database 명령을 하나의 transaction 에 묶어야 하는 상황에서 똑같은 code 를 반복해서 써야 한다.
  // 아래와 같은 method 를 쓰면 조합을 좀더 편리하게 할 수 있다.
  def putOneAction(data: ModelType): DBIOAction[Int, NoStream, Effect.Read with Effect.Write] = {
    tableQuery.filter(t ⇒ t.id === data.id && t.version === data.version).update(data.modifyVersion())
  }

  def postOneAction(data: ModelType): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    tableQuery.returning(tableQuery.map(_.id)).+=(data.initVersion())
  }

  def getOneAction(id: Option[Int]): DBIOAction[Option[ModelType], NoStream, Effect.Read] = {
    tableQuery.filter(_.id === id).result.headOption
  }

  def deleteOneAction(id: Option[Int], version: Option[Int]): DBIOAction[Int, NoStream, Effect.Write] = {
    tableQuery.filter(t ⇒ t.id === id && t.version === version).delete
  }
}
