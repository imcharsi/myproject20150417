/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.talk.history.TalkHistoryDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.history.UserNickHistoryDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.RoomTimelineTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.history.RoomTimelineDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomTimelineModel

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object RoomTimelineDao extends RoomTimelineDao with SlickDao[RoomTimelineModel, RoomTimelineTable] {
  override val tableQuery = RoomTimelineTable

  override def getOneAction(id: Option[Int]): DBIOAction[Option[RoomTimelineModel], NoStream, Effect.Read] = {
    // room timeline 에서는 room 을 구하지 않는다.
    RoomTimelineTable.filter(t ⇒ t.id === id).result.headOption.flatMap { x ⇒
      // 이와 같이 하면 불편하다. 아래는, room timeline model 에는 한개의 자료만 연결될수 있다는 전제를 두고 있다. 이는 check 제약조건으로 풀수 있긴하다.
      // 실제로는 아무것도 하지 않는 DBIOAction 이 있었으면 좋겠다.
      x.flatMap(_.talkHistoryId).map { talkHistoryId ⇒
        TalkHistoryDao.getOneAction(Option(talkHistoryId)).map(xx ⇒ x.map(_.copy(talkHistory = xx)))
      }.orElse {
        x.flatMap(_.banHistoryId).map { banHistoryId ⇒
          BanHistoryDao.getOneAction(Option(banHistoryId)).map(xx ⇒ x.map(_.copy(banHistory = xx)))
        }
      }.orElse {
        x.flatMap(_.permissionHistoryId).map { permissionHistoryId ⇒
          PermissionHistoryDao.getOneAction(Option(permissionHistoryId)).map(xx ⇒ x.map(_.copy(permissionHistory = xx)))
        }
      }.orElse {
        x.flatMap(_.roomHistoryId).map { roomHistoryId ⇒
          RoomHistoryDao.getOneAction(Option(roomHistoryId)).map(xx ⇒ x.map(_.copy(roomHistory = xx)))
        }
      }.orElse {
        x.flatMap(_.roomInOutHistoryId).map { roomInOutHistoryId ⇒
          RoomInOutHistoryDao.getOneAction(Option(roomInOutHistoryId)).map(xx ⇒ x.map(_.copy(roomInOutHistory = xx)))
        }
      }.orElse {
        x.flatMap(_.userNickHistoryId).map { userNickHistoryId ⇒
          UserNickHistoryDao.getOneAction(Option(userNickHistoryId)).map(xx ⇒ x.map(_.copy(userNickHistory = xx)))
        }
      }.get
    }
  }
}
