/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history.{ BanHistoryDao, RoomTimelineDao }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.{ BanTable, OperatorTable, RoomTable }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.RoomHistoryTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.{ UserLoginHistoryTable, UserNickHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.current.BanDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait.NotFound
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ BanModel, InvalidBanOperationException, NotOperatorException }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.BanHistoryModel.BanType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.{ BanHistoryModelEx, RoomTimelineModelEx }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object BanDao extends BanDao with SlickDao[BanModel, BanTable] {
  override val tableQuery = BanTable

  type QueryType = (((BanTable, Rep[Option[UserTable]]), Rep[Option[UserNickHistoryTable]]), Rep[Option[UserLoginHistoryTable]])
  type ResultType = (((BanModel, Option[UserModel]), Option[UserNickHistoryModel]), Option[UserLoginHistoryModel])

  override def getOneByUserRoom(userId: Option[Int], roomId: Option[Int]): Option[BanModel] = {
    DriverSettings.run(getOneByUserRoomAction(userId, roomId).transactionally)
  }

  def getOneByUserRoomAction(userId: Option[Int], roomId: Option[Int]): DBIOAction[Option[BanModel], NoStream, Effect.Read] = {
    BanTable.filter(t ⇒ t.userId === userId && t.roomId === roomId).result.headOption
  }

  def getAllByRoom(roomId: Option[Int]): Seq[BanModel] = {
    DriverSettings.run(getAllByRoomAction(roomId).transactionally)
  }

  private def innerJoin(q: Query[BanTable, BanModel, Seq]): Query[QueryType, ResultType, Seq] = {
    q.joinLeft(UserTable).on(_.userId === _.id).
      joinLeft(UserNickHistoryTable).on {
        case ((_, a), b) ⇒ a.flatMap(_.currentNickHistoryId) === b.id
      }.joinLeft(UserLoginHistoryTable).on {
        case (((_, a), _), b) ⇒ a.flatMap(_.currentLoginHistoryId) === b.id
      }
  }

  private def innerMap(x: ResultType): BanModel = {
    val (((ban, user), userNickHistory), userLoginHistory) = x
    ban.copy(user = user.map(_.copy(currentNickHistory = userNickHistory, currentLoginHistory = userLoginHistory)))
  }

  override def getOneAction(id: Option[Int]): DBIOAction[Option[BanModel], NoStream, Effect.Read] = {
    innerJoin(BanTable.filter(t ⇒ t.id === id)).result.headOption.map(x ⇒ x.map(innerMap))
  }

  override def clearUserBan(userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int]): Option[Int] = {
    DriverSettings.run(clearUserBanAction(userId, roomId, byWhoId).transactionally)
  }

  def getAllByRoomAction(roomId: Option[Int]): DBIOAction[Seq[BanModel], NoStream, Effect.Read] = {
    innerJoin(BanTable.filter(t ⇒ t.roomId === roomId)).result.map(x ⇒ x.map(innerMap))
  }

  def clearUserBanAction(userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    val now = DriverSettings.now()

    // 입장금지와 규칙이 비슷하다.
    RoomTable.filter(t ⇒ t.id === roomId).
      joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).result.headOption.map {
        case Some((x, xx)) ⇒ x.copy(currentDetail = xx)
        case None ⇒ throw new NotFound
      }.flatMap { roomModel ⇒
        // 자기가 자기를 입장허용하는 것은 안되도록 한다.
        if (byWhoId.isDefined && userId == byWhoId) {
          throw new InvalidBanOperationException
        }
        OperatorTable.filter(t ⇒ t.userId === byWhoId).result.headOption.map {
          case Some(x) ⇒ x
          case None ⇒
            // 방장 권한도 없고, 운영자 권한도 없으면 실패.
            if (roomModel.currentDetail.flatMap(_.userId) != byWhoId) {
              throw new NotOperatorException
            }
        }.flatMap { _ ⇒
          // 입장금지설정이 되어 있지 않으면 실패.
          BanDao.getOneByUserRoomAction(userId, roomId).map { x ⇒
            x match {
              case Some(_) ⇒ x
              case None ⇒ throw new InvalidBanOperationException
            }
          }
        }.flatMap { banModel ⇒
          // 여기까지 왔으면,
          // 1. 입장허용설정을 하는 사람은 방장이거나 운영자이고,
          // 2. 입장허용설정의 대상이 되는 사람은 현재 입장금지 설정이 되어 있는 사람이어야 한다.
          // 입장허용 이력을 만들고
          BanHistoryDao.postOneAction(BanHistoryModelEx(None, None, userId, roomModel.id, byWhoId, Some(BanType.Allow), now)).
            map(x ⇒ (x, banModel))
        }.flatMap {
          case (banHistoryId, banModel) ⇒
            // 입장금지 현재상태를 지우고
            BanDao.deleteOneAction(banModel.flatMap(_.id), banModel.flatMap(_.version)).map(_ ⇒ banHistoryId)
        }.flatMap { banHistoryId ⇒
          // 입장금지에 관한 room timeline 을 만든다.
          RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomModel.id, banHistoryId, None, None, None, None, None))
        }
      }
  }

}
