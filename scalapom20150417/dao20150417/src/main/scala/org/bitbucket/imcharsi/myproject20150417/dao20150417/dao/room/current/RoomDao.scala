/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current

import java.util.Calendar

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.talk.history.TalkHistoryDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.{ PermissionHistoryTable, RoomHistoryTable, RoomInOutHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.current.RoomDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnterRoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait.{ NotFound, VersionException }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.BanHistoryModel.BanType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object RoomDao extends RoomDao with SlickDao[RoomModel, RoomTable] with DateMapperTrait {
  override val tableQuery = RoomTable

  type QueryType = (((((((RoomTable, Rep[Option[RoomHistoryTable]]), Rep[Option[UserTable]]), Rep[Option[PermissionHistoryTable]]), Rep[Option[UserTable]]), Rep[Option[UserTable]]), Rep[Option[UserTable]]), Rep[Option[UserTable]])
  type ResultType = (((((((RoomModel, Option[RoomHistoryModel]), Option[UserModel]), Option[PermissionHistoryModel]), Option[UserModel]), Option[UserModel]), Option[UserModel]), Option[UserModel])

  override def getOneAction(id: Option[Int]): DBIOAction[Option[RoomModel], NoStream, Effect.Read] = {
    val action: DBIOAction[Option[ResultType], NoStream, Effect.Read] =
      innerJoin(RoomTable.filter(t ⇒ t.id === id)).result.headOption
    action.map(x ⇒ x.map(innerMap))
  }

  private def innerJoin(t: Query[RoomTable, RoomModel, Seq]): Query[QueryType, ResultType, Seq] = {
    t.joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).
      joinLeft(UserTable).on {
        case ((a, _), b) ⇒ a.byWhoId === b.id
      }.joinLeft(PermissionHistoryTable).on {
        case (((_, a), _), b) ⇒ a.flatMap(_.relatedPermissionHistoryId) === b.id
      }.joinLeft(UserTable).on {
        case ((((_, a), _), _), b) ⇒ a.flatMap(_.userId) === b.id
      }.joinLeft(UserTable).on {
        case (((((_, a), _), _), _), b) ⇒ a.flatMap(_.byWhoId) === b.id
      }.joinLeft(UserTable).on {
        case ((((((_, _), _), a), _), _), b) ⇒ a.flatMap(_.userId) === b.id
      }.joinLeft(UserTable).on {
        case (((((((_, _), _), a), _), _), _), b) ⇒ a.flatMap(_.byWhoId) === b.id
      }
  }

  private def innerMap(x: ResultType): RoomModel = {
    val (((((((room, roomHistory), roomByWho), permissionHistory), roomHistoryUser), roomHistoryByWho), permissionHistoryUser), permissionHistoryByWho) = x
    room.copy(currentDetail = roomHistory.map(_.copy(
      user = roomHistoryUser, byWho = roomHistoryByWho,
      relatedPermissionHistory = permissionHistory.map(_.copy(
        user = permissionHistoryUser, byWho = permissionHistoryByWho)),
      needPassword = roomHistory.flatMap(_.password).map(x ⇒ !x.isEmpty))),
      byWho = roomByWho)
  }

  override def createRoom(userModel: UserModel, roomModel: RoomModel): Option[(RoomModel, List[Option[Int]])] = {
    DriverSettings.run(createRoomAction(userModel, roomModel).transactionally)
  }

  override def enterRoom(userId: Option[Int], roomId: Option[Int], enterRoomModel: Option[EnterRoomModel]): Option[Int] = {
    DriverSettings.run(enterRoomAction(userId, roomId, enterRoomModel).transactionally)
  }

  override def leaveRoom(userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int]): List[Option[Int]] = {
    DriverSettings.run(leaveRoomAction(userId, roomId, byWhoId).transactionally)
  }

  override def closeRoom(roomId: Option[Int]): Unit = {
    DriverSettings.run(closeRoomAction(roomId).transactionally)
  }

  override def invite(roomId: Option[Int], userId: Option[Int], byWhoId: Option[Int]): Option[Int] = {
    DriverSettings.run(inviteAction(roomId, userId, byWhoId).transactionally)
  }

  override def talk(roomId: Option[Int], userId: Option[Int], talk: Option[String]): Option[Int] = {
    DriverSettings.run(talkAction(roomId, userId, talk).transactionally)
  }

  override def getAllCreatedRooms(userId: Option[Int] = None): Seq[RoomModel] = {
    DriverSettings.run(getAllCreatedRoomsAction(userId).transactionally)
  }

  override def revokeOwnerPermission(userId: Option[Int], roomId: Option[Int], candidateUserId: Option[Int]): List[Option[Int]] = {
    DriverSettings.run(revokeOwnerPermissionAction(userId, roomId, candidateUserId).transactionally)
  }

  override def setRoomName(roomId: Option[Int], name: Option[String], byWhoId: Option[Int]): Option[Int] = {
    DriverSettings.run(setRoomNameAction(roomId, name, byWhoId).transactionally)
  }

  def setRoomPassword(byWhoId: Option[Int], roomId: Option[Int], password: Option[String]): Option[Int] = {
    DriverSettings.run(setRoomPasswordAction(byWhoId, roomId, password).transactionally)
  }

  def createRoomAction(userModel: UserModel, roomModel: RoomModel): DBIOAction[Option[(RoomModel, List[Option[Int]])], NoStream, Effect.Read with Effect.Write] = {
    val now = DriverSettings.now()
    val list = mutable.MutableList[Option[Int]]()

    // 1. room model 부터 만든다. 개설시각과 개설자만 쓴다.
    RoomDao.postOneAction(RoomModelEx(None, None, now, None, None, userModel.id)).flatMap { roomId ⇒
      // 2. 개설자가 방장권한을 수여받게 되는 permission history/room timeline 을 만든다.
      PermissionHistoryDao.postOneAction(PermissionHistoryModelEx(None, None, userModel.id, roomId, None, Some(PermissionType.GrantOwner), now)).flatMap { permHistId1 ⇒
        RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, permHistId1, None, None, None, None)).flatMap { roomTimelineId1 ⇒
          list.+=(roomTimelineId1)
          // 3. 1.에서 만든 room id 와 2.에서 만든 permission history id 를 사용하여 room history 를 만든다.
          RoomHistoryDao.postOneAction(RoomHistoryModelEx(None, None, roomId, userModel.id, None, now, roomModel.currentDetail.flatMap(_.name), roomModel.currentDetail.flatMap(_.password), permHistId1)).flatMap { roomHistoryId ⇒
            // 4. 3.에서 만든 room history id 를 사용하여 room timeline 을 만든다.
            RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, None, roomHistoryId, None, None, None)).flatMap { roomTimelineId2 ⇒
              list.+=(roomTimelineId2)
              // 5. 1.에서 만든 room model 의 current detail id 를 3.에서 만든 room history id 로 바꿔쓴다.
              RoomTable.filter(t ⇒ t.id === roomId).result.headOption.flatMap { newRoomModel ⇒
                RoomDao.putOneAction(newRoomModel.map(_.copy(currentDetailId = roomHistoryId)).get).map {
                  case 1 ⇒
                  case _ ⇒ throw new VersionException
                }
              }.flatMap { _ ⇒
                // 6. 대화방 입장에 관한 room in/out history 와 room timeline, room attender 를 만든다.
                RoomInOutHistoryDao.postOneAction(RoomInOutHistoryModelEx(None, None, userModel.id, roomId, None, None, now, Some(EnterType.Enter))).flatMap { roomInOutHistoryId ⇒
                  RoomAttenderDao.postOneAction(RoomAttenderModelEx(None, None, userModel.id, roomId, roomInOutHistoryId)).flatMap { _ ⇒
                    RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, None, None, roomInOutHistoryId, None, None)).map { roomTimelineId3 ⇒
                      list.+=(roomTimelineId3)
                      (permHistId1)
                    }
                  }
                }
              }
            }
          }
        }
      }.flatMap { _ ⇒
        RoomDao.getOneAction(roomId).map(x ⇒ x.map((_, list.toList)))
      }
    }
  }

  def enterRoomAction(userId: Option[Int], roomId: Option[Int], enterRoomModel: Option[EnterRoomModel]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    val now = DriverSettings.now()

    // 1. 입장하려는 대화방이 없거나 폐쇄되었는지 확인한다.
    RoomTable.filter(t ⇒ t.id === roomId).joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).result.headOption.map {
      case None ⇒ throw new NotFound
      case Some((x, xx)) ⇒
        if (x.closedDateTime.isDefined) {
          throw new ClosedRoomException
        }
        x.copy(currentDetail = xx)
    }.flatMap { roomModel ⇒
      // 2. 입장금지 여부를 확인한다.
      BanDao.getOneByUserRoomAction(userId, roomId).map {
        case Some(_) ⇒ throw new BannedException
        case None ⇒
      }.flatMap { _ ⇒
        enterRoomModel.flatMap(_.invitationId) match {
          case x @ Some(_) ⇒
            // 3.1. 초대장이 제시되었으면, 무조건 입장하고, 암호가 설정되었든 아니든 초대장은 사용한 것으로 한다.
            InvitationTable.filter(t ⇒ t.id === x.asColumnOf[Option[Int]] && t.roomId === roomId && t.userId === userId && t.validity === Option(true) && t.expirationDateTime >= now).result.headOption.map {
              // 초대장이 없거나 만료되었거나 무효이거나 이 대화방에서 쓸수 있는 것이 아니거나 이 사용자를 초대하는 초대장이 아니거나.
              case None ⇒ throw new PrivateRoomException
              case Some(x) ⇒ x
            }.flatMap { invitation ⇒
              InvitationDao.putOneAction(invitation.copy(validity = Some(false))).map {
                case 1 ⇒ invitation.id
                case _ ⇒ throw new VersionException
              }
            }
          case None ⇒
            // 3.2. 초대장이 제시되지 않았으면 암호가 맞는지 확인한다.
            // 암호를 사용하지 않는 대화방이라면 사용자 역시 암호를 제시하지 않아야 한다. 결론은 None==None 이어야 한다.
            if (roomModel.currentDetail.flatMap(_.password).isDefined) {
              if (roomModel.currentDetail.flatMap(_.password) != enterRoomModel.flatMap(_.password)) {
                throw new PrivateRoomException
              }
            }
            nothingSql[Int]()
        }
      }.flatMap { invitationId ⇒
        // 4. room in/out history, room attender, room timeline 를 쓴다.
        // 초대장없이 입장한다면 invitationId 는 none 이 된다.
        RoomInOutHistoryDao.postOneAction(RoomInOutHistoryModelEx(None, None, userId, roomId, invitationId, None, now, Some(EnterType.Enter)))
      }.flatMap { roomInOutHistoryId ⇒
        RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, None, None, roomInOutHistoryId, None, None)).flatMap { roomTimelineId ⇒
          RoomAttenderDao.postOneAction(RoomAttenderModelEx(None, None, userId, roomId, roomInOutHistoryId)).map(_ ⇒ roomTimelineId)
        }
      }
    }
  }

  def nothingSql[T](): DBIOAction[Option[T], NoStream, Effect.Read] = Query(1).result.headOption.map(_ ⇒ None)

  def leaveRoomAction(userId: Option[Int], roomId: Option[Int], byWhoId: Option[Int]): DBIOAction[List[Option[Int]], NoStream, Effect.Read with Effect.Write] = {
    // 이와 같이 하기보다는 차라리 pl/sql 을 사용하는 것이 낫다. 일단, 이렇게 하자.
    // 여기서는 방을 나가는 것만 다룬다. 방에 아무도 없게 된 경우 방을 폐쇄하는 것에 관해서는 다루지 않는다.
    val now = DriverSettings.now()
    val list = mutable.MutableList[Option[Int]]()
    // 퇴장에 관한 room in/out history 남기기/room timeline 남기기 room attender 지우기.
    // 방장이 퇴장하는 경우에만 room history/room timeline 을 남기고, room 을 바꾼다.

    // 유효한 대화방인지 확인한다.
    RoomTable.filter(t ⇒ t.id === roomId && t.closedDateTime.isEmpty).joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).result.headOption.map {
      case Some((x, xx)) ⇒ x.copy(currentDetail = xx)
      case None ⇒ throw new NotFound
    }.flatMap { roomModel ⇒
      // 강제퇴장에 관한 경우를 먼저 다루기.
      val x = if (byWhoId.isDefined) {
        // 방장/운영자 구분없이 스스로를 강제퇴장시킬수 없다.
        val x = if (userId == byWhoId) {
          throw new InvalidBanOperationException
        } else if (byWhoId == roomModel.currentDetail.flatMap(_.userId)) {
          nothingSql()
        } else {
          // 운영자가 상대방을 강제퇴장시키는 경우 강제퇴장 당하는 사람은 운영자가 아니어야 한다.
          OperatorTable.filter(t ⇒ t.roomId === roomId && t.userId === byWhoId).result.headOption.map {
            case None ⇒
              // 강제퇴장하는 사람이 운영자권한이 없으면 실패.
              throw new InvalidBanOperationException
            case Some(_) ⇒
          }.flatMap { _ ⇒
            OperatorTable.filter(t ⇒ t.roomId === roomId && t.userId === userId).result.headOption.map {
              case None ⇒
              case Some(_) ⇒
                // 강제퇴장당하는 사람이 운영자권한이 있으면 실패.
                throw new InvalidBanOperationException
            }
          }
        }
        x.flatMap { _ ⇒
          // 강제퇴장ᅟ인 경우 ban/ban history/room timeline 남기기
          BanHistoryDao.postOneAction(BanHistoryModelEx(None, None, userId, roomId, byWhoId, Some(BanType.Disallow), now)).flatMap { banHistoryId ⇒
            BanDao.postOneAction(BanModelEx(None, None, userId, roomId, banHistoryId)).map(_ ⇒ banHistoryId).flatMap { banHistoryId ⇒
              RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, banHistoryId, None, None, None, None, None)).map { x ⇒
                list.+=(x)
                x
              }
            }
          }
        }
      } else {
        nothingSql()
      }
      x.map(_ ⇒ roomModel)
    }.flatMap { roomModel ⇒
      // 퇴장하는 사람이 방장인 경우
      if (roomModel.currentDetail.flatMap(_.userId) == userId) {
        // 방장권한회수에 관한 permission history 남기기/room timeline 남기기/room history 남기기
        PermissionHistoryDao.postOneAction(PermissionHistoryModelEx(None, None, userId, roomId, None, Some(PermissionType.RevokeOwner), now)).flatMap { revokeOwnerPermHistId ⇒
          RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, revokeOwnerPermHistId, None, None, None, None)).flatMap { roomTimelineId1 ⇒
            list.+=(roomTimelineId1)
            RoomHistoryDao.getOneAction(roomModel.currentDetailId).flatMap { roomHistory ⇒
              // 일단, 방장이 없다고 써놓는다. 아래에서, 방장후보를 찾으면 다시 방장이 있다고 쓴다.
              RoomHistoryDao.postOneAction(roomHistory.map(_.copy(userId = None, relatedPermissionHistoryId = revokeOwnerPermHistId, when = now)).get)
            }
          }
        }.flatMap { roomHistoryId ⇒
          // 방장후보구하기 방장권한 수여에 관한 permission history 남기기/room timeline 남기기/room history 남기기/room timeline 남기기
          // 방장후보를 구하지 못했으면 방장이 없다고 써야 한다.
          RoomAttenderTable.filter(t ⇒ t.roomId === roomId && t.userId =!= userId).
            joinLeft(RoomInOutHistoryTable).on(_.relatedRoomInOutHistoryId === _.id).
            sortBy { case ((a, b)) ⇒ b.flatMap(_.when).asc }.result.headOption.map { case Some((a, _)) ⇒ Some(a); case None ⇒ None }.flatMap { roomAttender ⇒
              PermissionHistoryDao.postOneAction(PermissionHistoryModelEx(None, None, roomAttender.flatMap(_.userId), roomId, None, Some(PermissionType.GrantOwner), now)).flatMap { grantOwnerPermHistId ⇒
                RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, grantOwnerPermHistId, None, None, None, None)).flatMap { roomTimelineId2 ⇒
                  list.+=(roomTimelineId2)
                  RoomHistoryDao.getOneAction(roomHistoryId).flatMap { roomHistory ⇒
                    RoomHistoryDao.postOneAction(roomHistory.map(_.copy(userId = roomAttender.flatMap(_.userId), relatedPermissionHistoryId = grantOwnerPermHistId, when = now)).get).flatMap { roomHistoryId ⇒
                      RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, None, roomHistoryId, None, None, None)).map { roomTimelineId3 ⇒
                        list.+=(roomTimelineId3)
                        roomHistoryId
                      }
                    }
                  }
                }
              }
            }
        }.flatMap { roomHistoryId ⇒
          // 아직, room model 을 갱신하지 않았다. 단순히 방장이 없다고 쓴 room history 를 room model 에 쓰거나
          // 방장을 새로 찾았다고 쓴 room history 를 room model 에 쓰거나 둘 중 하나다.
          RoomDao.putOneAction(roomModel.copy(currentDetailId = roomHistoryId)).map {
            case 1 ⇒
            case _ ⇒ throw new VersionException
          }
        }
      } else {
        // 퇴장하는 사람이 방장이 아니면, 아무것도 하지 않는다.
        nothingSql()
      }
    }.flatMap { _ ⇒
      // 퇴장하는 사람이 운영자인 경우 운영자권한회수에 관한 permission history 남기기/room timeline 남기기/ operator 에서 지우기.
      OperatorTable.filter(t ⇒ t.roomId === roomId && t.userId === userId).result.headOption.flatMap { operator ⇒
        operator match {
          case Some(operator) ⇒
            PermissionHistoryDao.postOneAction(PermissionHistoryModelEx(None, None, userId, roomId, None, Some(PermissionType.RevokeOperator), now)).flatMap { revokeOperatorPermHistId4 ⇒
              RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, revokeOperatorPermHistId4, None, None, None, None)).flatMap { roomTimelineId4 ⇒
                list.+=(roomTimelineId4)
                OperatorDao.deleteOneAction(operator.id, operator.version).map {
                  case 1 ⇒
                  case _ ⇒ throw new VersionException
                }
              }
            }
          case None ⇒ nothingSql()
        }
      }
    }.flatMap { _ ⇒
      // 퇴장에 관한 room in/out history 남기기/room timeline 남기기 room attender 지우기.
      RoomInOutHistoryDao.postOneAction(RoomInOutHistoryModelEx(None, None, userId, roomId, None, byWhoId, now, Some(EnterType.Leave))).flatMap { roomInOutHistoryId ⇒
        RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, None, None, roomInOutHistoryId, None, None)).flatMap { roomTimelineId5 ⇒
          list.+=(roomTimelineId5)
          RoomAttenderTable.filter(t ⇒ t.roomId === roomId && t.userId === userId).delete.map {
            case 1 ⇒
            case _ ⇒ throw new VersionException
          }
        }
      }.map { _ ⇒
        list.toList
      }
    }
  }

  def closeRoomAction(roomId: Option[Int]): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    // 방 폐쇄는 방에 아무도 없게 됐을 때 system 에 의해 자동으로 수행된다.
    // 따라서 소유자에 의한 수행시도인지를 확인하지 않는다.
    RoomTable.filter(t ⇒ t.id === roomId).
      joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).map {
        case (x, xx) ⇒
          (x, xx, RoomAttenderTable.filter(t ⇒ t.roomId === roomId).length)
      }.result.headOption.map {
        case Some((a, b, c)) ⇒ (a.copy(currentDetail = b), c)
        case None ⇒ throw new NotFound
      }.map {
        case (a, b) if (a.currentDetail.flatMap(_.userId).isEmpty && b == 0) ⇒ a
        case (a, b) if (a.currentDetail.flatMap(_.userId).isDefined && b > 0) ⇒ throw new NotEmptyRoomException
        case _ ⇒ ???
      }.flatMap { a ⇒
        RoomDao.putOneAction(a.copy(closedDateTime = DriverSettings.now())).map {
          case 1 ⇒
          case _ ⇒ throw new VersionException
        }
      }
  }

  def inviteAction(roomId: Option[Int], userId: Option[Int], byWhoId: Option[Int]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    // todo 초대장을 보내는 것도 RoomTimeline 에 포함시킬 것인가. 일단, 그냥 두자.
    // 초대하는 사람, 초대받는 사람 둘다 현재 접속여부는 가리지 말자.
    RoomTable.filter(t ⇒ t.id === roomId).
      joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).result.headOption.map {
        case None ⇒ throw new NotFound
        case Some((x, xx)) if x.closedDateTime.isDefined ⇒ throw new ClosedRoomException
        case Some((x, xx)) ⇒ x.copy(currentDetail = xx)
      }.flatMap { roomModel ⇒
        OperatorTable.filter(t ⇒ t.roomId === roomId && t.userId === byWhoId).result.headOption.map {
          // 초대하는 사람이 운영자 목록중에 없다면 방장인지 확인한다.
          case None if (roomModel.currentDetail.flatMap(_.userId) != byWhoId) ⇒ throw new NotOperatorException
          case _ ⇒ byWhoId
        }
      }.flatMap { x ⇒
        val calendar = Calendar.getInstance()
        val now = DriverSettings.now()
        now.foreach(calendar.setTime(_))
        calendar.add(Calendar.MINUTE, 5)
        val afterFiveMin = Some(calendar.getTime)
        InvitationDao.postOneAction(InvitationModelEx(None, None, userId, roomId, x, now, afterFiveMin, Some(true)))
      }
  }

  def talkAction(roomId: Option[Int], userId: Option[Int], talk: Option[String]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    val now = DriverSettings.now()
    // nick 이 없는 경우도 있을 수 있다고 본다. nick 이 없으면 사용자명을 쓴다.
    UserDao.getOneAction(userId).map(_.flatMap(_.currentNickHistoryId)).flatMap { x ⇒
      // 방이 폐쇄되지 않아야 한다.
      RoomTable.filter(t ⇒ t.id === roomId && t.closedDateTime.isEmpty).result.headOption.map {
        case Some(_) ⇒ x
        case None ⇒ throw new NotFound
      }
    }.flatMap { x ⇒
      // 현재 입장자 목록에 있어야 한다.
      RoomAttenderTable.filter(t ⇒ t.roomId === roomId && t.userId === userId).result.headOption.map {
        case Some(_) ⇒ x
        case None ⇒ throw new NotFound
      }
    }.flatMap { x ⇒
      // 대화이력을 남긴다.
      val talkHistoryModel = TalkHistoryModelEx(None, None, userId, roomId, x, now, talk)
      TalkHistoryDao.postOneAction(talkHistoryModel)
    }.flatMap { x ⇒
      // 대화이력을 시간이력에 남긴다.
      RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, None, None, None, x, None))
    }
  }

  def getAllCreatedRoomsAction(userId: Option[Int]): DBIOAction[Seq[RoomModel], NoStream, Effect.Read] = {
    innerJoin(RoomTable.filter(t ⇒ t.closedDateTime.isEmpty)).
      joinLeft(RoomAttenderTable).on {
        case ((((((((a, _), _), _), _), _), _), _), b) ⇒ a.id === b.roomId && b.userId === userId
      }.joinLeft(BanTable).on {
        case (((((((((a, _), _), _), _), _), _), _), _), b) ⇒ a.id === b.roomId && b.userId === userId
      }.sortBy { case (((((((((a, _), _), _), _), _), _), _), _), _) ⇒ a.createdDateTime.asc }.result.
      map { x ⇒
        x.map {
          case (((x, b), c)) ⇒ innerMap(x).copy(currentAttendingRoom = b.map(_.id.isDefined).orElse(Some(false)), currentBannedRoom = c.map(_.id.isDefined).orElse(Some(false)))
        }
      }
  }

  def revokeOwnerPermissionAction(userId: Option[Int], roomId: Option[Int], candidateUserId: Option[Int]): DBIOAction[List[Option[Int]], NoStream, Effect.Read with Effect.Write] = {
    val now = DriverSettings.now()
    // 1. 이 명령을 수행하는 userId 사용자가 방장인지 확인한다.
    // 2. 이 명령이 수행되는 roomId 대화방이 유효한지 확인한다.
    RoomTable.filter(t ⇒ t.id === roomId && t.closedDateTime.isEmpty).
      joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).
      filter { case (a, b) ⇒ b.flatMap(_.userId) === userId }.result.headOption.map {
        case Some((a, b)) ⇒ a.copy(currentDetail = b)
        case None ⇒ throw new NotFound
      }.flatMap { roomModel ⇒
        // 3. 방장 후보가 지정되지 않은 경우, 방장 후보를 찾는다. 지금 현재 방장인 userId 사용자를 제외한 나머지 중에서 찾는데, 방장 후보가 없으면 이 명령 자체가 실패해야 한다.
        val action = candidateUserId match {
          case Some(_) ⇒
            RoomAttenderTable.filter(t ⇒ t.roomId === roomId && t.userId === candidateUserId && t.userId =!= userId).map(_.userId).result.headOption
          case None ⇒
            RoomAttenderTable.filter(t ⇒ t.roomId === roomId && t.userId =!= userId).
              joinLeft(RoomInOutHistoryTable).on(_.relatedRoomInOutHistoryId === _.id).
              sortBy { case (a, b) ⇒ b.flatMap(_.when).asc }.
              map { case (a, _) ⇒ a.userId }.result.headOption
        }
        action.map {
          case Some(x) ⇒ x
          case None ⇒ throw new NotFound
        }.flatMap { candidateUserId ⇒
          // 4. userId 사용자의 방장권한을 회수하는 permission history 를 더한다.
          PermissionHistoryDao.postOneAction(PermissionHistoryModelEx(
            None, None, roomModel.currentDetail.flatMap(_.userId), roomModel.id, roomModel.currentDetail.flatMap(_.userId), Some(PermissionType.RevokeOwner), now)).flatMap { permHistId1 ⇒
            // 5. 방장후보 사용자에게 방장권한을 수여하는 permission history 를 더한다.
            PermissionHistoryDao.postOneAction(PermissionHistoryModelEx(
              None, None, candidateUserId, roomModel.id, roomModel.currentDetail.flatMap(_.userId), Some(PermissionType.GrantOwner), now)).flatMap { permHistId2 ⇒
              // 6. 현재 소유자가 바뀌었음을 뜻하는 room history 를 더한다. 앞의 room history 의 내용을 이어서 써야 한다.
              RoomHistoryDao.getOneAction(roomModel.currentDetailId).flatMap { x ⇒
                RoomHistoryDao.postOneAction(RoomHistoryModelEx(
                  None, None, roomModel.id, candidateUserId, roomModel.currentDetail.flatMap(_.userId), now, x.flatMap(_.name), x.flatMap(_.password), permHistId2))
              }.flatMap { roomHistoryId ⇒
                // 7. 현재 소유자가 바뀌었음을 뜻하도록 room 을 바꾼다.
                RoomDao.putOneAction(roomModel.copy(currentDetailId = roomHistoryId)).map {
                  case 1 ⇒ (roomModel, candidateUserId, permHistId1, permHistId2, roomHistoryId)
                  case _ ⇒ throw new VersionException
                }.flatMap { _ ⇒
                  // 8. room timeline 을 쓴다.
                  RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomModel.id, None, permHistId1, None, None, None, None)).flatMap { x ⇒
                    RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomModel.id, None, permHistId2, None, None, None, None)).flatMap { xx ⇒
                      RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomModel.id, None, None, roomHistoryId, None, None, None)).map(xxx ⇒ List(x, xx, xxx))
                    }
                  }
                }
              }
            }
          }
        }
      }
  }

  def setRoomPasswordAction(byWhoId: Option[Int], roomId: Option[Int], password: Option[String]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    val now = DriverSettings.now()
    // roomId 대화방이 유효한지 확인한다.
    RoomTable.filter(t ⇒ t.id === roomId).
      joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).
      result.headOption.map {
        case Some((x, xx)) ⇒
          if (x.closedDateTime.isDefined) {
            throw new ClosedRoomException
          }
          x.copy(currentDetail = xx)
        case None ⇒ throw new NotFound
      }.flatMap { roomModel ⇒
        // byWho 사용자가 방장이거나 운영자인지 확인한다.
        OperatorDao.getOneByUserRoomAction(byWhoId, roomId).map {
          case Some(_) ⇒
          case None ⇒ if (roomModel.currentDetail.flatMap(_.userId) != byWhoId) {
            throw new NotOperatorException
          }
        }.flatMap { _ ⇒
          // 이전의 room history 을 읽는다.
          RoomHistoryTable.filter(t ⇒ t.id === roomModel.currentDetailId).result.headOption.map {
            case Some(x) ⇒ x.copy(password = password, byWhoId = byWhoId, when = now)
            case None ⇒ throw new NotFound
          }.flatMap { roomHistory ⇒
            // 암호만 바꿔서 room history 를 다시 쓴다.
            RoomHistoryDao.postOneAction(roomHistory)
          }.flatMap { roomHistoryId ⇒
            // 새로 쓴 room history 를 가리키도록 room model 을 바꾼다.
            RoomDao.putOneAction(roomModel.copy(currentDetailId = roomHistoryId)).map {
              case 1 ⇒
              case _ ⇒ throw new VersionException
            }.flatMap { _ ⇒
              // room history 에 관한 room timeline 을 쓴다.
              RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, None, roomHistoryId, None, None, None))
            }
          }
        }
      }
  }

  def setRoomNameAction(roomId: Option[Int], name: Option[String], byWhoId: Option[Int]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    val now = DriverSettings.now()
    // 유효한 대화방인지 확인한다.
    RoomTable.filter(t ⇒ t.id === roomId).
      joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).
      result.headOption.map {
        case Some((x, xx)) ⇒
          if (x.closedDateTime.isDefined) {
            throw new ClosedRoomException
          }
          x.copy(currentDetail = xx)
        case None ⇒ throw new NotFound
      }.flatMap { room ⇒
        // 방장이거나 운영자인지 확인한다.
        OperatorTable.filter(t ⇒ t.roomId === roomId && t.userId === byWhoId).result.headOption.map {
          case Some(_) ⇒
          case None ⇒
            if (room.currentDetail.flatMap(_.userId) != byWhoId) {
              throw new NotOperatorException
            }
        }.flatMap { _ ⇒
          RoomHistoryTable.filter(t ⇒ t.id === room.currentDetailId).result.headOption.map {
            case Some(x) ⇒ x
            case None ⇒ throw new NotFound
          }.flatMap { roomHistory ⇒
            // room history 를 쓴다.
            RoomHistoryDao.postOneAction(roomHistory.copy(name = name, when = now, byWhoId = byWhoId)).flatMap { roomHistoryId ⇒
              // room timeline 을 쓴다.
              RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, None, roomHistoryId, None, None, None)).flatMap { roomTimelineId ⇒
                RoomDao.putOneAction(room.copy(currentDetailId = roomHistoryId)).map(_ ⇒ roomTimelineId)
              }
            }
          }
        }
      }
  }

  case class TempDataDump(roomModel: Option[RoomModel] = None, roomTimelineIds: List[Option[Int]] = Nil, permissionHistoryIds: List[Option[Int]] = Nil, roomHistoryModel: Option[RoomHistoryModel] = None, candidateOwner: Option[Int] = None, permissionHistoryIdForChangingOwner: Option[Int] = None)
}
