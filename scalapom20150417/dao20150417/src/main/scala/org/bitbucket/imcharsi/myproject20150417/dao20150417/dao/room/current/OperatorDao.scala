/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history.{ PermissionHistoryDao, RoomTimelineDao }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.{ OperatorTable, RoomAttenderTable, RoomTable }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.RoomHistoryTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.current.OperatorDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait.{ NotFound, VersionException }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ NotOperatorException, NotOwnerException, OperatorModel, OperatorModelEx }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.{ PermissionHistoryModelEx, RoomTimelineModelEx }

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object OperatorDao extends OperatorDao with SlickDao[OperatorModel, OperatorTable] {
  override val tableQuery = OperatorTable

  def getOneByUserRoom(userId: Option[Int], roomId: Option[Int]): Option[OperatorModel] = {
    DriverSettings.run(getOneByUserRoomAction(userId, roomId).transactionally)
  }

  override def grantOperatorPermission(ownerId: Option[Int], roomId: Option[Int], userId: Option[Int]): Option[Int] = {
    DriverSettings.run(grantOperatorPermissionAction(ownerId, roomId, userId).transactionally)
  }

  override def revokeOperatorPermission(userId: Option[Int], roomId: Option[Int], operatorUserId: Option[Int]): Option[Int] = {
    DriverSettings.run(revokeOperatorPermissionAction(userId, roomId, operatorUserId).transactionally)
  }

  def getOneByUserRoomAction(userId: Option[Int], roomId: Option[Int]): DBIOAction[Option[OperatorModel], NoStream, Effect.Read] = {
    OperatorTable.filter(t ⇒ t.userId === userId && t.roomId === roomId).result.headOption
  }

  def grantOperatorPermissionAction(ownerId: Option[Int], roomId: Option[Int], userId: Option[Int]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    // 운영자 권한 수여는 방장만 할 수 있다. 방장은 자신에게 운영자 권한을 수여 할 수 있다.

    val now = DriverSettings.now()
    // 1. roomId 대화방이 유효한 방인지 확인. ownerId 사용자가 방장인지 확인.
    RoomTable.filter(t ⇒ t.id === roomId && t.closedDateTime.isEmpty).
      joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).
      filter { case (a, b) ⇒ b.flatMap(_.userId) === ownerId }.result.headOption.map {
        case Some((a, b)) ⇒ a.copy(currentDetail = b)
        case None ⇒ throw new NotFound
      }.flatMap { roomModel ⇒
        // 2. userId 사용자가 대화방에 참가중인지 확인. userId 사용자가 이미 운영자권한을 갖고 있는지 확인하지 않는다. 이는 unique 제약조건으로 푼다.
        RoomAttenderTable.filter(t ⇒ t.roomId === roomId && t.userId === userId).result.headOption.map {
          case Some(x) ⇒ x
          case None ⇒
            throw new NotFound
        }.flatMap { roomAttenderModel ⇒
          // 3. 운영자권한 수여에 관한 permission history 를 더하기.
          PermissionHistoryDao.postOneAction(PermissionHistoryModelEx(None, None, userId, roomId, ownerId, Some(PermissionType.GrantOperator), now)).flatMap { permHistId ⇒
            // 4. userId 사용자가 현재 운영자임을 표시하는 operator 기록을 더하기.
            RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, permHistId, None, None, None, None)).flatMap { roomTimelineId ⇒
              // 5. 운영자권한 수여에 관한 room timeline 을 더하기.
              OperatorDao.postOneAction(OperatorModelEx(None, None, userId, roomId, permHistId)).map(_ ⇒ roomTimelineId)
            }
          }
        }
      }
  }

  def revokeOperatorPermissionAction(userId: Option[Int], roomId: Option[Int], operatorUserId: Option[Int]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    // 운영자 권한 회수는 방장만 할 수있다. 운영자 권한 반납은 운영자 자신도 할 수 있다.
    val now = DriverSettings.now()

    // 1. roomId 대화방이 유효한 방인지 확인.
    RoomTable.filter(t ⇒ t.id === roomId && t.closedDateTime.isEmpty).
      joinLeft(RoomHistoryTable).on(_.currentDetailId === _.id).result.headOption.map {
        case Some((a, b)) ⇒ a.copy(currentDetail = b)
        case None ⇒ throw new NotFound
      }.flatMap { roomModel ⇒
        // 2. userId 사용자가 방장이거나
        val x = if (roomModel.currentDetail.flatMap(_.userId) == userId) {
          Query(1).result.headOption.map(_ ⇒ None)
        } else {
          // 운영자인지 확인.
          OperatorTable.filter(t ⇒ t.roomId === roomId && t.userId === userId).result.headOption.map {
            // 운영자라면 자신의 운영자 권한을 반납하려는 것인지 확인.
            case x @ Some(_) if userId == operatorUserId ⇒ x
            case None ⇒ throw new NotOperatorException
            case _ ⇒ throw new NotOwnerException
          }
        }
        x.flatMap { operatorModel ⇒
          // 4. 운영자권한 회수/반납에 관한 permission history 를 더하기.
          PermissionHistoryDao.postOneAction(PermissionHistoryModelEx(None, None, userId, roomId, userId, Some(PermissionType.RevokeOperator), now)).flatMap { permHistId ⇒
            // 5. operatorId 사용자가 현재 운영자임을 표시하는 operator 기록 지우기.
            OperatorTable.filter(t ⇒ t.roomId === roomId && t.userId === operatorUserId).delete.map {
              case 1 ⇒ None
              case _ ⇒ throw new VersionException
            }.flatMap { _ ⇒
              // 6. 운영자권한 회수/반납에 관한 room timeline 을 더하기.
              RoomTimelineDao.postOneAction(RoomTimelineModelEx(None, None, roomId, None, permHistId, None, None, None, None))
            }
          }
        }
      }
  }

}
