/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417

import java.util.Date

import com.jolbox.bonecp.BoneCPDataSource
import net.sf.log4jdbc.DriverSpy
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.talk.history.TalkHistoryTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.{ UserLoginHistoryTable, UserNickHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.BanHistoryModel.BanType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType
import slick.driver.PostgresDriver

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Created by Kang Woo, Lee on 4/19/15.
 */
object DriverSettings {
  val driver = PostgresDriver
  val api = driver.api

  import api._

  private val dataSource = new BoneCPDataSource()
  dataSource.setJdbcUrl("jdbc:log4jdbc:postgresql://localhost/myproject20150417")
  dataSource.setUser("admin20150417")
  dataSource.setPassword("0")
  dataSource.setDriverClass(classOf[DriverSpy].getName)
  val database = Database.forDataSource(dataSource)
  private val dateMapper = MappedColumnType.base[java.util.Date, java.sql.Timestamp](x ⇒ new java.sql.Timestamp(x.getTime), x ⇒ new java.util.Date(x.getTime))
  private val banTypeMapper = MappedColumnType.base[BanType, Int](x ⇒ x.id, x ⇒ BanType(x))
  private val permissionTypeMapper = MappedColumnType.base[PermissionType, Int](x ⇒ x.id, x ⇒ PermissionType(x))
  private val enterTypeMapper = MappedColumnType.base[EnterType, Int](x ⇒ x.id, x ⇒ EnterType(x))

  val schemaList = List(UserTable.schema,
    UserNickHistoryTable.schema, UserLoginHistoryTable.schema,
    RoomTable.schema, BanTable.schema, InvitationTable.schema, OperatorTable.schema, RoomAttenderTable.schema,
    RoomHistoryTable.schema, RoomInOutHistoryTable.schema, BanHistoryTable.schema, PermissionHistoryTable.schema, RoomTimelineTable.schema,
    TalkHistoryTable.schema)

  val createScheme1 = schemaList.flatMap(_.create.statements).filterNot(_.contains("alter"))
  val createScheme2 = schemaList.flatMap(_.create.statements).filter(_.contains("alter"))
  val dropScheme1 = schemaList.reverse.flatMap(_.drop.statements).filterNot(_.contains("alter"))
  val dropScheme2 = schemaList.reverse.flatMap(_.drop.statements).filter(_.contains("alter"))

  trait DateMapperTrait {
    implicit val dateMapper = DriverSettings.dateMapper
  }

  trait BanTypeMapperTrait {
    implicit val banTypeMapper = DriverSettings.banTypeMapper
  }

  trait PermissionTypeMapperTrait {
    implicit val permissionTypeMapper = DriverSettings.permissionTypeMapper
  }

  trait EnterTypeMapperTrait {
    implicit val enterTypeMapper = DriverSettings.enterTypeMapper
  }

  //  slick.driver.JdbcActionComponent.SchemaActionExtensionMethodsImpl
  case class PlainTextSqlAction(seq: List[String]) extends driver.SimpleJdbcDriverAction[Unit]("schema.hi", seq) {
    def run(ctx: driver.Backend#Context, sql: Iterable[String]): Unit =
      for (s ← sql) ctx.session.withPreparedStatement(s)(_.execute)
  }

  def createSchemeUtil(): Unit = {
    val action = PlainTextSqlAction(DriverSettings.createScheme1).flatMap(_ ⇒ PlainTextSqlAction(DriverSettings.createScheme2))
    val future = DriverSettings.database.run(action)
    Await.ready(future, 1.days)
  }

  def dropSchemeUtil(): Unit = {
    val action = PlainTextSqlAction(DriverSettings.dropScheme2).flatMap(_ ⇒ PlainTextSqlAction(DriverSettings.dropScheme1))
    val future = DriverSettings.database.run(action)
    Await.ready(future, 1.days)
  }

  def now(): Option[Date] = {
    Some(new Date(System.currentTimeMillis()))
  }

  def run[T, E <: Effect](action: DBIOAction[T, NoStream, E]): T = {
    Await.result(database.run(action), Duration.Inf)
  }
}
