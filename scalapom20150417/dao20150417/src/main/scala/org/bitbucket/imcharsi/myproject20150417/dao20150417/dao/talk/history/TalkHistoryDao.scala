/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.talk.history

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.RoomTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.talk.history.TalkHistoryTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.{ UserLoginHistoryTable, UserNickHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.talk.history.TalkHistoryDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object TalkHistoryDao extends TalkHistoryDao with SlickDao[TalkHistoryModel, TalkHistoryTable] {
  override val tableQuery = TalkHistoryTable

  type QueryType = (((((TalkHistoryTable, Rep[Option[UserTable]]), Rep[Option[RoomTable]]), Rep[Option[UserNickHistoryTable]]), Rep[Option[UserLoginHistoryTable]]), Rep[Option[UserNickHistoryTable]])
  type ResultType = (((((TalkHistoryModel, Option[UserModel]), Option[RoomModel]), Option[UserNickHistoryModel]), Option[UserLoginHistoryModel]), Option[UserNickHistoryModel])

  private def innerJoin(q: Query[TalkHistoryTable, TalkHistoryModel, Seq]): Query[QueryType, ResultType, Seq] = {
    // talk history 에서는 room 의 상세를 구하지 않는다.
    q.joinLeft(UserTable).on(_.userId === _.id).joinLeft(RoomTable).on {
      case ((a, _), b) ⇒ a.roomId === b.id
    }.joinLeft(UserNickHistoryTable).on {
      case (((_, a), _), b) ⇒ a.flatMap(_.currentNickHistoryId) === b.id
    }.joinLeft(UserLoginHistoryTable).on {
      case ((((_, a), _), _), b) ⇒ a.flatMap(_.currentLoginHistoryId) === b.id
    }.joinLeft(UserNickHistoryTable).on {
      case (((((a, _), _), _), _), b) ⇒ a.nickId === b.id
    }
  }

  private def innerMap(x: ResultType): TalkHistoryModel = {
    val (((((talkHistory, user), room), userNickHistory), userLoginHistory), talkNickHistory) = x
    talkHistory.copy(
      user = user.map(_.copy(
        currentNickHistory = userNickHistory, currentLoginHistory = userLoginHistory)),
      room = room,
      nick = talkNickHistory)
  }

  override def getOneAction(id: Option[Int]): DBIOAction[Option[TalkHistoryModel], NoStream, Effect.Read] = {
    innerJoin(TalkHistoryTable.filter(t ⇒ t.id === id)).result.headOption.map(_.map(innerMap))
  }
}
