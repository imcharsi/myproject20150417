/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin.{ IdVersionTableTrait, UserRoomTableTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ RoomAttenderModel, RoomAttenderModelEx }

/**
 * Created by Kang Woo, Lee on 4/30/15.
 */
class RoomAttenderTable(t: Tag) extends Table[RoomAttenderModel](t, "room_attender_table") with IdVersionTableTrait[RoomAttenderModel] with UserRoomTableTrait[RoomAttenderModel] {
  def relatedRoomInOutHistoryId = column[Option[Int]]("related_room_inout_hist_id")

  // 한 대화방에 한 사용자는 참석을 동시에 한번만 할 수 있다.
  def unique1 = index(tableName + "_uq1", (userId, roomId), true)

  // 대화방 출입 이력은 한번만 사용될 수 있다.
  def unique2 = index(tableName + "_uq2", relatedRoomInOutHistoryId, true)

  override def * = (id, version, userId, roomId, relatedRoomInOutHistoryId) <> ((RoomAttenderModelEx.apply _).tupled, RoomAttenderModelEx.unapply)
}

object RoomAttenderTable extends TableQuery[RoomAttenderTable](x ⇒ new RoomAttenderTable(x))
