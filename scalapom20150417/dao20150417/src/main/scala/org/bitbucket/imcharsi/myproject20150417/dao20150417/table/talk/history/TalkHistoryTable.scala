/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.talk.history

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin.{ IdVersionTableTrait, UserRoomTableTrait, WhenTableTrait }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.UserNickHistoryTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.{ TalkHistoryModel, TalkHistoryModelEx }

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
class TalkHistoryTable(t: Tag) extends Table[TalkHistoryModel](t, "talk_hist_table") with IdVersionTableTrait[TalkHistoryModel] with UserRoomTableTrait[TalkHistoryModel] with WhenTableTrait[TalkHistoryModel] {
  // nick 은 null 일 수 있다고 본다.
  def nickId = column[Option[Int]]("nick_id")

  def talk = column[Option[String]]("talk")

  def foreignKey1 = foreignKey(tableName + "_fk1", nickId, UserNickHistoryTable)(_.id)

  override def * = (id, version, userId, roomId, nickId, when, talk) <> ((TalkHistoryModelEx.apply _).tupled, TalkHistoryModelEx.unapply)
}

object TalkHistoryTable extends TableQuery[TalkHistoryTable](x ⇒ new TalkHistoryTable(x))
