/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.{ InvitationTable, RoomTable }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.RoomInOutHistoryTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.{ UserLoginHistoryTable, UserNickHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.history.RoomInOutHistoryDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ InvitationModel, RoomModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }
import slick.dbio.{ DBIOAction, Effect, NoStream }

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object RoomInOutHistoryDao extends RoomInOutHistoryDao with SlickDao[RoomInOutHistoryModel, RoomInOutHistoryTable] {
  override val tableQuery = RoomInOutHistoryTable

  type ResultType = ((((((((((RoomInOutHistoryModel, Option[UserModel]), Option[RoomModel]), Option[UserModel]), Option[UserLoginHistoryModel]), Option[UserNickHistoryModel]), Option[UserLoginHistoryModel]), Option[UserNickHistoryModel]), Option[InvitationModel]), Option[UserModel]), Option[UserModel])
  type QueryType = ((((((((((RoomInOutHistoryTable, Rep[Option[UserTable]]), Rep[Option[RoomTable]]), Rep[Option[UserTable]]), Rep[Option[UserLoginHistoryTable]]), Rep[Option[UserNickHistoryTable]]), Rep[Option[UserLoginHistoryTable]]), Rep[Option[UserNickHistoryTable]]), Rep[Option[InvitationTable]]), Rep[Option[UserTable]]), Rep[Option[UserTable]])

  private def innerJoin(q: Query[RoomInOutHistoryTable, RoomInOutHistoryModel, Seq]): Query[QueryType, ResultType, Seq] = {
    // 초대장의 상세에 관해서는, 기본 정보만 구한다.
    q.joinLeft(UserTable).on(_.userId === _.id).
      joinLeft(RoomTable).on {
        case ((a, _), b) ⇒ a.roomId === b.id
      }.joinLeft(UserTable).on {
        case (((a, _), _), b) ⇒ a.byWhoId === b.id
      }.joinLeft(UserLoginHistoryTable).on {
        case ((((_, a), _), _), b) ⇒ a.flatMap(_.currentLoginHistoryId) === b.id
      }.joinLeft(UserNickHistoryTable).on {
        case (((((_, a), _), _), _), b) ⇒ a.flatMap(_.currentNickHistoryId) === b.id
      }.joinLeft(UserLoginHistoryTable).on {
        case ((((((_, _), _), a), _), _), b) ⇒ a.flatMap(_.currentLoginHistoryId) === b.id
      }.joinLeft(UserNickHistoryTable).on {
        case (((((((_, _), _), a), _), _), _), b) ⇒ a.flatMap(_.currentNickHistoryId) === b.id
      }.joinLeft(InvitationTable).on {
        case ((((((((a, _), _), _), _), _), _), _), b) ⇒ a.invitationId === b.id
      }.joinLeft(UserTable).on {
        case (((((((((_, _), _), _), _), _), _), _), a), b) ⇒ a.flatMap(_.userId) === b.id
      }.joinLeft(UserTable).on {
        case ((((((((((_, _), _), _), _), _), _), _), a), _), b) ⇒ a.flatMap(_.byWhoId) === b.id
      }
  }

  private def innerMap(x: ResultType): RoomInOutHistoryModel = {
    val ((((((((((roomInOutHistory, user), room), byWho), userLoginHistory), userNickHistory), byWhoLoginHistory), byWhoNickHistory), invitation), invitaionUser), invitationByWho) = x
    roomInOutHistory.copy(
      user = user.map(_.copy(
        currentLoginHistory = userLoginHistory, currentNickHistory = userNickHistory)),
      byWho = byWho.map(_.copy(
        currentLoginHistory = byWhoLoginHistory, currentNickHistory = byWhoNickHistory)),
      invitation = invitation.map(_.copy(user = invitaionUser, byWho = invitationByWho)),
      room = room)
  }

  override def getOneAction(id: Option[Int]): DBIOAction[Option[RoomInOutHistoryModel], NoStream, Effect.Read] = {
    innerJoin(RoomInOutHistoryTable.filter(t ⇒ t.id === id)).result.headOption.map(_.map(innerMap))
  }
}
