/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin.{ IdVersionTableTrait, UserRoomTableTrait }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.PermissionHistoryTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ OperatorModel, OperatorModelEx }

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
class OperatorTable(t: Tag) extends Table[OperatorModel](t, "operator_table") with IdVersionTableTrait[OperatorModel] with UserRoomTableTrait[OperatorModel] {
  def relatedPermissionHistoryId = column[Option[Int]]("related_perm_hist_id")

  def foreignKey1 = foreignKey(tableName + "_fk1", relatedPermissionHistoryId, PermissionHistoryTable)(_.id)

  def unique1 = index(tableName + "_uq1", (userId, roomId), true)

  override def * = (id, version, userId, roomId, relatedPermissionHistoryId) <> ((OperatorModelEx.apply _).tupled, OperatorModelEx.unapply)
}

object OperatorTable extends TableQuery[OperatorTable](x ⇒ new OperatorTable(x))
