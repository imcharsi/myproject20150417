/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin.IdVersionTableTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.{ UserLoginHistoryTable, UserNickHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.{ UserModel, UserModelEx }

/**
 * Created by Kang Woo, Lee on 4/19/15.
 */
class UserTable(t: Tag) extends Table[UserModel](t, "user_table") with DateMapperTrait with IdVersionTableTrait[UserModel] {
  def currentLoginHistoryId = column[Option[Int]]("current_login_hist_id")

  def currentNickHistoryId = column[Option[Int]]("current_nick_hist_id")

  def name = column[Option[String]]("name")

  def password = column[Option[String]]("password")

  def foreignKey1 = foreignKey(tableName + "_fk1", currentLoginHistoryId, UserLoginHistoryTable)(_.id)

  def foreignKey2 = foreignKey(tableName + "_fk2", currentNickHistoryId, UserNickHistoryTable)(_.id)

  def unique1 = index(tableName + "_uq1", name, true)

  override def * = (id, version, currentLoginHistoryId, currentNickHistoryId, name, password) <> ((UserModelEx.apply _).tupled, UserModelEx.unapply)
}

object UserTable extends TableQuery[UserTable](x ⇒ new UserTable(x))
