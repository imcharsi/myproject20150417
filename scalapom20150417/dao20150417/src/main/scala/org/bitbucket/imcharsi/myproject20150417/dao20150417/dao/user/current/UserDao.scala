/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current

import java.util.logging.{ Level, Logger }

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.history.{ UserLoginHistoryDao, UserNickHistoryDao }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.RoomAttenderTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.RoomTimelineTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.UserLoginHistoryTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait.{ NotFound, VersionException }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomTimelineModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel.{ AlreadyLoginException, AlreadyLogoutException }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserLoginHistoryModelEx, UserNickHistoryModelEx }
import slick.profile.SqlAction

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
object UserDao extends UserDao with SlickDao[UserModel, UserTable] with DateMapperTrait {

  override val tableQuery = UserTable

  override def login(name: String, password: String): Option[Int] = {
    val action = loginAction(name, password).transactionally
    val future = DriverSettings.database.run(action)
    Await.result(future, 1.day)
  }

  override def logout(userLoginHistoryId: Option[Int]): Boolean = {
    val action = logoutAction(userLoginHistoryId).transactionally
    val future = DriverSettings.database.run(action)
    Await.result(future, 1.day)
  }

  override def logoutForce(name: String, password: String): Option[Int] = {
    val action = logoutForceAction(name, password).transactionally
    val future = DriverSettings.database.run(action)
    Await.result(future, 1.day)
  }

  override def getAllLoginUsers(): Seq[UserModel] = {
    val action = getAllLoginUsersAction().transactionally
    val future = DriverSettings.database.run(action)
    Await.result(future, 1.day)
  }

  override def getUserByNamePassword(name: String, password: String): Option[UserModel] = {
    val action = getUserByNamePasswordAction(name, password).transactionally
    val future = DriverSettings.database.run(action)
    Await.result(future, 1.day)
  }

  override def getAllUsers(): Seq[UserModel] = {
    val action = getAllUsersAction().transactionally
    val future = DriverSettings.database.run(action)
    Await.result(future, 1.day)
  }

  override def setCurrentNick(userId: Option[Int], nick: Option[String]): (Option[Int], Seq[(Option[Int], Option[Int])]) = {
    DriverSettings.run(setCurrentNickAction(userId, nick).transactionally)
  }

  def logoutAction(userLoginHistoryId: Option[Int], signal: Map[Int, () ⇒ Any] = Map()): DBIOAction[Boolean, NoStream, Effect.Read with Effect.Write] = {
    signal.get(1).foreach(_.apply())
    // 접속종료 처리할 접속이력을 구한다.
    val step1 = UserLoginHistoryDao.getOneAction(userLoginHistoryId).map {
      case Some(x) ⇒
        if (x.logoutDateTime.isDefined) {
          // 이미 logout 한 접속이력이면 실패.
          throw new AlreadyLogoutException
        }
        // 접속종료 시각을 쓴다.
        x.copy(logoutDateTime = DriverSettings.now())
      case None ⇒ throw new NotFound
    }
    // 관련된 사용자 계정정보를 구한다. 사용자정보도 최대한 빨리 읽어놓고 시작해야 한다.
    // 사용자정보를 읽고 갱신하는 것을 나중에 하면, 예를 들어 다음과 같은 문제가 생길 수 있다.
    // 이 transaction 의 시작보다 늦게 시작한 transaction 이 사용자정보를 먼저 갱신하고 완료할 수 있는 것이고
    // 이 경우, 바뀐 version 을 무시하는 결과가 될 수 있기 때문이다. 늦게 시작한 transaction 이 관련 자료를 갱신하고 난 이후
    // 이 transaction 이 갱신하려는 그 자료를 또 읽는 것이므로 version 이 바뀌었는지를 구분할 수 없게 되는 것이다.
    val step2 = step1.map { x ⇒
      signal.get(2).foreach(_.apply())
      x
    }.flatMap { userLoginHistoryModel ⇒
      UserDao.getOneAction(userLoginHistoryModel.userId).map {
        case Some(userModel) ⇒
          // 극단적인 예로서, user login history model 을 구한 다음에 user model 을 구하기 전에 다른 transaction 이 logout 을 성공적으로 수행해버리는 경우도 있을 수 있다.
          // 이 경우는 다음의 문장에서 검사한다.
          userModel.currentLoginHistoryId match {
            case None ⇒ throw new AlreadyLogoutException // 이미 logout 을 했는데 또 logout 을 하려고 시도하고 있다.
            case x if x != userLoginHistoryId ⇒ throw new VersionException // 다르다면, 이 사용자가 login 을 하고 난 후 다른 사람이 강제 logout 을 하고 나서 login 을 했다는 결론이 된다.
            case _ ⇒ (userLoginHistoryModel, userModel.copy(currentLoginHistoryId = None)) // 새로 쓸 사용자 정보에는 접속이력이 없다고 표시한다.
          }
        case None ⇒ throw new NotFound
      }
    }
    // 접속이력에 접속종료 기록을 한다.
    val step3 = step2.map { x ⇒
      signal.get(3).foreach(_.apply())
      x
    }.flatMap {
      case (userLoginHistoryModel, userModel) ⇒
        UserLoginHistoryDao.putOneAction(userLoginHistoryModel).map { result ⇒
          if (result != 1) {
            throw new VersionException
          }
          userModel
        }
    }
    // 사용자 계정정보에, 현재 접속이력이 없다고 표시한다.
    val step4 = step3.map { x ⇒
      signal.get(4).foreach(_.apply())
      x
    }.flatMap { x ⇒
      UserDao.putOneAction(x).map {
        case 1 ⇒ true
        case _ ⇒ throw new VersionException
      }
    }
    step4.map { x ⇒
      signal.get(5).foreach(_.apply())
      x
    }
  }

  def loginAction(name: String, password: String, signal: Map[Int, () ⇒ Any] = Map()): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    // slick 의 method 반환형이 장황해지는 경향이 있다. 일일이 찾기 불편하다. 어떻게 찾을 것인가. 일부러 틀리게 쓴다음 compiler 가 알려주는 type 을 쓰면 된다.

    // test 를 할 수 있도록, signal 인자를 덧붙였다. 이와 같이 하지 않으면 test 에서 똑같은 code 를 중복해서 써야 한다. 약간의 낭비가 있긴 하다.

    // 이름을 signal 이라 쓰긴 했는데, 적당한 이름을 모르겠다. 이 인자는, 중간에 기다려야 하거나 깨워야 하는 동작을 수행하기 위해 쓴다.
    // 여기서 promise 와 future 를 같이 다루면, 두개 이상의 실행주체가 똑같은 동작을 하게 되면서 결국 교착상태를 만들게 되는 것같다.
    // 똑같은 동작 중에는 반드시 기다리는 동작이 있게 되는데, 둘다 서로를 기다리기 시작하는 문제가 생길 수 있는 것이다.
    // 따라서, 인자가 가리키는 함수 객체는, 기다리기만 하거나 깨우기만 하거나 둘다 하거나 다양할 수 있다.
    signal.get(1).foreach(_.apply())
    // 계정이름과 암호를 사용하여 계정이 있는지 확인한다.
    val step1: SqlAction[Option[UserModel], NoStream, Effect.Read] =
      UserTable.filter(t ⇒ t.name === name && t.password === password).result.headOption
    val step2 = step1.map { x ⇒
      // 왜 이와 같이 기다리거나 깨우는가. 바깥에서 기다리거나 깨우면 안된다.
      // 바깥은 action 을 선언하는 단계이지, 실제 database 명령이 수행되는 단계가 아니다.
      // 실제 database 명령이 수행을 시작하는 시점은, 아래의 database.run(...) 이다.
      // 그럼 수행 중간에 database 와는 관련없는 동작을 어떻게 끼워넣는가. 이와 같이 하면 수행중간에 동작을 끼워넣을 수 있다.
      // 여기서의 구현의 요지는, 각 database 문장을 실행한 뒤, 기다려야 하면 기다리고 깨워야 하면 깨우는 것이다.
      signal.get(2).foreach(_.apply())
      x
    }.flatMap {
      case Some(user) ⇒
        // 계정이 이미 사용중이면 실패.
        if (user.currentLoginHistoryId.isDefined) {
          throw new AlreadyLoginException
        }
        UserLoginHistoryDao.postOneAction(UserLoginHistoryModelEx(None, None, user.id, DriverSettings.now(), None)).
          map(id ⇒ (user, id))
      case _ ⇒
        throw new NotFound
    }
    val step3 = step2.map { x ⇒
      signal.get(3).foreach(_.apply())
      x
    }.flatMap {
      case (user, userLoginHistoryId) ⇒
        // 앞서 만들었던 접속이력을 사용자 정보에 다시 쓴다.
        UserDao.putOneAction(user.copy(currentLoginHistoryId = userLoginHistoryId)).map {
          case 1 ⇒ userLoginHistoryId
          // 만약, version 차이로 인하여 기록이 되지 않았다면, 실패.
          case _ ⇒ throw new VersionException
        }
    }
    step3.map { x ⇒
      signal.get(4).foreach(_.apply())
      x
    }
  }

  def logoutForceAction(name: String, password: String, signal: Map[Int, () ⇒ Unit] = Map()): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    signal.get(1).foreach(_.apply())
    val step1: SqlAction[Option[UserModel], NoStream, Effect.Read] =
      UserTable.filter(t ⇒ t.name === name && t.password === password).result.headOption
    val step2 = step1.map { x ⇒
      signal.get(2).foreach(_.apply())
      x match {
        case Some(x) if x.currentLoginHistoryId.isEmpty ⇒ throw new AlreadyLogoutException // 현재 접속 이력이 없다면
        case Some(x) if x.currentLoginHistoryId.isDefined ⇒
        case None ⇒ throw new NotFound // 해당 사용자가 없다면
      }
      x.get
    }.flatMap { x ⇒
      UserLoginHistoryDao.getOneAction(x.currentLoginHistoryId).map { xx ⇒
        xx match {
          case None ⇒ throw new NotFound // 해당 접속이력이 없을 때. 있을 수 없는 오류다. 이 오류가 생겼다는 말은, foreign key 설정이 안되었다는 말이다.
          case Some(x) if x.logoutDateTime.isDefined ⇒
            Logger.getLogger(getClass.getName).log(Level.SEVERE, "need to check.")
            throw new AlreadyLogoutException // 해당 접속 이력이 접속종료한 것으로 나타날 때. 이는 오류다.
          // user model 에서 current login history id 가 있다면, 관련된 user login history model 의 logout datetime 은 none 이어야 한다.
          // 다른 경우가 나타난다면 잘못된 것이다.
          case Some(xx) if xx.logoutDateTime.isEmpty ⇒ (x, xx)
        }
      }
    }
    val step3 = step2.map { x ⇒
      signal.get(3).foreach(_.apply())
      x
    }.flatMap {
      case (a, b) ⇒
        // 접속종료한 것으로 기록.
        UserLoginHistoryDao.putOneAction(b.copy(logoutDateTime = DriverSettings.now())).map { x ⇒
          x match {
            case 1 ⇒ a
            case _ ⇒ throw new VersionException
          }
        }
    }
    val step4 = step3.map { x ⇒
      signal.get(4).foreach(_.apply())
      x
    }.flatMap { userModel ⇒
      // 접속 이력이 없는 것으로 기록.
      UserDao.putOneAction(userModel.copy(currentLoginHistoryId = None)).map { x ⇒
        x match {
          case 1 ⇒ userModel.currentLoginHistoryId
          case _ ⇒ throw new VersionException
        }
      }
    }
    step4.map { x ⇒
      signal.get(5).foreach(_.apply())
      x
    }
  }

  def getAllLoginUsersAction(): DBIOAction[Seq[UserModel], NoStream, Effect.Read] = {
    // todo 상세한 내용이 필요하다. join 하기.
    val step1: DBIOAction[Seq[(UserModel, Option[UserLoginHistoryModel])], NoStream, Effect.Read] =
      UserTable.filter(t ⇒ t.currentLoginHistoryId.isDefined).
        joinLeft(UserLoginHistoryTable).on { case (a, b) ⇒ a.currentLoginHistoryId === b.id }.
        sortBy { case ((_, b)) ⇒ b.flatMap(_.loginDateTime).asc }.result
    step1.map(x ⇒ x.map { case (a, b) ⇒ a.copy(currentLoginHistory = b) })
  }

  def getUserByNamePasswordAction(name: String, password: String): DBIOAction[Option[UserModel], NoStream, Effect.Read] = {
    val step: DBIOAction[Option[UserModel], NoStream, Effect.Read] = UserTable.filter(t ⇒ t.name === name && t.password === password).result.headOption
    step
  }

  def getAllUsersAction(): DBIOAction[Seq[UserModel], NoStream, Effect.Read] = {
    val step: DBIOAction[Seq[UserModel], NoStream, Effect.Read] = UserTable.sortBy(t ⇒ t.id.asc).result
    step
  }

  def setCurrentNickAction(userId: Option[Int], nick: Option[String]): DBIOAction[(Option[Int], Seq[(Option[Int], Option[Int])]), NoStream, Effect.Read with Effect.Write] = {
    val now = DriverSettings.now()
    UserNickHistoryDao.postOneAction(UserNickHistoryModelEx(None, None, userId, now, nick)).flatMap { userNickHistoryId ⇒
      UserTable.filter(t ⇒ t.id === userId).result.headOption.map {
        case Some(userModel) ⇒ userModel.copy(currentNickHistoryId = userNickHistoryId)
        case None ⇒ throw new NotFound
      }.flatMap { userModel ⇒
        UserDao.putOneAction(userModel).map(_ ⇒ userNickHistoryId)
      }.flatMap { _ ⇒
        RoomAttenderTable.filter(t ⇒ t.userId === userId).map(_.roomId).result.
          flatMap { roomIds ⇒
            RoomTimelineTable.returning(RoomTimelineTable.map(_.id)).
              ++=(roomIds.map(x ⇒ RoomTimelineModelEx(None, None, x, None, None, None, None, None, userNickHistoryId))).
              map(x ⇒ (userNickHistoryId, x.zip(roomIds)))
          }
      }
    }
  }
}

