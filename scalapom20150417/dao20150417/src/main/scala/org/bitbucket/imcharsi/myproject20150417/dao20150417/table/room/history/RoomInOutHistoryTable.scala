/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.EnterTypeMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.InvitationTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.{ RoomInOutHistoryModel, RoomInOutHistoryModelEx }

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
class RoomInOutHistoryTable(t: Tag) extends Table[RoomInOutHistoryModel](t, "room_inout_hist_table") with CommonRoomHistoryTableTrait[RoomInOutHistoryModel] with EnterTypeMapperTrait {
  def invitationId = column[Option[Int]]("invitation_id")

  def enterType = column[Option[EnterType]]("enter_type")

  def foreignKey1 = foreignKey(tableName + "_fk1", invitationId, InvitationTable)(_.id)

  override def * = (id, version, userId, roomId, invitationId, byWhoId, when, enterType) <> ((RoomInOutHistoryModelEx.apply _).tupled, RoomInOutHistoryModelEx.unapply)
}

object RoomInOutHistoryTable extends TableQuery[RoomInOutHistoryTable](x ⇒ new RoomInOutHistoryTable(x))
