/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.{ OperatorTable, RoomAttenderTable, RoomTable }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.{ RoomHistoryTable, RoomInOutHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.{ UserLoginHistoryTable, UserNickHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.current.RoomAttenderDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ OperatorModel, RoomAttenderModel, RoomModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.{ RoomHistoryModel, RoomInOutHistoryModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/30/15.
 */
object RoomAttenderDao extends RoomAttenderDao with SlickDao[RoomAttenderModel, RoomAttenderTable] with DateMapperTrait {
  override val tableQuery = RoomAttenderTable

  type ResultType = (((((((RoomAttenderModel, Option[UserModel]), Option[RoomModel]), Option[RoomInOutHistoryModel]), Option[UserNickHistoryModel]), Option[UserLoginHistoryModel]), Option[OperatorModel]), Option[RoomHistoryModel])
  type QueryType = (((((((RoomAttenderTable, Rep[Option[UserTable]]), Rep[Option[RoomTable]]), Rep[Option[RoomInOutHistoryTable]]), Rep[Option[UserNickHistoryTable]]), Rep[Option[UserLoginHistoryTable]]), Rep[Option[OperatorTable]]), Rep[Option[RoomHistoryTable]])

  override def getOneAction(id: Option[Int]): DBIOAction[Option[RoomAttenderModel], NoStream, Effect.Read] = {
    innerJoin(RoomAttenderTable.filter(t ⇒ t.id === id)).result.headOption.map(x ⇒ x.map(innerMap))
  }

  override def retrieveUsersInRoom(roomId: Option[Int]): Seq[RoomAttenderModel] = {
    DriverSettings.run(retrieveUsersInRoomAction(roomId).transactionally)
  }

  override def retrieveRoomsThatUserAttended(userId: Option[Int]): Seq[RoomAttenderModel] = {
    DriverSettings.run(retrieveRoomsThatUserAttendedAction(userId).transactionally)
  }

  override def getOneWithUserRoom(userId: Option[Int], roomId: Option[Int]): Option[RoomAttenderModel] = {
    DriverSettings.run(getOneWithUserRoomAction(userId, roomId).transactionally)
  }

  // 중복을 줄이기 위해 쓴다.
  private def innerMap(x: ResultType): RoomAttenderModel = {
    val ((((((((roomAttender, roomAttenderUser), roomAttenderRoom), relatedRoomInOutHistory), roomAttenderUserNickHistory), roomAttenderUserLoginHistory), operator), roomHistory)) = x
    roomAttender.copy(
      user = roomAttenderUser.map(_.copy(
        currentNickHistory = roomAttenderUserNickHistory, currentLoginHistory = roomAttenderUserLoginHistory)),
      room = roomAttenderRoom.map(_.copy(currentDetail = roomHistory)),
      relatedRoomInOutHistory = relatedRoomInOutHistory,
      currentOwnerFlag = roomHistory.map(_.userId).map(_ == roomAttender.userId).orElse(Some(false)),
      currentOperatorFlag = operator.map(_ ⇒ true).orElse(Some(false)))
  }

  // 중복을 줄이기 위해 쓴다.
  private def innerJoin(t: Query[RoomAttenderTable, RoomAttenderModel, Seq]): Query[QueryType, ResultType, Seq] = {
    // room in/out history 의 상세는 구하지 않는다.
    t.joinLeft(UserTable).on(_.userId === _.id).
      joinLeft(RoomTable).on {
        case ((a, _), b) ⇒ a.roomId === b.id
      }.joinLeft(RoomInOutHistoryTable).on {
        case (((a, _), _), b) ⇒ a.relatedRoomInOutHistoryId === b.id
      }.joinLeft(UserNickHistoryTable).on {
        case ((((_, a), _), _), b) ⇒ a.flatMap(_.currentNickHistoryId) === b.id
      }.joinLeft(UserLoginHistoryTable).on {
        case (((((_, a), _), _), _), b) ⇒ a.flatMap(_.currentLoginHistoryId) === b.id
      }.joinLeft(OperatorTable).on {
        case ((((((a, _), _), _), _), _), b) ⇒ a.userId === b.userId && a.roomId === b.roomId
      }.joinLeft(RoomHistoryTable).on {
        case (((((((_, _), a), _), _), _), _), b) ⇒ a.flatMap(_.currentDetailId) === b.id
      }
  }

  def retrieveUsersInRoomAction(roomId: Option[Int]): DBIOAction[Seq[RoomAttenderModel], NoStream, Effect.Read] = {
    innerJoin(RoomAttenderTable.filter(t ⇒ t.roomId === roomId)).
      sortBy { case ((((((_, _), _), a), _), _), _) ⇒ a.flatMap(_.when).asc }.result.
      map(x ⇒ x.map(innerMap))
  }

  def retrieveRoomsThatUserAttendedAction(userId: Option[Int]): DBIOAction[Seq[RoomAttenderModel], NoStream, Effect.Read] = {
    innerJoin(RoomAttenderTable.filter(t ⇒ t.userId === userId)).
      sortBy { case ((((((_, _), _), a), _), _), _) ⇒ a.flatMap(_.when).asc }.result.
      map(x ⇒ x.map(innerMap))
  }

  def getOneWithUserRoomAction(userId: Option[Int], roomId: Option[Int]): DBIOAction[Option[RoomAttenderModel], NoStream, Effect.Read] = {
    innerJoin(RoomAttenderTable.filter(t ⇒ t.userId === userId && t.roomId === roomId)).result.headOption.
      map(x ⇒ x.map(innerMap))
  }
}
