/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.{ InvitationTable, RoomTable }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.RoomHistoryTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.current.InvitationDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait.VersionException
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ InvitationModel, RoomModel }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object InvitationDao extends InvitationDao with SlickDao[InvitationModel, InvitationTable] {
  override val tableQuery = InvitationTable

  type QueryType = ((((InvitationTable, Rep[Option[UserTable]]), Rep[Option[RoomTable]]), Rep[Option[RoomHistoryTable]]), Rep[Option[UserTable]])
  type ResultType = ((((InvitationModel, Option[UserModel]), Option[RoomModel]), Option[RoomHistoryModel]), Option[UserModel])

  def innerJoin(q: Query[InvitationTable, InvitationModel, Seq]): Query[QueryType, ResultType, Seq] = {
    q.joinLeft(UserTable).on(_.userId === _.id).
      joinLeft(RoomTable).on {
        case ((a, _), b) ⇒ a.roomId === b.id
      }.joinLeft(RoomHistoryTable).on {
        case (((_, _), a), b) ⇒ a.flatMap(_.currentDetailId) === b.id
      }.joinLeft(UserTable).on {
        case ((((a, _), _), _), b) ⇒ a.byWhoId === b.id
      }
  }

  def innerMap(x: ResultType): InvitationModel = {
    val ((((invitation, user), room), roomHistory), byWho) = x
    invitation.copy(user = user, room = room.map(_.copy(currentDetail = roomHistory)), byWho = byWho)
  }

  override def getOneAction(id: Option[Int]): DBIOAction[Option[InvitationModel], NoStream, Effect.Read] = {
    innerJoin(InvitationTable.filter(t ⇒ t.id === id)).result.headOption.map(x ⇒ x.map(innerMap))
  }

  override def getAllAboutUser(userId: Option[Int]): Seq[InvitationModel] = {
    DriverSettings.run(getAllAboutUserAction(userId).transactionally)
  }

  def invalidAllInvitationWithUser(userId: Option[Int]): Unit = {
    DriverSettings.run(invalidAllInvitationWithUserAction(userId).transactionally)
  }

  def invalidAllInvitationWithRoom(roomId: Option[Int]): Unit = {
    DriverSettings.run(invalidAllInvitationWithRoomAction(roomId).transactionally)
  }

  def getAllAboutUserAction(userId: Option[Int]): DBIOAction[Seq[InvitationModel], NoStream, Effect.Read] = {
    val now = DriverSettings.now()
    innerJoin(InvitationTable.filter(t ⇒ t.userId === userId && t.validity === Option(true) && t.expirationDateTime >= now)).result.map(x ⇒ x.map(innerMap))
  }

  def invalidAllInvitationWithUserAction(userId: Option[Int]): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    InvitationTable.filter(t ⇒ t.userId === userId && t.validity === Option(true)).result.flatMap { invitations ⇒
      val head = InvitationDao.putOneAction(invitations.head.copy(validity = Some(false))).map {
        case 1 ⇒
        case _ ⇒ throw new VersionException
      }
      invitations.tail.foldLeft(head) {
        case (last, x) ⇒ last.flatMap { _ ⇒
          InvitationDao.putOneAction(x.copy(validity = Some(false))).map {
            case 1 ⇒
            case _ ⇒ throw new VersionException
          }
        }
      }
    }
  }

  def invalidAllInvitationWithRoomAction(roomId: Option[Int]): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    InvitationTable.filter(t ⇒ t.roomId === roomId && t.validity === Option(true)).result.flatMap { invitations ⇒
      val head = InvitationDao.putOneAction(invitations.head.copy(validity = Some(false))).map {
        case 1 ⇒
        case _ ⇒ throw new VersionException
      }
      invitations.tail.foldLeft(head) {
        case (last, x) ⇒ last.flatMap { _ ⇒
          InvitationDao.putOneAction(x.copy(validity = Some(false))).map {
            case 1 ⇒
            case _ ⇒ throw new VersionException
          }
        }
      }
    }
  }
}
