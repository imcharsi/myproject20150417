/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current.RoomTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.RoomHistoryTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.{ UserLoginHistoryTable, UserNickHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.room.history.RoomHistoryDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.RoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }
import slick.dbio.{ DBIOAction, Effect, NoStream }

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object RoomHistoryDao extends RoomHistoryDao with SlickDao[RoomHistoryModel, RoomHistoryTable] {
  override val tableQuery = RoomHistoryTable

  type ResultType = (((((((RoomHistoryModel, Option[UserModel]), Option[RoomModel]), Option[UserModel]), Option[UserLoginHistoryModel]), Option[UserNickHistoryModel]), Option[UserLoginHistoryModel]), Option[UserNickHistoryModel])
  type QueryType = (((((((RoomHistoryTable, Rep[Option[UserTable]]), Rep[Option[RoomTable]]), Rep[Option[UserTable]]), Rep[Option[UserLoginHistoryTable]]), Rep[Option[UserNickHistoryTable]]), Rep[Option[UserLoginHistoryTable]]), Rep[Option[UserNickHistoryTable]])

  private def innerJoin(q: Query[RoomHistoryTable, RoomHistoryModel, Seq]): Query[QueryType, ResultType, Seq] = {
    q.joinLeft(UserTable).on(_.userId === _.id).
      joinLeft(RoomTable).on {
        case ((a, _), b) ⇒ a.roomId === b.id
      }.joinLeft(UserTable).on {
        case (((a, _), _), b) ⇒ a.byWhoId === b.id
      }.joinLeft(UserLoginHistoryTable).on {
        case ((((_, a), _), _), b) ⇒ a.flatMap(_.currentLoginHistoryId) === b.id
      }.joinLeft(UserNickHistoryTable).on {
        case (((((_, a), _), _), _), b) ⇒ a.flatMap(_.currentNickHistoryId) === b.id
      }.joinLeft(UserLoginHistoryTable).on {
        case ((((((_, _), _), a), _), _), b) ⇒ a.flatMap(_.currentLoginHistoryId) === b.id
      }.joinLeft(UserNickHistoryTable).on {
        case (((((((_, _), _), a), _), _), _), b) ⇒ a.flatMap(_.currentNickHistoryId) === b.id
      }
  }

  private def innerMap(x: ResultType): RoomHistoryModel = {
    val (((((((roomHistory, user), room), byWho), userLoginHistory), userNickHistory), byWhoLoginHistory), byWhoNickHistory) = x
    roomHistory.copy(
      user = user.map(_.copy(
        currentLoginHistory = userLoginHistory, currentNickHistory = userNickHistory)),
      byWho = byWho.map(_.copy(
        currentLoginHistory = byWhoLoginHistory, currentNickHistory = byWhoNickHistory)),
      room = room,
      needPassword = roomHistory.password.map(x ⇒ !x.isEmpty))
  }

  override def getOneAction(id: Option[Int]): DBIOAction[Option[RoomHistoryModel], NoStream, Effect.Read] = {
    innerJoin(RoomHistoryTable.filter(t ⇒ t.id === id)).result.headOption.map(_.map(innerMap))
  }
}
