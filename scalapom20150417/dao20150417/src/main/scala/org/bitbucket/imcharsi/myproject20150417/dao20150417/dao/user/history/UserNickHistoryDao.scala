/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.history

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.mixin.SlickDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.UserNickHistoryTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.user.history.UserNickHistoryDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait.NotFound
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.UserNickHistoryModel

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
object UserNickHistoryDao extends UserNickHistoryDao with SlickDao[UserNickHistoryModel, UserNickHistoryTable] {
  override val tableQuery = UserNickHistoryTable

  override def getOneAction(id: Option[Int]): DBIOAction[Option[UserNickHistoryModel], NoStream, Effect.Read] = {
    super.getOneAction(id)
    UserNickHistoryTable.filter(t ⇒ t.id === id).joinLeft(UserTable).on(_.userId === _.id).result.headOption.map {
      case Some((x, xx)) ⇒ Some(x.copy(user = xx))
      case None ⇒ throw new NotFound
    }
  }
}
