/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin.{ ByWhoTableTrait, IdVersionTableTrait }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history.RoomHistoryTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ RoomModel, RoomModelEx }

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
class RoomTable(t: Tag) extends Table[RoomModel](t, "room_table") with IdVersionTableTrait[RoomModel] with ByWhoTableTrait[RoomModel] with DateMapperTrait {
  def createdDateTime = column[Option[Date]]("created_datetime")

  def closedDateTime = column[Option[Date]]("closed_datetime")

  def currentDetailId = column[Option[Int]]("current_detail_id")

  def foreignKey1 = foreignKey(tableName + "_fk1", currentDetailId, RoomHistoryTable)(_.id)

  override def * = (id, version, createdDateTime, closedDateTime, currentDetailId, byWhoId) <> ((RoomModelEx.apply _).tupled, RoomModelEx.unapply)
}

object RoomTable extends TableQuery[RoomTable](x ⇒ new RoomTable(x))
