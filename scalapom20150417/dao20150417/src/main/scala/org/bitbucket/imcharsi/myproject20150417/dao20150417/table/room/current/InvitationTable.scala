/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current

import java.util.Date

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.DateMapperTrait
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.mixin.{ ByWhoTableTrait, IdVersionTableTrait, UserRoomTableTrait }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ InvitationModel, InvitationModelEx }

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
class InvitationTable(t: Tag) extends Table[InvitationModel](t, "invitation_table") with IdVersionTableTrait[InvitationModel] with UserRoomTableTrait[InvitationModel] with ByWhoTableTrait[InvitationModel] with DateMapperTrait {
  def invitationDateTime = column[Option[Date]]("invitation_datetime")

  def expirationDateTime = column[Option[Date]]("expiration_datetime")

  def validity = column[Option[Boolean]]("validity")

  override def * = (id, version, userId, roomId, byWhoId, invitationDateTime, expirationDateTime, validity) <> ((InvitationModelEx.apply _).tupled, InvitationModelEx.unapply)
}

object InvitationTable extends TableQuery[InvitationTable](x ⇒ new InvitationTable(x))
