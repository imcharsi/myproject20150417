/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417

import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.current._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.room.history._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.talk.history.TalkHistoryTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.history.{ UserLoginHistoryTable, UserNickHistoryTable }
import org.bitbucket.imcharsi.myproject20150417.model20150417.NotPersisted
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }
import org.bitbucket.imcharsi.myproject20150417.testutil20150417.TestUtilTrait
import org.scalatest.FunSuite
import slick.lifted.Rep

import scala.reflect.runtime.{ universe ⇒ ru }

/**
 * Created by Kang Woo, Lee on 4/19/15.
 */
class TestTableClassValidity extends FunSuite with TestUtilTrait {
  test("test table class validity 1") {
    assert(testTableClassValidity1[UserModel, UserTable])
    assert(testTableClassValidity1[UserLoginHistoryModel, UserLoginHistoryTable])
    assert(testTableClassValidity1[UserNickHistoryModel, UserNickHistoryTable])
    assert(testTableClassValidity1[RoomModel, RoomTable])
    assert(testTableClassValidity1[RoomHistoryModel, RoomHistoryTable])
    assert(testTableClassValidity1[BanModel, BanTable])
    assert(testTableClassValidity1[InvitationModel, InvitationTable])
    assert(testTableClassValidity1[OperatorModel, OperatorTable])
    assert(testTableClassValidity1[BanHistoryModel, BanHistoryTable])
    assert(testTableClassValidity1[PermissionHistoryModel, PermissionHistoryTable])
    assert(testTableClassValidity1[RoomInOutHistoryModel, RoomInOutHistoryTable])
    assert(testTableClassValidity1[TalkHistoryModel, TalkHistoryTable])
    assert(testTableClassValidity1[RoomTimelineModel, RoomTimelineTable])
    assert(testTableClassValidity1[RoomAttenderModel, RoomAttenderTable])
  }

  def testTableClassValidity1[ModelType: ru.TypeTag, TableType: ru.TypeTag]: Boolean = {
    // table 의 * method 를 정의하는 단계에서, compiler 에 의해 자동으로 검증을 하게 된다. 이 test 는 안해도 되는 건가.
    val a = gatherNotAnnotatedConstructorParameter[UserModel, NotPersisted, IdVersionModelTrait[_]]
    compareSymbolType(a.map(x ⇒ (x, x.typeSignature)), gatherTableMethod[UserTable])
  }

  def gatherTableMethod[T: ru.TypeTag]: List[(ru.Symbol, ru.Type)] = {
    ru.typeOf[UserTable].baseClasses.flatMap(_.typeSignature.decls).filter(x ⇒ x.isMethod && !x.isConstructor).
      map(x ⇒ (x, x.asMethod)).map(x ⇒ (x._1, x._2.returnType)).
      filter(_._2 <:< ru.typeOf[Rep[_]]).
      // 모든 table column 은 Option[_] 으로 정의되어야 한다고 정하자.
      filter(_._2.typeArgs.head <:< ru.typeOf[Option[_]]).
      map(x ⇒ (x._1, x._2.typeArgs.head)).toList
  }

  def compareSymbolType(a: List[(ru.Symbol, ru.Type)], b: List[(ru.Symbol, ru.Type)]): Boolean = {
    // 순서는 상관없다.
    val map1 = a.map(x ⇒ (x._1.name.toString.trim, x._2))
    val map2 = b.map(x ⇒ (x._1.name.toString.trim, x._2))
    // map2 가 map1 보다 더 클 수도 있다. 그래서 개수도 같은지 확인한다.
    map1.forall(x ⇒ map2.exists(y ⇒ x._1 == y._1 && x._2 == y._2)) && map1.length == map2.length
  }

}
