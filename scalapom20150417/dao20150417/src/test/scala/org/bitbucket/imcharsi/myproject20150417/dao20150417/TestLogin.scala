/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417

import java.util.logging.Logger

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait.VersionException
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel.AlreadyLogoutException
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModelEx
import org.scalatest.{ BeforeAndAfterAll, FunSuite }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future, Promise }
import scala.util.{ Failure, Success, Try }

/**
 * Created by Kang Woo, Lee on 4/22/15.
 */
class TestLogin extends FunSuite with BeforeAndAfterAll {
  val logger = Logger.getLogger(getClass.getName)
  //  http://stackoverflow.com/questions/15285284/how-to-configure-a-fine-tuned-thread-pool-for-futures
  //  implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10))

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createSchemeUtil()
  }

  override protected def afterAll(): Unit = {
    DriverSettings.dropSchemeUtil()
    super.afterAll()
  }

  test("test duplicated login/logout") {
    Some(UserModelEx(None, None, None, None, Some("user1"), Some("password1"))). // 새로 만들 계정의 계정 정보를 만든다.
      map(UserDao.postOne(_)). // 계정을 새로 만든다.
      flatMap(UserDao.getOne(_)). // 접속에 필요한 정보를 구한다.
      map { x ⇒
        val result = UserDao.login(x.name.get, x.password.get) // 접속 시도.
        Try {
          UserDao.login(x.name.get, x.password.get) // 중복 접속 시도.
        } match {
          case Failure(_) ⇒
          case Success(_) ⇒ assert(false)
        }
        result
      }.
      map { x ⇒
        val result = UserDao.logout(x) // 접속 종료 시도.
        Try {
          UserDao.logout(x) // 중복 접속 종료 시도.
        } match {
          case Failure(_) ⇒
          case Success(_) ⇒ assert(false)
        }
        result
      }.foreach(assert(_))
  }

  private def promiseComplete(execNo: Int, promise: Promise[Int])(a: Any, i: Int): Unit = {
    logger.info(f"before complete promise:${execNo}-${i}")
    a match {
      case x: Throwable ⇒ promise.failure(x)
      case _ ⇒ promise.success(i)
    }
    logger.info(f"after complete promise:${execNo}-${i}")
  }

  private def futureWait(execNo: Int, future: Future[Int])(a: Any, i: Int): Unit = {
    logger.info(f"wait future:${execNo}-${i}")
    a match {
      case x: FiniteDuration ⇒ Await.result(future, x)
      case _ ⇒ Await.result(future, 10.seconds)
    }
    logger.info(f"complete future:${execNo}-${i}")
  }

  def preparePromiseFutureMap(length: Int): (Map[Int, Map[Int, Any ⇒ Unit]], Map[Int, Map[Int, Any ⇒ Unit]]) = {
    val promiseMap = 1.to(2).map(x ⇒ (x, 1.to(length).map(_ ⇒ Promise[Int]()).toList)).toMap
    val futureMap = promiseMap.map { case (a, b) ⇒ (a, b.map(_.future)) }
    val promiseMap2 = promiseMap.map {
      case (mapIndex, promiseList) ⇒
        (mapIndex, promiseList.zipWithIndex.map {
          case (promise, index) ⇒
            (index + 1, (a: Any) ⇒ promiseComplete(mapIndex, promise)(a, index + 1))
        }.toMap)
    }
    val futureMap2 = futureMap.map {
      case (mapIndex, futureList) ⇒
        (mapIndex, futureList.zipWithIndex.map {
          case (future, index) ⇒
            (index + 1, (a: Any) ⇒ futureWait(mapIndex, future)(a, index + 1))
        }.toMap)
    }
    (promiseMap2, futureMap2)
  }

  // login 작업의 시간초과에 관한 검사는 의미가 없다. 시간초과는 future 에 대한 대기가 예외를 발생시키는 형태로 나타나게 되고,
  // transaction 안에서의 예외는 rollback 이므로, 결론은 분명하다.

  test("test login overlap 1") {
    // 서로 다른 실행 주체가, 동일한 작업을, 겹치는 시각에 수행했을 때, 둘 중 한 주체의 실행은 반드시 실패해야 하는데,
    // 과연 그런지 확인하는 것이 이 test 의 목적이다.
    // 어느 한쪽이 특정단계까지 갔다가, 기다리고 있는 반대쪽을 깨워주고 기다린다.
    // 반대쪽은 성공했든 실패했든 상관없이 끝에가서 기다리고 있는 반대쪽을 깨워준다.
    // 깨어난 반대쪽은 성공하거나 실패할 것으로 예상되는 작업을 계속 수행하야 한다.
    // 이렇게 하면 다양한 단계에서 시간이 겹치는 상황을 검사할 수 있다.
    // login 의 경우, 마지막에 있는 UserTable.putOneAction 이 수행되기 전에 다른 실행주체가 따로 시작하여 먼저 완료해버린다면, 먼저 시작했던 쪽은 실패하게 된다.
    // UserTable.putOneAction 을 수행하고 난 뒤에 transaction 완료전에 다른 실행주체가 따로 시작하면, 먼저 시작했던 쪽이 성공한다.
    // 요약하면, version 을 갱신하거나 행을 삭제하는 database 명령을 먼저 수행하는 쪽이 우선이다. 읽기나 더하기는 상관없다.

    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_4 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val exec1 = Future {
      val signal = Map((4, step1_4))
      val future = DriverSettings.database.run(UserDao.loginAction("user1", "password1", signal).transactionally)
      Await.result(future, 2.seconds)
    }
    val exec2 = Future {
      val signal = Map((1, step2_1))
      val future = DriverSettings.database.run(UserDao.loginAction("user1", "password1", signal).transactionally)
      // 왜 시간초과를 유도하는가. 앞에서 시작한 transaction 이 update 를 수행한 행을 잠그고 있기 때문에 이 transaction 은 대기하게 된다.
      // 한편, 시간초과라는 이유로 예외가 생길 뿐, 하던 작업은 아무런 영향을 받지 않는다.
      Try(Await.ready(future, 1.seconds))
      // 왜 바깥에서 promise 를 성공시켜주는가. 이 transaction 은 앞의 transaction 때문에 멈추게 된다.
      // 따라서 시간초과를 사용하여 하던 작업은 계속하게 놔두고, 앞의 transaction 이 기다리고 있는 promise 를 성공시켜주어야
      // 앞의 transaction 이 완료되면서, 이 transaction 도 진행할 수 있게 된다.
      step2_4()
      // 이 검사의 경우, 위 문장을 실행하여 promise 를 완료해주면, 걸려있던 이 transaction 은 계속 수행하지만,
      // version 차이로 인하여 rollback 된다.
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Success(x)) ⇒ UserDao.logout(x)
      case _ ⇒ assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Failure(_: VersionException)) ⇒ // 이 transaction 이 실패하게 된 이유가 정확히 version 차이 때문인지 확인한다.
      case _ ⇒ assert(false)
    }
  }

  test("test login overlap 2") {
    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_3 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val exec1 = Future {
      val signal = Map((3, step1_3))
      val future = DriverSettings.database.run(UserDao.loginAction("user1", "password1", signal).transactionally)
      Await.result(future, 2.seconds) // 이 검사의 경우 이 transaction 은 빨리 끝난다. 뒤 transaction 에 대기가 없기 때문이다.
    }
    val exec2 = Future {
      // 이 transaction 은, 앞 transaction 이 3단계 정지점에서 정지하게 될 때, 시작하게 된다.
      // 그 때까지는 user table 에 대한 변경이 없으므로, 이 transaction 은 성공하게 되고,
      // 앞 transaction 은 version 차이로 인해 실패하게 된다.
      val signal = Map((1, step2_1), (4, step2_4)) // 이 transaction 의 4단계 정지점에서 앞 transaction 의 promise 를 완료시켜줘도 된다.
      val future = DriverSettings.database.run(UserDao.loginAction("user1", "password1", signal).transactionally)
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Failure(_: VersionException)) ⇒ // 이 transaction 이 실패하게 된 이유가 정확히 version 차이 때문인지 확인한다.
      case _ ⇒ assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Success(x)) ⇒ UserDao.logout(x)
      case _ ⇒ assert(false)
    }
  }

  test("test logout overlap 1") {
    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_4 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val userLoginHistoryId = UserDao.login("user1", "password1")
    val exec1 = Future {
      val signal = Map((5, step1_4))
      val future = DriverSettings.database.run(UserDao.logoutAction(userLoginHistoryId, signal).transactionally)
      Await.result(future, 2.seconds)
    }
    val exec2 = Future {
      val signal = Map((1, step2_1))
      val future = DriverSettings.database.run(UserDao.logoutAction(userLoginHistoryId, signal).transactionally)
      Try(Await.ready(future, 1.seconds))
      step2_4()
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Success(x)) ⇒
      case _ ⇒ assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Failure(_: VersionException)) ⇒
      case _ ⇒ assert(false)
    }
  }

  test("test logout overlap 2") {
    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_4 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val userLoginHistoryId = UserDao.login("user1", "password1")
    val exec1 = Future {
      // 4단계 정지점에서 기다리고 있어도 이 transaction 이 성공한다.
      // 왜냐하면, 뒤 transaction 은 user login history model 의 version 차이로 인하여 실패하게 되기 때문이다.
      val signal = Map((4, step1_4))
      val future = DriverSettings.database.run(UserDao.logoutAction(userLoginHistoryId, signal).transactionally)
      Await.result(future, 2.seconds)
    }
    val exec2 = Future {
      val signal = Map((1, step2_1))
      val future = DriverSettings.database.run(UserDao.logoutAction(userLoginHistoryId, signal).transactionally)
      Try(Await.ready(future, 1.seconds))
      step2_4()
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Success(x)) ⇒
      case _ ⇒ assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Failure(_: VersionException)) ⇒
      case _ ⇒ assert(false)
    }
  }

  test("test logout overlap 3") {
    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_3 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val userLoginHistoryId = UserDao.login("user1", "password1")
    val exec1 = Future {
      val signal = Map((3, step1_3))
      val future = DriverSettings.database.run(UserDao.logoutAction(userLoginHistoryId, signal).transactionally)
      Await.result(future, 2.seconds)
    }
    val exec2 = Future {
      val signal = Map((1, step2_1), (5, step2_4))
      val future = DriverSettings.database.run(UserDao.logoutAction(userLoginHistoryId, signal).transactionally)
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Failure(_: VersionException)) ⇒
      case _ ⇒ assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Success(x)) ⇒
      case _ ⇒ assert(false)
    }
  }

  test("test logout overlap 4") {
    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_3 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val userLoginHistoryId = UserDao.login("user1", "password1")
    val exec1 = Future {
      val signal = Map((2, step1_3))
      val future = DriverSettings.database.run(UserDao.logoutAction(userLoginHistoryId, signal).transactionally)
      Await.result(future, 2.seconds)
    }
    val exec2 = Future {
      val signal = Map((1, step2_1))
      val future = DriverSettings.database.run(UserDao.logoutAction(userLoginHistoryId, signal).transactionally)
      // 2단계 정지점에서의 검사가 실패할 때가 가끔씩 있다. 뒤 transaction 이 끝나기 전에 앞 transaction 이 user model 을 읽을 수도 있고, 끝나고 난 후에 읽을 수도 있다.
      // 그래서, 앞 transaction 을 진행시키기에 앞서 뒤 transaction 을 완료시켜주자. 어떻게 틀리든 틀리는 것은 확실한데, 틀리는 이유가 program 동작 속도에 따라 달라지게 된다.
      Await.ready(future, 1.second) // 뒤 transaction 이 기다리는 시간은, 앞 transaction 이 user login history model 을 구하는 데 걸리는 시간이다. 따라서 1초 안에 끝난다.
      step2_4()
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Failure(_: AlreadyLogoutException)) ⇒
      case x ⇒ //println(x)
        assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Success(x)) ⇒
      case _ ⇒ assert(false)
    }
  }

  test("test logoutForce") {
    val userLoginHistoryId = UserDao.login("user1", "password1")
    UserDao.logoutForce("user1", "password1")
    Try(UserDao.logoutForce("user1", "password1")) match {
      case Success(_) ⇒ assert(false)
      case Failure(_) ⇒
    }
  }

  test("test logoutForce overlap 1") {
    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_3 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val userLoginHistoryId = UserDao.login("user1", "password1")
    val exec1 = Future {
      val signal = Map((3, step1_3))
      val future = DriverSettings.database.run(UserDao.logoutForceAction("user1", "password1", signal).transactionally)
      Await.result(future, 2.seconds)
    }
    val exec2 = Future {
      val signal = Map((1, step2_1))
      val future = DriverSettings.database.run(UserDao.logoutForceAction("user1", "password1", signal).transactionally)
      Try(Await.ready(future, 1.second))
      step2_4()
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Failure(_: VersionException)) ⇒
      case x ⇒ assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Success(x)) ⇒
      case _ ⇒ assert(false)
    }
  }

  test("test logoutForce overlap 2") {
    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_3 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val userLoginHistoryId = UserDao.login("user1", "password1")
    val exec1 = Future {
      val signal = Map((1, step1_3))
      val future = DriverSettings.database.run(UserDao.logoutForceAction("user1", "password1", signal).transactionally)
      Await.result(future, 2.seconds)
    }
    val exec2 = Future {
      val signal = Map((1, step2_1))
      val future = DriverSettings.database.run(UserDao.logoutForceAction("user1", "password1", signal).transactionally)
      Try(Await.ready(future, 1.second))
      step2_4()
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Failure(_: AlreadyLogoutException)) ⇒
      case x ⇒ assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Success(x)) ⇒
      case x ⇒ assert(false)
    }
  }

  test("test logoutForce overlap 3") {
    val (promiseMap, futureMap) = preparePromiseFutureMap(1)
    val step1_3 = () ⇒ {
      promiseMap(2)(1)(1)
      futureMap(1)(1)(10.seconds)
    }
    val step2_1 = () ⇒ {
      futureMap(2)(1)(10.seconds)
    }
    val step2_4 = () ⇒ {
      promiseMap(1)(1)(1)
    }
    val userLoginHistoryId = UserDao.login("user1", "password1")
    val exec1 = Future {
      val signal = Map((4, step1_3))
      val future = DriverSettings.database.run(UserDao.logoutForceAction("user1", "password1", signal).transactionally)
      Await.result(future, 2.seconds)
    }
    val exec2 = Future {
      val signal = Map((1, step2_1))
      val future = DriverSettings.database.run(UserDao.logoutForceAction("user1", "password1", signal).transactionally)
      Try(Await.ready(future, 1.second))
      step2_4()
      Await.result(future, 10.seconds)
    }
    Await.ready(exec1, 10.seconds).value match {
      case Some(Success(x)) ⇒
      case _ ⇒ assert(false)
    }
    Await.ready(exec2, 10.seconds).value match {
      case Some(Failure(_: VersionException)) ⇒
      case x ⇒ assert(false)
    }
  }

  test("getAllLoginUsers") {
    UserDao.postOne(UserModelEx(None, None, None, None, Some("user2"), Some("password1")))
    assert(UserDao.getAllLoginUsers().isEmpty)
    val x1 = UserDao.login("user1", "password1")
    assert(UserDao.getAllLoginUsers().length == 1)
    val x2 = UserDao.login("user2", "password1")
    assert(UserDao.getAllLoginUsers().length == 2)
    UserDao.logout(x1)
    assert(UserDao.getAllLoginUsers().length == 1)
    UserDao.logout(x2)
    assert(UserDao.getAllLoginUsers().isEmpty)
  }
}
