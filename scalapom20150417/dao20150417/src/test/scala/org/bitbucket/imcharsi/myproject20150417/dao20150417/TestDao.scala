/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417

import java.util.Calendar

import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.talk.history.TalkHistoryDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.history.{ UserLoginHistoryDao, UserNickHistoryDao }
import org.bitbucket.imcharsi.myproject20150417.model20150417.dao.mixin.Dao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.mixin.IdVersionModelTrait
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.{ UserLoginHistoryModel, UserNickHistoryModel }
import org.scalatest.{ BeforeAndAfterAll, FunSuite }

/**
 * Created by Kang Woo, Lee on 4/21/15.
 */
class TestDao extends FunSuite with BeforeAndAfterAll {

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createSchemeUtil()
  }

  override protected def afterAll(): Unit = {
    DriverSettings.dropSchemeUtil()
    super.afterAll()
  }

  test("Dao") {
    assert(template(new UserModel, UserDao) == true)
    assert(template(new UserLoginHistoryModel, UserLoginHistoryDao) == true)
    assert(template(new UserNickHistoryModel, UserNickHistoryDao) == true)
    assert(template(new BanModel, BanDao) == true)
    assert(template(new InvitationModel, InvitationDao) == true)
    assert(template(new OperatorModel, OperatorDao) == true)
    assert(template(new RoomModel, RoomDao) == true)
    assert(template(new BanHistoryModel, BanHistoryDao) == true)
    assert(template(new PermissionHistoryModel, PermissionHistoryDao) == true)
    assert(template(new RoomHistoryModel, RoomHistoryDao) == true)
    assert(template(new RoomInOutHistoryModel, RoomInOutHistoryDao) == true)
    // 아래는 일부러 검사에서 제외했다. room timeline 에 관련되는 자료가 하나도 없으면 예외가 일어나도록 했다.
    //    assert(template(new RoomTimelineModel, RoomTimelineDao) == true)
    assert(template(new TalkHistoryModel, TalkHistoryDao) == true)
    assert(template(new RoomAttenderModel, RoomAttenderDao) == true)
  }

  test("timestamp") {
    // java.sql.Date 를 사용하면 시분초가 기록되지 않는다.
    val calendar = Calendar.getInstance()
    DriverSettings.now().foreach(calendar.setTime(_))
    calendar.set(Calendar.HOUR, 1)
    calendar.set(Calendar.MINUTE, 2)
    calendar.set(Calendar.SECOND, 3)
    calendar.set(Calendar.MILLISECOND, 456)
    val now = Some(calendar.getTime)

    val result = RoomHistoryDao.getOne(RoomHistoryDao.postOne(RoomHistoryModelEx(None, None, None, None, None, now, None, None, None)))
    assert(now == result.flatMap(_.when))
  }

  private def template[ModelType <: IdVersionModelTrait[ModelType], D <: Dao[ModelType]](model: ModelType, dao: D): Boolean = {
    val id1 = dao.postOne(model)
    val model1 = dao.getOne(id1)
    val result1 = model1.map(dao.putOne(_))
    val model2 = dao.getOne(id1)
    val result2 = model2.map(x ⇒ dao.deleteOne(x.id, x.version))
    id1.isDefined && model1.isDefined && result1.exists(_ == 1) && model2.flatMap(_.version) == model1.flatMap(_.version).map(_ + 1) && result2.exists(_ == 1)
  }

}
