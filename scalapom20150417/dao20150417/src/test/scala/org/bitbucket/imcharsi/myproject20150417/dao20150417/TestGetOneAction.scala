/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417

import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current.{ InvitationDao, RoomAttenderDao, RoomDao }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.talk.history.TalkHistoryDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.history.UserNickHistoryDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current.{ InvitationModelEx, RoomModel, RoomModelEx }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.talk.history.TalkHistoryModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.history.UserNickHistoryModelEx
import org.scalatest.{ BeforeAndAfterAll, FunSuite }

/**
 * Created by Kang Woo, Lee on 5/16/15.
 */
class TestGetOneAction extends FunSuite with BeforeAndAfterAll {
  override protected def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createSchemeUtil()
  }

  override protected def afterAll(): Unit = {
    DriverSettings.dropSchemeUtil()
    super.afterAll()
  }

  test("BanHistoryDao") {
    val userModel1 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("password"))))
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("password"))))
    val userNickHistory1 = UserNickHistoryDao.getOne(UserNickHistoryDao.postOne(UserNickHistoryModelEx(None, None, userModel1.flatMap(_.id), None, Some("user1"))))
    val userNickHistory2 = UserNickHistoryDao.getOne(UserNickHistoryDao.postOne(UserNickHistoryModelEx(None, None, userModel2.flatMap(_.id), None, Some("user2"))))
    UserDao.putOne(userModel1.map(_.copy(currentNickHistoryId = userNickHistory1.flatMap(_.id))).get)
    UserDao.putOne(userModel2.map(_.copy(currentNickHistoryId = userNickHistory2.flatMap(_.id))).get)
    UserDao.login(userModel1.flatMap(_.name).get, "password")
    UserDao.login(userModel2.flatMap(_.name).get, "password")
    val roomModel = RoomDao.getOne(RoomDao.postOne(RoomModelEx(None, None, None, None, None, userModel1.flatMap(_.id))))
    val banHistoryModel = BanHistoryDao.getOne(BanHistoryDao.postOne(BanHistoryModelEx(None, None, userModel1.flatMap(_.id), roomModel.flatMap(_.id), userModel2.flatMap(_.id), None, None)))
    assert(banHistoryModel.flatMap(_.user).flatMap(_.id) == userModel1.flatMap(_.id))
    assert(banHistoryModel.flatMap(_.byWho).flatMap(_.id) == userModel2.flatMap(_.id))
    // ban history 를 구할 때는 room 의 상세내용을 구하지 않는다.
    assert(banHistoryModel.flatMap(_.room) == roomModel.map(_.copy(currentDetail = None, byWho = None /*, owner = None, relatedPermissionHistory = None*/ )))
    assert(banHistoryModel.flatMap(_.user).flatMap(_.currentNickHistory).isDefined)
    assert(banHistoryModel.flatMap(_.user).flatMap(_.currentLoginHistory).isDefined)
    assert(banHistoryModel.flatMap(_.byWho).flatMap(_.currentNickHistory).isDefined)
    assert(banHistoryModel.flatMap(_.byWho).flatMap(_.currentLoginHistory).isDefined)
  }

  test("RoomDao/RoomAttenderDao/RoomInOutHistoryDao/PermissionHistoryDao/RoomHistoryDao/TalkHistoryDao") {
    val userModel1 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("password"))))
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("password"))))
    val userNickHistory1 = UserNickHistoryDao.getOne(UserNickHistoryDao.postOne(UserNickHistoryModelEx(None, None, userModel1.flatMap(_.id), None, Some("user3"))))
    val userNickHistory2 = UserNickHistoryDao.getOne(UserNickHistoryDao.postOne(UserNickHistoryModelEx(None, None, userModel2.flatMap(_.id), None, Some("user4"))))
    UserDao.putOne(userModel1.map(_.copy(currentNickHistoryId = userNickHistory1.flatMap(_.id))).get)
    UserDao.putOne(userModel2.map(_.copy(currentNickHistoryId = userNickHistory2.flatMap(_.id))).get)
    UserDao.login(userModel1.flatMap(_.name).get, "password")
    UserDao.login(userModel2.flatMap(_.name).get, "password")
    val roomModel = RoomDao.createRoom(userModel1.get, new RoomModel().copy(currentDetail = Some(new RoomHistoryModel()))).map { case ((a, _)) ⇒ a }
    assert(roomModel.flatMap(_.currentDetail).isDefined)
    assert(roomModel.flatMap(_.byWho).flatMap(_.id) == userModel1.flatMap(_.id))
    assert(roomModel.flatMap(_.currentDetail).flatMap(_.user).flatMap(_.id) == userModel1.flatMap(_.id))
    assert(roomModel.flatMap(_.currentDetail).flatMap(_.relatedPermissionHistory).isDefined)
    val roomAttenderModel = RoomAttenderDao.getOne(RoomAttenderDao.retrieveUsersInRoom(roomModel.flatMap(_.id)).head.id)
    assert(roomAttenderModel.flatMap(_.user).flatMap(_.id) == userModel1.flatMap(_.id))
    assert(roomAttenderModel.flatMap(_.user).flatMap(_.currentNickHistory).isDefined)
    assert(roomAttenderModel.flatMap(_.user).flatMap(_.currentLoginHistory).isDefined)
    assert(roomAttenderModel.flatMap(_.room).flatMap(_.id) == roomModel.flatMap(_.id))
    assert(roomAttenderModel.flatMap(_.relatedRoomInOutHistory).isDefined)
    val invitationId = InvitationDao.postOne(InvitationModelEx(None, None, userModel1.flatMap(_.id), roomModel.flatMap(_.id), userModel1.flatMap(_.id), None, None, None))
    val roomInOutHistoryModel = RoomInOutHistoryDao.getOne(roomAttenderModel.flatMap(_.relatedRoomInOutHistoryId))
    roomInOutHistoryModel.map(_.copy(invitationId = invitationId, byWhoId = userModel2.flatMap(_.id))).foreach(RoomInOutHistoryDao.putOne)
    val roomInOutHistoryModel2 = RoomInOutHistoryDao.getOne(roomAttenderModel.flatMap(_.relatedRoomInOutHistoryId))
    assert(roomInOutHistoryModel2.flatMap(_.user).flatMap(_.id) == userModel1.flatMap(_.id))
    assert(roomInOutHistoryModel2.flatMap(_.user).flatMap(_.currentNickHistory).flatMap(_.id) == userNickHistory1.flatMap(_.id))
    assert(roomInOutHistoryModel2.flatMap(_.user).flatMap(_.currentLoginHistory).isDefined)
    assert(roomInOutHistoryModel2.flatMap(_.byWho).flatMap(_.id) == userModel2.flatMap(_.id))
    assert(roomInOutHistoryModel2.flatMap(_.byWho).flatMap(_.currentNickHistory).flatMap(_.id) == userNickHistory2.flatMap(_.id))
    assert(roomInOutHistoryModel2.flatMap(_.byWho).flatMap(_.currentLoginHistory).isDefined)
    assert(roomInOutHistoryModel2.flatMap(_.room).flatMap(_.id) == roomModel.flatMap(_.id))
    assert(roomInOutHistoryModel2.flatMap(_.invitation).flatMap(_.id) == invitationId)
    val permissionHistoryModel = PermissionHistoryDao.getOne(roomModel.flatMap(_.currentDetail).flatMap(_.relatedPermissionHistoryId))
    permissionHistoryModel.map(_.copy(byWhoId = userModel2.flatMap(_.id))).foreach(PermissionHistoryDao.putOne)
    val permissionHistoryModel2 = PermissionHistoryDao.getOne(roomModel.flatMap(_.currentDetail).flatMap(_.relatedPermissionHistoryId))
    assert(permissionHistoryModel2.flatMap(_.user).flatMap(_.id) == userModel1.flatMap(_.id))
    assert(permissionHistoryModel2.flatMap(_.user).flatMap(_.currentNickHistory).flatMap(_.id) == userNickHistory1.flatMap(_.id))
    assert(permissionHistoryModel2.flatMap(_.user).flatMap(_.currentLoginHistory).isDefined)
    assert(permissionHistoryModel2.flatMap(_.byWho).flatMap(_.id) == userModel2.flatMap(_.id))
    assert(permissionHistoryModel2.flatMap(_.byWho).flatMap(_.currentNickHistory).flatMap(_.id) == userNickHistory2.flatMap(_.id))
    assert(permissionHistoryModel2.flatMap(_.byWho).flatMap(_.currentLoginHistory).isDefined)
    assert(permissionHistoryModel2.flatMap(_.room).flatMap(_.id) == roomModel.flatMap(_.id))
    val roomHistoryModel = RoomHistoryDao.getOne(roomModel.flatMap(_.currentDetailId))
    roomHistoryModel.map(_.copy(byWhoId = userModel2.flatMap(_.id))).foreach(RoomHistoryDao.putOne)
    val roomHistoryModel2 = RoomHistoryDao.getOne(roomModel.flatMap(_.currentDetailId))
    assert(roomHistoryModel2.flatMap(_.user).flatMap(_.id) == userModel1.flatMap(_.id))
    assert(roomHistoryModel2.flatMap(_.user).flatMap(_.currentNickHistory).flatMap(_.id) == userNickHistory1.flatMap(_.id))
    assert(roomHistoryModel2.flatMap(_.user).flatMap(_.currentLoginHistory).isDefined)
    assert(roomHistoryModel2.flatMap(_.byWho).flatMap(_.id) == userModel2.flatMap(_.id))
    assert(roomHistoryModel2.flatMap(_.byWho).flatMap(_.currentNickHistory).flatMap(_.id) == userNickHistory2.flatMap(_.id))
    assert(roomHistoryModel2.flatMap(_.byWho).flatMap(_.currentLoginHistory).isDefined)
    assert(roomHistoryModel2.flatMap(_.room).flatMap(_.id) == roomModel.flatMap(_.id))
    val roomTimelineId = RoomDao.talk(roomModel.flatMap(_.id), userModel1.flatMap(_.id), Some("hi"))
    val roomTimelineModel = RoomTimelineDao.getOne(roomTimelineId)
    val talkHistoryModel = TalkHistoryDao.getOne(roomTimelineModel.flatMap(_.talkHistoryId))
    // talk history 에서는 room 의 상세를 구하지 않는다.
    assert(talkHistoryModel.flatMap(_.user).flatMap(_.id) == userModel1.flatMap(_.id))
    assert(talkHistoryModel.flatMap(_.user).flatMap(_.currentNickHistory).flatMap(_.id) == userNickHistory1.flatMap(_.id))
    assert(talkHistoryModel.flatMap(_.user).flatMap(_.currentLoginHistory).isDefined)
    assert(talkHistoryModel.flatMap(_.room).flatMap(_.id) == roomModel.flatMap(_.id))
    assert(talkHistoryModel.flatMap(_.nick).flatMap(_.id) == userNickHistory1.flatMap(_.id))
  }

  test("RoomTimelineDao") {
    val userModel1 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("password"))))
    UserDao.login(userModel1.flatMap(_.name).get, "password")
    val roomModel = RoomDao.createRoom(userModel1.get, new RoomModel().copy(currentDetail = Some(new RoomHistoryModel()))).map(_._1)
    val talkHistoryModel = TalkHistoryDao.getOne(TalkHistoryDao.postOne(new TalkHistoryModel()))
    val banHistoryModel = BanHistoryDao.getOne(BanHistoryDao.postOne(new BanHistoryModel()))
    val permissionHistoryModel = PermissionHistoryDao.getOne(PermissionHistoryDao.postOne(new PermissionHistoryModel()))
    val roomHistoryModel = RoomHistoryDao.getOne(RoomHistoryDao.postOne(new RoomHistoryModel()))
    val roomInOutHistoryModel = RoomInOutHistoryDao.getOne(RoomInOutHistoryDao.postOne(new RoomInOutHistoryModel()))
    val roomTimelineModel1 = RoomTimelineDao.getOne(RoomTimelineDao.postOne(new RoomTimelineModel().copy(roomId = roomModel.flatMap(_.id), talkHistoryId = talkHistoryModel.flatMap(_.id))))
    val roomTimelineModel2 = RoomTimelineDao.getOne(RoomTimelineDao.postOne(new RoomTimelineModel().copy(roomId = roomModel.flatMap(_.id), permissionHistoryId = permissionHistoryModel.flatMap(_.id))))
    val roomTimelineModel3 = RoomTimelineDao.getOne(RoomTimelineDao.postOne(new RoomTimelineModel().copy(roomId = roomModel.flatMap(_.id), roomHistoryId = roomHistoryModel.flatMap(_.id))))
    val roomTimelineModel4 = RoomTimelineDao.getOne(RoomTimelineDao.postOne(new RoomTimelineModel().copy(roomId = roomModel.flatMap(_.id), roomInOutHistoryId = roomInOutHistoryModel.flatMap(_.id))))
    val roomTimelineModel5 = RoomTimelineDao.getOne(RoomTimelineDao.postOne(new RoomTimelineModel().copy(roomId = roomModel.flatMap(_.id), banHistoryId = banHistoryModel.flatMap(_.id))))
    // room timeline 에서는 room 을 구하지 않는다.
    //    assert(roomTimelineModel1.flatMap(_.room).flatMap(_.id) == roomModel.flatMap(_.id))
    assert(roomTimelineModel1.flatMap(_.talkHistory).flatMap(_.id) == talkHistoryModel.flatMap(_.id))
    assert(roomTimelineModel2.flatMap(_.permissionHistory).flatMap(_.id) == permissionHistoryModel.flatMap(_.id))
    assert(roomTimelineModel3.flatMap(_.roomHistory).flatMap(_.id) == roomHistoryModel.flatMap(_.id))
    assert(roomTimelineModel4.flatMap(_.roomInOutHistory).flatMap(_.id) == roomInOutHistoryModel.flatMap(_.id))
    assert(roomTimelineModel5.flatMap(_.banHistory).flatMap(_.id) == banHistoryModel.flatMap(_.id))
  }
}
