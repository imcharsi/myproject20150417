/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417

import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.PlainTextSqlAction
import org.bitbucket.imcharsi.myproject20150417.dao20150417.DriverSettings.api._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.dao20150417.table.user.current.UserTable
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.{ UserModel, UserModelEx }
import org.scalatest.{ BeforeAndAfterAll, FunSuite }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.util.{ Failure, Success }

/**
 * Created by Kang Woo, Lee on 4/20/15.
 */
class TestDatabaseTransaction extends FunSuite with BeforeAndAfterAll {
  // 바뀐 slick api 에 관한 중요한 연습이다.

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    val action = PlainTextSqlAction(DriverSettings.createScheme1).flatMap(_ ⇒ PlainTextSqlAction(DriverSettings.createScheme2))
    val future = DriverSettings.database.run(action)
    Await.ready(future, 1.days)
  }

  override protected def afterAll(): Unit = {
    val action = PlainTextSqlAction(DriverSettings.dropScheme2).flatMap(_ ⇒ PlainTextSqlAction(DriverSettings.dropScheme1))
    val future = DriverSettings.database.run(action)
    Await.ready(future, 1.days)
    super.afterAll()
  }

  test("transaction exercise 1") {
    // 아래와 같이 하면 transaction 중간에 계산도 할 수 있다.
    val x = (for {
      x ← UserTable.returning(UserTable.map(_.id)) += UserModelEx(None, Some(1), None, None, Some("hi"), Some("bye"))
      y ← UserTable.returning(UserTable.map(_.id)) += UserModelEx(None, x.map(_ + 1), None, None, Some("hi"), Some("bye"))
      z ← UserTable.returning(UserTable.map(_.id)) += UserModelEx(None, y.map(_ + 1), None, None, Some("hi"), Some("bye"))
    } yield (x, y, z)).transactionally
    val xx = DriverSettings.database.run(x)
    printResult(Await.ready(xx, 1.day))
  }

  test("transaction exercise 2") {
    // transaction exercise 1 은 아래와 같이 풀어 쓸 수 있다.
    val a1 = UserTable.returning(UserTable.map(_.id)).+=(UserModelEx(None, Some(1), None, None, Some("hi"), Some("bye"))).
      // 위 줄의 실행결과는 아래 줄에서 보이는 것과 같다는 것을, 문법을 통해 확인할 수 있다. 지금 이시점에서는 아무런 동작도 하지 않는다.
      map(x ⇒ x)
    val a2 = a1.flatMap { x ⇒
      UserTable.returning(UserTable.map(_.id)).
        +=(UserModelEx(None, x.map(_ + 1), None, None, Some("hi"), Some("bye"))).
        map(xx ⇒ (x, xx))
    }
    val a3 = a2.flatMap {
      case (a, b) ⇒
        UserTable.returning(UserTable.map(_.id)).
          +=(UserModelEx(None, b.map(_ + 1), None, None, Some("hi"), Some("bye"))).
          map(xxx ⇒ (a, b, xxx))
    }
    val a4 = DriverSettings.database.run(a3.transactionally)
    printResult(Await.ready(a4, 1.day))
  }

  test("transaction exercise 3") {
    val a1 = UserTable.returning(UserTable.map(_.id)).+=(UserModelEx(None, Some(1), None, None, Some("hi"), Some("bye"))).
      flatMap(id ⇒ UserTable.filter(_.id === id).result.headOption)
    val future: Future[Option[UserModel]] = DriverSettings.database.run(a1.transactionally)
    printResult(Await.ready(future, 1.day))
  }

  test("explicitly type declaration exercise 1") {
    val xxx: Future[Seq[UserModel]] = DriverSettings.database.run(UserTable.result)
    printResult(Await.ready(xxx, 1.day))
  }

  test("hi") {
    val id = UserDao.postOne(UserModelEx(None, Some(1), None, None, TestUtil.generateUserName(), Some("bye")))
    val user = UserDao.getOne(id)
    user.foreach(UserDao.putOne(_))
    val user2 = UserDao.getOne(id)
    user2.foreach(x ⇒ UserDao.deleteOne(x.id, x.version))
  }

  private def printResult(x: Future[_]): Unit = {
    x.onComplete {
      case Success(xi) ⇒ println(xi)
      case Failure(x) ⇒ println(x)
    }
  }
}
