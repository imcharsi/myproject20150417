/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.dao20150417

import java.util.Calendar

import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.current._
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.room.history.{ PermissionHistoryDao, RoomHistoryDao, RoomInOutHistoryDao, RoomTimelineDao }
import org.bitbucket.imcharsi.myproject20150417.dao20150417.dao.user.current.UserDao
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.EnterRoomModel
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.current._
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.PermissionHistoryModel.PermissionType.{ GrantOwner, RevokeOwner }
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomHistoryModelEx
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.room.history.RoomInOutHistoryModel.EnterType.Leave
import org.bitbucket.imcharsi.myproject20150417.model20150417.model.user.current.UserModelEx
import org.scalatest.{ BeforeAndAfterAll, FunSuite }

import scala.util.{ Failure, Success, Try }

/**
 * Created by Kang Woo, Lee on 4/30/15.
 */
class TestCreateRoom extends FunSuite with BeforeAndAfterAll {

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createSchemeUtil()
  }

  override protected def afterAll(): Unit = {
    DriverSettings.dropSchemeUtil()
    super.afterAll()
  }

  test("create room/enter room/leave room") {
    // 사용자를 만들고,
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    // 대화방을 만들고
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, b)) ⇒ Some(a)
    }
    // roomAttender 를 구한다. 현재 방장인지 여부가 계산되는지 확인한다.
    // 운영자여부도 확인되는지 검사하는 과정은, 아래의 grantOperatorPermission/revokeOperatorPermission 검사에 있다.
    assert(RoomAttenderDao.getOneWithUserRoom(userModel.id, resultRoomModel.flatMap(_.id)).flatMap(_.currentOwnerFlag) == Some(true))
    // 암호가 필요없는 방이라는 표시가 있는지 확인한다.
    assert(resultRoomModel.flatMap(_.currentDetail).flatMap(_.needPassword) == None)

    // 새로운 사용자를 만들고
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    // 이미 만들어진 방에 입장하고
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    // 두번째 사용자는 방장이 아닌것으로 계산되어야 한다.
    assert(RoomAttenderDao.getOneWithUserRoom(userModel2.id, resultRoomModel.flatMap(_.id)).flatMap(_.currentOwnerFlag) == Some(false))
    // 현재 대화방에 입장한 사용자를 구하고
    val attendingUsers = RoomAttenderDao.retrieveUsersInRoom(resultRoomModel.flatMap(_.id))
    assert(attendingUsers.map(_.user.flatMap(_.id)).diff(Seq(userModel.id, userModel2.id)).isEmpty)
    // 중복 입장은 실패해야 한다.
    Try(RoomDao.enterRoom(userModel.id, resultRoomModel.flatMap(_.id), None)) match {
      case Success(_) ⇒ assert(false)
      case Failure(_) ⇒
    }
    Try(RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)) match {
      case Success(_) ⇒ assert(false)
      case Failure(_) ⇒
    }
    // 입장했던 방에서 퇴장하고
    val leaveRoomResult = RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    val resultRoomModel2 = RoomDao.getOne(resultRoomModel.flatMap(_.id))
    // 대화방의 현재 상태에 소유자가 바뀌어 있는지 확인한다.
    resultRoomModel2.map(_.currentDetail.flatMap(_.userId) == userModel2.id).foreach(assert(_))
    val leaveRoomResult2 = leaveRoomResult.map(RoomTimelineDao.getOne(_))
    val leaveRoomResult3 = leaveRoomResult2.map { x ⇒
      (x, PermissionHistoryDao.getOne(x.flatMap(_.permissionHistoryId)))
    }
    val leaveRoomResult4 = leaveRoomResult2.map { x ⇒
      (x, RoomInOutHistoryDao.getOne(x.flatMap(_.roomInOutHistoryId)))
    }
    // 방장이 퇴장을 했으므로 방장이 바뀌어야 한다.
    assert(RoomAttenderDao.getOneWithUserRoom(userModel2.id, resultRoomModel.flatMap(_.id)).flatMap(_.currentOwnerFlag) == Some(true))
    // 퇴장하면서 반환받은 이력을 확인한다.
    // 퇴장했으므로 대화방을 나갔다는 이력이 있어야 한다.
    assert(leaveRoomResult4.filter(_._2.flatMap(_.enterType) == Some(Leave)).filter(_._2.flatMap(_.userId) == userModel.id).length == 1)
    // 방장이 대화방을 나갔으므로 갖고 있던 방장권한이 회수되는 이력이 있어야 한다.
    assert(leaveRoomResult3.filter(_._2.flatMap(_.permissionType) == Some(RevokeOwner)).filter(_._2.flatMap(_.userId) == userModel.id).length == 1)
    // 방장후보를 구할 것으로 예상되므로, 방장권한이 수여되는 이력이 있어야 한다.
    assert(leaveRoomResult3.filter(_._2.flatMap(_.permissionType) == Some(GrantOwner)).filter(_._2.flatMap(_.userId) == userModel2.id).length == 1)

    resultRoomModel2.flatMap(_.currentDetail).map(_.userId).foreach(x ⇒ assert(x == userModel2.id))
    resultRoomModel2.map(_.currentDetailId).foreach(x ⇒ assert(x != resultRoomModel.flatMap(_.currentDetailId)))
    resultRoomModel2.flatMap(_.currentDetail).map(_.relatedPermissionHistoryId).foreach(x ⇒ assert(x != resultRoomModel.flatMap(_.currentDetail).flatMap(_.relatedPermissionHistoryId)))

    // 아직 한 사람이 남아 있고 방폐쇄를 시도하여 실패를 예상한다.
    // 반드시 특정 예외가 던져져야 한다.
    Try(RoomDao.closeRoom(resultRoomModel.flatMap(_.id))) match {
      case scala.util.Failure(x: NotEmptyRoomException) ⇒
      case _ ⇒ assert(false)
    }

    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)

    val resultRoomModel3: Option[RoomModel] = RoomDao.getOne(resultRoomModel.flatMap(_.id))
    resultRoomModel3.flatMap(_.currentDetail).map(_.userId).foreach(x ⇒ assert(x.isEmpty))
    resultRoomModel3.map(_.currentDetailId).foreach(x ⇒ assert(x != resultRoomModel2.flatMap(_.currentDetailId)))
    resultRoomModel3.flatMap(_.currentDetail).map(_.relatedPermissionHistoryId).foreach(x ⇒ assert(x != resultRoomModel2.flatMap(_.currentDetail).flatMap(_.relatedPermissionHistoryId)))
    resultRoomModel3.map(_.closedDateTime).foreach(x ⇒ assert(x.isEmpty))

    // 아무도 없는 방을 폐쇄한다. 성공해야 한다.
    Try(RoomDao.closeRoom(resultRoomModel.flatMap(_.id))) match {
      case Success(_) ⇒
      case x ⇒ assert(false)
    }

    val resultRoomModel4: Option[RoomModel] = RoomDao.getOne(resultRoomModel.flatMap(_.id))
    resultRoomModel4.flatMap(_.currentDetail).map(_.userId).foreach(x ⇒ assert(x.isEmpty))
    resultRoomModel4.map(_.currentDetailId).foreach(x ⇒ assert(x == resultRoomModel3.flatMap(_.currentDetailId)))
    resultRoomModel4.flatMap(_.currentDetail).map(_.relatedPermissionHistoryId).foreach(x ⇒ assert(x == resultRoomModel3.flatMap(_.currentDetail).flatMap(_.relatedPermissionHistoryId)))
    // 폐쇄시각만 바뀌어야 한다.
    resultRoomModel4.map(_.closedDateTime).foreach(x ⇒ assert(x.isDefined))

    // 폐쇄된 방에 입장할 수 없어야 한다.
    Try(RoomDao.enterRoom(userModel.id, resultRoomModel.flatMap(_.id), None)) match {
      case Failure(x: ClosedRoomException) ⇒
      case _ ⇒ assert(false)
    }
  }

  test("invitation") {
    // 사용자를 만들고,
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), Some("password"), None)))
    // 암호를 사용하는 대화방을 만들고
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    // 암호가 필요한 방이라는 표시가 됐는지 확인한다.
    assert(resultRoomModel.flatMap(_.currentDetail).flatMap(_.needPassword) == Some(true))

    // 새로운 사용자를 만들고
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel3 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get

    // 암호없이 입장을 시도한다. 실패해야 한다.
    Try(RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)) match {
      case Failure(x) ⇒
      case Success(_) ⇒ assert(false)
    }
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), Option(EnterRoomModel(Some("password"), None)))
    // 권한이 없는 사용자의 초대는 실패해야 한다.
    Try(RoomDao.invite(resultRoomModel.flatMap(_.id), userModel3.id, userModel2.id)) match {
      case Failure(x) ⇒
      case Success(_) ⇒ assert(false)
    }
    val invitationId = RoomDao.invite(resultRoomModel.flatMap(_.id), userModel3.id, userModel.id)
    // 초대장 목록에서 초대장이 구해지는지 확인한다.
    assert(InvitationDao.getAllAboutUser(userModel3.id).map(_.id) == List(invitationId))
    // 틀린 암호와 유효한 초대장을 가지고 입장을 시도한다. 암호가 틀려도 초대장이 유효하면 성공해야 한다.
    RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), Option(EnterRoomModel(Some("incorrect"), invitationId)))
    // 초대장을 사용했으니 유효한 초대장은 없어야 한다.
    assert(InvitationDao.getAllAboutUser(userModel3.id).isEmpty)
    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel3.id, resultRoomModel.flatMap(_.id), None)
    // 사용한 초대장을 가지고 다시 입장을 시도한다. 실패해야 한다.
    Try(RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), Option(EnterRoomModel(None, invitationId)))) match {
      case Failure(x) ⇒
      case Success(_) ⇒ assert(false)
    }
    // 앞에서는 초대장의 유효시간 범위 내에서 입장을 시도했다. 유효시간이 만료된 초대장으로 입장을 시도한다.
    val invitationId2 = RoomDao.invite(resultRoomModel.flatMap(_.id), userModel3.id, userModel.id)
    // 초대장을 만들 때 유효시간을 외부에서 정할 수 없으므로, 검사목적으로 직접 초대장의 유효시간을 조정한다.
    val now = DriverSettings.now()
    val calendar = Calendar.getInstance()
    calendar.setTime(now.get)
    calendar.add(Calendar.SECOND, -1)
    val newNow = calendar.getTime
    InvitationDao.getOne(invitationId2).map(_.copy(expirationDateTime = Some(newNow), invitationDateTime = Some(newNow))).map(InvitationDao.putOne(_))
    // 유효시간을 조작했다. 이 초대장은 초대장목록에 없어야 한다.
    assert(InvitationDao.getAllAboutUser(userModel3.id).isEmpty)
    Try(RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), Option(EnterRoomModel(None, invitationId2)))) match {
      case Failure(x) ⇒
      case Success(_) ⇒ assert(false)
    }
    // 운영자권한을 수여한다. 앞에서는 운영자권한 없이 하는 초대가 실패하는 것을 확인하였다.
    // 우선, 다시 입장한다. 여기서 암호검사까지 했다.
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), Option(EnterRoomModel(Some("password"), None)))
    // 운영자권한을 수여한다.
    OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel2.id)
    // 운영자가 초대를 한다.
    val invitationId3 = RoomDao.invite(resultRoomModel.flatMap(_.id), userModel3.id, userModel2.id)
    // 운영자가 보낸 초대장을 가지고 입장한다.
    RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), Option(EnterRoomModel(Some("incorrect"), invitationId3)))
    RoomDao.leaveRoom(userModel3.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)

    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("talk/change nick") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }

    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    val roomTimeline1 = RoomTimelineDao.getOne(RoomDao.talk(resultRoomModel.flatMap(_.id), userModel.id, Some("talk 1")))
    assert(roomTimeline1.flatMap(_.talkHistoryId).isDefined)
    // 대화명을 쓰지 않은 상태에서는 대화명이 없다고 계산되어야 한다.
    assert(roomTimeline1.flatMap(_.talkHistory).flatMap(_.nick).isEmpty)
    val result = UserDao.setCurrentNick(userModel2.id, Some("nick#1"))
    // 대화명을 바꾸면 참가중인 대화방에서 room timeline 도 만들어지는지 확인한다.
    assert(result._2.map(_._1).flatMap(RoomTimelineDao.getOne(_)).flatMap(_.userNickHistory).map(_.nick) == List(Some("nick#1")))
    val roomTimeline2 = RoomTimelineDao.getOne(RoomDao.talk(resultRoomModel.flatMap(_.id), userModel2.id, Some("talk 2")))
    assert(roomTimeline2.flatMap(_.talkHistoryId).isDefined)
    // 대화명을 쓴 후에는 대화명이 있다고 계산되어야 한다.
    assert(roomTimeline2.flatMap(_.talkHistory).flatMap(_.nick).flatMap(_.nick) == Some("nick#1"))
    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    Try(RoomDao.talk(resultRoomModel.flatMap(_.id), userModel2.id, Some("talk 3"))) match {
      case Failure(_) ⇒
      case Success(_) ⇒ assert(false)
    }
    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("getAllCreatedRooms/retrieveRoomsThatUserAttended") {
    assert(RoomDao.getAllCreatedRooms().isEmpty)
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    assert(RoomDao.getAllCreatedRooms().length == 1)
    assert(RoomAttenderDao.retrieveRoomsThatUserAttended(userModel.id).length == 1)
    val roomModel2 = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #2"), None, None)))
    val resultRoomModel2 = RoomDao.createRoom(userModel, roomModel2) match {
      case Some((a, _)) ⇒ Some(a)
    }
    assert(RoomDao.getAllCreatedRooms().length == 2)
    assert(RoomAttenderDao.retrieveRoomsThatUserAttended(userModel.id).length == 2)
    RoomDao.leaveRoom(userModel.id, resultRoomModel2.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel2.flatMap(_.id))
    assert(RoomDao.getAllCreatedRooms().length == 1)
    assert(RoomAttenderDao.retrieveRoomsThatUserAttended(userModel.id).length == 1)
    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
    assert(RoomDao.getAllCreatedRooms().isEmpty)
    assert(RoomAttenderDao.retrieveRoomsThatUserAttended(userModel.id).isEmpty)
  }

  test("getAllCreatedRooms with userId") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    assert(RoomDao.getAllCreatedRooms(userModel.id).flatMap(_.currentAttendingRoom).filter(_ == true).length == 1)
    assert(RoomDao.getAllCreatedRooms(userModel2.id).flatMap(_.currentAttendingRoom).filter(_ == true).isEmpty)

    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    assert(RoomDao.getAllCreatedRooms(userModel2.id).flatMap(_.currentAttendingRoom).filter(_ == true).length == 1)

    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("leaveRoom and revokeOwner 1") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    // 추가로 입장한다.
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), Some(EnterRoomModel(None, None)))
    // 추가로 입장했던 사람이 퇴장한다. 퇴장하면서 만들어진 room timeline 목록을 구한다.
    val roomTimelineList = RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None).
      map(x ⇒ RoomTimelineDao.getOne(x)).
      flatten
    // 방장 아닌 사람이 퇴장했으므로 권한변경에 관한 이력은 없어야 한다.
    assert(roomTimelineList.filter(_.permissionHistoryId.isDefined).isEmpty)
    val resultRoomModel2 = RoomDao.getOne(resultRoomModel.flatMap(_.id))
    // 방장 아닌 사람이 퇴장했으므로 방장은 바뀌지 않아야 한다.
    assert(resultRoomModel.flatMap(_.currentDetail).flatMap(_.userId) == resultRoomModel2.flatMap(_.currentDetail).flatMap(_.userId))
    // 방장이 퇴장한다. 퇴장하면서 만들어진 room timeline 목록을 구한다.
    val roomTimelineList2 = RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None).
      map(x ⇒ RoomTimelineDao.getOne(x)).
      flatten
    // 방장이 퇴장했으므로 권한변경에 관한 이력이 있어야 한다.
    assert(roomTimelineList2.filter(_.permissionHistoryId.isDefined).isEmpty == false)
    val resultRoomModel3 = RoomDao.getOne(resultRoomModel.flatMap(_.id))
    // 방장이 퇴장했으므로 소유자가 바뀌어야 한다.
    assert(resultRoomModel3.flatMap(_.currentDetail).flatMap(_.userId).isEmpty)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("leaveRoom and revokeOwner 2") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    // 추가로 입장한다.
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), Some(EnterRoomModel(None, None)))
    // 방장이 퇴장한다. 퇴장하면서 만들어진 room timeline 목록을 구한다.
    val roomTimelineList = RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None).
      map(x ⇒ RoomTimelineDao.getOne(x)).
      flatten
    // 방장이 퇴장했으므로 권한변경에 관한 이력이 있어야 한다.
    assert(roomTimelineList.filter(_.permissionHistoryId.isDefined).isEmpty == false)
    val resultRoomModel2 = RoomDao.getOne(resultRoomModel.flatMap(_.id))
    // 방장이 퇴장했으므로 현재 소유자가 바뀌어야 한다.
    assert(resultRoomModel2.flatMap(_.currentDetail).flatMap(_.userId) == userModel2.id)
    // 역시 방장이 퇴장한다. 남은 사람이 한사람이기 때문이다. 퇴장하면서 만들어진 room timeline 목록을 구한다.
    val roomTimelineList2 = RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None).
      map(x ⇒ RoomTimelineDao.getOne(x)).
      flatten
    // 방장인 사람이 퇴장했으므로 권한변경에 관한 이력이 있어야 한다.
    assert(roomTimelineList2.filter(_.permissionHistoryId.isDefined).isEmpty == false)
    val resultRoomModel3 = RoomDao.getOne(resultRoomModel.flatMap(_.id))
    // 방장이 퇴장했으므로 소유자가 바뀌어야 한다.
    assert(resultRoomModel3.flatMap(_.currentDetail).flatMap(_.userId).isEmpty)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("leaveRoom and byWhoId by Owner") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }

    // 대화방 목록을 조회할 때, 입장금지된 대화방이 아닌 것으로 나타나야 한다.
    assert(RoomDao.getAllCreatedRooms(userModel2.id).filter(_.id === resultRoomModel.flatMap(_.id)).map(_.currentBannedRoom) == List(Some(false)))

    // 추가로 입장한다.
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), Some(EnterRoomModel(None, None)))
    // 자발적인 퇴장이다.
    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    // 입장금지자가 없어야 한다.
    assert(BanDao.getAllByRoom(resultRoomModel.flatMap(_.id)).isEmpty)
    // 이번에는 강제퇴장을 시험하기 위해 다시 입장한다.
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), Some(EnterRoomModel(None, None)))
    // 자기자신에 대한 강제퇴장은 할 수 없다.
    assert(Try(RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), userModel2.id)).isFailure)
    // 방장권한이 있어도 자기자신은 강제퇴장시킬 수 없다.
    assert(Try(RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)
    // 권한이 없는 사람은 다른사람을 강제퇴장시킬 수 없다.
    assert(Try(RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), userModel2.id)).isFailure)
    // 방장에 의해 강제퇴장된다.
    val roomTimelineList = RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), userModel.id).
      map(x ⇒ RoomTimelineDao.getOne(x)).
      flatten
    // 강제퇴장에 관한 room timeline 이력이 있는지 살펴본다.
    assert(roomTimelineList.filter(_.banHistoryId.isDefined).isEmpty == false)
    // 입장금지자 목록이 있는지 살펴본다.
    val banList: Seq[BanModel] = BanDao.getAllByRoom(resultRoomModel.flatMap(_.id))
    assert(banList.headOption.flatMap(_.user).flatMap(_.id) == userModel2.id)

    // 대화방 목록을 조회할 때, 입장금지된 대화방인 것으로 나타나야 한다.
    val rooms: Seq[RoomModel] = RoomDao.getAllCreatedRooms(userModel2.id)
    assert(rooms.filter(_.id === resultRoomModel.flatMap(_.id)).map(_.currentBannedRoom) == List(Some(true)))

    // 강제퇴장 이후 입장 시도는 실패해야 한다.
    assert(Try(RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), Some(EnterRoomModel(None, None)))).isFailure)
    // 입장금지를 해제한다.
    BanDao.clearUserBan(userModel2.id, resultRoomModel.flatMap(_.id), userModel.id)
    // 입장금지자가 없어야 한다.
    assert(BanDao.getAllByRoom(resultRoomModel.flatMap(_.id)).isEmpty)
    // 입장할 수 있어야 한다.
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), Some(EnterRoomModel(None, None)))
    // 중복된 입장금지 해제는 실패해야 한다.
    assert(Try(BanDao.clearUserBan(userModel2.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)

    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("revokeOwnerPermission") {
    val emptyEnterRoomModel = Some(EnterRoomModel(None, None))
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    // 방장후보가 없으면 방장권한 위임을 할 수 없다.
    assert(Try(RoomDao.revokeOwnerPermission(userModel.id, resultRoomModel.flatMap(_.id), None)).isFailure)
    // 방장은 자신을 방장후보로 방장권한 위임을 수행할 수 없다.
    assert(Try(RoomDao.revokeOwnerPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)
    // 대화방에 있지 않은 사람에게 방장권한을 위임할 수 없다.
    assert(Try(RoomDao.revokeOwnerPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel2.id)).isFailure)

    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)
    // 방장이 아닌 사람은 방장권한 위임을 할 수 없다.
    assert(Try(RoomDao.revokeOwnerPermission(userModel2.id, resultRoomModel.flatMap(_.id), None)).isFailure)
    assert(Try(RoomDao.revokeOwnerPermission(userModel2.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)

    // 방장후보를 정하지 않아도 방장권한이 위임되어야 한다.
    val resultRoomTimelineIdList = RoomDao.revokeOwnerPermission(userModel.id, resultRoomModel.flatMap(_.id), None)
    val resultRoomTimelineList = resultRoomTimelineIdList.flatMap(x ⇒ RoomTimelineDao.getOne(x))
    assert(resultRoomTimelineList.flatMap(_.permissionHistory).filter(_.user.flatMap(_.id) == userModel.id).flatMap(_.permissionType).filter(_ == PermissionType.RevokeOwner) == List(PermissionType.RevokeOwner))
    assert(resultRoomTimelineList.flatMap(_.permissionHistory).filter(_.user.flatMap(_.id) == userModel2.id).flatMap(_.permissionType).filter(_ == PermissionType.GrantOwner) == List(PermissionType.GrantOwner))
    assert(resultRoomTimelineList.flatMap(_.roomHistory).flatMap(_.user).map(_.id) == List(userModel2.id))
    // 더 이상 방장이 아니므로 방장권한을 위임할 수 없다.
    assert(Try(RoomDao.revokeOwnerPermission(userModel.id, resultRoomModel.flatMap(_.id), None)).isFailure)
    // 방장후보를 정하고 방장권한을 위임한다.
    RoomDao.revokeOwnerPermission(userModel2.id, resultRoomModel.flatMap(_.id), userModel.id)
    // 자신을 방장후보로 지정한다면, 대화방에 방장을 제외한 다른 사람이 있더라도 방장권한 위임은 실패해야 한다.
    assert(Try(RoomDao.revokeOwnerPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)

    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("grantOperatorPermission/revokeOperatorPermission") {
    val emptyEnterRoomModel = Some(EnterRoomModel(None, None))
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }

    // room attender 의 운영자여부 계산기능이 잘 동작하는지 확인한다.
    assert(RoomAttenderDao.getOneWithUserRoom(userModel.id, resultRoomModel.flatMap(_.id)).flatMap(_.currentOperatorFlag) == Some(false))
    // 방장은 자신에게 운영자권한을 수여할 수 있다.
    val permHistId = RoomTimelineDao.getOne(OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel.id)).flatMap(_.permissionHistoryId)
    assert(permHistId.isDefined)
    assert(PermissionHistoryDao.getOne(permHistId).filter(_.permissionType == Some(PermissionType.GrantOperator)).filter(_.userId == userModel.id).isDefined)
    assert(RoomAttenderDao.getOneWithUserRoom(userModel.id, resultRoomModel.flatMap(_.id)).flatMap(_.currentOperatorFlag) == Some(true))
    // 중복해서 운영자권한을 수여할 수 없다.
    assert(Try(OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)
    // 운영자는 자신의 운영자권한을 반납할 수 있다.
    val permHistId2 = RoomTimelineDao.getOne(OperatorDao.revokeOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel.id)).flatMap(_.permissionHistoryId)
    assert(permHistId2.isDefined)
    assert(PermissionHistoryDao.getOne(permHistId2).filter(_.permissionType == Some(PermissionType.RevokeOperator)).filter(_.userId == userModel.id).isDefined)
    // 중복해서 운영자권한을 회수할 수 없다.
    assert(Try(OperatorDao.revokeOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)

    // 다른 사용자 입장
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)
    // 방장만 운영자권한을 수여할 수 있다. 운영자가 아닌 사람의 운영자권한 수여시도.
    assert(Try(OperatorDao.grantOperatorPermission(userModel2.id, resultRoomModel.flatMap(_.id), userModel2.id)).isFailure)
    // 방장이 운영자권한수여
    OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel2.id)
    // 방장만 운영자권한을 수여할 수 있다. 방장이 아닌 운영자가 다른사람에게 운영자권한 수여시도.
    assert(Try(OperatorDao.grantOperatorPermission(userModel2.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)
    // 방장이 다시 자신에게 운영자권한 수여.
    OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel.id)
    // 방장만 운영자권한을 회수할 수 있다. 방장이 아닌 운영자가 방장의 운영자권한 회수시도.
    assert(Try(OperatorDao.revokeOperatorPermission(userModel2.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)
    // 방장이 아닌 운영자가 자신의 운영자권한을 반납.
    OperatorDao.revokeOperatorPermission(userModel2.id, resultRoomModel.flatMap(_.id), userModel2.id)
    // 운영자가 아닌 사람이 다른 사람의 운영자권한 회수 시도.
    assert(Try(OperatorDao.revokeOperatorPermission(userModel2.id, resultRoomModel.flatMap(_.id), userModel.id)).isFailure)
    // 운영자가 아닌 사람이 자신의 운영자권한 반납시도.
    assert(Try(OperatorDao.revokeOperatorPermission(userModel2.id, resultRoomModel.flatMap(_.id), userModel2.id)).isFailure)

    // 대화방에서 퇴장할 때, 운영자권한도 자동으로 없어지는지 확인하기 위해서 다시 운영자권한 수여.
    OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel2.id)
    // 방장이 아닌 운영자 퇴장.
    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    // 자동으로 운영자권한도 회수되었는지 확인.
    assert(OperatorDao.getOneByUserRoom(userModel2.id, resultRoomModel.flatMap(_.id)).isEmpty)

    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("setRoomPassword") {
    val emptyEnterRoomModel = Some(EnterRoomModel(None, None))
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel3 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    // 암호없이 대화방을 만들었다.
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }

    // 암호없는 대화방에 입장한다.
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)
    // 운영자나 방장만 암호를 정할 수 있다.
    assert(Try(RoomDao.setRoomPassword(userModel2.id, resultRoomModel.flatMap(_.id), Some("password"))).isFailure)
    OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel2.id)
    // 암호를 정한다.
    val roomTimelineId = RoomDao.setRoomPassword(userModel.id, resultRoomModel.flatMap(_.id), Some("password"))
    // 암호가 실제로 바뀌었는지 확인한다.
    assert(RoomHistoryDao.getOne(RoomTimelineDao.getOne(roomTimelineId).flatMap(_.roomHistoryId)).flatMap(_.password) == Some("password"))

    // 암호있는 대화방에 암호없이 입장시도한다.
    assert(Try(RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)).isFailure)
    // 암호있는 대화방에 암호를 사용하여 입장시도한다.
    RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel.map(_.copy(password = Some("password"))))
    // 운영자도 암호를 바꿀 수 있는지 확인하기 위해 일단 퇴장한다.
    RoomDao.leaveRoom(userModel3.id, resultRoomModel.flatMap(_.id), None)

    // 운영자가 암호를 바꾼다.
    RoomDao.setRoomPassword(userModel2.id, resultRoomModel.flatMap(_.id), Some("password2"))
    // 틀린 암호를 사용하여 입장 시도한다.
    assert(Try(RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel.map(_.copy(password = Some("password"))))).isFailure)

    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("leaveRoom and byWhoId by Operator") {
    val emptyEnterRoomModel = Some(EnterRoomModel(None, None))
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel3 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)
    RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)
    // 운영자 권한없이 강제퇴장을 시도한다.
    assert(Try(RoomDao.leaveRoom(userModel3.id, resultRoomModel.flatMap(_.id), userModel2.id)).isFailure)
    // 운영자권한을 수여한다.
    OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel2.id)
    RoomDao.leaveRoom(userModel3.id, resultRoomModel.flatMap(_.id), userModel2.id)
    // 입장금지 된 방에 입장을 시도한다.
    assert(Try(RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)).isFailure)
    // 입장금지 설정을 운영자가 지운다.
    BanDao.clearUserBan(userModel3.id, resultRoomModel.flatMap(_.id), userModel2.id)
    // 다시 입장한다.
    RoomDao.enterRoom(userModel3.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)

    RoomDao.leaveRoom(userModel3.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("change room name") {
    val emptyEnterRoomModel = Some(EnterRoomModel(None, None))
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), None, None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    val retrievedRoomModel = RoomDao.getOne(resultRoomModel.flatMap(_.id))
    assert(retrievedRoomModel.flatMap(_.currentDetail).flatMap(_.name) == Some("room #1"))

    RoomDao.enterRoom(userModel2.id, resultRoomModel.flatMap(_.id), emptyEnterRoomModel)

    // 방장이나 운영자만 대화방 제목을 바꿀 수 있다.
    assert(Try(RoomDao.setRoomName(resultRoomModel.flatMap(_.id), Some("room #2"), userModel2.id)).isFailure)

    // 방장은 대화방 제목을 바꿀 수 있다.
    val roomTimelineId = RoomDao.setRoomName(resultRoomModel.flatMap(_.id), Some("room #2"), userModel.id)
    assert(RoomTimelineDao.getOne(roomTimelineId).flatMap(_.roomHistory).flatMap(_.name) == Some("room #2"))
    assert(RoomDao.getOne(resultRoomModel.flatMap(_.id)).flatMap(_.currentDetail).flatMap(_.name) == Some("room #2"))

    // 운영자 권한을 수여한다.
    OperatorDao.grantOperatorPermission(userModel.id, resultRoomModel.flatMap(_.id), userModel2.id)

    // 운영자도 대화방 제목을 바꿀 수 있다.
    val roomTimelineId2 = RoomDao.setRoomName(resultRoomModel.flatMap(_.id), Some("room ##2"), userModel2.id)
    assert(RoomTimelineDao.getOne(roomTimelineId2).flatMap(_.roomHistory).flatMap(_.name) == Some("room ##2"))
    assert(RoomDao.getOne(resultRoomModel.flatMap(_.id)).flatMap(_.currentDetail).flatMap(_.name) == Some("room ##2"))

    RoomDao.leaveRoom(userModel2.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("invalidAllInvitationWithUser") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userLoginHistoryId = UserDao.login(userModel.name.get, userModel.password.get)
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), Some("password"), None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    assert(resultRoomModel.flatMap(_.currentDetail).flatMap(_.needPassword) == Some(true))

    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userLoginHistoryId2 = UserDao.login(userModel2.name.get, userModel2.password.get)

    val invitationId = RoomDao.invite(resultRoomModel.flatMap(_.id), userModel2.id, userModel.id)
    val invitationId2 = RoomDao.invite(resultRoomModel.flatMap(_.id), userModel2.id, userModel.id)
    assert(InvitationDao.getOne(invitationId).flatMap(_.validity) == Some(true))
    assert(InvitationDao.getOne(invitationId2).flatMap(_.validity) == Some(true))

    UserDao.logout(userLoginHistoryId2)
    InvitationDao.invalidAllInvitationWithUser(userModel2.id)
    assert(InvitationDao.getOne(invitationId).flatMap(_.validity) == Some(false))
    assert(InvitationDao.getOne(invitationId2).flatMap(_.validity) == Some(false))

    UserDao.logout(userLoginHistoryId)
    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))
  }

  test("invalidAllInvitationWithRoom") {
    val userModel = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userLoginHistoryId = UserDao.login(userModel.name.get, userModel.password.get)
    val roomModel = RoomModelEx(None, None, None, None, None, None).copy(currentDetail = Some(RoomHistoryModelEx(None, None, None, None, None, None, Some("room #1"), Some("password"), None)))
    val resultRoomModel = RoomDao.createRoom(userModel, roomModel) match {
      case Some((a, _)) ⇒ Some(a)
    }
    assert(resultRoomModel.flatMap(_.currentDetail).flatMap(_.needPassword) == Some(true))

    val userModel2 = UserDao.getOne(UserDao.postOne(UserModelEx(None, None, None, None, TestUtil.generateUserName(), Some("bye")))).get
    val userLoginHistoryId2 = UserDao.login(userModel2.name.get, userModel2.password.get)

    val invitationId = RoomDao.invite(resultRoomModel.flatMap(_.id), userModel2.id, userModel.id)
    val invitationId2 = RoomDao.invite(resultRoomModel.flatMap(_.id), userModel2.id, userModel.id)
    assert(InvitationDao.getOne(invitationId).flatMap(_.validity) == Some(true))
    assert(InvitationDao.getOne(invitationId2).flatMap(_.validity) == Some(true))

    UserDao.logout(userLoginHistoryId2)
    assert(InvitationDao.getOne(invitationId).flatMap(_.validity) == Some(true))
    assert(InvitationDao.getOne(invitationId2).flatMap(_.validity) == Some(true))

    UserDao.logout(userLoginHistoryId)
    RoomDao.leaveRoom(userModel.id, resultRoomModel.flatMap(_.id), None)
    RoomDao.closeRoom(resultRoomModel.flatMap(_.id))

    InvitationDao.invalidAllInvitationWithRoom(resultRoomModel.flatMap(_.id))
    assert(InvitationDao.getOne(invitationId).flatMap(_.validity) == Some(false))
    assert(InvitationDao.getOne(invitationId2).flatMap(_.validity) == Some(false))
  }
}
