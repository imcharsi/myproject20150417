/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.testutil20150417

import scala.reflect.runtime.{ universe ⇒ ru }

/**
 * Created by Kang Woo, Lee on 4/19/15.
 */
trait TestUtilTrait {
  // type parameter 가 하나 늘었다. IdVersionModelTrait type 을 직접 참조하면 maven module 간에 의존성이 순환하게 된다.

  def testModelClassValidity1[T: ru.TypeTag, IdVersionModelTrait: ru.TypeTag]: Boolean = {
    //    http://stackoverflow.com/questions/19366337/scala-how-to-get-the-type-for-a-field-using-reflection-api
    val t = ru.typeOf[T]
    // 주어진 type 안에 있는 method 가 아닌 모든 것 중, type 이 Option[IdVersionTrait[_]] 의 sub type 인 것을 고른다.
    t.decls.filterNot(_.isMethod).filter(_.typeSignature <:< ru.typeOf[Option[IdVersionModelTrait]]).
      // 골라낸 것의 이름을 구하여, 뒤에 Id 를 붙이고 이름을 만든다.
      map(_.asTerm).map(_.name).map(_.toString.trim).map(x ⇒ f"${x}Id").map(ru.TermName(_)).
      // 만든 이름을 주어진 type 안에 있는 method 가 아닌 것 중에서 찾고, 찾아낸 것의 type 이 Option[Int] 인지 확인한다.
      map(t.decl(_)).filterNot(_.isMethod).map(_.typeSignature).forall(_ =:= ru.typeOf[Option[Int]])
    // 요약하면, foreign key 를 위한 field 가 정의되었는지 확인하는 것이다.
  }

  def testModelClassValidity2[T: ru.TypeTag, IdVersionModelTrait: ru.TypeTag]: Boolean = {
    // 모든 model 들은 IdVersionTrait 를 구현해야 한다.
    ru.typeOf[T] <:< ru.typeOf[IdVersionModelTrait]
  }

  def gatherNotAnnotatedConstructorParameter[T: ru.TypeTag, A: ru.TypeTag, IdVersionModelTrait: ru.TypeTag]: List[ru.Symbol] = {
    //    http://stackoverflow.com/questions/11468571/how-to-access-annotation-defined-on-case-class-field-at-runtime
    ru.symbolOf[T].asClass.primaryConstructor.typeSignature.paramLists.head.map(x ⇒ (x, x.annotations.map(_.tree.tpe))).
      // 인자에 annotation 이 붙지 않았으면서,
      filterNot(_._2.exists(_ =:= ru.typeOf[A])).map(_._1).
      // 인자의 type 이 Option[IdVersionTrait[_]] 의 sub type 이 아닌 인자를 구한다.
      filterNot(_.typeSignature <:< ru.typeOf[Option[IdVersionModelTrait]])
  }

  def gatherApplyParameter(x: Any): List[ru.Symbol] = {
    //    http://stackoverflow.com/questions/11062166/dynamic-method-invocation-with-new-scala-reflection-api
    val mirror = ru.runtimeMirror(x.getClass.getClassLoader)
    mirror.reflect(x).symbol.typeSignature.member(ru.TermName("apply")).asMethod.paramLists.head
  }

  def gatherUnapplyReturnType(x: Any): List[ru.Type] = {
    //    http://stackoverflow.com/questions/11062166/dynamic-method-invocation-with-new-scala-reflection-api
    val mirror = ru.runtimeMirror(x.getClass.getClassLoader)
    // 반환 type 의
    mirror.reflect(x).symbol.typeSignature.member(ru.TermName("unapply")).asMethod.returnType.
      // type parameter 인자를 구한다. unapply 의 반환형은 Option[...] 이거나 Option[(...)] 이다.
      typeArgs.flatMap(_.typeArgs)
  }

  def compareType(a: List[ru.Symbol], b: List[ru.Symbol]): Boolean = {
    a.map(x ⇒ (x.name.toString.trim, x.typeSignature)) == b.map(x ⇒ (x.name.toString.trim, x.typeSignature))
  }

  def testModelClassValidity3[T: ru.TypeTag, A: ru.TypeTag, IdVersionModelTrait: ru.TypeTag](companion: Any): Boolean = {
    val a = gatherNotAnnotatedConstructorParameter[T, A, IdVersionModelTrait]
    val b = gatherApplyParameter(companion)
    // companion object 의 apply 가 받는 인자와, model 의 생성자 인자 중 Option[IdVersionTrait[_]] 의 sub type 이 아닌 인자를 비교한다. 누락을 찾기 위해서 쓴다.
    compareType(a, b) &&
      // unapply 의 반환형이 apply 의 인자형과 같은지 비교한다.
      a.map(_.typeSignature) == gatherUnapplyReturnType(companion)
  }

  def testModelClassValidity4[T: ru.TypeTag]: Boolean = {
    // 인자없는 생성자를 썼는지 확인한다. jackson 이 json 변환을 할 때 필요하다.
    ru.typeOf[T].decls.filter(_.isMethod).filter(_.isConstructor).map(_.asMethod).flatMap(_.paramLists).exists(_.isEmpty)
  }

}
