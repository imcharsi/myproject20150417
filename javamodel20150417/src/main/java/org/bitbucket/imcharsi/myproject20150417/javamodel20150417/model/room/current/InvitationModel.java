/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.iface.IByWhoModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.iface.IIdVersionModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.iface.IUserRoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.current.UserModel;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Kang Woo, Lee on 5/5/15.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class InvitationModel implements Serializable, IIdVersionModel, IUserRoomModel, IByWhoModel {
    private Integer id;
    private Integer version;
    private Integer userId;
    private Integer roomId;
    private Integer byWhoId;
    private Date invitationDateTime;
    private Date expirationDateTime;
    private Boolean validity;
    private UserModel user;
    private RoomModel room;
    private UserModel byWho;
}
