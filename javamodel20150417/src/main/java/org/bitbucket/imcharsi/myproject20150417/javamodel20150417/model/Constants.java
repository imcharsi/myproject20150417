/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model;

import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.BanType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.EnterType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.PermissionType;

/**
 * Created by Kang Woo, Lee on 5/5/15.
 */
public class Constants {
    final public static Integer enter = 0;
    final public static Integer leave = 1;

    public static final Integer grantOwner = 0;
    public static final Integer revokeOwner = 1;
    public static final Integer grantOperator = 2;
    public static final Integer revokeOperator = 3;

    public static final Integer allow = 0;
    public static final Integer disallow = 1;

    public static final Integer userModelType = 1;
    public static final Integer userLoginHistoryModelType = 2;
    public static final Integer userNickHistoryModelType = 3;
    public static final Integer roomModelType = 4;
    public static final Integer invitationModelType = 5;
    public static final Integer operatorModelType = 6;
    public static final Integer roomAttenderModelType = 7;
    public static final Integer banModelType = 8;
    public static final Integer roomHistoryModelType = 9;
    public static final Integer banHistoryModelType = 10;
    public static final Integer permissionHistoryModelType = 11;
    public static final Integer roomInOutHistoryModelType = 12;
    public static final Integer roomTimelineModelType = 13;
    public static final Integer talkHistoryModelType = 14;
    public static final Integer sayHiModelType = 15;
    public static final Integer logoutModelType = 16;

    static private class EnterTypeConvert implements IEnumConvert<EnterType> {
        @Override
        public EnterType valueOf(Integer i) {
            if (i == Constants.enter) {
                return EnterType.Enter;
            } else if (i == Constants.leave) {
                return EnterType.Leave;
            } else {
                throw new RuntimeException();
            }
        }
    }

    static private class BanTypeConvert implements IEnumConvert<BanType> {
        @Override
        public BanType valueOf(Integer id) {
            if (id == Constants.allow) {
                return BanType.Allow;
            } else if (id == Constants.disallow) {
                return BanType.Disallow;
            } else {
                throw new RuntimeException();
            }
        }
    }

    static private class PermissionTypeConvert implements IEnumConvert<PermissionType> {
        @Override
        public PermissionType valueOf(Integer i) {
            if (i == Constants.grantOwner) {
                return PermissionType.GrantOwner;
            } else if (i == Constants.revokeOwner) {
                return PermissionType.RevokeOwner;
            } else if (i == Constants.grantOperator) {
                return PermissionType.GrantOperator;
            } else if (i == Constants.revokeOperator) {
                return PermissionType.RevokeOperator;
            } else {
                throw new RuntimeException();
            }
        }
    }

    static private class EnvelopeDataTypeConvert implements IEnumConvert<EnvelopeDataType> {
        @Override
        public EnvelopeDataType valueOf(Integer id) {
            if (id == Constants.userModelType) {
                return EnvelopeDataType.UserModelType;
            } else if (id == Constants.userLoginHistoryModelType) {
                return EnvelopeDataType.UserLoginHistoryModelType;
            } else if (id == Constants.userNickHistoryModelType) {
                return EnvelopeDataType.UserNickHistoryModelType;
            } else if (id == Constants.roomModelType) {
                return EnvelopeDataType.RoomModelType;
            } else if (id == Constants.invitationModelType) {
                return EnvelopeDataType.InvitationModelType;
            } else if (id == Constants.operatorModelType) {
                return EnvelopeDataType.OperatorModelType;
            } else if (id == Constants.roomAttenderModelType) {
                return EnvelopeDataType.RoomAttenderModelType;
            } else if (id == Constants.banModelType) {
                return EnvelopeDataType.BanModelType;
            } else if (id == Constants.roomHistoryModelType) {
                return EnvelopeDataType.RoomHistoryModelType;
            } else if (id == Constants.banHistoryModelType) {
                return EnvelopeDataType.BanHistoryModelType;
            } else if (id == Constants.permissionHistoryModelType) {
                return EnvelopeDataType.PermissionHistoryModelType;
            } else if (id == Constants.roomInOutHistoryModelType) {
                return EnvelopeDataType.RoomInOutHistoryModelType;
            } else if (id == Constants.roomTimelineModelType) {
                return EnvelopeDataType.RoomTimelineModelType;
            } else if (id == Constants.talkHistoryModelType) {
                return EnvelopeDataType.TalkHistoryModelType;
            } else if (id == Constants.sayHiModelType) {
                return EnvelopeDataType.SayHiModelType;
            } else if (id == Constants.logoutModelType) {
                return EnvelopeDataType.LogoutModelType;
            } else {
                throw new RuntimeException();
            }
        }
    }

    public static final IEnumConvert<EnterType> enterTypeConvert = new EnterTypeConvert();
    public static final IEnumConvert<BanType> banTypeConvert = new BanTypeConvert();
    public static final IEnumConvert<PermissionType> permissionTypeConvert = new PermissionTypeConvert();
    public static final IEnumConvert<EnvelopeDataType> envelopeDataTypeConvert = new EnvelopeDataTypeConvert();
}
