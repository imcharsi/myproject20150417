/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model;

/**
 * Created by Kang Woo, Lee on 5/5/15.
 */
public enum EnvelopeDataType implements IEnum<EnvelopeDataType> {
    UserModelType(Constants.userModelType), UserLoginHistoryModelType(Constants.userLoginHistoryModelType),
    UserNickHistoryModelType(Constants.userNickHistoryModelType),
    RoomModelType(Constants.roomModelType), InvitationModelType(Constants.invitationModelType),
    OperatorModelType(Constants.operatorModelType), RoomAttenderModelType(Constants.roomAttenderModelType),
    BanModelType(Constants.banModelType), RoomHistoryModelType(Constants.roomHistoryModelType), BanHistoryModelType(Constants.banHistoryModelType),
    PermissionHistoryModelType(Constants.permissionHistoryModelType), RoomInOutHistoryModelType(Constants.roomInOutHistoryModelType),
    RoomTimelineModelType(Constants.roomTimelineModelType), TalkHistoryModelType(Constants.talkHistoryModelType),
    SayHiModelType(Constants.sayHiModelType), LogoutModelType(Constants.logoutModelType);

    @Override
    public Integer getId() {
        return id;
    }

    private Integer id;

    EnvelopeDataType(Integer id) {
        this.id = id;
    }
}
