/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.iface.IIdVersionModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.iface.IRoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.talk.history.TalkHistoryModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.history.UserNickHistoryModel;

import java.io.Serializable;

/**
 * Created by Kang Woo; Lee on 5/5/15.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class RoomTimelineModel implements Serializable, IIdVersionModel, IRoomModel {
    private Integer id;
    private Integer version;
    private Integer roomId;
    private Integer banHistoryId;
    private Integer permissionHistoryId;
    private Integer roomHistoryId;
    private Integer roomInOutHistoryId;
    private Integer talkHistoryId;
    private Integer userNickHistoryId;
    private RoomModel room;
    private BanHistoryModel banHistory;
    private PermissionHistoryModel permissionHistory;
    private RoomHistoryModel roomHistory;
    private RoomInOutHistoryModel roomInOutHistory;
    private TalkHistoryModel talkHistory;
    private UserNickHistoryModel userNickHistory;
}
