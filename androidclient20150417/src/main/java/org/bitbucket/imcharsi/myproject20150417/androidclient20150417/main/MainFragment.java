/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.*;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.*;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.asynctask.AsyncHttpClientTask;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomListFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.settings.MainPreferenceFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.user.UserListFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.CertificationModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.current.UserModel;
import org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417.DefaultObjectMapper;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * Created by Kang Woo, Lee on 5/8/15.
 */
@Getter
@Setter
public class MainFragment extends Fragment implements TemporaryHolder {
    static final public String TAG = MainFragment.class.getSimpleName();
    public static final String COOKIE = "cookie";
    public static final String USER_ID = "userId";
    private Button buttonLogin;
    private Button buttonLogout;
    private Button buttonWebSocket;
    private Button buttonPreferences;
    private Button buttonAllLoginUsers;
    private Button buttonAllCreatedRooms;
    private Button buttonAllEtcMessages;
    private Button buttonInvitationList;
    private Button buttonRegisterUser;
    private String cookie;
    private Integer userId;
    private ObjectGraph objectGraph;
    private WebSocketService.InnerService innerService;
    private WebSocketServiceConnection webSocketServiceConnection;
    private TemporaryHolder.WebSocketServiceReceiver receiver;
    // test 를 쉽게 하기 위해 이와 같이 쓴다. test 목적이 아니면 이와 같이 참조변수를 둘 필요가 없다.
    private AlertDialog registerUserDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setObjectGraph(MainApplication.retrieveMainApplication(getActivity().getApplication()));
        setCookie(null);
        if (savedInstanceState != null) {
            prepareState(savedInstanceState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getCookie() != null) {
            outState.putString(COOKIE, getCookie());
        }
        if (getUserId() != null) {
            outState.putInt(USER_ID, getUserId());
        }
    }

    private void prepareState(final Bundle bundle) {
        if (bundle.containsKey(MainFragment.COOKIE)) {
            setCookie(bundle.getString(MainFragment.COOKIE));
        }
        if (bundle.containsKey(MainFragment.USER_ID)) {
            setUserId(bundle.getInt(MainFragment.USER_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.main_fragment_layout, container, false);
        setButtonLogin((Button) view.findViewById(R.id.buttonLogin));
        setButtonLogout((Button) view.findViewById(R.id.buttonLogout));
        setButtonWebSocket((Button) view.findViewById(R.id.buttonWebSocket));
        setButtonAllLoginUsers((Button) view.findViewById(R.id.buttonAllLoginUsers));
        setButtonAllCreatedRooms((Button) view.findViewById(R.id.buttonAllCreatedRooms));
        setButtonAllEtcMessages((Button) view.findViewById(R.id.buttonAllEtcMessages));
        setButtonInvitationList((Button) view.findViewById(R.id.buttonInvitationList));
        setButtonRegisterUser((Button) view.findViewById(R.id.buttonRegisterUser));
        setButtonPreferences((Button) view.findViewById(R.id.buttonPreferences));
        getButtonLogin().setOnClickListener(new LoginButtonListener());
        getButtonLogout().setOnClickListener(new LogoutButtonListener());
        getButtonWebSocket().setOnClickListener(new WebSocketButtonListener());
        getButtonAllLoginUsers().setOnClickListener(new AllLoginUsersButtonListener());
        getButtonAllCreatedRooms().setOnClickListener(new AllCreatedRoomsButtonListener());
        getButtonAllEtcMessages().setOnClickListener(new AllEtcMessagesButtonListener());
        getButtonInvitationList().setOnClickListener(new InvitationListButtonListener());
        getButtonRegisterUser().setOnClickListener(new RegisterUserButtonListener());
        getButtonPreferences().setOnClickListener(new PreferencesButtonListener());
        refreshButtonState();
        return view;
    }

    public void refreshButtonState() {
        final boolean isConnected = getInnerService() == null ? false : getInnerService().isConnected();
        getButtonLogin().setEnabled(getCookie() == null);
        getButtonLogout().setEnabled(getCookie() != null);
        getButtonWebSocket().setEnabled(getCookie() != null);
        getButtonAllLoginUsers().setEnabled(getCookie() != null);
        getButtonAllCreatedRooms().setEnabled(getCookie() != null);
        getButtonInvitationList().setEnabled(getCookie() != null);
        getButtonAllEtcMessages().setEnabled(getCookie() != null);
        getButtonLogin().setText(R.string.textButtonLogin);
        getButtonLogout().setText(R.string.textButtonLogout);
        if (isConnected == false) {
            getButtonWebSocket().setText(R.string.textButtonConnectWebSocket);
        } else {
            getButtonWebSocket().setText(R.string.textButtonDisconnectWebSocket);
        }
    }

    private void convertWaitState(final Button button) {
        button.setText(R.string.textWait);
        button.setEnabled(false);
    }

    private class LoginButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            convertWaitState(getButtonLogin());
            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            final String accountName = preferences.getString(getActivity().getResources().getString(R.string.keyAccountName), null);
            final String accountPassword = preferences.getString(getActivity().getResources().getString(R.string.keyAccountPassword), null);
            getObjectGraph().get(LoginAsyncTask.class).
                    // 직접 setter 를 사용하여 필요한 인자를 채울수도 있었지만, 이렇게 하는 이유는 누락을 막기 위해서이다.
                            init(MainFragment.this, new CertificationModel(accountName, accountPassword, true)).
                    executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class LogoutButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            convertWaitState(getButtonLogout());
            getObjectGraph().get(LogoutAsyncTask.class).init(MainFragment.this).
                    executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class WebSocketButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            convertWaitState(getButtonWebSocket());
            new InnerAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        private class InnerAsyncTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    if (MainFragment.this.getInnerService().isConnected() == false) {
                        MainFragment.this.getInnerService().connect(getCookie());
                    } else {
                        MainFragment.this.getInnerService().disconnect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                refreshButtonState();
            }
        }
    }

    abstract private class AbstractButtonListener implements View.OnClickListener {
        abstract protected String getTag();

        abstract protected Fragment getFragment();

        @Override
        public void onClick(View v) {
            final FragmentTransaction transaction = getFragmentManager().beginTransaction();
            final Bundle bundle = new Bundle();
            if (getCookie() != null) {
                bundle.putString(COOKIE, getCookie());
            }
            if (getUserId() != null) {
                bundle.putInt(USER_ID, getUserId());
            }
            final Fragment fragment = getFragment();
            fragment.setArguments(bundle);
            transaction.replace(R.id.main_activity_layout, fragment, getTag());
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    private class InvitationListButtonListener extends AbstractButtonListener {
        @Override
        protected String getTag() {
            return InvitationListFragment.TAG;
        }

        @Override
        protected Fragment getFragment() {
            return getObjectGraph().get(InvitationListFragment.class);
        }
    }

    private class AllLoginUsersButtonListener extends AbstractButtonListener {
        @Override
        protected String getTag() {
            return UserListFragment.TAG;
        }

        @Override
        protected Fragment getFragment() {
            return getObjectGraph().get(UserListFragment.class);
        }
    }

    private class AllCreatedRoomsButtonListener extends AbstractButtonListener {
        @Override
        protected String getTag() {
            return RoomListFragment.TAG;
        }

        @Override
        protected Fragment getFragment() {
            return getObjectGraph().get(RoomListFragment.class);
        }
    }

    private class AllEtcMessagesButtonListener extends AbstractButtonListener {
        @Override
        protected String getTag() {
            return AllEtcMessagesListFragment.TAG;
        }

        @Override
        protected Fragment getFragment() {
            return getObjectGraph().get(AllEtcMessagesListFragment.class);
        }
    }

    private class RegisterUserButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.register_user_dialog_layout, null);
            final AlertDialog dialog = new AlertDialog.Builder(getActivity()).
                    setView(dialogView).
                    setCancelable(true).
                    setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final EditText editTextUserName = (EditText) dialogView.findViewById(R.id.editTextUserName);
                            final EditText editTextUserPassword = (EditText) dialogView.findViewById(R.id.editTextUserPassword);
                            final String password1 = editTextUserPassword.getText().toString().trim();
                            final String name1 = editTextUserName.getText().toString().trim();
                            String password2 = password1;
                            if (password1.isEmpty()) {
                                password2 = null;
                            }
                            String name2 = name1;
                            if (name2.isEmpty()) {
                                name2 = null;
                            }
                            getObjectGraph().get(RegisterUserAsyncTask.class).
                                    init(MainFragment.this, name2, password2).
                                    executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    setRegisterUserDialog(null);
                }
            }).create();
            setRegisterUserDialog(dialog);
            dialog.show();
        }
    }

    @Getter
    @Setter
    public static class LoginAsyncTask extends AsyncHttpClientTask<Void, Integer, String> {
        // 이 field 도 dagger 의존성 주입으로 채워볼려고 했는데, 잘 안된다.
        // 할려고 했던 것은, master module 에 선언되어 있는 것 외에, 추가로 여기서 ObjectGraph.plus 기능을 사용하여
        // 이 class 의 instance 를 만들기 위해 필요한 main fragment instance 는
        // 현재 이 main fragment 의 instance 가 되도록 할려고 했는데, 잘 안된다.
        // 검사를 편하게 할려고 의존성 주입을 사용했는데, 위의 문제를 해결하기 위해 지역적으로 의존성 module 을 나누게 되면,
        // 검사에서 의존성 주입기능을 편하게 쓸 수 없게 된다.
        private MainFragment mainFragment;
        private CertificationModel certificationModel;

        @Inject
        public LoginAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        // http://stackoverflow.com/questions/16040125/using-dagger-for-dependency-injection-on-constructors
        // dagger 에서는 의존성을 할당받는 시점에서, 생성자에게 전달할 인자를 정할 수 없다.
        // objectGraph.get(....class, param1, param2, ...); 와 같이 쓸 수는 없다.
        // 대안으로, 아래와 같은 method 를 명시적으로 쓴다고 약속한다. 명시적인 절차를 두지 않으면 필요한 절차를 잊을 수 있다.
        public LoginAsyncTask init(MainFragment mainFragment, CertificationModel certificationModel) {
            setMainFragment(mainFragment);
            setCertificationModel(certificationModel);
            // cookie 를 설정할 필요 없다. login 을 하는 단계이다. cookie 를 구해야 한다.
            return this;
        }

        @Override
        protected String doInBackground(Void... voids) {
            // login 을 하기에 앞서서 web socket 을 끊지 않고, logout 을 하고 난뒤에 web socket 을 끊기로 했다.
            try {
                final Response response = login(getCertificationModel());
                if (response.getStatusCode() == 200) {
                    // 이와 같이 초기화를 하면 되나. 접속에 성공했다면 앞의 web socket 은 끊긴다. 자동으로 끊어지게 놔두는 것이 좋겠다.
                    // fixme 가끔씩 web socket 연결 종료가 표시가 안되는 문제가 있다. 실제로 server 가 web socket 을 끊지 않은 것인지, client 가 미쳐 인식을 못한 것인지 알수가 없다.
                    getMainFragment().getInnerService().init();
                    getMainFragment().getInnerService().initLogoutChecked();
                    return retrieveCookie(response.getHeaders("Set-Cookie"));
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        private String retrieveCookie(final List<String> stringList) {
            final Set<String> set = new HashSet<String>();
            for (String header : stringList) {
                for (String string : header.split(";")) {
                    set.add(string);
                }
            }
            final StringBuffer stringBuffer = new StringBuffer();
            for (String string : set) {
                stringBuffer.append(string).append(";");
            }
            return stringBuffer.toString();
        }

        private Response login(CertificationModel certificationModel) throws IOException, ExecutionException, InterruptedException {
            final String serverUrl = ((MainApplication) getMainFragment().getActivity().getApplication()).getServerUrl();
            final String certificationModelJson = DefaultObjectMapper.objectMapper.writeValueAsString(certificationModel);
            final Response response = getAsyncHttpClient().preparePost(serverUrl + "/login").
                    setBody(certificationModelJson).
                    addHeader("Content-Type", MainApplication.ContentType).
                    execute().get();
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            getMainFragment().setCookie(s);
            getMainFragment().refreshButtonState();
            // 자동으로 연결되도록 하기. 실패하는 경우 재시도는 하지 않는다.
            getMainFragment().getButtonWebSocket().performClick();
            getMainFragment().getObjectGraph().get(SelfAsyncTask.class).init(getMainFragment()).
                    executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Getter
    @Setter
    public static class SelfAsyncTask extends AsyncHttpClientTask<Void, Void, UserModel> {
        private MainFragment mainFragment;

        @Inject
        public SelfAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public SelfAsyncTask init(MainFragment mainFragment) {
            setMainFragment(mainFragment);
            setCookie(getMainFragment().getCookie());
            return this;
        }

        @Override
        protected UserModel doInBackground(Void... params) {
            // userId 를 구한다.
            try {
                final String serverUrl = ((MainApplication) getMainFragment().getActivity().getApplication()).getServerUrl();
                final Response response = getAsyncHttpClient().prepareGet(serverUrl + "/user/self").
                        setHeader("Cookie", getMainFragment().getCookie()).
                        addHeader("Accept", MainApplication.ContentType).
                        execute().get();
                return DefaultObjectMapper.objectMapper.readValue(response.getResponseBody(), UserModel.class);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(UserModel userModel) {
            super.onPostExecute(userModel);
            if (userModel != null) {
                getMainFragment().setUserId(userModel.getId());
            }
        }
    }

    @Getter
    @Setter
    public static class LogoutAsyncTask extends AsyncHttpClientTask<Void, Object, Integer> {
        private MainFragment mainFragment;

        @Inject
        public LogoutAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public LogoutAsyncTask init(MainFragment mainFragment) {
            setMainFragment(mainFragment);
            setCookie(getMainFragment().getCookie());
            return this;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            try {
                return logout().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        private Response logout() throws IOException, ExecutionException, InterruptedException {
            final String serverUrl = ((MainApplication) getMainFragment().getActivity().getApplication()).getServerUrl();
            final Response response = getAsyncHttpClient().prepareDelete(serverUrl + "/login").
                    addHeader("Cookie", getCookie()).
                    execute().get();
            return response;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if (integer != null && integer == 200) {
                getMainFragment().setCookie(null);
                getMainFragment().getInnerService().init();
                try {
                    getMainFragment().getInnerService().disconnect(); // 자동으로 안끊길때도 있다. 왜 그런가.
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            getMainFragment().refreshButtonState();
        }
    }

    @Getter
    @Setter
    public static class RegisterUserAsyncTask extends AsyncHttpClientTask<Void, Void, Integer> {
        private MainFragment mainFragment;
        private String userName;
        private String userPassword;

        @Inject
        public RegisterUserAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public RegisterUserAsyncTask init(MainFragment mainFragment, String userName, String userPassword) {
            setMainFragment(mainFragment);
            setUserName(userName);
            setUserPassword(userPassword);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            try {
                return registerUser().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        private Response registerUser() throws IOException, ExecutionException, InterruptedException {
            final String serverUrl = ((MainApplication) getMainFragment().getActivity().getApplication()).getServerUrl();
            final UserModel userModel = new UserModel();
            userModel.setName(userName);
            userModel.setPassword(userPassword);
            final String userModelJson = DefaultObjectMapper.objectMapper.writeValueAsString(userModel);
            final Response response = getAsyncHttpClient().preparePost(serverUrl + "/user").
                    setBody(userModelJson).
                    addHeader("Content-Type", MainApplication.ContentType).
                    execute().get();
            return response;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if (integer != null && integer == 200) {
                Toast.makeText(getMainFragment().getActivity(), R.string.textSuccess, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getMainFragment().getActivity(), R.string.textFail, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static class WebSocketServiceConnection extends AbstractWebSocketServiceConnection<MainFragment> {
        public WebSocketServiceConnection init(MainFragment mainFragment) {
            setFragment(mainFragment);
            return this;
        }

        @Override
        protected TemporaryHolder.WebSocketServiceReceiver getReceiver() {
            return getFragment().getObjectGraph().
                    get(WebSocketServiceReceiver.class).
                    init(getFragment().getInnerService(), getFragment().getActivity(), getFragment());
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            super.onServiceConnected(name, service);
            if (getFragment().getInnerService().isLogoutChecked()) {
                getFragment().setCookie(null);
                try {
                    getFragment().getInnerService().disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            getFragment().refreshButtonState();
        }
    }

    @Getter
    @Setter
    public static class WebSocketServiceReceiver extends TemporaryHolder.WebSocketServiceReceiver<MainFragment> {
    }

    @Override
    public void onStart() {
        super.onStart();
        setWebSocketServiceConnection(getObjectGraph().get(WebSocketServiceConnection.class).init(this));
        getActivity().bindService(
                new Intent(getActivity(), WebSocketService.class),
                getWebSocketServiceConnection(),
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(getReceiver());
        getActivity().unbindService(getWebSocketServiceConnection());
    }

    private class PreferencesButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            final FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.main_activity_layout, new MainPreferenceFragment(), MainPreferenceFragment.TAG);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
