/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.user;

import android.app.ListFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Response;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.MainApplication;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.TemporaryHolder;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.WebSocketService;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.asynctask.AsyncHttpClientTask;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MainFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.current.UserModel;
import org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417.DefaultObjectMapper;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Kang Woo, Lee on 5/10/15.
 */
@Getter
@Setter
public class UserListFragment extends ListFragment implements TemporaryHolder {
    static public final String TAG = UserListFragment.class.getSimpleName();
    private UserModelAdapter adapter;
    private String cookie;
    private ObjectGraph objectGraph;
    private WebSocketService.InnerService innerService;
    private WebSocketServiceConnection serviceConnection;
    private TemporaryHolder.WebSocketServiceReceiver receiver;

    public static class WebSocketServiceConnection extends TemporaryHolder.AbstractWebSocketServiceConnection<UserListFragment> {
        public WebSocketServiceConnection init(UserListFragment userListFragment) {
            setFragment(userListFragment);
            return this;
        }

        @Override
        protected TemporaryHolder.WebSocketServiceReceiver getReceiver() {
            return getFragment().getObjectGraph().
                    get(WebSocketServiceReceiver.class).
                    init(getFragment().getInnerService(), getFragment().getActivity(), getFragment());
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            super.onServiceConnected(name, service);
            getFragment().setInnerService((((WebSocketService.SampleBinder) service).getService()));
            if (getFragment().getInnerService().isLogoutChecked()) {
                getFragment().getFragmentManager().popBackStack();
            }
        }
    }

    public static class WebSocketServiceReceiver extends TemporaryHolder.WebSocketServiceReceiver<UserListFragment> {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAdapter(new UserModelAdapter());
        final Bundle bundle = getArguments();
        if (bundle != null) {
            prepareCookie(bundle);
        } else if (savedInstanceState != null) {
            prepareCookie(savedInstanceState);
        }
        setObjectGraph(((MainApplication) getActivity().getApplication()).getObjectGraph());
    }

    private void prepareCookie(final Bundle bundle) {
        if (bundle.containsKey(MainFragment.COOKIE)) {
            setCookie(bundle.getString(MainFragment.COOKIE));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getCookie() != null) {
            outState.putString(MainFragment.COOKIE, getCookie());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setServiceConnection(getObjectGraph().get(WebSocketServiceConnection.class).init(this));
        getActivity().bindService(new Intent(getActivity(), WebSocketService.class), getServiceConnection(), Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(getReceiver());
        getActivity().unbindService(getServiceConnection());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = super.onCreateView(inflater, container, savedInstanceState);
        setListAdapter(getAdapter());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getObjectGraph().get(UserListFragmentAsyncTask.class).init(this).
                executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class UserModelAdapter extends ArrayAdapter<UserModel> {
        public UserModelAdapter() {
            super(UserListFragment.this.getActivity(), android.R.layout.simple_list_item_1);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);
            final TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText(getItem(position).getName());
            return view;
        }
    }

    @Getter
    @Setter
    public static class UserListFragmentAsyncTask extends AsyncHttpClientTask<Void, Void, List<UserModel>> {
        private UserListFragment userListFragment;

        @Inject
        public UserListFragmentAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public UserListFragmentAsyncTask init(UserListFragment userListFragment) {
            setUserListFragment(userListFragment);
            return this;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getUserListFragment().setListShown(false);
        }

        @Override
        protected List<UserModel> doInBackground(Void... params) {
            try {
                final String serverUrl = ((MainApplication) getUserListFragment().getActivity().getApplication()).getServerUrl();
                final ListenableFuture<Response> execute = getAsyncHttpClient().prepareGet(serverUrl + "/user/allLoginUsers").
                        addHeader("Accept", MainApplication.ContentType).
                        addHeader("Cookie", getUserListFragment().getCookie()).
                        execute();
                final Response response = execute.get();
                return DefaultObjectMapper.objectMapper.readValue(response.getResponseBody(),
                        DefaultObjectMapper.objectMapper.getTypeFactory().constructCollectionType(List.class, UserModel.class));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<UserModel> userModels) {
            super.onPostExecute(userModels);
            getUserListFragment().getAdapter().clear();
            getUserListFragment().getAdapter().addAll(userModels);
            getUserListFragment().setListShown(true);
        }
    }
}
