/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.providers.netty.NettyAsyncHttpProvider;
import com.ning.http.client.websocket.DefaultWebSocketListener;
import com.ning.http.client.websocket.WebSocket;
import com.ning.http.client.websocket.WebSocketUpgradeHandler;
import dagger.ObjectGraph;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeDataType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.RoomTimelineModel;
import org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417.DefaultObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;

/**
 * Created by Kang Woo, Lee on 5/12/15.
 */
@Getter
@Setter
public class WebSocketService extends Service {
    // 왜 service 를 쓰는가. service 는 service 를 시작시킨 component 와는 완전히 별개의 lifecycle 을 거치게 되고, 따라서 화면 회전과 같은 것에 영향을 받지 않는다. 그래서 service 를 쓴다.
    // 한편, 이 app 은 service 를 사용하는 주체가 매 순간 하나이기 때문에, 외부로부터의 경쟁은 없지만, 내부의 경쟁이 있다.
    // web socket thread 와 service code 를 부르는 thread 사이에서 여러 자원들을 두고 두고 경쟁할 수 있는 것이다. 따라서 semaphore 를 써야 한다.
    // 이 방법은 단순하게 하는 방법이지 좋은 방법은 아니다. android 에서 actor 를 쓸 수 있다면 정말 좋겠는데, 쓸 수 있는 것이 없다. 알려져 있는 것은 전부 jdk 7 을 요구하거나 scala 가 필요하다.
    private final Semaphore semaphore = new Semaphore(1, true);
    private final InnerService innerService = new InnerService();
    final private IBinder binder = new SampleBinder();
    private ObjectGraph objectGraph;
    private WebSocket webSocket;
    // envelope model id 는 매 사용자 접속마다 고유하다. 한번 사용자가 접속하면 접속을 종료할 때까지는 유일한 id 가 사용된다. message 자체는 모아서 다뤄도 되겠다.
    private Map<Integer, EnvelopeModel<?>> messageMap;
    // 방별로 각 message 의 시간순서를 유지해야 한다. 기타 message 는 key 를 -1 로 하자.
    private Map<Integer, List<Integer>> messageQueueByRoom;
    // 추가로 그려야 할 message 만을 구분하기 위해 쓴다. broadcast 를 받은 receiver 가 목록을 구해가면 없어진다.
    private Map<Integer, List<QueueEntity>> partialUpdateMessageQueueByRoom;
    private String cookie;
    private boolean logoutChecked;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class QueueEntity implements Serializable {
        boolean redrawFlag;
        List<Integer> messageIdList;
    }

    // web socket 구현은, 별개의 thread 에서 굴러가도록 만들어져 있기 때문에, 화면을 회전시킨다고 해서 web socket thread 가 종료하지는 않는다.
    // 그런데, web socket 을 사용자 component 에서 만들게 되면 화면 회전 등과 같은 경우 참조를 잃어버리게 되는 것이 문제다. 그래서 service 를 쓴다.
    // 시점을 알리기 위해 broadcast 가 필요하다. web socket 이 외부에 의해 끊겼는지, message 가 왔는지 두 개만 알릴 수 있으면 된다.

    @Override
    public void onCreate() {
        super.onCreate();
        setObjectGraph(((MainApplication) getApplicationContext()).getObjectGraph());
        setWebSocket(null);
        setMessageMap(new HashMap<Integer, EnvelopeModel<?>>());
        setMessageQueueByRoom(new HashMap<Integer, List<Integer>>());
        setPartialUpdateMessageQueueByRoom(new HashMap<Integer, List<QueueEntity>>());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.getInnerService().disconnect();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    // 아래의 모든 method 는 blocking 방식을 의도했다.
    // 이 class 도 의존성 주입으로 할것인지는 일단 해보고 판단하자.
    public class InnerService {
        public boolean connect(String cookie) throws InterruptedException, ExecutionException, IOException {
            try {
                getSemaphore().acquire();
                // 이미 연결되었으면 실패.
                if (getWebSocket() != null) {
                    return false;
                }
                // todo ssl 을 사용하는 경우에 대한 준비가 안되어 있다. ssl 을 사용할 때 살펴보기로 하자.
                final String serverUrl = ((MainApplication) getApplication()).getServerUrl();
                // 매번 새로운 http client 를 쓰기로 하자.
                // 동일한 http client 를 사용해서 두번을 연결했다가 끊었다 반복해보니 잘 안된다.
                AsyncHttpClientConfig config = new AsyncHttpClientConfig.Builder().build();
                final AsyncHttpClient client = new AsyncHttpClient(new NettyAsyncHttpProvider(config), config);
                final WebSocket webSocket = client.prepareGet(serverUrl.replace("http://", "ws://") + "/user/self/ws").
                        addHeader("Cookie", cookie).
                        execute(new WebSocketUpgradeHandler.Builder().
                                addWebSocketListener(
                                        getObjectGraph().
                                                get(WebSocketListener.class).
                                                init(client, WebSocketService.this, getApplication())
                                ).build()).get();
                webSocket.sendTextMessage("hi");
                setWebSocket(webSocket);
                setCookie(cookie);
            } finally {
                getSemaphore().release();
            }
            return true;
        }

        public boolean disconnect() {
            WebSocket webSocket = null;
            boolean result = false;
            try {
                getSemaphore().acquire();
                if (getWebSocket() != null) {
                    result = true;
                    webSocket = getWebSocket();
                    setWebSocket(null);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }

            // web socket listener 에 등록된 내용에 따라 종료를 성공하면 broadcast 가 시작되는데, onClose 에서도 semaphore 를 사용했다.
            // 여기서 semaphore 를 묶어놓고 onClose listener 로 들어가서 또 semaphore 를 묶으려고 시도하면 교착상태가 된다.
            if (webSocket != null) {
                webSocket.close();
            }
            return result;
        }

        public void init() {
            try {
                getSemaphore().acquire();
                getMessageMap().clear();
                getMessageQueueByRoom().clear();
                getPartialUpdateMessageQueueByRoom().clear();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
        }

        public void clearRoom(Integer roomId) {
            try {
                getSemaphore().acquire();
                final List<Integer> list = getMessageQueueByRoom().get(roomId);
                if (list != null) {
                    for (Integer id : list) {
                        getMessageMap().remove(id);
                    }
                    getMessageQueueByRoom().remove(roomId);
                    getPartialUpdateMessageQueueByRoom().remove(roomId);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
        }

        public boolean isIdListEmpty(Integer roomId) {
            boolean result = false;
            try {
                getSemaphore().acquire();
                result = !getPartialUpdateMessageQueueByRoom().containsKey(roomId) || getPartialUpdateMessageQueueByRoom().get(roomId).isEmpty();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
            return result;
        }

        public List<QueueEntity> getIdList(Integer roomId) {
            List<QueueEntity> result = null;
            try {
                getSemaphore().acquire();
                if (getPartialUpdateMessageQueueByRoom().containsKey(roomId)) {
                    result = getPartialUpdateMessageQueueByRoom().get(roomId);
                    getPartialUpdateMessageQueueByRoom().remove(roomId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
            return result;
        }

        public List<Integer> getAllIdList(Integer roomId) {
            List<Integer> result = null;
            try {
                getSemaphore().acquire();
                final List<Integer> list = getMessageQueueByRoom().get(-1);
                if (list != null) {
                    // reflection 을 사용해서 내부 자료를 변경하려는 시도까지 생각할 필요가 있나.
                    result = Collections.unmodifiableList(list);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
            return result;
        }

        public Set<Integer> getKeyList() {
            Set<Integer> keySet = new TreeSet<Integer>();
            try {
                getSemaphore().acquire();
                keySet.addAll(getPartialUpdateMessageQueueByRoom().keySet());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
            return keySet;
        }

        public EnvelopeModel<?> getMessage(Integer id) {
            EnvelopeModel<?> envelopeModel = null;
            try {
                getSemaphore().acquire();
                envelopeModel = getMessageMap().get(id);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
            return envelopeModel;
        }

        public boolean isConnected() {
            boolean result = false;
            try {
                getSemaphore().acquire();
                if (getWebSocket() != null) {
                    result = getWebSocket().isOpen();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
            return result;
        }

        public boolean isLogoutChecked() {
            boolean result = false;
            try {
                getSemaphore().acquire();
                result = WebSocketService.this.isLogoutChecked();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
            return result;
        }

        public void initLogoutChecked() {
            boolean result = false;
            try {
                getSemaphore().acquire();
                setLogoutChecked(false);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
        }

        public void message() throws InterruptedException {
            try {
                getSemaphore().acquire();
                getWebSocket().sendTextMessage("hi there");
            } finally {
                getSemaphore().release();
            }
        }

        // receiver 를 등록하는 시점에서 message 가 남아있었다면 receiver 를 등록하고 난 직후부터 바로 message 를 받아야 하는데,
        // 등록을 마친 시점에 맞춰서 어떻게 broadcast 를 할 것인가. 그래서 client 가 receiver 를 등록할 때에 한해서 명시적으로 broadcast 를 해줄것을 요청한다.
        // 그런데 이와 같이 쓰면, thread 기준으로 보면 요청이 아니라 자기가 자기한테 broadcast 를 하는 것처럼 된다.
        // 여기서 web socket 이 현재 끊어진 상태에 있다면 끊겼음을 알리는 broadcast 와, message 가 남아있다면 message 를 가져가라고 알리는 broadcast 두개를 보낸다.
        // 명시적으로 broadcast 해줄 것을 요청할 때는, 아직 읽지 않은 message id 목록을 새로 쓴다. 화면을 다시 그리기 위해 쓴다.
        public void retryBroadcast(Integer roomId) {
            try {
                getSemaphore().acquire();
                if (getWebSocket() == null) {
                    final Intent intent = new Intent("close web socket");
                    getApplication().sendBroadcast(intent);
                }
                getPartialUpdateMessageQueueByRoom().remove(roomId);
                getPartialUpdateMessageQueueByRoom().put(roomId, new LinkedList<QueueEntity>());
                if (roomId != null && getMessageQueueByRoom().containsKey(roomId)) {
                    final List<Integer> newList = new LinkedList<Integer>();
                    for (Integer integer : getMessageQueueByRoom().get(roomId)) {
                        newList.add(integer);
                    }
                    getPartialUpdateMessageQueueByRoom().get(roomId).add(new QueueEntity(true, newList));
                } else {
                    // 새로 그리기를 요청했는데, 앞에서 clearRoom 을 했다면, 화면을 다 지워야 한다.
                    // 그런데 queue 에 아무것도 없으면 아무것도 하지 않기 때문에 이와 같이 아무것도 없는 것을 queue 에 넣어준다.
                    getPartialUpdateMessageQueueByRoom().get(roomId).add(new QueueEntity(true, Collections.EMPTY_LIST));
                }
                final Intent intent = new Intent("server message");
                getApplication().sendBroadcast(intent);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
        }

        // 기타 message 가 도착했음을 수시로 알려야 할 때 쓴다.
        // partial queue 를 조작하지 않으므로, receiver 는 쌓여있는 partial queue 만 본다.
        // 위의 retryBroadcast 를 쓰면, 매번 partial queue 가 새로 채워지므로, 사용자가 보기에는 매번 새로운 기타 message 가 도착한 것처럼 보이게 된다.
        public void retryJustBroadcast() {
            try {
                getSemaphore().acquire();
                if (getWebSocket() == null) {
                    final Intent intent = new Intent("close web socket");
                    getApplication().sendBroadcast(intent);
                }
                final Intent intent = new Intent("server message");
                getApplication().sendBroadcast(intent);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                getSemaphore().release();
            }
        }
    }

    public class SampleBinder extends Binder {
        public InnerService getService() {
            return WebSocketService.this.getInnerService();
        }
    }

    @Getter
    public static class WebSocketListener extends DefaultWebSocketListener {
        private WebSocketService webSocketService;
        private Context context;
        private AsyncHttpClient client;

        public WebSocketListener init(AsyncHttpClient client, WebSocketService webSocketService, Context context) {
            this.webSocketService = webSocketService;
            this.context = context;
            this.client = client;
            return this;
        }

        @Override
        public void onMessage(String message) {
            Log.i(getClass().getSimpleName(), message);
            final List<EnvelopeModel<?>> envelopeModelList;
            // parcelable 을 구현한 class 의 instance 만 broadcast 로 보낼 수 있다. model 에 android 의존성을 두고 싶지 않다.
            // 그래서 client 보고 가져가라고 알리는 방식으로 했다.
            // 일단, json 을 변환하고 응답까지 완료하면 그때부터 semaphore 를 써서 내부 작업을 하자.
            if (message == null || message.trim().isEmpty()) {
                return;
            }
            try {
                envelopeModelList = DefaultObjectMapper.deserializeEnvelopeList(message);
                final List<Integer> idList = new LinkedList<Integer>();
                for (EnvelopeModel<?> envelopeModel : envelopeModelList) {
                    // 필요없는 자료는 골라낸다. 예를 들어, web socket 접속을 한 뒤 보내는 client 가 보낸 인사말에 대한 server 응답은 제외한다.
                    // 이외에도 제외할 것이 여럿 있을 수 있는데, 일단 이것만 쓴다.
                    if (envelopeModel.getDataType() != EnvelopeDataType.SayHiModelType) {
                        idList.add(envelopeModel.getId());
                        if (envelopeModel.getDataType() == EnvelopeDataType.LogoutModelType) {
                            getWebSocketService().setLogoutChecked(true);
                        }
                    }
                }

                // 골라내고, 처리할 남은 것이 없으면, 여기서 끝낸다. 앞서 열어뒀던 async http client 는 finally 에서 닫힌다.
                if (idList.isEmpty()) {
                    return;
                }

                final String serverUrl = ((MainApplication) getWebSocketService().getApplication()).getServerUrl();
                // 여기서 완료응답을 하지 않으면, 읽지 않은 것들이 계속 오게 된다. 예를 들면, 여러개의 대화방에 참여중일 때,
                // 현재 보고 있지 않은 나머지 대화방의 message 들을 완료하지 않는다면 계속 쌓이게 된다.
                // 각 대화방 fragment 들이 각자 알아서 완료응답을 하도록 하기에는 너무 복잡하다. 단순하게 하자.
                // 여기서 완료응답을 한다.
                getWebSocketService().getObjectGraph().get(AsyncHttpClient.class).
                        preparePut(serverUrl + "/user/self/messageQueue/complete").
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(idList)).
                        setHeader("Cookie", this.getWebSocketService().getCookie()).
                        setHeader("Content-Type", MainApplication.ContentType).
                        execute().get();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            try {
                this.getWebSocketService().getSemaphore().acquire();
                // 중복을 각 대화방/기타 message 별로 분리한다.
                final Map<Integer, Boolean> conflictMap = new HashMap<Integer, Boolean>();
                // 새로 추가된 message 의 id 를 각 대화방/기타 message 별로 분리한다.
                final Map<Integer, List<Integer>> newIdListMap = new HashMap<Integer, List<Integer>>();
                final Map<Integer, List<Integer>> messageQueueByRoom = this.getWebSocketService().getMessageQueueByRoom();
                final Map<Integer, EnvelopeModel<?>> messageMap = this.getWebSocketService().getMessageMap();
                final Map<Integer, List<QueueEntity>> partialUpdateMessageQueueByRoom = this.getWebSocketService().getPartialUpdateMessageQueueByRoom();
                for (EnvelopeModel<?> envelopeModel : envelopeModelList) {
                    final Integer roomId;
                    if (envelopeModel.getDataType() == EnvelopeDataType.RoomTimelineModelType) {
                        final EnvelopeModel<RoomTimelineModel> newEnvelopeModel = (EnvelopeModel<RoomTimelineModel>) envelopeModel;
                        final RoomTimelineModel roomTimelineModel = newEnvelopeModel.getData();
                        roomId = roomTimelineModel.getRoomId();
                    } else {
                        roomId = -1;
                    }
                    // 전체 message map 에 이 message 가 이미 있다면
                    if (messageMap.containsKey(envelopeModel.getId())) {
                        // 중복이 있었던 것으로 본다.
                        conflictMap.put(roomId, true);
                    } else {
                        // 대화방 별 message id 목록에 이 message id 를 추가한다.
                        prepareMapEntry(messageQueueByRoom, roomId).add(envelopeModel.getId());
                        // 새로 추가된 message id 만을 구분할 list 에 id 를 넣는다.
                        prepareMapEntry(newIdListMap, roomId).add(envelopeModel.getId());
                        messageMap.put(envelopeModel.getId(), envelopeModel);
                    }
                }
                for (Map.Entry<Integer, List<Integer>> entry : newIdListMap.entrySet()) {
                    // 중복이 있었는지 여부를 확인한다.
                    if (conflictMap.containsKey(entry.getKey()) && conflictMap.get(entry.getKey())) {
                        // 중복이 있었다면
                        // 화면 갱신을 위해 참고해야 할 id 목록을 새로 쓴다.
                        partialUpdateMessageQueueByRoom.remove(entry.getKey());
                        partialUpdateMessageQueueByRoom.put(entry.getKey(), new LinkedList<QueueEntity>());
                        final List<Integer> newList = new LinkedList<Integer>();
                        for (Integer id : messageQueueByRoom.get(entry.getKey())) {
                            newList.add(id);
                        }
                        partialUpdateMessageQueueByRoom.get(entry.getKey()).add(new QueueEntity(true, newList));
                    } else {
                        // 중복이 없었다면
                        // 화면 갱신을 위해 참고해야 할 id 목록에, 이번에 새로 받은 message 의 id 들을 더한다.
                        if (!partialUpdateMessageQueueByRoom.containsKey(entry.getKey())) {
                            partialUpdateMessageQueueByRoom.put(entry.getKey(), new LinkedList<QueueEntity>());
                        }
                        partialUpdateMessageQueueByRoom.get(entry.getKey()).add(new QueueEntity(false, newIdListMap.get(entry.getKey())));
                    }
                }
                final Intent intent = new Intent("server message"); // fixme 상수를 정하기.
                getContext().sendBroadcast(intent);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                this.getWebSocketService().getSemaphore().release();
            }
        }

        private List<Integer> prepareMapEntry(Map<Integer, List<Integer>> map, Integer id) {
            if (!map.containsKey(id)) {
                map.put(id, new LinkedList<Integer>());
            }
            return map.get(id);
        }

        @Override
        public void onClose(WebSocket websocket) {
            try {
                this.getWebSocketService().getSemaphore().acquire();
                final Intent intent = new Intent("close web socket"); // fixme 상수를 정하기.
                this.getWebSocketService().setWebSocket(null);
                client.close();
                getContext().sendBroadcast(intent);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                this.getWebSocketService().getSemaphore().release();
            }
        }
    }
}
