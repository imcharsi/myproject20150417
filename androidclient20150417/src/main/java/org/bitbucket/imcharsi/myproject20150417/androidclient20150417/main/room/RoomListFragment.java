/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Response;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.MainApplication;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.R;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.TemporaryHolder;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.WebSocketService;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.asynctask.AsyncHttpClientTask;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MainFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnterRoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.RoomHistoryModel;
import org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417.DefaultObjectMapper;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Kang Woo, Lee on 5/10/15.
 */
@Getter
@Setter
public class RoomListFragment extends ListFragment implements TemporaryHolder {
    public static final String TAG = RoomListFragment.class.getName();
    public static final String ROOM_ID = "roomId";
    private String cookie;
    private Integer userId;
    private RoomListArrayAdapter adapter;
    private ObjectGraph objectGraph;
    private WebSocketService.InnerService innerService;
    private WebSocketServiceConnection serviceConnection;
    private Menu menu;
    private TemporaryHolder.WebSocketServiceReceiver receiver;

    public static class WebSocketServiceConnection extends TemporaryHolder.AbstractWebSocketServiceConnection<RoomListFragment> {
        public WebSocketServiceConnection init(RoomListFragment roomListFragment) {
            setFragment(roomListFragment);
            return this;
        }

        @Override
        protected TemporaryHolder.WebSocketServiceReceiver getReceiver() {
            return getFragment().getObjectGraph().
                    get(WebSocketServiceReceiver.class).
                    init(getFragment().getInnerService(), getFragment().getActivity(), getFragment());
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            super.onServiceConnected(name, service);
            getFragment().setInnerService((((WebSocketService.SampleBinder) service).getService()));
            if (getFragment().getInnerService().isLogoutChecked()) {
                getFragment().getFragmentManager().popBackStack();
            }
        }
    }

    public static class WebSocketServiceReceiver extends TemporaryHolder.WebSocketServiceReceiver<RoomListFragment> {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            prepare(getArguments());
        } else if (savedInstanceState != null) {
            prepare(savedInstanceState);
        }
        setAdapter(new RoomListArrayAdapter());
        setHasOptionsMenu(true);
        setObjectGraph(((MainApplication) getActivity().getApplication()).getObjectGraph());
    }

    @Override
    public void onStart() {
        super.onStart();
        setServiceConnection(getObjectGraph().get(WebSocketServiceConnection.class).init(this));
        getActivity().bindService(new Intent(getActivity(), WebSocketService.class), getServiceConnection(), Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(getReceiver());
        getActivity().unbindService(getServiceConnection());
    }

    private void prepare(final Bundle bundle) {
        if (bundle.containsKey(MainFragment.COOKIE)) {
            setCookie(bundle.getString(MainFragment.COOKIE));
        }
        if (bundle.containsKey(MainFragment.USER_ID)) {
            setUserId(bundle.getInt(MainFragment.USER_ID));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getCookie() != null) {
            outState.putString(MainFragment.COOKIE, getCookie());
        }
        if (getUserId() != null) {
            outState.putInt(MainFragment.USER_ID, getUserId());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = super.onCreateView(inflater, container, savedInstanceState);
        setListAdapter(getAdapter());
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.room_list_menu, menu);
        setMenu(menu);
    }

    // test 를 쉽게 하기 위해 쓴다. test 단계에서는 이 method 를 override 하여 필요한 listener 를 덧붙인다.
    protected AlertDialog createCreateRoomDialog(AlertDialog.Builder builder) {
        return builder.create();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // 비활성화된 option item 은 반드시 다시 활성화되어야 한다.
        switch (item.getItemId()) {
            case R.id.menuItemCreateRoom:
                final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.create_room_dialog_layout, null);
                final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity()).
                        setView(dialogView).
                        setCancelable(true).
                        setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                item.setEnabled(false);
                                final EditText editTextRoomName = (EditText) dialogView.findViewById(R.id.editTextRoomName);
                                final EditText editTextRoomPassword = (EditText) dialogView.findViewById(R.id.editTextRoomPassword);
                                final String password1 = editTextRoomPassword.getText().toString().trim();
                                String password2 = password1;
                                if (password1.isEmpty()) {
                                    password2 = null;
                                }
                                getObjectGraph().get(CreateRoomAsyncTask.class).
                                        init(RoomListFragment.this, editTextRoomName.getText().toString(), password2).
                                        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                createCreateRoomDialog(dialog).show();
                return true;
            case R.id.menuItemRefreshRoomList:
                item.setEnabled(false);
                // menu 가 눌려지면 동작을 완료할 때까지는 비활성상태에 있어야 한다.
                getObjectGraph().get(RoomListAsyncTask.class).
                        init(this).
                        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected AlertDialog createEnterRoomDialog(AlertDialog.Builder builder) {
        return builder.create();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        final RoomModel roomModel = getAdapter().getItem(position);
        if ((roomModel.getCurrentAttendingRoom() != null && roomModel.getCurrentAttendingRoom() == true) ||
                (roomModel.getCurrentDetail().getNeedPassword() == null || roomModel.getCurrentDetail().getNeedPassword() == false)) {
            getObjectGraph().get(EnterRoomAsyncTask.class).init(this, roomModel.getId(), null).
                    executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.enter_room_dialog_layout, null);
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).
                    setView(dialogView).
                    setCancelable(true).
                    setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final EditText editTextRoomPassword = (EditText) dialogView.findViewById(R.id.editTextRoomPassword);
                            final String password1 = editTextRoomPassword.getText().toString().trim();
                            String password2 = password1;
                            if (password1.isEmpty()) {
                                password2 = null;
                            }
                            getObjectGraph().get(EnterRoomAsyncTask.class).init(RoomListFragment.this, roomModel.getId(), password2).
                                    executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            createEnterRoomDialog(builder).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getObjectGraph().get(RoomListAsyncTask.class).
                init(this).
                executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public class RoomListArrayAdapter extends ArrayAdapter<RoomModel> {
        public RoomListArrayAdapter() {
            super(RoomListFragment.this.getActivity(), android.R.layout.simple_list_item_1);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);
            final TextView textView = (TextView) view.findViewById(android.R.id.text1);
            final StringBuilder stringBuilder = new StringBuilder();
            final RoomModel roomModel = getItem(position);
            stringBuilder.append(roomModel.getCurrentDetail().getName());
            if (roomModel.getCurrentDetail() != null) {
                if (roomModel.getCurrentDetail().getNeedPassword() != null && roomModel.getCurrentDetail().getNeedPassword() == true) {
                    stringBuilder.append(" (?)");
                }
                if (roomModel.getCurrentAttendingRoom() != null && roomModel.getCurrentAttendingRoom() == true) {
                    stringBuilder.append(" (in)");
                }
                textView.setText(stringBuilder.toString());
            }
            return view;
        }
    }

    @Getter
    @Setter
    public static class RoomListAsyncTask extends AsyncHttpClientTask<Void, Void, List<RoomModel>> {
        private RoomListFragment roomListFragment;

        @Inject
        public RoomListAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public RoomListAsyncTask init(RoomListFragment roomListFragment) {
            setRoomListFragment(roomListFragment);
            setCookie(getRoomListFragment().getCookie());
            return this;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getRoomListFragment().setListShown(false);
        }

        @Override
        protected List<RoomModel> doInBackground(Void... params) {
            try {
                // 바로 room 목록을 구하면, 지우기를 성공했지만, 속도차이로 인해서 여전히 목록에 남아있는 방을 보게 된다.
                // 0.1초도 충분하지 않아서 0.3초로 했다. 0.3초는 충분한가. 충분하지 않다. 1초로 해보자.
                // 방을 나갈 때 기다릴 것인가, 방 목록을 구할 때 기다릴 것인가. 구할 때 기다려보기로 하자.
                Thread.sleep(1000);
                final String serverUrl = ((MainApplication) getRoomListFragment().getActivity().getApplication()).getServerUrl();
                final ListenableFuture<Response> execute = getAsyncHttpClient().prepareGet(serverUrl + "/room/allCreatedRooms").
                        addHeader("Accept", MainApplication.ContentType).
                        addHeader("Cookie", getCookie()).
                        execute();
                final Response response = execute.get();
                if (response.getStatusCode() == 200) {
                    return DefaultObjectMapper.objectMapper.readValue(response.getResponseBody(),
                            DefaultObjectMapper.objectMapper.getTypeFactory().constructCollectionType(List.class, RoomModel.class));
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<RoomModel> roomModels) {
            super.onPostExecute(roomModels);
            try {
                getRoomListFragment().getAdapter().clear();
                if (roomModels != null) {
                    getRoomListFragment().getAdapter().addAll(roomModels);
                }
                getRoomListFragment().setListShown(true);
                // 아래 역시 마찬가지다. 이 fragment 를 벗어난 시점에서 이 async task 가 동작을 완료하면 문제된다. 그냥 무시한다.
                getRoomListFragment().getMenu().findItem(R.id.menuItemRefreshRoomList).setEnabled(true);
            } catch (Exception e) {
                // 대화방 퇴장에 이어 폐쇄가 일어나는 경우, 퇴장에 이어 바로 대화방 목록을 조회하면 폐쇄중인 대화방이 그대로 뜨는 문제 때문에
                // 조회에 앞서 항상 일정시간 기다려주기로 했다. 기다리는 중에 이 fragment 를 벗어나면 이 async task 가 작업을 완료하는 시점에서
                // 유효하지 않는 fragment 에 대한 작업을 시도하게 된다. 실패할수도 있는 상황인데, 실패하도록 내버려두자.
                e.printStackTrace();
            }
        }
    }

    @Getter
    @Setter
    public static class CreateRoomAsyncTask extends AsyncHttpClientTask<Void, Void, Integer> {
        private RoomListFragment roomListFragment;
        private String name;
        private String password;

        @Inject
        public CreateRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public CreateRoomAsyncTask init(RoomListFragment roomListFragment, String name, String password) {
            setRoomListFragment(roomListFragment);
            setCookie(getRoomListFragment().getCookie());
            setName(name);
            setPassword(password);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final String serverUrl = ((MainApplication) getRoomListFragment().getActivity().getApplication()).getServerUrl();
                final RoomModel roomModel = new RoomModel();
                roomModel.setCurrentDetail(new RoomHistoryModel());
                roomModel.getCurrentDetail().setName(getName());
                roomModel.getCurrentDetail().setPassword(getPassword());
                final ListenableFuture<Response> execute = getAsyncHttpClient().preparePost(serverUrl + "/room").
                        addHeader("Content-Type", MainApplication.ContentType).
                        addHeader("Cookie", getCookie()).
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(roomModel)).
                        setBodyEncoding("UTF-8").
                        execute();
                final Response response = execute.get();
                if (response.getStatusCode() == 200) {
                    return DefaultObjectMapper.objectMapper.readValue(response.getResponseBody(), Integer.class);
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer roomId) {
            super.onPostExecute(roomId);
            final Bundle bundle = new Bundle();
            bundle.putString(MainFragment.COOKIE, getCookie());
            bundle.putInt(RoomListFragment.ROOM_ID, roomId);
            if (getRoomListFragment().getUserId() != null) {
                bundle.putInt(MainFragment.USER_ID, getRoomListFragment().getUserId());
            }
            final RoomFragment roomFragment = getRoomListFragment().getObjectGraph().get(RoomFragment.class);
            roomFragment.setArguments(bundle);
            final FragmentTransaction transaction = getRoomListFragment().getActivity().getFragmentManager().beginTransaction();
            transaction.replace(R.id.main_activity_layout, roomFragment, RoomFragment.TAG);
            transaction.addToBackStack(null);
            transaction.commit();
            getRoomListFragment().getMenu().findItem(R.id.menuItemCreateRoom).setEnabled(true);
        }
    }

    @Getter
    @Setter
    public static class EnterRoomAsyncTask extends AsyncHttpClientTask<Void, Void, Integer> {
        private RoomListFragment roomListFragment;
        private Integer roomId;
        private String password;

        @Inject
        public EnterRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public EnterRoomAsyncTask init(RoomListFragment roomListFragment, Integer roomId, String password) {
            setRoomListFragment(roomListFragment);
            setCookie(getRoomListFragment().getCookie());
            setRoomId(roomId);
            setPassword(password);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final String serverUrl = ((MainApplication) getRoomListFragment().getActivity().getApplication()).getServerUrl();
                final StringBuilder stringBuilder = new StringBuilder(serverUrl).
                        append("/room/").
                        append(getRoomId()).
                        append("/roomAttenders/self");
                final Response response = getAsyncHttpClient().prepareGet(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        setHeader("Accept", MainApplication.ContentType).
                        execute().get();
                // 이미 입장해 있다면
                if (response.getStatusCode() == 200) {
                    // 아무것도 하지 않는다.
                    return getRoomId();
                }
                // 다음과 같은 경우가 문제된다. 대화방에서 퇴장했다가 다시 똑같은 대화방에 입장할 경우,
                // 퇴장에 이어 service 에 쌓아놓았던 message 를 지우는 작업을 했지만,
                // 이에 이어 server 로부터 받는 퇴장 message 가 다시 message queue 에 쌓이게 된다.
                // 똑같은 대화방에 다시 입장할 경우, 앞의 퇴장과 관련된 message 가 남아있게 되는데,
                // 새로 입장하는 경우에는 입장하기에 앞서서 입장하려는 방에 관해 쌓여 있었을 모든 message 를 지운다.
                // 왜 입장 후에 정리를 하지 않고, 입장 전에 정리를 하는가.
                // 입장에 이어 server 가 보내게 되는 입장 관련 message 를 지우게 될 수 있기 때문에, 입장전에 정리를 한다.
                getRoomListFragment().getInnerService().clearRoom(getRoomId());
                // 아직 입장해 있지 않으므로 입장을 시도한다.
                final EnterRoomModel enterRoomModel = new EnterRoomModel();
                enterRoomModel.setPassword(getPassword());
                final StringBuilder stringBuilder2 = new StringBuilder(serverUrl).
                        append("/room/").
                        append(getRoomId()).
                        append("/roomAttenders");
                final ListenableFuture<Response> execute = getAsyncHttpClient().preparePost(stringBuilder2.toString()).
                        addHeader("Cookie", getCookie()).
                        setHeader("Content-Type", MainApplication.ContentType).
                        setBodyEncoding("UTF-8").
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(enterRoomModel)).
                        execute();
                final Response response2 = execute.get();
                if (response2.getStatusCode() == 200) {
                    return getRoomId();
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer roomId) {
            super.onPostExecute(roomId);
            if (roomId == null) {
                getRoomListFragment().getObjectGraph().get(RoomListAsyncTask.class).
                        init(getRoomListFragment()).
                        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                return;
            }
            final Bundle bundle = new Bundle();
            bundle.putString(MainFragment.COOKIE, getCookie());
            bundle.putInt(RoomListFragment.ROOM_ID, roomId);
            if (getRoomListFragment().getUserId() != null) {
                bundle.putInt(MainFragment.USER_ID, getRoomListFragment().getUserId());
            }
            final RoomFragment roomFragment = getRoomListFragment().getObjectGraph().get(RoomFragment.class);
            roomFragment.setArguments(bundle);
            final FragmentTransaction transaction = getRoomListFragment().getActivity().getFragmentManager().beginTransaction();
            transaction.replace(R.id.main_activity_layout, roomFragment, RoomFragment.TAG);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
