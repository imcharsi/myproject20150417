/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room;

import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Kang Woo, Lee on 5/22/15.
 */
@Getter
@Setter
public class BannedUserListView extends ListView {
    private RoomFragment roomFragment;

    public BannedUserListView(Context context) {
        super(context);
    }

    public BannedUserListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BannedUserListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (getRoomFragment() != null && visibility == View.VISIBLE) {
            getRoomFragment().getObjectGraph().get(RoomFragment.BannedUserListAsyncTask.class).init(getRoomFragment()).
                    executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
}
