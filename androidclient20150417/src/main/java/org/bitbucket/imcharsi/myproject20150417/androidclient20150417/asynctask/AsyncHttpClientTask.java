/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.asynctask;

import android.os.AsyncTask;
import com.ning.http.client.AsyncHttpClient;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Kang Woo, Lee on 5/10/15.
 */
@Getter
@Setter
abstract public class AsyncHttpClientTask<Param, Progress, Result> extends AsyncTask<Param, Progress, Result> {
    private AsyncHttpClient asyncHttpClient;
    private String cookie;

    public AsyncHttpClientTask(AsyncHttpClient asyncHttpClient) {
        setAsyncHttpClient(asyncHttpClient);
    }
}
