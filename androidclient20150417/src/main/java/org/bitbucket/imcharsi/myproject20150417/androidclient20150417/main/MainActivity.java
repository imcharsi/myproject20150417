/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.MainApplication;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.R;

/**
 * Created by Kang Woo, Lee on 5/8/15.
 */
@Getter
@Setter
public class MainActivity extends Activity {
    private MainFragment mainFragment;
    private ObjectGraph objectGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        setObjectGraph(MainApplication.retrieveMainApplication(getApplication()));

        if (getFragmentManager().findFragmentByTag(MainFragment.TAG) == null) {
            final FragmentTransaction transaction = getFragmentManager().beginTransaction();
            setMainFragment(getObjectGraph().get(MainFragment.class));
            transaction.replace(R.id.main_activity_layout, getMainFragment(), MainFragment.TAG);
            transaction.commit();
        }
    }
}
