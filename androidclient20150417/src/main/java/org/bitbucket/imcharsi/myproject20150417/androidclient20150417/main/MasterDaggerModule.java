/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.providers.netty.NettyAsyncHttpProvider;
import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.AllEtcMessagesListFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.WebSocketService;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomListFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.user.UserListFragment;

import javax.inject.Singleton;

/**
 * Created by Kang Woo, Lee on 5/10/15.
 */
@Module(injects = {
        MainFragment.class, UserListFragment.class, RoomListFragment.class,
        MainFragment.LoginAsyncTask.class, MainFragment.LogoutAsyncTask.class, MainFragment.WebSocketServiceConnection.class, MainFragment.SelfAsyncTask.class,
        MainFragment.WebSocketServiceReceiver.class, MainFragment.RegisterUserAsyncTask.class,
        UserListFragment.UserListFragmentAsyncTask.class, UserListFragment.WebSocketServiceConnection.class,
        RoomListFragment.RoomListAsyncTask.class, RoomListFragment.CreateRoomAsyncTask.class, RoomListFragment.EnterRoomAsyncTask.class,
        RoomFragment.WebSocketServiceConnection.class, RoomFragment.WebSocketServiceReceiver.class, RoomFragment.RoomAttenderListAsyncTask.class, RoomFragment.LeaveRoomForceAsyncTask.class,
        RoomFragment.TalkAsyncTask.class, RoomFragment.LeaveRoomAsyncTask.class, RoomListFragment.WebSocketServiceConnection.class, RoomFragment.GrantOperatorAsyncTask.class,
        RoomFragment.RevokeOperatorAsyncTask.class, RoomFragment.TransferOwnerAsyncTask.class, RoomFragment.BannedUserListAsyncTask.class,
        RoomFragment.ClearBanAsyncTask.class, RoomFragment.LoginUserListAsyncTask.class, RoomFragment.InviteAsyncTask.class,
        RoomFragment.ChangePasswordAsyncTask.class, RoomFragment.ChangeCurrentNickAsyncTask.class, RoomFragment.ChangeRoomNameAsyncTask.class,
        RoomFragment.class,
        WebSocketService.WebSocketListener.class,
        AsyncHttpClient.class,
        InvitationListFragment.class, InvitationListFragment.InvitationListFragmentAsyncTask.class, InvitationListFragment.EnterRoomAsyncTask.class, InvitationListFragment.WebSocketServiceConnection.class,
        InvitationListFragment.WebSocketServiceReceiver.class,
        UserListFragment.WebSocketServiceReceiver.class,
        RoomListFragment.WebSocketServiceReceiver.class,
        AllEtcMessagesListFragment.WebSocketServiceConnection.class, AllEtcMessagesListFragment.WebSocketServiceReceiver.class, AllEtcMessagesListFragment.class
})
public class MasterDaggerModule {
    @Provides
    public WebSocketService.WebSocketListener provideWebSocketListener() {
        return new WebSocketService.WebSocketListener();
    }

    @Provides
    @SuppressWarnings("unused")
    public RoomFragment provideRoomFragment() {
        return new RoomFragment();
    }

    @Provides
    public RoomFragment.WebSocketServiceConnection provideRoomFragmentWSSC() {
        return new RoomFragment.WebSocketServiceConnection();
    }

    @Provides
    public RoomFragment.WebSocketServiceReceiver provideRoomFragmentWSSR() {
        return new RoomFragment.WebSocketServiceReceiver();
    }

    @Provides
    public MainFragment provideMainFragment() {
        return new MainFragment();
    }

    @Provides
    public MainFragment.WebSocketServiceConnection provideMainFragmentWSSC() {
        return new MainFragment.WebSocketServiceConnection();
    }

    @Provides
    public MainFragment.WebSocketServiceReceiver provideMainFragmentWSSR() {
        return new MainFragment.WebSocketServiceReceiver();
    }

    @Provides
    public RoomListFragment provideRoomListFragment() {
        return new RoomListFragment();
    }

    @Provides
    public RoomListFragment.WebSocketServiceConnection provideRoomListFragmentWSSC() {
        return new RoomListFragment.WebSocketServiceConnection();
    }

    @Provides
    public RoomListFragment.WebSocketServiceReceiver provideRoomListFragmentWSSR() {
        return new RoomListFragment.WebSocketServiceReceiver();
    }

    @Provides
    public InvitationListFragment provideInvitationListFragment() {
        return new InvitationListFragment();
    }

    @Provides
    public InvitationListFragment.WebSocketServiceConnection provideInvitationListFragmentWSSC() {
        return new InvitationListFragment.WebSocketServiceConnection();
    }

    @Provides
    public InvitationListFragment.WebSocketServiceReceiver provideInvitationListFragmentWSSR() {
        return new InvitationListFragment.WebSocketServiceReceiver();
    }

    @Provides
    public UserListFragment provideUserListFragment() {
        return new UserListFragment();
    }

    @Provides
    public UserListFragment.WebSocketServiceConnection provideUserListFragmentWSSC() {
        return new UserListFragment.WebSocketServiceConnection();
    }

    @Provides
    public UserListFragment.WebSocketServiceReceiver provideUserListFragmentWSSR() {
        return new UserListFragment.WebSocketServiceReceiver();
    }

    @Provides
    public AllEtcMessagesListFragment provideAllEtcMessagesListFragment() {
        return new AllEtcMessagesListFragment();
    }

    @Provides
    public AllEtcMessagesListFragment.WebSocketServiceConnection provideAllEtcMessagesListFragmentWSSC() {
        return new AllEtcMessagesListFragment.WebSocketServiceConnection();
    }

    @Provides
    public AllEtcMessagesListFragment.WebSocketServiceReceiver provideAllEtcMessagesListFragmentWSSR() {
        return new AllEtcMessagesListFragment.WebSocketServiceReceiver();
    }

    @Provides
    @Singleton
    public AsyncHttpClient provideAsyncHttpClient() {
        // 아래는 안된다. 이유는 모른다.
        // final AsyncHttpClient client = new AsyncHttpClient(new JDKAsyncHttpProvider(config), config);
        AsyncHttpClientConfig config = new AsyncHttpClientConfig.Builder().build();
        return new AsyncHttpClient(new NettyAsyncHttpProvider(config), config);
    }

    public static final ObjectGraph MASTER_OBJECT_GRAPH = ObjectGraph.create(MasterDaggerModule.class);
}
