/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.*;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.MainApplication;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.R;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.WebSocketService;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.asynctask.AsyncHttpClientTask;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MainFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeDataType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.BanModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.InvitationModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomAttenderModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.RoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.history.*;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.talk.history.TalkHistoryModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.current.UserModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.user.history.UserNickHistoryModel;
import org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417.DefaultObjectMapper;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by Kang Woo, Lee on 5/11/15.
 */
@Getter
@Setter
public class RoomFragment extends Fragment {
    static public final String TAG = RoomFragment.class.getName();
    static public final int CONTEXT_MENUITEM_LEAVE_ROOM_FORCE = 1;
    private static final int CONTEXT_MENUITEM_GRANT_OPERATOR = 2;
    private static final int CONTEXT_MENUITEM_REVOKE_OPERATOR = 3;
    private static final int CONTEXT_MENUITEM_TRANSFER_OWNER = 4;
    private static final int CONTEXT_MENUITEM_CLEAR_BAN = 1;
    private static final int CONTEXT_MENUITEM_INVITE = 1;

    final private Random random = new Random();
    private String cookie;
    private Button buttonRandomTalk;
    private Button buttonTalk;
    private EditText editTextTalk;
    private ListView listViewTalk;
    private Integer roomId;
    private Integer userId;
    private WebSocketService.InnerService innerService;
    private WebSocketServiceConnection serviceConnection;
    private WebSocketServiceReceiver receiver;
    private ObjectGraph objectGraph;
    private ListView listView;
    private EnvelopeModelAdapter envelopeModelAdapter;
    private Menu menu;
    private RoomAttenderListView listViewRoomAttenderList;
    private BannedUserListView listViewBannedUserList;
    private LoginUserListView listViewLoginUserList;
    private RoomAttenderModelAdapter roomAttenderModelAdapter;
    private BanModelAdapter banModelAdapter;
    private UserModelAdapter userModelAdapter;
    private Display display;

    @Getter
    @Setter
    public static class WebSocketServiceConnection implements ServiceConnection {
        private RoomFragment roomFragment;

        public WebSocketServiceConnection init(RoomFragment roomFragment) {
            setRoomFragment(roomFragment);
            return this;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            getRoomFragment().setInnerService((((WebSocketService.SampleBinder) service).getService()));
            // 이 단계에 이르면, receiver 설정이 이미 완료되어 있으므로, 여기서 broadcast 를 해줄 것을 명시적으로 요청하면 된다.
            getRoomFragment().getInnerService().retryBroadcast(getRoomFragment().getRoomId());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            getRoomFragment().setInnerService(null);
        }
    }

    @Getter
    @Setter
    public static class WebSocketServiceReceiver extends BroadcastReceiver {
        private RoomFragment roomFragment;

        public WebSocketServiceReceiver init(RoomFragment mainFragment) {
            setRoomFragment(mainFragment);
            return this;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean changedRoomAttenderList = false;
            boolean changedBannedUserList = false;
            Log.i(getClass().getName(), intent.toString());

            final List<WebSocketService.QueueEntity> idList = getRoomFragment().getInnerService().getIdList(getRoomFragment().getRoomId());
            if (idList != null) {
                for (WebSocketService.QueueEntity queueEntity : idList) {
                    if (queueEntity.isRedrawFlag()) {
                        getRoomFragment().getEnvelopeModelAdapter().clear();
                    }
                    // 이 단계에서는 logout message 를 확인할 필요가 없다. 여기서는 대화방 퇴장여부만 확인하면 된다.
                    for (Integer id : queueEntity.getMessageIdList()) {
                        final EnvelopeModel<?> envelopeModel = getRoomFragment().getInnerService().getMessage(id);
                        getRoomFragment().getEnvelopeModelAdapter().add(envelopeModel);
                        // 여러가지 message 유형에 따라 처리를 달리 해야 하는데,
                        // 아래에서는 우선, 강제퇴장의 경우를 다룬다. 강제퇴장에 의한 강제퇴장은 강제퇴장 기능을 다루지 않았기 때문에 확인할 수 없고,
                        // 강제접속종료로 인한 강제퇴장의 경우를 확인할 수 있다.
                        if (envelopeModel.getDataType() == EnvelopeDataType.RoomTimelineModelType) {
                            final RoomTimelineModel roomTimelineModel = (RoomTimelineModel) envelopeModel.getData();
                            if (roomTimelineModel.getRoomInOutHistoryId() != null) {
                                final RoomInOutHistoryModel roomInOutHistoryModel = roomTimelineModel.getRoomInOutHistory();
                                // 누구에 의한 강제퇴장인지는 구분하지 않는다. 사용자 본인의 계정의 강제접속종료로 인한 퇴장일수 있기 때문이다.
                                if (roomInOutHistoryModel.getUserId() == getRoomFragment().getUserId() && roomInOutHistoryModel.getEnterType() == EnterType.Leave) {
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(getRoomFragment().getActivity());
                                    int messageId;
                                    if (getRoomFragment().getInnerService().isLogoutChecked()) {
                                        messageId = R.string.textLogout;
                                    } else {
                                        messageId = R.string.textLeaveRoom;
                                    }
                                    builder.setMessage(messageId).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).create().show();
                                    getRoomFragment().onOptionsItemSelected(getRoomFragment().getMenu().findItem(R.id.menuItemOut));
                                }
                                // 대화방 참가자 구성이 바뀌었음을 표시한다.
                                changedRoomAttenderList = true;
                            }
                            if (roomTimelineModel.getPermissionHistoryId() != null) {
                                changedRoomAttenderList = true;
                            }
                            if (roomTimelineModel.getBanHistoryId() != null) {
                                changedBannedUserList = true;
                            }
                        }
                    }
                }
            }
            // todo 현재 scroll 이 바닥에 있지 않으면 scroll 을 강제하지 않도록 하자.
            if (getRoomFragment().getListView().getAdapter().getCount() > 0) {
                getRoomFragment().getListView().setSelection(getRoomFragment().getEnvelopeModelAdapter().getCount() - 1);
            }
            // 대화방 참가자 목록을 갱신한다. 대화방 참가자 목록의 가시성은 여기서는 생각하지 않는다.
            if (changedRoomAttenderList) {
                getRoomFragment().getObjectGraph().get(RoomAttenderListAsyncTask.class).init(getRoomFragment()).
                        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
            if (changedBannedUserList) {
                getRoomFragment().getObjectGraph().get(BannedUserListAsyncTask.class).init(getRoomFragment()).
                        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            final List<WebSocketService.QueueEntity> idList2 = getRoomFragment().getInnerService().getIdList(-1);
            if (idList2 != null) {
                for (WebSocketService.QueueEntity queueEntity : idList2) {
                    for (Integer id : queueEntity.getMessageIdList()) {
                        final EnvelopeModel<?> envelopeModel = getRoomFragment().getInnerService().getMessage(id);
                        if (envelopeModel.getDataType() == EnvelopeDataType.InvitationModelType) {
                            Toast.makeText(getRoomFragment().getActivity(), R.string.textInvite, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setMenu(menu);
        inflater.inflate(R.menu.room_fragment_menu, menu);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setObjectGraph(MainApplication.retrieveMainApplication(getActivity().getApplication()));
        Bundle bundle = getArguments();
        if (bundle == null) {
            bundle = savedInstanceState;
        }
        if (bundle != null) {
            if (bundle.containsKey(RoomListFragment.ROOM_ID)) {
                setRoomId(bundle.getInt(RoomListFragment.ROOM_ID));
            }
            if (bundle.containsKey(MainFragment.COOKIE)) {
                setCookie(bundle.getString(MainFragment.COOKIE));
            }
            if (bundle.containsKey(MainFragment.USER_ID)) {
                setUserId(bundle.getInt(MainFragment.USER_ID));
            }
        }
        setHasOptionsMenu(true);
        setDisplay(((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getRoomId() != null) {
            outState.putInt(RoomListFragment.ROOM_ID, getRoomId());
        }
        if (getCookie() != null) {
            outState.putString(MainFragment.COOKIE, getCookie());
        }
        if (getUserId() != null) {
            outState.putInt(MainFragment.USER_ID, getUserId());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.room_fragment_layout, container, false);

        setButtonRandomTalk((Button) view.findViewById(R.id.buttonRandomTalk));
        setButtonTalk((Button) view.findViewById(R.id.buttonTalk));
        setEditTextTalk((EditText) view.findViewById(R.id.editTextTalk));
        setListView((ListView) view.findViewById(R.id.listView));
        setEnvelopeModelAdapter(new EnvelopeModelAdapter());
        getListView().setAdapter(getEnvelopeModelAdapter());
        getButtonTalk().setOnClickListener(new ButtonTalkListener());
        getButtonRandomTalk().setOnClickListener(new ButtonRandomTalkListener());
        // http://stackoverflow.com/questions/7506230/set-position-size-of-ui-element-as-percentage-of-screen-size
        // xml 선언에서 비율로 높이를 정하는 방법이 있나.
        final Point point = new Point(0, 0);
        getDisplay().getSize(point);

        setRoomAttenderModelAdapter(new RoomAttenderModelAdapter());
        setListViewRoomAttenderList((RoomAttenderListView) view.findViewById(R.id.listViewRoomAttenderList));
        getListViewRoomAttenderList().setAdapter(getRoomAttenderModelAdapter());
        getListViewRoomAttenderList().setVisibility(View.INVISIBLE);
        getListViewRoomAttenderList().setRoomFragment(this);
        getListViewRoomAttenderList().getLayoutParams().height = (int) (point.y * 0.3);

        setBanModelAdapter(new BanModelAdapter());
        setListViewBannedUserList((BannedUserListView) view.findViewById(R.id.listViewBannedUserList));
        getListViewBannedUserList().setAdapter(getBanModelAdapter());
        getListViewBannedUserList().setVisibility(View.INVISIBLE);
        getListViewBannedUserList().setRoomFragment(this);
        getListViewBannedUserList().getLayoutParams().height = (int) (point.y * 0.3);

        setUserModelAdapter(new UserModelAdapter());
        setListViewLoginUserList((LoginUserListView) view.findViewById(R.id.listViewLoginUserList));
        getListViewLoginUserList().setAdapter(getUserModelAdapter());
        getListViewLoginUserList().setVisibility(View.INVISIBLE);
        getListViewLoginUserList().setRoomFragment(this);
        getListViewLoginUserList().getLayoutParams().height = (int) (point.y * 0.3);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        final IntentFilter intentFilter = new IntentFilter("hi there");
        intentFilter.addAction("close web socket");
        intentFilter.addAction("server message");
        // 여기서 이와 같이 하지 않으면, 여기저기서 retryBroadcast 를 해야 한다.
        setReceiver(getObjectGraph().get(WebSocketServiceReceiver.class).init(this));
        setServiceConnection(getObjectGraph().get(WebSocketServiceConnection.class).init(this));
        getActivity().bindService(new Intent(getActivity(), WebSocketService.class), getServiceConnection(), Context.BIND_AUTO_CREATE);
        getActivity().registerReceiver(getReceiver(), intentFilter);
        registerForContextMenu(getListViewRoomAttenderList());
        registerForContextMenu(getListViewBannedUserList());
        registerForContextMenu(getListViewLoginUserList());
    }

    @Override
    public void onStop() {
        super.onStop();
        // 대화방 fragment 를 나갔다는 이유로 대화방을 자동퇴장하지는 않는다.
        // 이미 입장해있는 방에 대해 대화방 fragment 만 다시 그리려면 입장해있는지 여부를 확인해야 한다.
        // /room/:roomId/roomAttenders/self 에 대해 get 요청을 하여 200 응답이면 입장해있는 것으로 판단한다.
        unregisterForContextMenu(getListViewRoomAttenderList());
        unregisterForContextMenu(getListViewBannedUserList());
        unregisterForContextMenu(getListViewLoginUserList());
        getActivity().unbindService(getServiceConnection());
        getActivity().unregisterReceiver(getReceiver());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        // context menu 를 수행하는 사용자에 따라 context menu 가 다르게 보여야 한다.
//        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
//        getRoomAttenderModelAdapter().getItem(info.position);
        if (v == getListViewRoomAttenderList()) {
            menu.add(1, CONTEXT_MENUITEM_LEAVE_ROOM_FORCE, Menu.NONE, R.string.textLeaveRoom);
            menu.add(1, CONTEXT_MENUITEM_GRANT_OPERATOR, Menu.NONE, R.string.textGrantOperator);
            menu.add(1, CONTEXT_MENUITEM_REVOKE_OPERATOR, Menu.NONE, R.string.textRevokeOperator);
            menu.add(1, CONTEXT_MENUITEM_TRANSFER_OWNER, Menu.NONE, R.string.textTransferOwner);
        } else if (v == getListViewBannedUserList()) {
            menu.add(2, CONTEXT_MENUITEM_CLEAR_BAN, Menu.NONE, R.string.textClearBan);
        } else if (v == getListViewLoginUserList()) {
            menu.add(3, CONTEXT_MENUITEM_INVITE, Menu.NONE, R.string.textInvite);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuItemOut:
                final String serverUrl = ((MainApplication) getActivity().getApplication()).getServerUrl();
                getObjectGraph().get(LeaveRoomAsyncTask.class).init(RoomFragment.this, serverUrl, getRoomId()).
                        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                return true;
            case R.id.menuItemClear:
                getInnerService().clearRoom(getRoomId());
                getInnerService().retryBroadcast(getRoomId());
                return true;
            case R.id.menuItemRoomAttenderList:
                // 실제로 자료를 구하는 과정은, visibility listener 에서 시작한다.
                if (getListViewRoomAttenderList().getVisibility() == View.VISIBLE) {
                    getListViewRoomAttenderList().setVisibility(View.INVISIBLE);
                } else {
                    getListViewRoomAttenderList().setVisibility(View.VISIBLE);
                    getListViewLoginUserList().setVisibility(View.INVISIBLE);
                    getListViewBannedUserList().setVisibility(View.INVISIBLE);
                }
                return true;
            case R.id.menuItemBannedUserList:
                if (getListViewBannedUserList().getVisibility() == View.VISIBLE) {
                    getListViewBannedUserList().setVisibility(View.INVISIBLE);
                } else {
                    getListViewBannedUserList().setVisibility(View.VISIBLE);
                    getListViewLoginUserList().setVisibility(View.INVISIBLE);
                    getListViewRoomAttenderList().setVisibility(View.INVISIBLE);
                }
                return true;
            case R.id.menuItemLoginUserList:
                if (getListViewLoginUserList().getVisibility() == View.VISIBLE) {
                    getListViewLoginUserList().setVisibility(View.INVISIBLE);
                } else {
                    getListViewLoginUserList().setVisibility(View.VISIBLE);
                    getListViewBannedUserList().setVisibility(View.INVISIBLE);
                    getListViewRoomAttenderList().setVisibility(View.INVISIBLE);
                }
                return true;
            case R.id.menuItemChangePassword:
                final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.enter_room_dialog_layout, null);
                new AlertDialog.Builder(getActivity()).
                        setView(dialogView).
                        setCancelable(true).
                        setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final EditText editTextRoomPassword = (EditText) dialogView.findViewById(R.id.editTextRoomPassword);
                                final String password1 = editTextRoomPassword.getText().toString().trim();
                                String password2 = password1;
                                if (password1.isEmpty()) {
                                    password2 = null;
                                }
                                getObjectGraph().get(ChangePasswordAsyncTask.class).
                                        init(RoomFragment.this, getRoomId(), password2).
                                        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create().show();
                return true;
            case R.id.menuItemChangeCurrentNick:
                final View dialogView2 = LayoutInflater.from(getActivity()).inflate(R.layout.change_current_nick_dialog_layout, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).
                        setView(dialogView2).
                        setCancelable(true).
                        setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final EditText editTextNick = (EditText) dialogView2.findViewById(R.id.editTextNick);
                                final String nick1 = editTextNick.getText().toString().trim();
                                String nick2 = nick1;
                                if (nick1.isEmpty()) {
                                    nick2 = null;
                                }
                                getObjectGraph().get(ChangeCurrentNickAsyncTask.class).
                                        init(RoomFragment.this, getRoomId(), nick2).
                                        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                createChangeCurrentNickDialog(builder).show();
                return true;
            case R.id.menuItemChangeRoomName:
                final View dialogView3 = LayoutInflater.from(getActivity()).inflate(R.layout.change_room_name_dialog_layout, null);
                final AlertDialog.Builder builder2 =
                        new AlertDialog.Builder(getActivity()).
                                setView(dialogView3).
                                setCancelable(true).
                                setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        final EditText editTextNick = (EditText) dialogView3.findViewById(R.id.editTextRoomName);
                                        final String roomName1 = editTextNick.getText().toString().trim();
                                        String roomName2 = roomName1;
                                        if (roomName1.isEmpty()) {
                                            roomName2 = null;
                                        }
                                        getObjectGraph().get(ChangeRoomNameAsyncTask.class).
                                                init(RoomFragment.this, getRoomId(), roomName2).
                                                executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                    }
                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                createChangeRoomNameDialog(builder2).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected AlertDialog createChangeRoomNameDialog(AlertDialog.Builder builder) {
        return builder.create();
    }

    protected AlertDialog createChangeCurrentNickDialog(AlertDialog.Builder builder) {
        return builder.create();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (item.getGroupId() == 1) {
            final RoomAttenderModel roomAttenderModel = getRoomAttenderModelAdapter().getItem(info.position);
            final String serverUrl = ((MainApplication) getActivity().getApplication()).getServerUrl();
            switch (item.getItemId()) {
                case CONTEXT_MENUITEM_LEAVE_ROOM_FORCE:
                    getObjectGraph().get(LeaveRoomForceAsyncTask.class).
                            init(RoomFragment.this, roomAttenderModel.getUserId(), serverUrl).
                            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    return true;
                case CONTEXT_MENUITEM_GRANT_OPERATOR:
                    getObjectGraph().get(GrantOperatorAsyncTask.class).
                            init(RoomFragment.this, roomAttenderModel.getUserId(), serverUrl).
                            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    return true;
                case CONTEXT_MENUITEM_REVOKE_OPERATOR:
                    getObjectGraph().get(RevokeOperatorAsyncTask.class).
                            init(RoomFragment.this, roomAttenderModel.getUserId(), serverUrl).
                            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    return true;
                case CONTEXT_MENUITEM_TRANSFER_OWNER:
                    getObjectGraph().get(TransferOwnerAsyncTask.class).
                            init(RoomFragment.this, roomAttenderModel.getUserId(), serverUrl).
                            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    return true;
                default:
                    return super.onContextItemSelected(item);
            }
        } else if (item.getGroupId() == 2) {
            final BanModel banModel = getBanModelAdapter().getItem(info.position);
            final String serverUrl = ((MainApplication) getActivity().getApplication()).getServerUrl();
            switch (item.getItemId()) {
                case CONTEXT_MENUITEM_CLEAR_BAN:
                    getObjectGraph().get(ClearBanAsyncTask.class).
                            init(RoomFragment.this, banModel.getUserId(), serverUrl).
                            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    return true;
                default:
                    return super.onContextItemSelected(item);
            }
        } else if (item.getGroupId() == 3) {
            final UserModel userModel = getUserModelAdapter().getItem(info.position);
            final String serverUrl = ((MainApplication) getActivity().getApplication()).getServerUrl();
            switch (item.getItemId()) {
                case CONTEXT_MENUITEM_INVITE:
                    getObjectGraph().get(InviteAsyncTask.class).
                            init(RoomFragment.this, userModel.getId(), serverUrl).
                            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    return true;
                default:
                    return super.onContextItemSelected(item);
            }
        } else {
            return super.onContextItemSelected(item);
        }
    }

    private class ButtonRandomTalkListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            getEditTextTalk().setText(Integer.toString(getRandom().nextInt()));
            getButtonTalk().performClick();
        }
    }

    private class ButtonTalkListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            final String talk = getEditTextTalk().getText().toString();
            getObjectGraph().get(TalkAsyncTask.class).init(RoomFragment.this, getRoomId()).
                    executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, talk);
            // 대화를 보내고 나서, 대화입력창에 쓰여있는 글자들을 지운다.
            getEditTextTalk().setText("");
        }
    }

    @Getter
    @Setter
    abstract public static class AbstractListAsyncTask<T> extends AsyncHttpClientTask<Void, Void, List<T>> {
        private RoomFragment roomFragment;
        private ArrayAdapter<T> adapter;

        public AbstractListAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public AbstractListAsyncTask<T> init(RoomFragment roomFragment) {
            setRoomFragment(roomFragment);
            setCookie(getRoomFragment().getCookie());
            return this;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getAdapter().clear();
        }

        @Override
        protected void onPostExecute(List<T> models) {
            super.onPostExecute(models);
            if (models != null) {
                getAdapter().addAll(models);
            }
        }
    }

    public static class RoomAttenderListAsyncTask extends AbstractListAsyncTask<RoomAttenderModel> {
        @Inject
        public RoomAttenderListAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public RoomAttenderListAsyncTask init(RoomFragment roomFragment) {
            super.init(roomFragment);
            setAdapter(getRoomFragment().getRoomAttenderModelAdapter());
            return this;
        }

        @Override
        protected List<RoomAttenderModel> doInBackground(Void... params) {
            try {
                // 현재 참가자 목록이 가시상태에 있는지를 확인한다.
                if (getRoomFragment().getListViewRoomAttenderList().getVisibility() != View.VISIBLE) {
                    return null;
                }
                final String serverUrl = ((MainApplication) getRoomFragment().getActivity().getApplication()).getServerUrl();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(serverUrl).
                        append("/room/").
                        append(getRoomFragment().getRoomId()).
                        append("/roomAttenders");
                final Response response = getAsyncHttpClient().prepareGet(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        addHeader("Accept", MainApplication.ContentType).
                        execute().get();
                final List<RoomAttenderModel> list = DefaultObjectMapper.objectMapper.readValue(
                        response.getResponseBody(),
                        DefaultObjectMapper.objectMapper.getTypeFactory().constructCollectionType(List.class, RoomAttenderModel.class));
                return list;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static class BannedUserListAsyncTask extends AbstractListAsyncTask<BanModel> {
        @Inject
        public BannedUserListAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public BannedUserListAsyncTask init(RoomFragment roomFragment) {
            super.init(roomFragment);
            setAdapter(getRoomFragment().getBanModelAdapter());
            return this;
        }

        @Override
        protected List<BanModel> doInBackground(Void... params) {
            try {
                // 현재 참가자 목록이 가시상태에 있는지를 확인한다.
                if (getRoomFragment().getListViewBannedUserList().getVisibility() != View.VISIBLE) {
                    return null;
                }
                final String serverUrl = ((MainApplication) getRoomFragment().getActivity().getApplication()).getServerUrl();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(serverUrl).
                        append("/room/").
                        append(getRoomFragment().getRoomId()).
                        append("/ban");
                final Response response = getAsyncHttpClient().prepareGet(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        addHeader("Accept", MainApplication.ContentType).
                        execute().get();
                final List<BanModel> list = DefaultObjectMapper.objectMapper.readValue(
                        response.getResponseBody(),
                        DefaultObjectMapper.objectMapper.getTypeFactory().constructCollectionType(List.class, BanModel.class));
                return list;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static class LoginUserListAsyncTask extends AbstractListAsyncTask<UserModel> {
        @Inject
        public LoginUserListAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public LoginUserListAsyncTask init(RoomFragment roomFragment) {
            super.init(roomFragment);
            setAdapter(getRoomFragment().getUserModelAdapter());
            return this;
        }

        @Override
        protected List<UserModel> doInBackground(Void... params) {
            try {
                // 현재 참가자 목록이 가시상태에 있는지를 확인한다.
                if (getRoomFragment().getListViewLoginUserList().getVisibility() != View.VISIBLE) {
                    return null;
                }
                final String serverUrl = ((MainApplication) getRoomFragment().getActivity().getApplication()).getServerUrl();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(serverUrl).
                        append("/user/allLoginUsers");
                final Response response = getAsyncHttpClient().prepareGet(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        addHeader("Accept", MainApplication.ContentType).
                        execute().get();
                final List<UserModel> list = DefaultObjectMapper.objectMapper.readValue(
                        response.getResponseBody(),
                        DefaultObjectMapper.objectMapper.getTypeFactory().constructCollectionType(List.class, UserModel.class));
                return list;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

    }

    @Getter
    @Setter
    abstract public static class AbstractAsyncTask extends AsyncHttpClientTask<Void, Void, Integer> {
        private RoomFragment roomFragment;
        private Integer targetUserId;
        private String serverUrl;

        public AbstractAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public AbstractAsyncTask init(RoomFragment roomFragment, Integer targetUserId, String serverUrl) {
            setRoomFragment(roomFragment);
            setCookie(getRoomFragment().getCookie());
            setTargetUserId(targetUserId);
            setServerUrl(serverUrl);
            return this;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if (integer != null && integer == 200) {
                Toast.makeText(getRoomFragment().getActivity(), R.string.textSuccess, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getRoomFragment().getActivity(), R.string.textFail, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static class InviteAsyncTask extends AbstractAsyncTask {
        @Inject
        public InviteAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public InviteAsyncTask init(RoomFragment roomFragment, Integer targetUserId, String serverUrl) {
            super.init(roomFragment, targetUserId, serverUrl);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(getServerUrl()).
                        append("/room/").
                        append(getRoomFragment().getRoomId()).
                        append("/invitation");
                final InvitationModel invitationModel = new InvitationModel();
                invitationModel.setUserId(getTargetUserId());
                return getAsyncHttpClient().preparePost(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        setHeader("Content-Type", MainApplication.ContentType).
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(invitationModel)).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static class ClearBanAsyncTask extends AbstractAsyncTask {
        @Inject
        public ClearBanAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public ClearBanAsyncTask init(RoomFragment roomFragment, Integer targetUserId, String serverUrl) {
            super.init(roomFragment, targetUserId, serverUrl);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(getServerUrl()).
                        append("/room/").
                        append(getRoomFragment().getRoomId()).
                        append("/ban/").
                        append(getTargetUserId());
                return getAsyncHttpClient().prepareDelete(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

    }

    public static class TransferOwnerAsyncTask extends AbstractAsyncTask {
        @Inject
        public TransferOwnerAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public TransferOwnerAsyncTask init(RoomFragment roomFragment, Integer targetUserId, String serverUrl) {
            super.init(roomFragment, targetUserId, serverUrl);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(getServerUrl()).
                        append("/room/").
                        append(getRoomFragment().getRoomId());
                final RoomModel roomModel = new RoomModel();
                final RoomHistoryModel roomHistoryModel = new RoomHistoryModel();
                roomModel.setCurrentDetail(roomHistoryModel);
                roomModel.getCurrentDetail().setUserId(getTargetUserId());
                return getAsyncHttpClient().preparePut(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        setHeader("Content-Type", MainApplication.ContentType).
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(roomModel)).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

    }

    public static class RevokeOperatorAsyncTask extends AbstractAsyncTask {
        @Inject
        public RevokeOperatorAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public RevokeOperatorAsyncTask init(RoomFragment roomFragment, Integer targetUserId, String serverUrl) {
            super.init(roomFragment, targetUserId, serverUrl);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(getServerUrl()).
                        append("/room/").
                        append(getRoomFragment().getRoomId()).
                        append("/operator/").append(getTargetUserId());
                return getAsyncHttpClient().prepareDelete(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

    }

    public static class GrantOperatorAsyncTask extends AbstractAsyncTask {
        @Inject
        public GrantOperatorAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public GrantOperatorAsyncTask init(RoomFragment roomFragment, Integer targetUserId, String serverUrl) {
            super.init(roomFragment, targetUserId, serverUrl);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(getServerUrl()).
                        append("/room/").
                        append(getRoomFragment().getRoomId()).
                        append("/operator");
                return getAsyncHttpClient().preparePost(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        setHeader("Content-Type", MainApplication.ContentType).
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(getTargetUserId())).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static class LeaveRoomForceAsyncTask extends AbstractAsyncTask {
        @Inject
        public LeaveRoomForceAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public LeaveRoomForceAsyncTask init(RoomFragment roomFragment, Integer targetUserId, String serverUrl) {
            super.init(roomFragment, targetUserId, serverUrl);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(getServerUrl()).
                        append("/room/").
                        append(getRoomFragment().getRoomId()).
                        append("/roomAttenders/").append(getTargetUserId());
                return getAsyncHttpClient().prepareDelete(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    @Getter
    @Setter
    public static class ChangePasswordAsyncTask extends AsyncHttpClientTask<String, Void, Integer> {
        private RoomFragment roomFragment;
        private Integer roomId;
        private String password;

        @Inject
        public ChangePasswordAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public ChangePasswordAsyncTask init(RoomFragment roomFragment, Integer roomId, String password) {
            setRoomFragment(roomFragment);
            setCookie(getRoomFragment().getCookie());
            setRoomId(roomId);
            setPassword(password);
            return this;
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                final String serverUrl = ((MainApplication) getRoomFragment().getActivity().getApplication()).getServerUrl();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(serverUrl).
                        append("/room/").
                        append(getRoomId()).
                        append("/nick");
                return getAsyncHttpClient().preparePut(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        addHeader("Content-Type", MainApplication.ContentType).
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(getPassword())).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if (integer != null && integer == 200) {
                Toast.makeText(getRoomFragment().getActivity(), R.string.textSuccess, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getRoomFragment().getActivity(), R.string.textFail, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Getter
    @Setter
    public static class ChangeRoomNameAsyncTask extends AsyncHttpClientTask<String, Void, Integer> {
        private RoomFragment roomFragment;
        private Integer roomId;
        private String name;

        @Inject
        public ChangeRoomNameAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public ChangeRoomNameAsyncTask init(RoomFragment roomFragment, Integer roomId, String name) {
            setRoomFragment(roomFragment);
            setCookie(getRoomFragment().getCookie());
            setRoomId(roomId);
            setName(name);
            return this;
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                final String serverUrl = ((MainApplication) getRoomFragment().getActivity().getApplication()).getServerUrl();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(serverUrl).
                        append("/room/").
                        append(getRoomId()).
                        append("/name");
                return getAsyncHttpClient().preparePut(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        addHeader("Content-Type", MainApplication.ContentType).
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(getName())).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if (integer != null && integer == 200) {
                Toast.makeText(getRoomFragment().getActivity(), R.string.textSuccess, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getRoomFragment().getActivity(), R.string.textFail, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Getter
    @Setter
    public static class ChangeCurrentNickAsyncTask extends AsyncHttpClientTask<String, Void, Integer> {
        private RoomFragment roomFragment;
        private Integer roomId;
        private String nick;

        @Inject
        public ChangeCurrentNickAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public ChangeCurrentNickAsyncTask init(RoomFragment roomFragment, Integer roomId, String nick) {
            setRoomFragment(roomFragment);
            setCookie(getRoomFragment().getCookie());
            setRoomId(roomId);
            setNick(nick);
            return this;
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                final String serverUrl = ((MainApplication) getRoomFragment().getActivity().getApplication()).getServerUrl();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(serverUrl).
                        append("/user/self/nick");
                return getAsyncHttpClient().preparePut(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        addHeader("Content-Type", MainApplication.ContentType).
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(getNick())).
                        execute().get().getStatusCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if (integer != null && integer == 200) {
                Toast.makeText(getRoomFragment().getActivity(), R.string.textSuccess, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getRoomFragment().getActivity(), R.string.textFail, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Getter
    @Setter
    public static class TalkAsyncTask extends AsyncHttpClientTask<String, Void, Void> {
        private RoomFragment roomFragment;
        private Integer roomId;

        @Inject
        public TalkAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public TalkAsyncTask init(RoomFragment roomFragment, Integer roomId) {
            setRoomFragment(roomFragment);
            setCookie(getRoomFragment().getCookie());
            setRoomId(roomId);
            return this;
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                final String serverUrl = ((MainApplication) getRoomFragment().getActivity().getApplication()).getServerUrl();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(serverUrl).
                        append("/room/").
                        append(getRoomId()).
                        append("/talk");
                final TalkHistoryModel talkHistoryModel = new TalkHistoryModel();
                talkHistoryModel.setTalk(params[0]);
                getAsyncHttpClient().preparePost(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        addHeader("Content-Type", MainApplication.ContentType).
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(talkHistoryModel)).
                        execute().get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Getter
    @Setter
    public static class LeaveRoomAsyncTask extends AsyncHttpClientTask<Void, Void, Void> {
        private RoomFragment roomFragment;
        private String serverUrl;
        private Integer roomId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getRoomFragment().getMenu().findItem(R.id.menuItemOut).setEnabled(false);
        }

        @Inject
        public LeaveRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public LeaveRoomAsyncTask init(RoomFragment roomFragment, String serverUrl, Integer roomId) {
            setRoomFragment(roomFragment);
            setCookie(getRoomFragment().getCookie());
            setServerUrl(serverUrl);
            setRoomId(roomId);
            return this;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                final StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(getServerUrl()).
                        append("/room/").
                        append(getRoomId()).
                        append("/roomAttenders/self");
                getAsyncHttpClient().prepareDelete(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        execute().get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            getRoomFragment().getActivity().getFragmentManager().popBackStack();
            // service 에 남아있던 이 대화방에 관한 모든 자료를 지운다.
            getRoomFragment().getInnerService().clearRoom(getRoomFragment().getRoomId());
            getRoomFragment().getMenu().findItem(R.id.menuItemOut).setEnabled(true);
        }
    }

    public class EnvelopeModelAdapter extends ArrayAdapter<EnvelopeModel<?>> {
        final private Integer resource;
        final private TimeZone timeZone = TimeZone.getTimeZone("Asia/Seoul");
        final private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        final private Calendar calendar = Calendar.getInstance(timeZone);

        public EnvelopeModelAdapter() {
            super(RoomFragment.this.getActivity(), R.layout.list_entry);
            resource = R.layout.list_entry;
            // http://beginnersbook.com/2013/05/java-date-timezone/
            dateFormat.setTimeZone(timeZone);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Boolean debugMessage = PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(getResources().getString(R.string.keyDebugMessage), false);
            // http://stackoverflow.com/questions/7439394/android-how-to-make-rows-in-a-listview-have-variable-heights
            final LayoutInflater inflater = (LayoutInflater) RoomFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;
            if (convertView == null) {
                view = inflater.inflate(resource, parent, false);
            } else {
                view = convertView;
            }
            final TextView listEntry = (TextView) view.findViewById(R.id.listEntry);
            final EnvelopeModel<?> item = getItem(position);
            if (debugMessage) {
                listEntry.setText(item.toString());
            } else {
                final StringBuilder stringBuilder = new StringBuilder();
                if (item.getDataType() == EnvelopeDataType.RoomTimelineModelType) {
                    final RoomTimelineModel roomTimelineModel = (RoomTimelineModel) item.getData();
                    if (roomTimelineModel.getTalkHistory() != null) {
                        final TalkHistoryModel talkHistoryModel = roomTimelineModel.getTalkHistory();
                        stringBuilder.append(dateFormat.format(talkHistoryModel.getWhen())).append(" ");
                        if (talkHistoryModel.getNick() != null) {
                            stringBuilder.append(talkHistoryModel.getNick().getNick());
                        } else {
                            stringBuilder.append("?");
                        }
                        stringBuilder.append("(").append(talkHistoryModel.getUser().getName()).append(")").append(" ");
                        stringBuilder.append(" : ").append(talkHistoryModel.getTalk());
                    } else if (roomTimelineModel.getRoomInOutHistory() != null) {
                        final RoomInOutHistoryModel roomInOutHistoryModel = roomTimelineModel.getRoomInOutHistory();
                        stringBuilder.append(dateFormat.format(roomInOutHistoryModel.getWhen())).append(" ");
                        stringBuilder.append(roomInOutHistoryModel.getUser().getName()).append(" ");
                        if (roomInOutHistoryModel.getEnterType() == EnterType.Enter) {
                            stringBuilder.append("come in.");
                        } else {
                            stringBuilder.append("go out");
                            if (roomInOutHistoryModel.getByWho() != null) {
                                stringBuilder.append(" by ").append(roomInOutHistoryModel.getByWho().getName());
                            }
                            stringBuilder.append(".");
                        }
                    } else if (roomTimelineModel.getPermissionHistory() != null) {
                        final PermissionHistoryModel permissionHistoryModel = roomTimelineModel.getPermissionHistory();
                        stringBuilder.append(dateFormat.format(permissionHistoryModel.getWhen())).append(" ");
                        if (permissionHistoryModel.getUser() != null) {
                            stringBuilder.append(permissionHistoryModel.getUser().getName());
                        } else {
                            stringBuilder.append("null user");
                        }
                        stringBuilder.append(" ");
                        if (permissionHistoryModel.getPermissionType() == PermissionType.GrantOwner) {
                            stringBuilder.append("is owner");
                        } else if (permissionHistoryModel.getPermissionType() == PermissionType.RevokeOwner) {
                            stringBuilder.append("is not owner");
                        } else if (permissionHistoryModel.getPermissionType() == PermissionType.GrantOperator) {
                            stringBuilder.append("is operator");
                        } else if (permissionHistoryModel.getPermissionType() == PermissionType.RevokeOperator) {
                            stringBuilder.append("is not operator");
                        }
                        if (permissionHistoryModel.getByWho() != null) {
                            stringBuilder.append(" by ").append(permissionHistoryModel.getByWho().getName());
                        }
                        stringBuilder.append(".");
                    } else if (roomTimelineModel.getRoomHistory() != null) {
                        final RoomHistoryModel roomHistoryModel = roomTimelineModel.getRoomHistory();
                        stringBuilder.append(dateFormat.format(roomHistoryModel.getWhen())).append(" ");
                        stringBuilder.append("room status is changed");
                        if (roomHistoryModel.getByWho() != null) {
                            stringBuilder.append(" by ").append(roomHistoryModel.getByWho().getName());
                        }
                        stringBuilder.append(".");
                    } else if (roomTimelineModel.getUserNickHistory() != null) {
                        final UserNickHistoryModel userNickHistoryModel = roomTimelineModel.getUserNickHistory();
                        stringBuilder.append(dateFormat.format(userNickHistoryModel.getWhen())).append(" ");
                        stringBuilder.append("user(").append(userNickHistoryModel.getUser().getName()).append(")'s nick is changed to ");
                        if (userNickHistoryModel.getNick() == null) {
                            stringBuilder.append(" nothing");
                        } else {
                            stringBuilder.append(userNickHistoryModel.getNick());
                        }
                        stringBuilder.append(".");
                    }
                    // todo ban history 도 room timeline 에 같이 온다. 이것도 다뤄야 한다.
                }
                listEntry.setText(stringBuilder.toString());
            }
            return view;
        }
    }

    public class RoomAttenderModelAdapter extends ArrayAdapter<RoomAttenderModel> {
        public RoomAttenderModelAdapter() {
            super(getActivity(), android.R.layout.simple_list_item_1);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);
            final TextView textView = (TextView) view.findViewById(android.R.id.text1);
            final RoomAttenderModel roomAttenderModel = getItem(position);
            final UserModel userModel = roomAttenderModel.getUser();
            final StringBuilder stringBuilder = new StringBuilder(userModel.getName());
            if (userModel.getId() == getUserId()) {
                stringBuilder.append(" (*)");
            }
            if (roomAttenderModel.getCurrentOwnerFlag() != null && roomAttenderModel.getCurrentOwnerFlag() == true) {
                stringBuilder.append(" (Own)");
            }
            if (roomAttenderModel.getCurrentOperatorFlag() != null && roomAttenderModel.getCurrentOperatorFlag() == true) {
                stringBuilder.append(" (Op)");
            }
            textView.setText(stringBuilder.toString());
            return view;
        }
    }

    public class BanModelAdapter extends ArrayAdapter<BanModel> {
        public BanModelAdapter() {
            super(getActivity(), android.R.layout.simple_list_item_1);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);
            final TextView textView = (TextView) view.findViewById(android.R.id.text1);
            final BanModel banModel = getItem(position);
            final UserModel userModel = banModel.getUser();
            final StringBuilder stringBuilder = new StringBuilder(userModel.getName());
//            if (userModel.getId() == getUserId()) {
//                stringBuilder.append(" (*)");
//            }
//            if (banModel.getCurrentOwnerFlag() != null && banModel.getCurrentOwnerFlag() == true) {
//                stringBuilder.append(" (Own)");
//            }
//            if (banModel.getCurrentOperatorFlag() != null && banModel.getCurrentOperatorFlag() == true) {
//                stringBuilder.append(" (Op)");
//            }
            textView.setText(stringBuilder.toString());
            return view;
        }
    }

    public class UserModelAdapter extends ArrayAdapter<UserModel> {
        public UserModelAdapter() {
            super(getActivity(), android.R.layout.simple_list_item_1);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);
            final TextView textView = (TextView) view.findViewById(android.R.id.text1);
            final UserModel userModel = getItem(position);
            final StringBuilder stringBuilder = new StringBuilder(userModel.getName());
//            if (userModel.getId() == getUserId()) {
//                stringBuilder.append(" (*)");
//            }
//            if (banModel.getCurrentOwnerFlag() != null && banModel.getCurrentOwnerFlag() == true) {
//                stringBuilder.append(" (Own)");
//            }
//            if (banModel.getCurrentOperatorFlag() != null && banModel.getCurrentOperatorFlag() == true) {
//                stringBuilder.append(" (Op)");
//            }
            textView.setText(stringBuilder.toString());
            return view;
        }
    }
}
