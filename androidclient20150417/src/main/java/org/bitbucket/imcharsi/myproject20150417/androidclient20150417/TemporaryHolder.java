/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.app.Activity;
import android.app.Fragment;
import android.content.*;
import android.os.IBinder;
import android.widget.Toast;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MainFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeDataType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeModel;

import java.util.List;

/**
 * Created by Kang Woo, Lee on 5/23/15.
 */
public interface TemporaryHolder {
    ObjectGraph getObjectGraph();

    void setObjectGraph(ObjectGraph o);

    WebSocketService.InnerService getInnerService();

    void setInnerService(WebSocketService.InnerService s);

    TemporaryHolder.WebSocketServiceReceiver getReceiver();

    void setReceiver(TemporaryHolder.WebSocketServiceReceiver r);

    Activity getActivity();

    @Getter
    @Setter
    // room fragment 를 제외한 나머지 fragment 는 기타 message 를 받아야 한다.
    abstract class AbstractWebSocketServiceConnection<T extends TemporaryHolder> implements ServiceConnection {
        private T fragment;

        abstract protected TemporaryHolder.WebSocketServiceReceiver getReceiver();

        protected void retryBroadcast() {
            getFragment().getInnerService().retryJustBroadcast();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            getFragment().setInnerService((((WebSocketService.SampleBinder) service).getService()));
            final IntentFilter intentFilter = new IntentFilter("hi there");
            intentFilter.addAction("close web socket");
            intentFilter.addAction("server message");
            getFragment().setReceiver(getReceiver());
            getFragment().getActivity().registerReceiver(getFragment().getReceiver(), intentFilter);
            retryBroadcast();
        }

        // http://stackoverflow.com/questions/17713453/onservicedisconnected-not-called-after-calling-service-stopself
        // 여기서 receiver 를 등록 해재헤서는 안된다.
        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    }

    @Getter
    @Setter
    class WebSocketServiceReceiver<F extends Fragment> extends BroadcastReceiver {
        private WebSocketService.InnerService innerService;
        private Context context;
        private F fragment;

        public WebSocketServiceReceiver init(WebSocketService.InnerService innerService, Context context, F fragment) {
            setInnerService(innerService);
            setContext(context);
            setFragment(fragment);
            return this;
        }

        protected void processMessage(EnvelopeModel<?> envelopeModel) {
            if (envelopeModel.getDataType() == EnvelopeDataType.InvitationModelType) {
                Toast.makeText(getContext(), R.string.textInvite, Toast.LENGTH_SHORT).show();
            } else if (envelopeModel.getDataType() == EnvelopeDataType.LogoutModelType) {
                getInnerService().init();
                try {
                    getInnerService().disconnect(); // 자동으로 안끊길때도 있다. 왜 그런가.
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (getFragment() instanceof MainFragment) {
                    final MainFragment mainFragment = (MainFragment) getFragment();
                    mainFragment.setCookie(null);
                    mainFragment.refreshButtonState();
                } else if (getFragment() != null) {
                    getFragment().getFragmentManager().popBackStack();
                }
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            final List<WebSocketService.QueueEntity> idList2 = getInnerService().getIdList(-1);
            if (idList2 != null) {
                for (WebSocketService.QueueEntity queueEntity : idList2) {
                    for (Integer id : queueEntity.getMessageIdList()) {
                        final EnvelopeModel<?> envelopeModel = getInnerService().getMessage(id);
                        processMessage(envelopeModel);
                    }
                }
            }
        }
    }
}
