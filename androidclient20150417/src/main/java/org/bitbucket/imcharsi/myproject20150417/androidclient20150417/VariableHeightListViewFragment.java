/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Kang Woo, Lee on 5/10/15.
 */
public class VariableHeightListViewFragment extends Fragment {

    private ListView listView;
    private SampleAdapter sampleAdapter;
    final private Random random = new Random();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.var_height_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.goToBottom:
                // http://stackoverflow.com/questions/3606530/listview-scroll-to-the-end-of-the-list-after-updating-the-list
                // 아래와 같이 하면 바로 이동한다.
                // listView.setSelection(envelopeModelAdapter.getCount() - 1);
                // 아래와 같이 하면 부드럽게 이동한다.
                listView.smoothScrollToPosition(sampleAdapter.getCount() - 1);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.var_height_list_view_fragment, container, false);
        this.listView = (ListView) view.findViewById(R.id.listView);
//        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        sampleAdapter = new SampleAdapter();
        for (int i = 0; i < 20; i++) {
            sampleAdapter.add(Integer.toHexString(random.nextInt()));
        }
        listView.setAdapter(sampleAdapter);
        return view;
    }

    private class SampleAdapter extends ArrayAdapter<String> {
        final private Integer resource;

        public SampleAdapter() {
            super(VariableHeightListViewFragment.this.getActivity(), R.layout.list_entry);
            resource = R.layout.list_entry;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // http://stackoverflow.com/questions/7439394/android-how-to-make-rows-in-a-listview-have-variable-heights
            final LayoutInflater inflater = (LayoutInflater) VariableHeightListViewFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;
            if (convertView == null) {
                view = inflater.inflate(resource, parent, false);
            } else {
                view = convertView;
            }
            final TextView listEntry = (TextView) view.findViewById(R.id.listEntry);
            listEntry.setText(getItem(position));
            listEntry.setHeight(random.nextInt(100) + 30);
            return view;
        }
    }
}
