/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.MasterDaggerModule;

/**
 * Created by Kang Woo, Lee on 5/10/15.
 */
@Getter
@Setter
public class MainApplication extends Application {
    private ObjectGraph objectGraph;
    public static final String ContentType = "application/json;charset=utf-8";

    protected ObjectGraph prepareObjectGraph() {
        return MasterDaggerModule.MASTER_OBJECT_GRAPH;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setObjectGraph(prepareObjectGraph());
        // todo app 을 명시적으로 종료할 때 service 도 종료해야 한다. 단순히 화면회전으로 인한 destroy 로는 service 가 멈춰서는 안된다.
        startService(new Intent(this, WebSocketService.class));
    }

    public static ObjectGraph retrieveMainApplication(final Application application) {
        return ((MainApplication) application).getObjectGraph();
    }

    public String getServerUrl() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String serverUrl = preferences.getString(this.getResources().getString(R.string.keyServerUrl),
                this.getResources().getString(R.string.textDefaultServerUrl));
        return serverUrl;
    }
}
