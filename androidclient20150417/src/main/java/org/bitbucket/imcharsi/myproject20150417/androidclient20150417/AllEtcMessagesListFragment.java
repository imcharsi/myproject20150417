/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417;

import android.app.ListFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeDataType;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnvelopeModel;

import java.util.List;

/**
 * Created by Kang Woo, Lee on 5/26/15.
 */
@Getter
@Setter
public class AllEtcMessagesListFragment extends ListFragment implements TemporaryHolder {
    static final public int EtcMessageRoomId = -1;
    public static final String TAG = AllEtcMessagesListFragment.class.getName();
    private ObjectGraph objectGraph;
    private WebSocketService.InnerService innerService;
    private TemporaryHolder.WebSocketServiceReceiver receiver;
    private WebSocketServiceConnection webSocketServiceConnection;
    private EnvelopeModelAdapter envelopeModelAdapter;

    @Getter
    @Setter
    public static class WebSocketServiceConnection extends TemporaryHolder.AbstractWebSocketServiceConnection<AllEtcMessagesListFragment> {
        private AllEtcMessagesListFragment allEtcMessagesListFragment;

        public WebSocketServiceConnection init(AllEtcMessagesListFragment fragment) {
            setFragment(fragment);
            return this;
        }

        @Override
        protected void retryBroadcast() {
            // 이 fragment 에서는 기타 message 에 대해 toast 를 하지 않는다.
            // 한편, 이 fragment 에서는 모든 message 를 전부 보여야 한다.
            getFragment().getInnerService().retryBroadcast(EtcMessageRoomId);
        }

        @Override
        protected TemporaryHolder.WebSocketServiceReceiver getReceiver() {
            return getFragment().getObjectGraph().
                    get(WebSocketServiceReceiver.class).
                    init(getFragment().getInnerService(), getFragment().getActivity(), getFragment());
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            super.onServiceConnected(name, service);
            if (getFragment().getInnerService().isLogoutChecked()) {
                getFragment().getFragmentManager().popBackStack();
            }
        }
    }

    @Getter
    @Setter
    public static class WebSocketServiceReceiver extends TemporaryHolder.WebSocketServiceReceiver<AllEtcMessagesListFragment> {
        public TemporaryHolder.WebSocketServiceReceiver init(WebSocketService.InnerService innerService, Context context, AllEtcMessagesListFragment allEtcMessagesListFragment) {
            super.init(innerService, context, allEtcMessagesListFragment);
            return this;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean logoutForceFlag = false;
            final List<WebSocketService.QueueEntity> idList = getInnerService().getIdList(EtcMessageRoomId);
            if (idList != null) {
                for (WebSocketService.QueueEntity queueEntity : idList) {
                    if (queueEntity.isRedrawFlag()) {
                        getFragment().getEnvelopeModelAdapter().clear();
                    }
                    for (Integer id : queueEntity.getMessageIdList()) {
                        final EnvelopeModel<?> envelopeModel = getInnerService().getMessage(id);
                        getFragment().getEnvelopeModelAdapter().add(envelopeModel);
                        if (envelopeModel.getDataType() == EnvelopeDataType.LogoutModelType) {
                            logoutForceFlag = true;
                        }
                    }
                }
            }
            if (getFragment().getEnvelopeModelAdapter().getCount() > 0) {
                getFragment().getListView().setSelection(getFragment().getEnvelopeModelAdapter().getCount() - 1);
            }
            if (logoutForceFlag) {
                getFragment().getFragmentManager().popBackStack();
            }
        }
    }

    // todo room fragment 도 이와 같은 기능을 쓴다. 정리하기.
    private class EnvelopeModelAdapter extends ArrayAdapter<EnvelopeModel<?>> {
        final private Integer resource;

        public EnvelopeModelAdapter() {
            super(AllEtcMessagesListFragment.this.getActivity(), R.layout.list_entry);
            resource = R.layout.list_entry;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater inflater = (LayoutInflater) AllEtcMessagesListFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;
            if (convertView == null) {
                view = inflater.inflate(resource, parent, false);
            } else {
                view = convertView;
            }
            final TextView listEntry = (TextView) view.findViewById(R.id.listEntry);
            listEntry.setText(getItem(position).toString());
            return view;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setObjectGraph(MainApplication.retrieveMainApplication(getActivity().getApplication()));
        setEnvelopeModelAdapter(new EnvelopeModelAdapter());
        setListAdapter(getEnvelopeModelAdapter());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.etc_message_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuItemClear:
                getInnerService().clearRoom(EtcMessageRoomId);
                getInnerService().retryBroadcast(EtcMessageRoomId);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setWebSocketServiceConnection(getObjectGraph().get(WebSocketServiceConnection.class).init(this));
        getActivity().bindService(
                new Intent(getActivity(), WebSocketService.class),
                getWebSocketServiceConnection(),
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(getReceiver());
        getActivity().unbindService(getWebSocketServiceConnection());
    }
}
