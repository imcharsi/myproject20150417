/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Response;
import dagger.ObjectGraph;
import lombok.Getter;
import lombok.Setter;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.MainApplication;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.R;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.TemporaryHolder;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.WebSocketService;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.asynctask.AsyncHttpClientTask;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomFragment;
import org.bitbucket.imcharsi.myproject20150417.androidclient20150417.main.room.RoomListFragment;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.EnterRoomModel;
import org.bitbucket.imcharsi.myproject20150417.javamodel20150417.model.room.current.InvitationModel;
import org.bitbucket.imcharsi.myproject20150417.javamodelmapper20150417.DefaultObjectMapper;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Kang Woo, Lee on 5/23/15.
 */
@Getter
@Setter
public class InvitationListFragment extends ListFragment implements TemporaryHolder {
    public static final String TAG = InvitationListFragment.class.getName();
    private InvitationModelAdapter adapter;
    private String cookie;
    private ObjectGraph objectGraph;
    private WebSocketService.InnerService innerService;
    private WebSocketServiceConnection serviceConnection;
    private TemporaryHolder.WebSocketServiceReceiver receiver;

    public static class WebSocketServiceConnection extends TemporaryHolder.AbstractWebSocketServiceConnection<InvitationListFragment> {
        public WebSocketServiceConnection init(InvitationListFragment roomListFragment) {
            setFragment(roomListFragment);
            return this;
        }

        @Override
        protected TemporaryHolder.WebSocketServiceReceiver getReceiver() {
            return getFragment().getObjectGraph().get(WebSocketServiceReceiver.class).init(getFragment().getInnerService(), getFragment().getActivity(), getFragment());
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            super.onServiceConnected(name, service);
            getFragment().setInnerService((((WebSocketService.SampleBinder) service).getService()));
            if (getFragment().getInnerService().isLogoutChecked()) {
                getFragment().getFragmentManager().popBackStack();
            }
        }
    }

    public static class WebSocketServiceReceiver extends TemporaryHolder.WebSocketServiceReceiver<InvitationListFragment> {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAdapter(new InvitationModelAdapter());
        final Bundle bundle = getArguments();
        if (bundle != null) {
            prepareCookie(bundle);
        } else if (savedInstanceState != null) {
            prepareCookie(savedInstanceState);
        }
        setObjectGraph(((MainApplication) getActivity().getApplication()).getObjectGraph());
    }

    private void prepareCookie(final Bundle bundle) {
        if (bundle.containsKey(MainFragment.COOKIE)) {
            setCookie(bundle.getString(MainFragment.COOKIE));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getCookie() != null) {
            outState.putString(MainFragment.COOKIE, getCookie());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setServiceConnection(getObjectGraph().get(WebSocketServiceConnection.class).init(this));
        getActivity().bindService(new Intent(getActivity(), WebSocketService.class), getServiceConnection(), Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(getReceiver());
        getActivity().unbindService(getServiceConnection());
    }

    @Override
    public void onResume() {
        super.onResume();
        getObjectGraph().get(InvitationListFragmentAsyncTask.class).init(this).
                executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        final InvitationModel invitationModel = getAdapter().getItem(position);
        getObjectGraph().get(EnterRoomAsyncTask.class).
                init(this, invitationModel.getRoomId(), invitationModel.getUserId(), invitationModel.getId()).
                executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = super.onCreateView(inflater, container, savedInstanceState);
        setListAdapter(getAdapter());
        return view;
    }

    private class InvitationModelAdapter extends ArrayAdapter<InvitationModel> {
        public InvitationModelAdapter() {
            super(InvitationListFragment.this.getActivity(), android.R.layout.simple_list_item_1);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = super.getView(position, convertView, parent);
            final TextView textView = (TextView) view.findViewById(android.R.id.text1);
            final InvitationModel invitationModel = getItem(position);
            final StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(invitationModel.getRoom().getCurrentDetail().getName()).
                    append(":").
                    append(invitationModel.getByWho().getName());
            textView.setText(stringBuilder.toString());
            return view;
        }
    }

    @Getter
    @Setter
    public static class InvitationListFragmentAsyncTask extends AsyncHttpClientTask<Void, Void, List<InvitationModel>> {
        private InvitationListFragment invitationListFragment;

        @Inject
        public InvitationListFragmentAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public InvitationListFragmentAsyncTask init(InvitationListFragment invitationListFragment) {
            setInvitationListFragment(invitationListFragment);
            return this;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getInvitationListFragment().setListShown(false);
        }

        @Override
        protected List<InvitationModel> doInBackground(Void... params) {
            try {
                final String serverUrl = ((MainApplication) getInvitationListFragment().getActivity().getApplication()).getServerUrl();
                final ListenableFuture<Response> execute = getAsyncHttpClient().prepareGet(serverUrl + "/user/self/invitation").
                        addHeader("Accept", MainApplication.ContentType).
                        addHeader("Cookie", getInvitationListFragment().getCookie()).
                        execute();
                final Response response = execute.get();
                return DefaultObjectMapper.objectMapper.readValue(response.getResponseBody(),
                        DefaultObjectMapper.objectMapper.getTypeFactory().constructCollectionType(List.class, InvitationModel.class));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<InvitationModel> invitationModels) {
            super.onPostExecute(invitationModels);
            getInvitationListFragment().getAdapter().clear();
            getInvitationListFragment().getAdapter().addAll(invitationModels);
            getInvitationListFragment().setListShown(true);
        }
    }

    @Getter
    @Setter
    public static class EnterRoomAsyncTask extends AsyncHttpClientTask<Void, Void, Integer> {
        private InvitationListFragment invitationListFragment;
        private Integer roomId;
        private Integer userId;
        private Integer invitationId;

        @Inject
        public EnterRoomAsyncTask(AsyncHttpClient asyncHttpClient) {
            super(asyncHttpClient);
        }

        public EnterRoomAsyncTask init(InvitationListFragment roomListFragment, Integer roomId, Integer userId, Integer intivationId) {
            setInvitationListFragment(roomListFragment);
            setCookie(getInvitationListFragment().getCookie());
            setRoomId(roomId);
            setUserId(userId);
            setInvitationId(intivationId);
            return this;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                final String serverUrl = ((MainApplication) getInvitationListFragment().getActivity().getApplication()).getServerUrl();
                final StringBuilder stringBuilder = new StringBuilder(serverUrl).
                        append("/room/").
                        append(getRoomId()).
                        append("/roomAttenders/self");
                final Response response = getAsyncHttpClient().prepareGet(stringBuilder.toString()).
                        addHeader("Cookie", getCookie()).
                        setHeader("Accept", MainApplication.ContentType).
                        execute().get();
                if (response.getStatusCode() == 200) {
                    return getRoomId();
                }
                getInvitationListFragment().getInnerService().clearRoom(getRoomId());
                final EnterRoomModel enterRoomModel = new EnterRoomModel();
                enterRoomModel.setInvitationId(getInvitationId());
                final StringBuilder stringBuilder2 = new StringBuilder(serverUrl).
                        append("/room/").
                        append(getRoomId()).
                        append("/roomAttenders");
                final ListenableFuture<Response> execute = getAsyncHttpClient().preparePost(stringBuilder2.toString()).
                        addHeader("Cookie", getCookie()).
                        setHeader("Content-Type", MainApplication.ContentType).
                        setBodyEncoding("UTF-8").
                        setBody(DefaultObjectMapper.objectMapper.writeValueAsString(enterRoomModel)).
                        execute();
                final Response response2 = execute.get();
                // 폐쇄된 대화방에 입장하려고 시도할 때 실패를 뜻하는 응답이 바로 돌아오지 않는다.
                // 이유는, 입장시도가 actor message 요청이기 때문이다. 응답을 할 actor 가 없기 때문에 시간초과될 때까지 기다린 후 실패 응답을 받게 된다.
                if (response2.getStatusCode() == 200) {
                    return getRoomId();
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer roomId) {
            super.onPostExecute(roomId);
            if (roomId == null) {
                Toast.makeText(getInvitationListFragment().getActivity(), R.string.textFail, Toast.LENGTH_SHORT).show();
                return;
            }
            final Bundle bundle = new Bundle();
            bundle.putString(MainFragment.COOKIE, getCookie());
            bundle.putInt(RoomListFragment.ROOM_ID, roomId);
            if (getUserId() != null) {
                bundle.putInt(MainFragment.USER_ID, getUserId());
            }
            final RoomFragment roomFragment = new RoomFragment();
            roomFragment.setArguments(bundle);
            final FragmentTransaction transaction = getInvitationListFragment().getActivity().getFragmentManager().beginTransaction();
            transaction.replace(R.id.main_activity_layout, roomFragment, RoomFragment.TAG);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
